import 'package:flutter/material.dart';

class CustomRadio extends StatefulWidget {
  int btnValue;
  String title;

  CustomRadio({this.title, this.btnValue});

  @override
  State createState() {
    return _CustomRadioState();
  }
}

class _CustomRadioState extends State<CustomRadio> {
  List gender = ["Male", "Female"];
  String select;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Radio(
          activeColor: Theme.of(context).primaryColor,
          value: gender[widget.btnValue],
          groupValue: select,
          onChanged: (value) {
            setState(() {
              select = value;
            });
          },
        ),
        Text(widget.title)
      ],
    );
  }
}
