import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomEditIcon extends StatelessWidget {
  final Function onClick;

  CustomEditIcon({this.onClick});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: CircleAvatar(
        backgroundColor: Colors.teal,
        radius: 14.0,
        child: Icon(
          Icons.edit,
          color: Colors.white,
          size: 16.0,
        ),
      ),
      onTap: onClick,
    );
  }
}
