import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class CustomCard extends StatelessWidget {
  final String name;
  final String dateOfBirth;
  final String gender;
  final String dateFormatPattern;
  String route;
  Object object;

  CustomCard(
      {this.name: "",
      this.gender: "",
      this.dateOfBirth: "",
      this.route: "",
      this.object,
      this.dateFormatPattern = 'd/M/y'});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          margin: EdgeInsets.only(left: 8, right: 8, top: 8, bottom: 4),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 5, bottom: 5),
              ),
              ListTile(
                //sentiment_very_satisfied
                leading: Icon(
                  Icons.arrow_forward_ios,
                  color: Colors.teal,
                ),
                title: Text(name),
                subtitle: Text(gender),
              ),
              ListTile(
                leading: Icon(Icons.date_range, color: Colors.teal,),
                title: Text(getDate()),
              ),
              Padding(
                padding: EdgeInsets.only(top: 5, bottom: 5),
              ),
            ],
          ),
        ),
        decoration: BoxDecoration(boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(.5),
            blurRadius: 20.0, // soften the shadow
            spreadRadius: 0.0, //extend the shadow
            offset: Offset(
              5.0, // Move to right 10  horizontally
              5.0, // Move to bottom 10 Vertically
            ),
          ),
        ]),
      ),
      onTap: () {
        if (route != null)
          Navigator.pushNamed(context, '/' + route, arguments: object);
      },
    );
  }

  String getDate() {
    try {
      return DateFormat(dateFormatPattern).format(DateTime.parse(dateOfBirth));
    } catch (e) {
      return dateOfBirth;
    }
  }
}
