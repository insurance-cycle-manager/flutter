import 'package:flutter/material.dart';

class CustomAppBar extends StatefulWidget implements PreferredSizeWidget {
  final TextEditingController searchQuery;
  final String title;
  Icon actionIcon;
  bool isSearching;
  String searchText;
  final Function handleClick;
  final List<String> optionsList;
  @override
  final Size preferredSize;

  CustomAppBar({
    this.title,
    this.actionIcon: const Icon(Icons.search),
    this.isSearching,
    this.searchText,
    this.searchQuery,
    this.handleClick,
    this.optionsList,
  }) : preferredSize = Size.fromHeight(60.0);

  @override
  _CustomAppBarState createState() => _CustomAppBarState();
}

class _CustomAppBarState extends State<CustomAppBar> {
  Widget appBarTitle;

  @override
  void initState() {
    //widget.isSearching = false;
    appBarTitle = Text(widget.title);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: appBarTitle,
      actions: <Widget>[
        Padding(
          padding: EdgeInsets.only(right: 20.0),
          child: GestureDetector(
            child: IconButton(
                icon: widget.actionIcon,
                onPressed: () {
                  setState(() {
                    if (widget.actionIcon.icon == Icons.search) {
                      widget.actionIcon = new Icon(
                        Icons.close,
                      );
                      appBarTitle = new TextField(
                        controller: widget.searchQuery,
                        style: TextStyle(color: Colors.white),
                        decoration: new InputDecoration(
                          prefixIcon: new Icon(Icons.search),
                          hintText: "Search...",
                        ),
                      );
                      _handleSearchStart();
                    } else {
                      _handleSearchEnd();
                    }
                  });
                }),
          ),
        ),
        PopupMenuButton<String>(
          onSelected: widget.handleClick,
          itemBuilder: (BuildContext context) {
            List<String> s = widget.optionsList;
            return s.map((String choice) {
              return PopupMenuItem<String>(
                value: choice,
                child: Text(choice),
              );
            }).toList();
          },
        )
      ],
    );
  }

  void _handleSearchStart() {
    setState(() {
      widget.isSearching = true;
    });
  }

  void _handleSearchEnd() {
    setState(() {
      widget.actionIcon = new Icon(
        Icons.search,
      );
      appBarTitle = new Text(widget.title);
      widget.isSearching = false;
      widget.searchQuery.clear();
    });
  }
}
