import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';

class CustomTextField extends StatefulWidget {
  final bool autoFocus;
  bool obscureText;
  final IconData icon;
  final String labelText;
  final Function validationFun;
  final fontSize;
  final TextEditingController controller;
  final TextInputType keyboardType;
  final String hintText;

  CustomTextField({this.autoFocus = false,
      this.icon,
      this.labelText,
    this.validationFun,
      this.fontSize = 15.0,
      this.controller,
      this.keyboardType,
      this.obscureText = false,
      this.hintText});

  @override
  State<StatefulWidget> createState() {
    return _MyTextField();
  }
}

class _MyTextField extends State<CustomTextField> {
  FocusNode myFocusNode;

  @override
  void initState() {
    myFocusNode = new FocusNode();
    myFocusNode.addListener(() {
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(30.0, 10.0, 20.0, 10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: TextFormField(
              cursorColor: Colors.teal,
              keyboardType: widget.keyboardType,
              validator: widget.validationFun,
              controller: widget.controller,
              autofocus: widget.autoFocus,
              focusNode: myFocusNode,
              obscureText: widget.obscureText,
              decoration: InputDecoration(
                border: new OutlineInputBorder(
                    borderSide: new BorderSide(color: Colors.teal)),
                hintText: widget.hintText,
                prefixIcon: Icon(
                  widget.icon,
                  color: myFocusNode.hasFocus ? Colors.teal : Colors.grey,
                ),
                contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 10.0),
                labelText: widget.labelText,
                hintStyle: TextStyle(color: Colors.grey),
                labelStyle: TextStyle(
                    color: myFocusNode.hasFocus ? Colors.teal : Colors.grey),
                errorStyle: TextStyle(fontSize: 12.0),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    myFocusNode.dispose();
    super.dispose();
  }
}
