import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:medcare/networking/httpAuth.dart' as auth;
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/auth/user.dart';
import 'package:medcare/screen/fullScreen.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NavigationDrawer extends StatefulWidget {
  @override
  State createState() {
    return _NavigationDrawerState();
  }
}

class _NavigationDrawerState extends State<NavigationDrawer> {
  List<String> settingList = [];
  Map<String, IconData> icons = Map();
  List<String> roles = [];
  bool callBack;
  User user;
  File _image;

  @override
  void initState() {
    user = User();
    callBack = true;
    loadUserFromShare();
    settingList.add('Profile');
    settingList.add('Service staffer');
    settingList.add('Service providers');
    settingList.add('Patients');
    settingList.add('Insurance Companies');
    settingList.add('Third Party Administrators');
    settingList.add('Packages');
    settingList.add('Visits');
    settingList.add('Administration');
    //settingList.add('Administration config');
    //settingList.add('Setting');
    settingList.add('About');
    settingList.add('div');
    settingList.add('Logout');

    icons['Doctor'] = Icons.mail_outline;
    icons['visits'] = Icons.supervisor_account;
    icons['add role'] = Icons.add;
    icons['packages'] = Icons.assignment;
    icons['service staffer'] = Icons.business;
    icons['service providers'] = Icons.local_hospital;
    icons['insurance companies'] = Icons.assignment_turned_in;
    icons['third party administrators'] = Icons.account_balance;
    icons['pharmacist'] = Icons.local_pharmacy;
    icons['profile'] = Icons.person;
    icons['patients'] = Icons.accessibility_new;
    icons['Manage Templates'] = Icons.attach_file;
    icons['administration'] = Icons.verified_user;
    icons['administration config'] = Icons.verified_user;
    icons['setting'] = Icons.settings;
    icons['about'] = Icons.info;
    icons['logout'] = Icons.exit_to_app;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          UserAccountsDrawerHeader(
            currentAccountPicture: InkWell(
              child: CircleAvatar(
                backgroundImage: _image == null
                    ? AssetImage("assets/images/person.jpeg")
                    : FileImage(_image),
              ),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => FullScreenPage(_image)));
              },
            ),
            accountName: Text(user.username),
            accountEmail: Text(user.email),
            otherAccountsPictures: <Widget>[
              InkWell(
                child: Icon(
                  Icons.email,
                  color: Colors.white,
                ),
                onTap: () {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                          title: Text("Email"),
                          content: ListView.builder(
                            shrinkWrap: true,
                            itemBuilder: _buildEmailList,
                            itemCount: user.userEmails.length,
                          ));
                    },
                  );
                },
              ),
            ],
          ),
          Expanded(
            child: ListView.builder(
              shrinkWrap: true,
              itemBuilder: _buildList,
              itemCount: settingList.length,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildList(BuildContext context, int index) {
    switch (settingList[index].toLowerCase()) {
      case 'service staffer':
        {
          return ExpansionTile(
            title: Row(
              children: <Widget>[
                Icon(
                  Icons.business,
                  color: Colors.grey,
                ),
                Padding(
                  padding: EdgeInsets.only(right: 33),
                ),
                Flexible(
                  child: Text(
                    "Service staffers",
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                ),
              ],
            ),
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  FlatButton(
                    padding: EdgeInsets.only(left: 75),
                    child: Text("Doctor"),
                    onPressed: () {
                      Navigator.popUntil(
                          context, ModalRoute.withName('/homePage'));
                      Navigator.pushNamed(context, '/service staffer',
                          arguments: "DOCTOR");
                    },
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  FlatButton(
                    padding: EdgeInsets.only(left: 75),
                    child: Text("Nurse"),
                    onPressed: () {
                      Navigator.popUntil(
                          context, ModalRoute.withName('/homePage'));
                      Navigator.pushNamed(context, '/service staffer',
                          arguments: "NURSE");
                    },
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  FlatButton(
                    padding: EdgeInsets.only(left: 75),
                    child: Text("Pharmacist"),
                    onPressed: () {
                      Navigator.popUntil(
                          context, ModalRoute.withName('/homePage'));
                      Navigator.pushNamed(context, '/service staffer',
                          arguments: "PHARMACIST");
                    },
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  FlatButton(
                    padding: EdgeInsets.only(left: 75),
                    child: Text("Rardiographer"),
                    onPressed: () {
                      Navigator.popUntil(
                          context, ModalRoute.withName('/homePage'));
                      Navigator.pushNamed(context, '/service staffer',
                          arguments: "RADIOGRAPHER");
                    },
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  FlatButton(
                    padding: EdgeInsets.only(left: 75),
                    child: Text("Lab Specialist"),
                    onPressed: () {
                      Navigator.popUntil(
                          context, ModalRoute.withName('/homePage'));
                      Navigator.pushNamed(context, '/service staffer',
                          arguments: "LAB_SPECIALIST");
                    },
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  FlatButton(
                    padding: EdgeInsets.only(left: 75),
                    child: Text("Receptionist"),
                    onPressed: () {
                      Navigator.popUntil(
                          context, ModalRoute.withName('/homePage'));
                      Navigator.pushNamed(context, '/service staffer',
                          arguments: "RECEPTIONIST");
                    },
                  ),
                ],
              ),
            ],
          );
        }
        break;
      case 'service providers':
        {
          return ExpansionTile(
            title: Row(
              children: <Widget>[
                Icon(
                  Icons.grid_off,
                  color: Colors.grey,
                ),
                Padding(
                  padding: EdgeInsets.only(right: 33),
                ),
                Flexible(
                  child: Text(
                    "Service providers",
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                ),
              ],
            ),
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  FlatButton(
                    padding: EdgeInsets.only(left: 75),
                    child: Text("Hospital"),
                    onPressed: () {
                      Navigator.popUntil(
                          context, ModalRoute.withName('/homePage'));
                      Navigator.pushNamed(context, '/service provider',
                          arguments: "HOSPITAL");
                    },
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  FlatButton(
                    padding: EdgeInsets.only(left: 75),
                    child: Text("Pharmacy"),
                    onPressed: () {
                      Navigator.popUntil(
                          context, ModalRoute.withName('/homePage'));
                      Navigator.pushNamed(context, '/service provider',
                          arguments: "PHARMACY");
                    },
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  FlatButton(
                    padding: EdgeInsets.only(left: 75),
                    child: Text("Laboratory"),
                    onPressed: () {
                      Navigator.popUntil(
                          context, ModalRoute.withName('/homePage'));
                      Navigator.pushNamed(context, '/service provider',
                          arguments: "LABORATORY");
                    },
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  FlatButton(
                    padding: EdgeInsets.only(left: 75),
                    child: Text("Radiograph"),
                    onPressed: () {
                      Navigator.popUntil(
                          context, ModalRoute.withName('/homePage'));
                      Navigator.pushNamed(context, '/service provider',
                          arguments: "RADIOGRAPHOFFICE");
                    },
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  FlatButton(
                    padding: EdgeInsets.only(left: 75),
                    child: Text("Clinic"),
                    onPressed: () {
                      Navigator.popUntil(
                          context, ModalRoute.withName('/homePage'));
                      Navigator.pushNamed(context, '/service provider',
                          arguments: "CLINIC");
                    },
                  ),
                ],
              ),
            ],
          );
        }
        break;
//      case 'manage templates':
//        {
//          return ExpansionTile(
//            title: Row(
//              children: <Widget>[
//                Icon(
//                  Icons.attach_file,
//                  color: Colors.grey,
//                ),
//                Padding(
//                  padding: EdgeInsets.only(right: 33),
//                ),
//                Flexible(
//                  child: Text(
//                    "Manage Templates",
//                    overflow: TextOverflow.ellipsis,
//                    maxLines: 2,
//                  ),
//                ),
//              ],
//            ),
//            children: <Widget>[
//              Row(
//                mainAxisAlignment: MainAxisAlignment.start,
//                children: <Widget>[
//                  FlatButton(
//                    padding: EdgeInsets.only(left: 75),
//                    child: Text("Drugs"),
//                    onPressed: () {
//                      Navigator.popUntil(
//                          context, ModalRoute.withName('/homePage'));
//                      Navigator.pushNamed(context, '/drugs');
//                    },
//                  ),
//                ],
//              ),
//              Row(
//                mainAxisAlignment: MainAxisAlignment.start,
//                children: <Widget>[
//                  FlatButton(
//                    padding: EdgeInsets.only(left: 75),
//                    child: Text("Surgeries"),
//                    onPressed: () {
//                      Navigator.popUntil(
//                          context, ModalRoute.withName('/homePage'));
//                      Navigator.pushNamed(context, '/surgeries');
//                    },
//                  ),
//                ],
//              ),
//              Row(
//                mainAxisAlignment: MainAxisAlignment.start,
//                children: <Widget>[
//                  FlatButton(
//                    padding: EdgeInsets.only(left: 75),
//                    child: Text("Lab Tests"),
//                    onPressed: () {
//                      Navigator.popUntil(
//                          context, ModalRoute.withName('/homePage'));
//                      Navigator.pushNamed(context, '/labtests');
//                    },
//                  ),
//                ],
//              ),
//              Row(
//                mainAxisAlignment: MainAxisAlignment.start,
//                children: <Widget>[
//                  FlatButton(
//                    padding: EdgeInsets.only(left: 75),
//                    child: Text("Radiographs"),
//                    onPressed: () {
//                      Navigator.popUntil(
//                          context, ModalRoute.withName('/homePage'));
//                      Navigator.pushNamed(context, '/radiographs');
//                    },
//                  ),
//                ],
//              ),
//            ],
//          );
//        }
//        break;

//      case 'administration':
//        {
//          for (int i = 0; i < user.userRoles.length; i++) {
//            if (user.userRoles[i].roleName.toLowerCase() == "administrator") {
//              return ExpansionTile(
//                title: Row(
//                  children: <Widget>[
//                    Icon(
//                      Icons.verified_user,
//                      color: Colors.grey,
//                    ),
//                    Padding(
//                      padding: EdgeInsets.only(right: 33),
//                    ),
//                    Flexible(
//                      child: Text(
//                        "Administration",
//                        overflow: TextOverflow.ellipsis,
//                        maxLines: 2,
//                      ),
//                    ),
//                  ],
//                ),
//                children: <Widget>[
//                  Row(
//                    mainAxisAlignment: MainAxisAlignment.start,
//                    children: <Widget>[
//                      FlatButton(
//                        padding: EdgeInsets.only(left: 75),
//                        child: Text("Addresses"),
//                        onPressed: () {
//                          Navigator.popUntil(
//                              context, ModalRoute.withName('/homePage'));
//                          Navigator.pushNamed(context, '/addressesAdmin');
//                        },
//                      ),
//                    ],
//                  ),
//                  Row(
//                    mainAxisAlignment: MainAxisAlignment.start,
//                    children: <Widget>[
//                      FlatButton(
//                        padding: EdgeInsets.only(left: 75),
//                        child: Text("Add role"),
//                        onPressed: () {
//                          Navigator.popUntil(
//                              context, ModalRoute.withName('/homePage'));
//                          Navigator.pushNamed(context, '/add role admin');
//                        },
//                      ),
//                    ],
//                  ),
//                  Row(
//                    mainAxisAlignment: MainAxisAlignment.start,
//                    children: <Widget>[
//                      FlatButton(
//                        padding: EdgeInsets.only(left: 75),
//                        child: Text("Delete role"),
//                        onPressed: () {
//                          Navigator.popUntil(
//                              context, ModalRoute.withName('/homePage'));
//                          Navigator.pushNamed(context, '/delete role admin');
//                        },
//                      ),
//                    ],
//                  ),
//                  Row(
//                    mainAxisAlignment: MainAxisAlignment.start,
//                    children: <Widget>[
//                      FlatButton(
//                        padding: EdgeInsets.only(left: 75),
//                        child: Text("Confirm Roles"),
//                        onPressed: () {
//                          Navigator.popUntil(
//                              context, ModalRoute.withName('/homePage'));
//                          Navigator.pushNamed(context, '/role control');
//                        },
//                      ),
//                    ],
//                  ),
//                  Row(
//                    mainAxisAlignment: MainAxisAlignment.start,
//                    children: <Widget>[
//                      FlatButton(
//                        padding: EdgeInsets.only(left: 75),
//                        child: Text("Confirm Addresses"),
//                        onPressed: () {
//                          Navigator.popUntil(
//                              context, ModalRoute.withName('/homePage'));
//                          Navigator.pushNamed(context, '/addressesControl');
//                        },
//                      ),
//                    ],
//                  ),
//                  Row(
//                    mainAxisAlignment: MainAxisAlignment.start,
//                    children: <Widget>[
//                      FlatButton(
//                        padding: EdgeInsets.only(left: 75),
//                        child: Text("Confirm Phone Numbers"),
//                        onPressed: () {
//                          Navigator.popUntil(
//                              context, ModalRoute.withName('/homePage'));
//                          Navigator.pushNamed(context, '/phonesControl');
//                        },
//                      ),
//                    ],
//                  ),
//                  Row(
//                    mainAxisAlignment: MainAxisAlignment.start,
//                    children: <Widget>[
//                      FlatButton(
//                        padding: EdgeInsets.only(left: 75),
//                        child: Text("Confirm Emails"),
//                        onPressed: () {
//                          Navigator.popUntil(
//                              context, ModalRoute.withName('/homePage'));
//                          Navigator.pushNamed(context, '/emailsControl');
//                        },
//                      ),
//                    ],
//                  ),
//                ],
//              );
//            }
//          }
//          return Container();
//        }
//        break;
      case 'administration':
        {
          for (int i = 0; i < user.userRoles.length; i++) {
            if (user.userRoles[i].roleName.toLowerCase() == "administrator") {
              return ListTile(
                leading: Icon(icons[settingList[index].toLowerCase()]),
                title: Text(settingList[index]),
                onTap: () {
                  Navigator.popUntil(context, ModalRoute.withName('/homePage'));
                  Navigator.pushNamed(
                      context, '/' + settingList[index].toLowerCase());
                },
              );
            }
          }
          return Container();
        }
        break;
      case 'div':
        {
          return Divider();
        }
        break;
      default:
        {
          return ListTile(
            leading: Icon(icons[settingList[index].toLowerCase()]),
            title: Text(settingList[index]),
            onTap: () {
              switch (settingList[index].toLowerCase()) {
                case 'logout':
                  {
                    auth.logout();
                    Navigator.of(context).pushNamedAndRemoveUntil(
                        '/welcome', (Route<dynamic> route) => false);
                  }
                  break;
                default:
                  {
                    Navigator.popUntil(
                        context, ModalRoute.withName('/homePage'));
                    Navigator.pushNamed(
                        context, '/' + settingList[index].toLowerCase());
                  }
                  break;
              }
            },
          );
        }
        break;
    }
  }

  void loadUserFromShare() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    //load image
    try {
      _image = File(prefs.getString('Profile photo'));
      if (_image == null) {
        String url = DotEnv()
            .env['DOWNLOAD_IMAGE']
            .replaceAll("document", prefs.getString('document'));
        var response = await client.getPrivateDataJPG(url, context);
        Directory tempDir = await getTemporaryDirectory();
        String tempPath = tempDir.path;
        File file = new File('$tempPath/profile.png');
        prefs.setString('Profile photo', '$tempPath/profile.png');
        await file.writeAsBytes(response.bodyBytes);
        _image = file;
      }
    } catch (e) {}

    //load user from server
    String url = DotEnv().env['GET_ACC_INFO'];
    dynamic response;

    response = await client.getPrivateData(url, context);

    final parsed = await jsonDecode(response.body);
    user = User.fromJson(parsed['data']);
    setState(() {});
  }

  Widget _buildEmailList(BuildContext context, int index) {
    return FlatButton(
      child: Text(user.userEmails[index].email),
      onPressed: () {},
    );
  }
}
