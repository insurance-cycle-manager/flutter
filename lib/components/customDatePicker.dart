import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';

class CustomDatePicker extends StatefulWidget {
  final String name;

  Function(String myDate) onPressed;

  @override
  State createState() {
    return _CustomDatePickerState();
  }

  CustomDatePicker({this.name, this.onPressed});
}

class _CustomDatePickerState extends State<CustomDatePicker> {
  String _date = "Not set";

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 30, right: 20, top: 15),
      child: RaisedButton(
        color: Colors.white,
        shape: RoundedRectangleBorder(
            side: BorderSide(color: Colors.green),
            borderRadius: BorderRadius.circular(7.0)),
        elevation: 4.0,
        onPressed: () {
          DatePicker.showDatePicker(context,
              theme: DatePickerTheme(
                containerHeight: 210.0,
              ),
              showTitleActions: true,
              minTime: DateTime(1900, 1, 1),
              maxTime: DateTime.now(), onConfirm: (date) {
            _date = '${date.year} - ${date.month} - ${date.day}';
            widget.onPressed(_date);
            setState(() {});
          }, currentTime: DateTime.now(), locale: LocaleType.en);
        },
        child: Container(
          alignment: Alignment.center,
          height: 50.0,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                widget.name,
                style: TextStyle(
                  color: Colors.teal,
                ),
              ),
              Row(
                children: <Widget>[
                  Container(
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.date_range,
                          size: 18.0,
                          color: Colors.teal,
                        ),
                        Text(
                          "$_date",
                          style: TextStyle(color: Colors.teal),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
