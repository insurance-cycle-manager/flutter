import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomTFEdit extends StatefulWidget {
  String label;
  String hintText;
  TextEditingController textCont = TextEditingController();
  bool status = true;

  CustomTFEdit({this.label,this.status:true,this.hintText,this.textCont});
  @override
  State createState() {
    return _CustomTFEditState();
  }
}

class _CustomTFEditState extends State<CustomTFEdit> {


  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
            padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
            child: new Row(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                new Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Text(
                      widget.label,
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ],
            )),
        Padding(
            padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
            child: new Row(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                new Flexible(
                  child: new TextField(
                    controller: widget.textCont,
                    enabled: !widget.status,
                    autofocus: !widget.status,
                  ),
                ),
              ],
            )),
      ],
    );
  }
}
