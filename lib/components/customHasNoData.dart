import 'package:flutter/material.dart';

class CustomHasNoData extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text("This page has no data!"),
    );
  }
}
