import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  const CustomButton(
      {this.bName,
      this.bFunc,
        this.bColor: const Color(0xFF07a383),
        this.bTextColor = Colors.white});

  final String bName;
  final Function bFunc;
  final Color bColor;
  final Color bTextColor;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 30.0, right: 30.0, top: 30.0),
      alignment: Alignment.center,
      child: Row(
        children: <Widget>[
          Expanded(
            child: FlatButton(
              color: bColor,
              padding: EdgeInsets.all(0),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30.0)),

              onPressed: bFunc,
              //gotoLogin(),
              child: Container(
                padding: const EdgeInsets.symmetric(
                    vertical: 20.0, horizontal: 20.0),
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(
                      Radius.circular(30.0) //         <--- border radius here
                      ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        bName,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: bTextColor, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
