import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:medcare/screen/Admin/administration.dart';
import 'package:medcare/screen/Admin/insuranceConnections.dart';
import 'package:medcare/screen/about.dart';
import 'package:medcare/screen/addressAdmin/addAddressAdmin.dart';
import 'package:medcare/screen/addressAdmin/addressesAdmin.dart';
import 'package:medcare/screen/addressAdmin/addressesControl.dart';
import 'package:medcare/screen/addressAdmin/detailsAddressAdmin.dart';
import 'package:medcare/screen/auth/login.dart';
import 'package:medcare/screen/auth/signUp.dart';
import 'package:medcare/screen/auth/splash.dart';
import 'package:medcare/screen/auth/welcomePage.dart';
import 'package:medcare/screen/emailAdmin/EmailsAdmin.dart';
import 'package:medcare/screen/emailAdmin/EmailsControl.dart';
import 'package:medcare/screen/emailAdmin/addEmailAdmin.dart';
import 'package:medcare/screen/emailAdmin/detailsEmailsAdmin.dart';
import 'package:medcare/screen/homePage.dart';
import 'package:medcare/screen/insuranceCompany/addInsuranceCompany.dart';
import 'package:medcare/screen/insuranceCompany/detailsInsuranceCompany.dart';
import 'package:medcare/screen/insuranceCompany/insuranceCompanies.dart';
import 'package:medcare/screen/insuranceCompany/insuranceCompaniesMe.dart';
import 'package:medcare/screen/package/addPackage.dart';
import 'package:medcare/screen/package/detailsPackage.dart';
import 'package:medcare/screen/package/packages.dart';
import 'package:medcare/screen/patient/addPatient.dart';
import 'package:medcare/screen/patient/detalisPatient.dart';
import 'package:medcare/screen/patient/patients.dart';
import 'package:medcare/screen/phoneNumberAdmin/addphoneNumberAdmin.dart';
import 'package:medcare/screen/phoneNumberAdmin/detailsphoneNumberAdmin.dart';
import 'package:medcare/screen/phoneNumberAdmin/phoneNumbersAdmin.dart';
import 'package:medcare/screen/phoneNumberAdmin/phoneNumbersControl.dart';
import 'package:medcare/screen/profile/address/addAddress.dart';
import 'package:medcare/screen/profile/email/addEmail.dart';
import 'package:medcare/screen/profile/phoneNumber/addPhone.dart';
import 'package:medcare/screen/profile/profile.dart';
import 'package:medcare/screen/profile/role/addRole.dart';
import 'package:medcare/screen/profile/role/roles.dart';
import 'package:medcare/screen/roleAdmin/addRoleAdmin.dart';
import 'package:medcare/screen/roleAdmin/deleteRoleAdmin.dart';
import 'package:medcare/screen/roleAdmin/rolesControl.dart';
import 'package:medcare/screen/roleAdmin/showUserInfo.dart';
import 'package:medcare/screen/serviceProvider/detailsSP.dart';
import 'package:medcare/screen/serviceProvider/serviceProviders.dart';
import 'package:medcare/screen/serviceStaffer/detailsSS.dart';
import 'package:medcare/screen/serviceStaffer/serviceStaffers.dart';
import 'package:medcare/screen/setting.dart';
import 'package:medcare/screen/template/drug/addDrug.dart';
import 'package:medcare/screen/template/drug/detailsDrug.dart';
import 'package:medcare/screen/template/drug/drugs.dart';
import 'package:medcare/screen/template/labTest/addLabTest.dart';
import 'package:medcare/screen/template/labTest/detailsLabTest.dart';
import 'package:medcare/screen/template/labTest/labTests.dart';
import 'package:medcare/screen/template/listTemplate.dart';
import 'package:medcare/screen/template/radiograph/addRadiograph.dart';
import 'package:medcare/screen/template/radiograph/detailsRadiograph.dart';
import 'package:medcare/screen/template/radiograph/radiographs.dart';
import 'package:medcare/screen/template/surgery/addSurgery.dart';
import 'package:medcare/screen/template/surgery/detailsSurgery.dart';
import 'package:medcare/screen/template/surgery/surgeries.dart';
import 'package:medcare/screen/tpa/addTpa.dart';
import 'package:medcare/screen/tpa/detailsTpa.dart';
import 'package:medcare/screen/tpa/tpas.dart';
import 'package:medcare/screen/tpa/tpasMe.dart';
import 'package:medcare/screen/unauthorized.dart';
import 'package:medcare/screen/visit/addVisit.dart';
import 'package:medcare/screen/visit/detailsVisit.dart';
import 'package:medcare/screen/visit/labTestVisit/addLabTestVist.dart';
import 'package:medcare/screen/visit/labTestVisit/detailsLabTestVisit.dart';
import 'package:medcare/screen/visit/labTestVisit/labTestsVisit.dart';
import 'package:medcare/screen/visit/prescription/addPrescription.dart';
import 'package:medcare/screen/visit/prescription/detailsPrescription.dart';
import 'package:medcare/screen/visit/prescription/prescriptions.dart';
import 'package:medcare/screen/visit/radiographVisit/addRadiographVisit.dart';
import 'package:medcare/screen/visit/radiographVisit/deatailsRadiograph.dart';
import 'package:medcare/screen/visit/radiographVisit/radiographsVisit.dart';
import 'package:medcare/screen/visit/surgeryVisit/addSurgeryVisit.dart';
import 'package:medcare/screen/visit/surgeryVisit/detailsSurgeryVisit.dart';
import 'package:medcare/screen/visit/surgeryVisit/surgeriesVisit.dart';
import 'package:medcare/screen/visit/visits.dart';

Future<void> main() async {
  await DotEnv().load(".env");

  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DynamicTheme(
        data: (brightness) => ThemeData(
              brightness: Brightness.light,
              primaryColor: Color(0xFF07a383),
              accentColor: Color(0xFF07a383),
              primarySwatch: Colors.teal,
            ),
        themedWidgetBuilder: (context, theme) {
          return MaterialApp(
            theme: theme,
            //TODO change all cards to ListTile
            routes: {
              '/splash': (context) => Splash(),
              '/welcome': (context) => WelcomePage(),
              '/signup': (context) => SignUp(),
              '/login': (context) => Login(),
              '/homePage': (context) => HomePage(),
              '/setting': (context) => SettingPage(),
              '/about': (context) => AboutPage(),
              '/profile': (context) => ProfilePage(),
              '/logout': (context) => WelcomePage(),
              '/patients': (context) => Patients(),
              '/addPatient': (context) => AddPatient(),
              '/detailsPatient': (context) => DetailsPatient(),
              '/detailsIC': (context) => DetailsIC(),
              '/addIc': (context) => AddIC(),
              '/third party administrators': (context) => TPAs(),
              '/addTpa': (context) => AddTpa(),
              '/detailsTpa': (context) => DetailsTPA(),
              '/service provider': (context) => ServiceProviders(),
              '/service staffer': (context) => ServiceStaffers(),
              '/detailsSP': (context) => DetailsSP(),
              '/detailsSS': (context) => DetailsSS(),
              '/detailsPackage': (context) => DetailsPackage(),
              '/packages': (context) => Packages(),
              '/addPackage': (context) => AddPackage(),
              '/visits': (context) => VisitsPage(),
              '/addVisit': (context) => AddVisit(),
              '/detailsVisit': (context) => DetailsVisit(),
              '/drugs': (context) => Drugs(),
              '/addDrug': (context) => AddDrug(),
              '/detailsDrug': (context) => DetailsDrug(),
              '/surgeries': (context) => Surgeries(),
              '/addSurgery': (context) => AddSurgery(),
              '/detailsSurgery': (context) => DetailsSurgery(),
              '/labtests': (context) => LabTests(),
              '/addLabTest': (context) => AddLabTest(),
              '/detailsLabTest': (context) => DetailsLabTest(),
              '/radiographs': (context) => Radiographs(),
              '/addRadiograph': (context) => AddRadiograph(),
              '/detailsRadiograph': (context) => DetailsRadiograph(),
              '/role control': (context) => RolesControl(),
              '/user info': (context) => UseInfoPage(),
              '/prescriptions': (context) => Prescriptions(),
              '/add prescription': (context) => AddPrescription(),
              '/detailsPrescription': (context) => DetailsPrescription(),
              '/list template': (context) => ListTemplate(),
              '/addRole': (context) => AddRole(),
              '/add role admin': (context) => AddRoleAdmin(),
              '/addEmail': (context) => AddEmail(),
              '/addPhone': (context) => AddPhone(),
              '/delete role admin': (context) => DeleteRoleAdmin(),
              '/roles': (context) => RolesPage(),
              '/addressesAdmin': (context) => AddressesAdmin(),
              '/addAddressAdmin': (context) => AddAddressAdmin(),
              '/detailsAddressAdmin': (context) => DetailsAddressAdmin(),
              '/emailsAdmin': (context) => EmailsAdmin(),
              '/addEmailAdmin': (context) => AddEmailAdmin(),
              '/detailsEmailAdmin': (context) => DetailsEmailAdmin(),
              '/phonesAdmin': (context) => PhoneNumbersAdmin(),
              '/addPhoneAdmin': (context) => AddPhoneNumberAdmin(),
              '/detailsPhoneAdmin': (context) => DetailsPhoneNumberAdmin(),
              '/add user address': (context) => AddUserAddress(),
              '/addressesControl': (context) => AddressesControl(),
              '/emailsControl': (context) => EmailsControl(),
              '/phonesControl': (context) => PhoneNumbersControl(),
              '/insurance companies': (context) => IC(),
              '/my insurance companies': (context) => MyIC(),
              '/my third party administrators': (context) => MyTPA(),
              '/administration': (context) => Administration(),
              '/AddICConnection': (context) => AddICConnection(),
              '/LabTests': (context) => LabTests(),
              '/AddLabTestVisit': (context) => AddLabTestVisit(),
              '/LabTestsVisit': (context) => LabTestsVisit(),
              '/DetailsLabTestVisit': (context) => DetailsLabTestVisit(),
              '/SurgeriesVisit': (context) => SurgeriesVisit(),
              '/DetailsSurgeryVisit': (context) => DetailsSurgeryVisit(),
              '/AddSurgeryVisit': (context) => AddSurgeryVisit(),
              '/RadriographsVisit': (context) => RadriographsVisit(),
              '/AddRadriographVisit': (context) => AddRadriographVisit(),
              '/DetailsRadriographVisit': (context) =>
                  DetailsRadriographVisit(),
              '/unauthorized': (context) => Unauthorized(),
            },
            initialRoute: '/splash',
            title: 'MedCare',
            //debugShowCheckedModeBanner: false,
          );
        });
  }
}

/*
https://dartpad.dartlang.org/5dd318e491031df8b31a8fc7f2fbe320
 */
