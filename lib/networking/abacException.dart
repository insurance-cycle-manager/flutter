class AbacException implements Exception {
  @override
  String toString() {
    return "ABAC Exception: You're not allowed to access this function";
  }
}
