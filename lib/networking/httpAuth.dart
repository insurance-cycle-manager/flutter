import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:medcare/screen/auth/user.dart';
import 'package:medcare/screen/profile/role/Role.dart';
import 'package:medcare/screen/unauthorized.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'abacException.dart';

Future<User> register(User userForRegistration, BuildContext context) async {
  Map<String, String> header = {
    HttpHeaders.contentTypeHeader: 'application/json'
  };

  try {
    var response = await http.post(DotEnv().env['AUTH_URL_REGISTER'],
        headers: header, body: jsonEncode(userForRegistration));
    var reqBody = jsonDecode(response.body);
    if (response.statusCode == 406) {
      throw AbacException();
    }
    if (response.statusCode != HttpStatus.ok) {
      Fluttertoast.showToast(
          msg: reqBody["message"],
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          toastLength: Toast.LENGTH_LONG,
          backgroundColor: Colors.redAccent,
          textColor: Colors.white,
          fontSize: 16.0);
      return null;
    } else {
      Fluttertoast.showToast(
          msg: reqBody["message"],
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          toastLength: Toast.LENGTH_LONG,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0);
      return User.fromJson(jsonDecode(reqBody["data"]));
    }
  } on AbacException {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => Unauthorized()));
  }
}

Future<String> login(User userForLogin, BuildContext context) async {
  Map<String, String> header = {'Content-Type': 'application/json'};
  var response, body;
  try {
    response = await http.post(DotEnv().env['AUTH_URL_LOGIN'],
        headers: header, body: jsonEncode(userForLogin));
    body = jsonDecode(response.body);
    if (response.statusCode == 406) {
      throw AbacException();
    }
    if (response.statusCode != HttpStatus.ok) {
      Fluttertoast.showToast(
          msg: body["message"],
          gravity: ToastGravity.CENTER,
          toastLength: Toast.LENGTH_LONG,
          fontSize: 3.0,
          backgroundColor: Colors.redAccent);

      return null;
    }
  } on AbacException {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => Unauthorized()));
  }

  final SharedPreferences prefs = await SharedPreferences.getInstance();
  Map<String, dynamic> saveUser = body['data']['user'];
  saveUser['password'] = userForLogin.password;
  saveUser['token'] = body["data"]["token"];
  prefs.setString('UserLoginData', jsonEncode(saveUser));

  prefs.setString("activeRole", jsonEncode(Role(roleName: "User")));
  return body["data"]["token"];
}

Future<bool> setMobileToken(String token) async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setBool("is_login", true);
  return prefs.setString("jwt", token);
}

Future<String> getMobileToken(BuildContext context) async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  var token = prefs.getString("jwt");
  Map<String, dynamic> tokenDecoded = parseJwt(token);
  int exp = tokenDecoded['exp'];
  if (isTokenExpired(exp)) {
    String body = prefs.getString('UserLoginData');
    Map<String, dynamic> json = jsonDecode(body);
    User user = User.fromJson(json);
    var loginResponse =
    await login(User.userToLogin(user.username, user.password), context);
    if (loginResponse != null) {
      await setMobileToken(loginResponse);
    }
    token = prefs.getString("jwt");
  }
  return token;
}

bool isTokenExpired(int exp) {
  int bufferSeconds = 10;
  if (exp != null) {
    var currentTime = DateTime.now();
    DateTime expDate = DateTime.fromMillisecondsSinceEpoch(exp * 1000);
    Duration durationRemaining = expDate.difference(currentTime);
    return (durationRemaining.inSeconds - bufferSeconds) <= 0 ? true : false;
  }
  return false;
}

Map<String, dynamic> parseJwt(String token) {
  final parts = token.split('.');
  if (parts.length != 3) {
    throw Exception('invalid token');
  }

  final payload = _decodeBase64(parts[1]);
  final payloadMap = json.decode(payload);
  if (payloadMap is! Map<String, dynamic>) {
    throw Exception('invalid payload');
  }

  return payloadMap;
}

String _decodeBase64(String str) {
  String output = str.replaceAll('-', '+').replaceAll('_', '/');

  switch (output.length % 4) {
    case 0:
      break;
    case 2:
      output += '==';
      break;
    case 3:
      output += '=';
      break;
    default:
      throw Exception('Illegal base64url string!"');
  }

  return utf8.decode(base64Url.decode(output));
}

Future<void> logout() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.remove('jwt');
  prefs.remove("is_login");
}
