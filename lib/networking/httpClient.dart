import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpAuth.dart' as auth;
import 'package:medcare/screen/profile/role/Role.dart';
import 'package:medcare/screen/unauthorized.dart';
import 'package:shared_preferences/shared_preferences.dart';

void showMessage(var response) {
  var body = jsonDecode(response.body);
  switch (response.statusCode) {
    case HttpStatus.created:
    case HttpStatus.ok:
      {
        Fluttertoast.showToast(
            msg: body["message"],
            gravity: ToastGravity.CENTER,
            toastLength: Toast.LENGTH_LONG,
            fontSize: 3.0,
            textColor: Colors.green);
      }
      break;
    case HttpStatus.badRequest:
    case HttpStatus.internalServerError:
    case HttpStatus.forbidden:
    case HttpStatus.notFound:
      {
        Fluttertoast.showToast(
            msg: body["message"] ?? body['Message'],
            gravity: ToastGravity.CENTER,
            toastLength: Toast.LENGTH_LONG,
            fontSize: 3.0,
            textColor: Colors.red);
      }
      break;
    default:
      {
        Fluttertoast.showToast(
            msg: "Unknown status code",
            gravity: ToastGravity.CENTER,
            toastLength: Toast.LENGTH_LONG,
            fontSize: 3.0,
            backgroundColor: Colors.black12);
      }
  }
}

Future deleteObject(String url, BuildContext context, String route) async {
  var token = await auth.getMobileToken(context);
  try {
    Map<String, String> header = {
      HttpHeaders.contentTypeHeader: 'application/json',
      'Authorization': 'Bearer $token'
    };
    http.Response response;
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Alert!"),
          content: Text("Are you sure you want to delete it?"),
          actions: <Widget>[
            FlatButton(
                onPressed: () async {
                  response = await http.delete(url, headers: header);
                  if (response.statusCode == 406) {
                    throw AbacException();
                  }
                  if (route != null) {
                    Navigator.pop(context);
                    Navigator.pop(context);
                    Navigator.pushNamed(context, route);
                  }
                  showMessage(response);
                },
                child: Text('Yes')),
            FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text('No')),
          ],
        );
      },
    );
    return response.toString();
  } on AbacException {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => Unauthorized()));
  }
}

Future editObject(String url, Map<String, dynamic> json,
    BuildContext context) async {
  var token = await auth.getMobileToken(context);
  try {
    Map<String, String> header = {
      HttpHeaders.contentTypeHeader: 'application/json',
      'Authorization': 'Bearer $token'
    };
    http.Response response = await http.put(url,
        headers: header,
        body: jsonEncode(json),
        encoding: Encoding.getByName("utf-8"));

    if (response.statusCode == 406) {
      throw AbacException();
    }

    showMessage(response);
    return response.toString();
  } on AbacException {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => Unauthorized()));
  }
}

Future<String> postWithTokenJSON(Map<String, dynamic> json, String url,
    BuildContext context,
    {Map<String, String> header}) async {
  var token = await auth.getMobileToken(context);
  try {
    Map<String, String> header = {
      HttpHeaders.contentTypeHeader: 'application/json',
      'Authorization': 'Bearer $token'
    };
    http.Response response =
    await http.post(url, headers: header, body: jsonEncode(json));

    if (response.statusCode == 406) {
      throw AbacException();
    }
    showMessage(response);
    return response.toString();
  } on AbacException {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => Unauthorized()));
  }
}

Future getPrivateDataJPG(String url, BuildContext context) async {
  var token = await auth.getMobileToken(context);
  if (token == null) return null;
  Map<String, String> header = {
    HttpHeaders.contentTypeHeader: 'image/jpeg',
    HttpHeaders.authorizationHeader: 'Bearer $token'
  };

  try {
    http.Response response = await http.get(url, headers: header);
    //showMessage(response);
    if (response.statusCode == 406) {
      throw AbacException();
    }

    if (response.statusCode != HttpStatus.ok) {
      return null;
    }
    return response;
  } on AbacException {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => Unauthorized()));
  }
}

Future postFormData(String url, File file, BuildContext context) async {
  var token = await auth.getMobileToken(context);
  if (token == null) return null;
  Map<String, String> header = {
    HttpHeaders.authorizationHeader: 'Bearer $token'
  };
  try {
    String fileName = file.path
        .split('/')
        .last;

    FormData formData = FormData.fromMap({
      "file": await MultipartFile.fromFile(
        file.path,
        filename: fileName,
      ),
    });
    Dio dio = new Dio();
    return dio
        .post(url,
        data: formData,
        options: Options(
          method: 'POST',
          headers: header,
        ))
        .then((value) => value);
  } on AbacException {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => Unauthorized()));
  }
}

Future<String> postJSON(Map<String, dynamic> json, String url,
    BuildContext context,
    {Map<String, String> header}) async {
  Map<String, String> header = {
    HttpHeaders.contentTypeHeader: 'application/json'
  };
  try {
    http.Response response =
    await http.post(url, headers: header, body: jsonEncode(json));

    if (response.statusCode == 406) {
      throw AbacException();
    }

    //showMessage(response);
    return response.toString();
  } on AbacException {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => Unauthorized()));
  }
}

Future getPrivateData(String url, BuildContext context) async {
  var token = await auth.getMobileToken(context);
  if (token == null) return null;
  Map<String, String> header = {
    HttpHeaders.contentTypeHeader: 'application/json',
    HttpHeaders.authorizationHeader: 'Bearer $token'
  };

  try {
    http.Response response = await http.get(url, headers: header);
    //showMessage(response);

    if (response.statusCode == 406) {
      throw AbacException();
    }

    if (response.statusCode != HttpStatus.ok) {
      return null;
    }
    return response;
  } on AbacException {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => Unauthorized()));
  }
}

Future getPublicData(String url, Map<String, String> header,
    BuildContext context) async {
  //header[HttpHeaders.authorizationHeader] = 'Bearer $token';

  header[HttpHeaders.contentTypeHeader] = 'application/json';

  try {
    http.Response response = await http.get(url, headers: header);
    print(response.body);

    showMessage(response);

    return response;
  } on AbacException {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => Unauthorized()));
  }
}

Future<Role> getActiveRole() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  Role role = Role.fromJson(jsonDecode(prefs.getString("activeRole")));
  return role;
}

Future<void> setActiveRole(Role role) async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setString("activeRole", jsonEncode(role));
  return null;
}

Future<bool> isActiveRole(Role role) async {
  Role _role = await getActiveRole();
  return _role.roleName == role.roleName;
}
