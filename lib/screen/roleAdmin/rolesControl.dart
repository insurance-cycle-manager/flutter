import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/auth/user.dart';
import 'package:medcare/screen/unauthorized.dart';

class RolesControl extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _RolesControlState();
  }
}

class _RolesControlState extends State<RolesControl> {
  Widget appBarTitle = new Text("Roles Requests");
  Icon actionIcon = new Icon(Icons.search);
  final TextEditingController _searchQuery = new TextEditingController();
  final key = new GlobalKey<ScaffoldState>();
  bool _isSearching;
  String _searchText = "";
  List<RequestRole> requestRoles = [];

  Future<List<RequestRole>> _listFuture;

  List<String> status = ["PENDING", "CANCELED", "APPROVED", "REJECTED"];

  _RolesControlState() {
    _searchQuery.addListener(() {
      if (_searchQuery.text.isEmpty) {
        setState(() {
          _isSearching = false;
          _searchText = "";
        });
      } else {
        setState(() {
          _isSearching = true;
          _searchText = _searchQuery.text;
        });
      }
    });
  }

  @override
  void initState() {
    setState(() {});
    _isSearching = false;
    _listFuture = updateAndGetList();
    super.initState();
  }

  Future<void> refreshList() {
    // reload
    setState(() {
      _listFuture = updateAndGetList();
    });
    return null;
  }

  Future<List<RequestRole>> updateAndGetList() async {
    try {
      var response =
      await client.getPrivateData(DotEnv().env['GET_ROLES_ADMIN'], context);

      final parsed = await jsonDecode(response.body);

      return (parsed["data"] as List)
          .map<RequestRole>((json) => RequestRole.fromJson(json))
          .toList();
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
      return [];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: key,
      appBar: AppBar(
        title: this.appBarTitle,
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: GestureDetector(
              child: IconButton(
                  icon: actionIcon,
                  onPressed: () {
                    setState(() {
                      if (this.actionIcon.icon == Icons.search) {
                        this.actionIcon = new Icon(
                          Icons.close,
                        );
                        this.appBarTitle = new TextField(
                          controller: _searchQuery,
                          decoration: new InputDecoration(
                            prefixIcon: new Icon(Icons.search),
                            hintText: "Search...",
                          ),
                        );
                        _handleSearchStart();
                      } else {
                        _handleSearchEnd();
                      }
                    });
                  }),
            ),
          ),
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {},
                child: Icon(Icons.more_vert),
              )),
        ],
      ),
      body: FutureBuilder<List<RequestRole>>(
        future: _listFuture,
        builder:
            (BuildContext context, AsyncSnapshot<List<RequestRole>> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return SpinKitCircle(
              color: Colors.teal,
            );
          } else if (snapshot.connectionState == ConnectionState.done) {
            requestRoles = snapshot.data ?? <RequestRole>[];
            return Scaffold(
              backgroundColor: Color(0xffE5E5E5),
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView.builder(
                    itemCount: requestRoles.length,
                    itemBuilder: _isSearching ? _buildSearchList : _buildList),
              ),
            );
          } else {
            return Scaffold(
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 250,
                    ),
                    Icon(Icons.error),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[Text("Something went wrong")],
                    ),
                    Expanded(
                      child: Text(
                        "Please check your internet connection",
                        style: TextStyle(fontSize: 15),
                      ),
                    )
                  ],
                ),
              ),
            );
          }
        },
      ),
    );
  }

  void sendConfirm(String response, int index) async {
    int state = response == "APPROVE" ? 1 : 0;
    var json = {
      "RoleName": requestRoles[index].roleName,
      "UserName": requestRoles[index].user.username,
      "Response": response.toUpperCase(),
    };
    client.postWithTokenJSON(
        json, DotEnv().env['POST_ROLE_CONFIRMATION'], context);
    requestRoles.removeAt(index);
    setState(() {});
  }

  Widget _buildList(BuildContext context, int index) {
    return InkWell(
      child: Container(
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          margin: EdgeInsets.only(left: 8, right: 8, top: 8, bottom: 4),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 5, bottom: 5),
              ),
              ListTile(
                leading: Icon(Icons.sentiment_very_satisfied),
                title: Text(requestRoles[index].user.username),
              ),
              ListTile(
                leading: Icon(Icons.directions_walk),
                title: Text(requestRoles[index].roleName),
              ),
              requestRoles[index].status != 2
                  ? Padding(
                      padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          FlatButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30.0)),
                            color: Colors.green,
                            child: Row(
                              children: <Widget>[
                                Icon(
                                  Icons.check_circle,
                                  color: Colors.white,
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: 8),
                                ),
                                Text(
                                  "Approve",
                                  style: TextStyle(color: Colors.white),
                                )
                              ],
                            ),
                            onPressed: () {
                              sendConfirm("APPROVE", index);
                            },
                          ),
                          FlatButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30.0)),
                            color: Colors.red,
                            child: Row(
                              children: <Widget>[
                                Icon(
                                  Icons.cancel,
                                  color: Colors.white,
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: 8),
                                ),
                                Text(
                                  "Reject",
                                  style: TextStyle(color: Colors.white),
                                )
                              ],
                            ),
                            onPressed: () {
                              sendConfirm("REJECT", index);
                            },
                          ),
                        ],
                      ),
                    )
                  : Container()
            ],
          ),
        ),
        decoration: BoxDecoration(boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(.5),
            blurRadius: 20.0, // soften the shadow
            spreadRadius: 0.0, //extend the shadow
            offset: Offset(
              5.0, // Move to right 10  horizontally
              5.0, // Move to bottom 10 Vertically
            ),
          ),
        ]),
      ),
      onTap: () {
        Navigator.pushNamed(context, '/user info',
            arguments: requestRoles[index].user.username);
      },
    );
  }

  Widget _buildSearchList(BuildContext context, int index) {
    if (_searchText.isNotEmpty) {
      String name = requestRoles[index].user.username;
      if (name.toLowerCase().contains(_searchText.toLowerCase())) {
        return InkWell(
          child: Container(
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              margin: EdgeInsets.only(left: 8, right: 8, top: 8, bottom: 4),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(top: 5, bottom: 5),
                  ),
                  ListTile(
                    leading: Icon(Icons.sentiment_very_satisfied),
                    title: Text(requestRoles[index].user.username),
                  ),
                  ListTile(
                    leading: Icon(Icons.directions_walk),
                    title: Text(requestRoles[index].roleName),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        FlatButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30.0)),
                          color: Colors.green,
                          child: Row(
                            children: <Widget>[
                              Icon(
                                Icons.check_circle,
                                color: Colors.white,
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: 8),
                              ),
                              Text(
                                "Approve",
                                style: TextStyle(color: Colors.white),
                              )
                            ],
                          ),
                          onPressed: () {
                            sendConfirm("APPROVE", index);
                          },
                        ),
                        FlatButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30.0)),
                          color: Colors.red,
                          child: Row(
                            children: <Widget>[
                              Icon(
                                Icons.cancel,
                                color: Colors.white,
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: 8),
                              ),
                              Text(
                                "Reject",
                                style: TextStyle(color: Colors.white),
                              )
                            ],
                          ),
                          onPressed: () {
                            sendConfirm("REJECT", index);
                          },
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
            decoration: BoxDecoration(boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(.5),
                blurRadius: 20.0, // soften the shadow
                spreadRadius: 0.0, //extend the shadow
                offset: Offset(
                  5.0, // Move to right 10  horizontally
                  5.0, // Move to bottom 10 Vertically
                ),
              ),
            ]),
          ),
          onTap: () {
            Navigator.pushNamed(context, '/user info',
                arguments: requestRoles[index].user.username);
          },
        );
      } else
        return Container(
          height: 0,
        );
    } else
      return InkWell(
        child: Container(
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            margin: EdgeInsets.only(left: 8, right: 8, top: 8, bottom: 4),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top: 5, bottom: 5),
                ),
                ListTile(
                  leading: Icon(Icons.sentiment_very_satisfied),
                  title: Text(requestRoles[index].user.username),
                ),
                ListTile(
                  leading: Icon(Icons.directions_walk),
                  title: Text(requestRoles[index].roleName),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      FlatButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0)),
                        color: Colors.green,
                        child: Row(
                          children: <Widget>[
                            Icon(
                              Icons.check_circle,
                              color: Colors.white,
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: 8),
                            ),
                            Text(
                              "Approve",
                              style: TextStyle(color: Colors.white),
                            )
                          ],
                        ),
                        onPressed: () {
                          sendConfirm("APPROVE", index);
                        },
                      ),
                      FlatButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0)),
                        color: Colors.red,
                        child: Row(
                          children: <Widget>[
                            Icon(
                              Icons.cancel,
                              color: Colors.white,
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: 8),
                            ),
                            Text(
                              "Reject",
                              style: TextStyle(color: Colors.white),
                            )
                          ],
                        ),
                        onPressed: () {
                          sendConfirm("REJECT", index);
                        },
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(.5),
              blurRadius: 20.0, // soften the shadow
              spreadRadius: 0.0, //extend the shadow
              offset: Offset(
                5.0, // Move to right 10  horizontally
                5.0, // Move to bottom 10 Vertically
              ),
            ),
          ]),
        ),
        onTap: () {
          Navigator.pushNamed(context, '/user info',
              arguments: requestRoles[index].user.username);
        },
      );
  }

  void _handleSearchStart() {
    setState(() {
      _isSearching = true;
    });
  }

  void _handleSearchEnd() {
    setState(() {
      this.actionIcon = new Icon(
        Icons.search,
      );
      this.appBarTitle = new Text(
        "Search Sample",
      );
      _isSearching = false;
      _searchQuery.clear();
    });
  }
}

class RequestRole {
  User user;
  String roleName;
  int status;

  RequestRole({this.user, this.roleName, this.status});

  factory RequestRole.fromJson(Map<String, dynamic> json) {
    return RequestRole(
        user: User.fromJson(json['user']),
        roleName: json['role']['name'] as String,
        status: json['status'] as int);
  }
}
