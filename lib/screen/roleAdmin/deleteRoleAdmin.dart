import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:medcare/components/customButton.dart';
import 'package:medcare/components/customTextField.dart';
import 'package:medcare/networking/httpClient.dart' as client;

class DeleteRoleAdmin extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _DeleteRoleAdminState();
  }
}

class _DeleteRoleAdminState extends State<DeleteRoleAdmin> {
  final usernameCon = TextEditingController();
  final roleNameCon = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Delete Role"),
      ),
      body: Form(
        key: _formKey,
        child: ListView(
          children: <Widget>[
            SizedBox(
              height: 200,
            ),
            CustomTextField(
              hintText: "Username",
              controller: usernameCon,
              validationFun: (value) {
                if (value.isEmpty) {
                  return 'can\'t be empty';
                } else
                  return null;
              },
            ),
            CustomTextField(
              validationFun: (value) {
                if (value.isEmpty) {
                  return 'can\'t be empty';
                } else
                  return null;
              },
              hintText: "Role name",
              controller: roleNameCon,
            ),
            SizedBox(
              height: 50,
            ),
            CustomButton(
              bName: "Delete",
              bColor: Colors.redAccent,
              bTextColor: Colors.white,
              bFunc: () async {
                if (_formKey.currentState.validate()) {
                  String url = DotEnv().env['DELETE_ROLE_ADMIN'];
                  url = url.replaceAll("USERNAME", usernameCon.text);
                  url = url.replaceAll(
                      "ROLENAME", roleNameCon.text.toUpperCase());
                  return client.deleteObject(url, context, "/homePage");
                }
              },
            )
          ],
        ),
      ),
    );
  }
}
