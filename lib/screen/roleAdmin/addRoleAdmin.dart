import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:medcare/components/customButton.dart';
import 'package:medcare/components/customTextField.dart';
import 'package:medcare/networking/httpClient.dart' as client;

class AddRoleAdmin extends StatefulWidget {
  @override
  State createState() {
    return _AddRoleAdminState();
  }
}

class _AddRoleAdminState extends State<AddRoleAdmin> {
  //final roleName = TextEditingController();
  final title = TextEditingController();
  final username = TextEditingController();
  final email = TextEditingController();
  final addressCode = TextEditingController();
  final phoneNumber = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  String roleName;
  List<String> _roles = []; // Option 2

  @override
  void initState() {
    load();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add Role"),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.only(top: 40),
        child: Form(
          key: _formKey,
          child: Wrap(
            runSpacing: 25,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(30.0, 10.0, 40.0, 10.0),
                child: DropdownButtonFormField<String>(
                  value: roleName,
                  hint: Text('Role'),
                  icon: Icon(Icons.arrow_downward),
                  iconSize: 24,
                  style: TextStyle(color: Colors.teal),
                  onChanged: (String newValue) {
                    setState(() {
                      roleName = newValue;
                    });
                  },
                  validator: (value) => value == null ? 'field required' : null,
                  items: _roles.map((role) {
                    return DropdownMenuItem(
                      child: Text(role),
                      value: role,
                    );
                  }).toList(),
                ),
              ),
              CustomTextField(
                labelText: "Username",
                controller: username,
              ),
              CustomTextField(
                labelText: "Title",
                controller: title,
              ),
              CustomTextField(
                labelText: "Email",
                controller: email,
              ),
              CustomTextField(
                labelText: "Address Code",
                controller: addressCode,
              ),
              CustomTextField(
                labelText: "Phone Number",
                controller: phoneNumber,
              ),
              CustomButton(
                bName: "Submit",
                bFunc: () {
                  var json = {
                    "RoleName": roleName,
                    "UserName": username.text,
                    "Title": title.text,
                    "PreferredEmail": email.text,
                    "PreferredAddressCode": addressCode.text,
                    "PreferredPhoneNumber": phoneNumber.text,
                  };
                  client.postWithTokenJSON(
                      json, DotEnv().env['ADD_ROLE_ADMIN'], context);

                  Navigator.popUntil(context, ModalRoute.withName('/homePage'));
                },
                bTextColor: Colors.white,
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<String> load() async {
    var listResponse =
    await client.getPublicData(DotEnv().env['GET_ROLES_SIGNUP'], {}, context);

    final parsed = jsonDecode(listResponse.body);
    _roles =
        (parsed["data"] as List).map<String>((json) => json['name']).toList();
    setState(() {
      _roles = List.from(_roles);
      if (_roles.isEmpty) {
        _roles.add('Empty');
      }
    });
  }
}
