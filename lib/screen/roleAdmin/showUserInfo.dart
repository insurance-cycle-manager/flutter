import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:medcare/components/customEditButton.dart';
import 'package:medcare/components/customEditIcon.dart';
import 'package:medcare/components/customeTFEdit.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/auth/user.dart';
import 'package:medcare/screen/unauthorized.dart';

class UseInfoPage extends StatefulWidget {
  @override
  _UseInfoState createState() => _UseInfoState();
}

class _UseInfoState extends State<UseInfoPage>
    with SingleTickerProviderStateMixin {
  bool _status = true;
  TextEditingController nameCont = TextEditingController();
  TextEditingController emailCont = TextEditingController();

  User user;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    String name = ModalRoute.of(context).settings.arguments;
    return Scaffold(
        appBar: AppBar(
          title: Text("User Info"),
        ),
        body: FutureBuilder<String>(
          future: loadUserInfo(name),
          builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              return Container(
                color: Colors.white,
                child: ListView(
                  children: <Widget>[
                    new Container(
                      height: 250.0,
                      color: Colors.white,
                      child: Padding(
                        padding: EdgeInsets.only(top: 45.0),
                        child:
                            new Stack(fit: StackFit.loose, children: <Widget>[
                          new Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              new Container(
                                  width: 140.0,
                                  height: 140.0,
                                  decoration: new BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: new DecorationImage(
                                      image: new ExactAssetImage(
                                          'assets/images/male.png'),
                                      fit: BoxFit.cover,
                                    ),
                                  )),
                            ],
                          ),
                          Padding(
                              padding: EdgeInsets.only(top: 90.0, right: 100.0),
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  new CircleAvatar(
                                    backgroundColor: Colors.teal,
                                    radius: 25.0,
                                    child: new Icon(
                                      Icons.camera_alt,
                                      color: Colors.white,
                                    ),
                                  )
                                ],
                              )),
                        ]),
                      ),
                    ),
                    new Container(
                      color: Color(0xffFFFFFF),
                      child: Padding(
                        padding: EdgeInsets.only(bottom: 25.0),
                        child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                                padding: EdgeInsets.only(
                                    left: 25.0, right: 25.0, top: 25.0),
                                child: new Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    new Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        new Text(
                                          'Edit',
                                          style: TextStyle(
                                              fontSize: 18.0,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                    new Column(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        _status
                                            ? CustomEditIcon(
                                                onClick: () {
                                                  setState(() {
                                                    _status = false;
                                                  });
                                                },
                                              )
                                            : new Container(),
                                      ],
                                    )
                                  ],
                                )),
                            CustomTFEdit(
                              label: "Name",
                              textCont: nameCont,
                            ),
                            CustomTFEdit(
                              label: "Email",
                              textCont: emailCont,
                              status: _status,
                            ),
                            !_status
                                ? CustomEditButton(
                                    onSave: () {
                                      setState(() {
                                        _status = true;
                                        FocusScope.of(context)
                                            .requestFocus(FocusNode());
                                      });
                                    },
                                    onCancel: () {
                                      setState(() {
                                        _status = true;
                                        FocusScope.of(context)
                                            .requestFocus(FocusNode());
                                      });
                                    },
                                  )
                                : new Container(),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              );
            } else
              return SpinKitCircle(
                //https://github.com/jogboms/flutter_spinkit#-showcase
                color: Colors.teal,
              );
          },
        ));
  }

  Future<String> loadUserInfo(String name) async {
    try {
      String url = DotEnv().env['GET_ACC_INFO'];
      var response = await client.getPrivateData(url, context);

      final parsed = await jsonDecode(response.body);

      user = User.fromJson(parsed['data']);
      nameCont.text = user.username;
      emailCont.text = user.email;
      return response;
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
      return "";
    }
  }
}
