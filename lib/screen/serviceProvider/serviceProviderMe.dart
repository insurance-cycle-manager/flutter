import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:medcare/components/customEditButton.dart';
import 'package:medcare/components/customEditIcon.dart';
import 'package:medcare/components/customeTFEdit.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/addressAdmin/address.dart';
import 'package:medcare/screen/providersStaffers/providersStaffers.dart';
import 'package:medcare/screen/serviceProvider/changeStatusSP.dart';
import 'package:medcare/screen/serviceProvider/inviteTPA.dart';
import 'package:medcare/screen/serviceProvider/serviceProvider.dart';
import 'package:medcare/screen/tpa/serviceProviderConnection.dart';
import 'package:medcare/screen/unauthorized.dart';

class ServiceProviderMe extends StatefulWidget {
  final String SPtype;

  const ServiceProviderMe({Key key, this.SPtype}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _ServiceProviderMeState();
  }
}

class _ServiceProviderMeState extends State<ServiceProviderMe> {
  ServiceProvider sp;
  String title;
  bool _status = true;
  List<Address> addresses = [];

  String addressTitle;
  String addressCode;

  TextEditingController typeCont = TextEditingController();
  TextEditingController emailCont = TextEditingController();
  TextEditingController titleCont = TextEditingController();
  TextEditingController nameCont = TextEditingController();
  TextEditingController stateCont = TextEditingController();
  TextEditingController codeCont = TextEditingController();
  TextEditingController joinedATCont = TextEditingController();
  TextEditingController addressCont = TextEditingController();

  @override
  void initState() {
    updateAndGetList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    String imageAsset;
    switch (widget.SPtype.toLowerCase()) {
      case 'hospital':
        {
          imageAsset = 'assets/images/hospital.jpg';
        }
        break;
      case 'clinic':
        {
          imageAsset = 'assets/images/clinic.jpg';
        }
        break;
      case 'pharmacy':
        {
          imageAsset = 'assets/images/ph.jpg';
        }
        break;
      case 'laboratory':
        {
          imageAsset = 'assets/images/lab.jpg';
        }
        break;
      case 'radiograph':
        {
          imageAsset = 'assets/images/rad.jfif';
        }
        break;
    }
    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              expandedHeight: 200.0,
              floating: false,
              pinned: true,
              flexibleSpace: FlexibleSpaceBar(
                centerTitle: true,
                background: Image.asset(imageAsset),
                //   Image.network("https://images.pexels.com/photos/396547/pexels-photo-396547.jpeg?auto=compress&cs=tinysrgb&h=350",
                //   fit: BoxFit.cover,)
              ),
              actions: <Widget>[
                PopupMenuButton<String>(
                  onSelected: handleClick,
                  itemBuilder: (BuildContext context) {
                    List<String> s = [
                      'Invite TPA',
                      'Change status',
                      'Staffers'
                    ];
                    return s.map((String choice) {
                      return PopupMenuItem<String>(
                        value: choice,
                        child: Text(choice),
                      );
                    }).toList();
                  },
                )
              ],
            ),
          ];
        },
        body: FutureBuilder<void>(
          future: updateAndGetList(),
          builder: (BuildContext context, AsyncSnapshot<void> snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              addressCont.text = sp.address == null ? "" : sp.address.title;
              nameCont.text = sp.user.username;
              titleCont.text = sp.title;
              emailCont.text = sp.user.email;
              typeCont.text = sp.type;
              joinedATCont.text = sp.joinedAt;
              return Container(
                color: Color(0xffFFFFFF),
                child: Padding(
                  padding: EdgeInsets.only(bottom: 25.0),
                  child: ListView(
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.only(
                              left: 25.0, right: 25.0, top: 25.0),
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              new Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  new Text(
                                    'Edit',
                                    style: TextStyle(
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                              new Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  _status
                                      ? CustomEditIcon(
                                          onClick: () {
                                            setState(() {
                                              _status = false;
                                            });
                                          },
                                        )
                                      : new Container(),
                                ],
                              )
                            ],
                          )),
                      CustomTFEdit(
                        label: "Name",
                        textCont: nameCont,
                      ),
                      CustomTFEdit(
                        label: "Title",
                        status: _status,
                        textCont: titleCont,
                      ),
                      CustomTFEdit(
                        label: "Type",
                        textCont: typeCont,
                      ),
                      CustomTFEdit(
                        label: "Email",
                        status: _status,
                        textCont: emailCont,
                      ),
                      CustomTFEdit(
                        label: "Joined at",
                        textCont: joinedATCont,
                      ),
                      _status
                          ? CustomTFEdit(
                              label: "Address",
                              textCont: addressCont,
                            )
                          : Padding(
                              padding:
                                  EdgeInsets.fromLTRB(30.0, 10.0, 20.0, 10.0),
                              child: DropdownButtonFormField<String>(
                                value: addressTitle,
                                isExpanded: true,
                                hint: Text(
                                  'Address',
                                  overflow: TextOverflow.ellipsis,
                                ),
                                icon: Icon(Icons.arrow_downward),
                                iconSize: 24,
                                style: TextStyle(color: Colors.teal),
                                onChanged: (String newValue) {
                                  setState(() {
                                    addressTitle = newValue;
                                    addresses.forEach((e) {
                                      if (e.title == newValue) {
                                        addressCode = e.addressCode;
                                      }
                                    });
                                  });
                                },
                                validator: (value) =>
                                    value == null ? 'field required' : null,
                                items: addresses.map((address) {
                                  return DropdownMenuItem(
                                    child: Text(
                                      address.title,
                                      overflow: TextOverflow.fade,
                                    ),
                                    value: address.title,
                                  );
                                }).toList(),
                              ),
                            ),
                      !_status
                          ? CustomEditButton(
                              onSave: () {
                                setState(() {
                                  String url = DotEnv().env['EDIT_IC'];

                                  _status = true;
                                  FocusScope.of(context)
                                      .requestFocus(new FocusNode());
                                });
                              },
                              onCancel: () {
                                setState(() {
                                  _status = true;
                                  FocusScope.of(context)
                                      .requestFocus(new FocusNode());
                                });
                              },
                            )
                          : new Container(),
                    ],
                  ),
                ),
              );
            } else
              return SpinKitCircle(
                //https://github.com/jogboms/flutter_spinkit#-showcase
                color: Colors.teal,
              );
          },
        ),
      ),
    );
  }

  void handleClick(String value) async {
    switch (value) {
      case 'Invite TPA':
        {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => InviteTPA(
                  sp: sp,
                ),
              ));
        }
        break;
      case 'Change status':
        {
          try {
            String url = DotEnv().env['Get_Connection_for_SP'];
            url = url.replaceAll("SpType", widget.SPtype);
            var response = await client.getPrivateData(url, context);

            final parsed = await jsonDecode(response.body);

            sp.serviceProvidersConnections = (parsed["data"] as List)
                .map<ServiceProviderConnection>(
                    (json) => ServiceProviderConnection.fromJson(json))
                .toList();
          } on AbacException {
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => Unauthorized()));
            return;
          }
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => ChangeStatusSp(
                  sp: sp,
                ),
              ));
        }
        break;
      case 'Staffers':
        {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => ProvidersStaffers(
                        SPtype: sp.type,
                        SPcode: sp.serviceProviderCode,
                      )));
        }
        break;
    }
  }

  Future<void> updateAndGetList() async {
    try {
      String url = DotEnv().env['GET_USER_SERVICE_PROVIDERS_DETAILS'];
      url = url.replaceAll("TYPE", widget.SPtype);
      var response = await client.getPrivateData(url, context);

      final parsed = await jsonDecode(response.body);

      sp = ServiceProvider.fromJson(parsed['data']);
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
      return;
    }
  }
}
