import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:medcare/components/customButton.dart';
import 'package:medcare/components/customTextField.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/serviceProvider/serviceProvider.dart';

class InviteTPA extends StatefulWidget {
  final tPACode = TextEditingController();
  final iCCode = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final ServiceProvider sp;

  InviteTPA({Key key, this.sp}) : super(key: key);

  @override
  State createState() {
    return _InviteTPAState();
  }
}

class _InviteTPAState extends State<InviteTPA> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Invite TPA"),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.only(top: 40),
        child: Form(
          key: widget._formKey,
          child: Wrap(
            runSpacing: 25,
            children: <Widget>[
              CustomTextField(
                labelText: "TPA code",
                controller: widget.tPACode,
              ),
              CustomTextField(
                labelText: "IC code",
                controller: widget.iCCode,
              ),
              CustomButton(
                bName: "Submit",
                bFunc: () {
                  var json = {
                    "TpaCode": widget.tPACode.text,
                    "InsuranceCompanies": [widget.iCCode.text],
                    "ServiceProviderCode": widget.sp.serviceProviderCode
                  };
                  client.postWithTokenJSON(
                      json,
                      DotEnv()
                          .env['Invite_TPA']
                          .replaceAll("SPTYPE", widget.sp.type), context);
                  Navigator.pop(context);
                },
                bTextColor: Colors.white,
              )
            ],
          ),
        ),
      ),
    );
  }
}
