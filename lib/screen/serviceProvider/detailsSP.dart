import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl/intl.dart';
import 'package:medcare/components/customEditButton.dart';
import 'package:medcare/components/customEditIcon.dart';
import 'package:medcare/components/customeTFEdit.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/addressAdmin/address.dart';
import 'package:medcare/screen/providersStaffers/providersStaffers.dart';
import 'package:medcare/screen/serviceProvider/serviceProvider.dart';
import 'package:medcare/screen/unauthorized.dart';

class DetailsSP extends StatefulWidget {
  @override
  State createState() {
    return _DetailsSPState();
  }
}

class _DetailsSPState extends State<DetailsSP> {
  bool _status = true;
  ServiceProvider sp;
  List<Address> addresses = [];
  String addressTitle;
  String addressCode;

  TextEditingController typeCont = TextEditingController();
  TextEditingController emailCont = TextEditingController();
  TextEditingController titleCont = TextEditingController();
  TextEditingController nameCont = TextEditingController();
  TextEditingController stateCont = TextEditingController();
  TextEditingController codeCont = TextEditingController();
  TextEditingController joinedATCont = TextEditingController();
  TextEditingController addressCont = TextEditingController();

  @override
  Widget build(BuildContext context) {
    sp = ModalRoute.of(context).settings.arguments;
    typeCont.text = sp.type;
    joinedATCont.text = DateFormat('d/M/y').format(DateTime.parse(sp.joinedAt));

    String imageAsset;
    switch (sp.type) {
      case 'Hospital':
        {
          imageAsset = 'assets/images/hospital.jpg';
        }
        break;
      case 'Clinic':
        {
          imageAsset = 'assets/images/clinic.jpg';
        }
        break;
      case 'Pharmacy':
        {
          imageAsset = 'assets/images/ph.jpg';
        }
        break;
      case 'Laboratory':
        {
          imageAsset = 'assets/images/lab.jpg';
        }
        break;
      case 'Radiograph':
        {
          imageAsset = 'assets/images/rad.jpg';
        }
        break;
    }

    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              expandedHeight: 200.0,
              floating: false,
              pinned: true,
              flexibleSpace: FlexibleSpaceBar(
                centerTitle: true,
                background: Image.asset(
                  imageAsset,
                  fit: BoxFit.cover,
                ),
                //   Image.network("https://images.pexels.com/photos/396547/pexels-photo-396547.jpeg?auto=compress&cs=tinysrgb&h=350",
                //   fit: BoxFit.cover,)
              ),
              actions: <Widget>[
                PopupMenuButton<String>(
                  onSelected: handleClick,
                  itemBuilder: (BuildContext context) {
                    List<String> s = ['Staffers'];
                    return s.map((String choice) {
                      return PopupMenuItem<String>(
                        value: choice,
                        child: Text(choice),
                      );
                    }).toList();
                  },
                )
              ],
            ),
          ];
        },
        body: FutureBuilder<String>(
          future: loadsPs(),
          builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              addressCont.text = sp.address == null ? "" : sp.address.title;
              nameCont.text = sp.user.username;
              titleCont.text = sp.title;
              emailCont.text = sp.user.email;
              codeCont.text = sp.serviceProviderCode;
              return Container(
                color: Color(0xffFFFFFF),
                child: Padding(
                  padding: EdgeInsets.only(bottom: 25.0),
                  child: ListView(
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.only(
                              left: 25.0, right: 25.0, top: 25.0),
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              new Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  new Text(
                                    'Edit',
                                    style: TextStyle(
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                              new Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  _status
                                      ? CustomEditIcon(
                                          onClick: () {
                                            setState(() {
                                              _status = false;
                                            });
                                          },
                                        )
                                      : new Container(),
                                ],
                              )
                            ],
                          )),
                      CustomTFEdit(
                        label: "Name",
                        textCont: nameCont,
                      ),
                      CustomTFEdit(
                        label: "Title",
                        status: _status,
                        textCont: titleCont,
                      ),
                      CustomTFEdit(
                        label: "Sp Code",
                        status: _status,
                        textCont: codeCont,
                      ),
                      CustomTFEdit(
                        label: "Type",
                        textCont: typeCont,
                      ),
                      CustomTFEdit(
                        label: "Email",
                        status: _status,
                        textCont: emailCont,
                      ),
                      CustomTFEdit(
                        label: "Joined at",
                        textCont: joinedATCont,
                      ),
                      _status
                          ? CustomTFEdit(
                              label: "Address",
                              textCont: addressCont,
                            )
                          : Padding(
                              padding:
                                  EdgeInsets.fromLTRB(30.0, 10.0, 20.0, 10.0),
                              child: DropdownButtonFormField<String>(
                                value: addressTitle,
                                isExpanded: true,
                                hint: Text(
                                  'Address',
                                  overflow: TextOverflow.ellipsis,
                                ),
                                icon: Icon(Icons.arrow_downward),
                                iconSize: 24,
                                style: TextStyle(color: Colors.teal),
                                onChanged: (String newValue) {
                                  setState(() {
                                    addressTitle = newValue;
                                    addresses.forEach((e) {
                                      if (e.title == newValue) {
                                        addressCode = e.addressCode;
                                      }
                                    });
                                  });
                                },
                                validator: (value) =>
                                    value == null ? 'field required' : null,
                                items: addresses.map((address) {
                                  return DropdownMenuItem(
                                    child: Text(
                                      address.title,
                                      overflow: TextOverflow.fade,
                                    ),
                                    value: address.title,
                                  );
                                }).toList(),
                              ),
                            ),
                      !_status
                          ? CustomEditButton(
                              onSave: () {
                                setState(() {
                                  String url = DotEnv().env['EDIT_IC'];

                                  _status = true;
                                  FocusScope.of(context)
                                      .requestFocus(new FocusNode());
                                });
                              },
                              onCancel: () {
                                setState(() {
                                  _status = true;
                                  FocusScope.of(context)
                                      .requestFocus(new FocusNode());
                                });
                              },
                            )
                          : new Container(),
                    ],
                  ),
                ),
              );
            } else
              return SpinKitCircle(
                //https://github.com/jogboms/flutter_spinkit#-showcase
                color: Colors.teal,
              );
          },
        ),
      ),
    );
  }

  void handleClick(String value) async {
    switch (value) {
      case 'Staffers':
        {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => ProvidersStaffers(
                        SPtype: sp.type,
                        SPcode: sp.serviceProviderCode,
                      )));
        }
        break;
    }
  }

  Future<String> loadsPs() async {
    String url = DotEnv().env['GET_SERVICE_PROVIDER'];
    url = url.replaceAll("TYPE", sp.type);
    //TODO set a correct title
    url = url.replaceAll("TITLE", sp.title);
    try {
      var response = await client.getPrivateData(url, context);

      final parsed = await jsonDecode(response.body);

      sp = ServiceProvider.fromJson(parsed["data"]);

      /// load addresses
      response =
      await client.getPrivateData(DotEnv().env['GET_USER_Addresses'], context);

      final parsed2 = await jsonDecode(response.body);
      List<Address> _userAddresses = [];

      _userAddresses = (parsed2["data"] as List)
          .map<Address>((json) => Address.fromJson(json['address']))
          .toList();

      // load all addresses
      response =
      await client.getPrivateData(DotEnv().env['GET_Addresses'], context);

      final parsed1 = await jsonDecode(response.body);
      addresses = (parsed1["data"] as List)
          .map<Address>((json) => Address.fromJson(json))
          .toList();

      _userAddresses.forEach((element) {
        addresses.removeWhere((e) => e.title == element.title);
      });
      setState(() {});
      return response;
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
      return "";
    }
  }
}
