import 'package:medcare/screen/addressAdmin/address.dart';
import 'package:medcare/screen/auth/user.dart';
import 'package:medcare/screen/tpa/serviceProviderConnection.dart';

class ServiceProvider {
  String title;
  String type;
  String joinedAt;
  String serviceProviderCode;
  Address address;
  User user;
  List<ServiceProviderConnection> serviceProvidersConnections;

  ServiceProvider(
      {this.title,
      this.type,
      this.joinedAt,
      this.address,
      this.user,
      this.serviceProviderCode,
      this.serviceProvidersConnections});

  factory ServiceProvider.fromJson(Map<String, dynamic> json) {
    List<ServiceProviderConnection> _serviceProvidersConnections = [];

    User user;
    Address address;
    if (json['user'] != null) {
      user = User.fromJson(json['user']);
    }
    if (json['address'] != null) {
      address = Address.fromJson(json['address']);
    }
    if (json['serviceProvidersConnections'] != null) {
      var spConnectionObjJson = json['serviceProvidersConnections'] as List;
      _serviceProvidersConnections = spConnectionObjJson
          .map((spConnectionJson) =>
          ServiceProviderConnection.fromJson(spConnectionJson))
          .toList();
    }
    return ServiceProvider(
        title: json["title"],
        type: json["type"],
        joinedAt: json["joinedAt"],
        serviceProviderCode: json['serviceProviderCode'],
        address: address,
        user: user,
        serviceProvidersConnections: _serviceProvidersConnections);
  }

  Map<String, dynamic> toJson() => {};
}
