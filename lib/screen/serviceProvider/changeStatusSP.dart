import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:intl/intl.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/serviceProvider/serviceProvider.dart';

class ChangeStatusSp extends StatefulWidget {
  final ServiceProvider sp;

  const ChangeStatusSp({Key key, this.sp}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _ChangeStatusSpState();
  }
}

class _ChangeStatusSpState extends State<ChangeStatusSp> {
  final key = new GlobalKey<ScaffoldState>();

  List<String> status = [
    "Pending1",
    "Pending2",
    "Approved",
    "Rejected",
    "TpaDeactivated",
    "SpDeactivated"
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: key,
        appBar: AppBar(title: Text("Change Status")),
        body: ListView.builder(
            itemCount: widget.sp.serviceProvidersConnections.length,
            itemBuilder: _buildList));
  }

  void sendConfirm(String response, int index) async {
    var json = {
      "status": status.indexOf(response),
    };
    client.editObject(
        DotEnv().env['Change_Status_SP'].replaceAll("connectionCode",
            widget.sp.serviceProvidersConnections[index].connectionCode),
        json, context);
    setState(() {
      widget.sp.serviceProvidersConnections[index].status =
          status.indexOf(response);
    });
  }

  Widget _buildList(BuildContext context, int index) {
    return Container(
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        margin: EdgeInsets.only(left: 8, right: 8, top: 8, bottom: 4),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 5, bottom: 5),
            ),
            ListTile(
              leading: Icon(Icons.code),
              title: Text(
                  widget.sp.serviceProvidersConnections[index].connectionCode),
            ),
            ListTile(
              leading: Icon(Icons.date_range),
              title: Text(DateFormat('d/M/y').format(DateTime.parse(
                  widget.sp.serviceProvidersConnections[index].createdAt))),
            ),
            buildButtons(index)
          ],
        ),
      ),
      decoration: BoxDecoration(boxShadow: [
        BoxShadow(
          color: Colors.grey.withOpacity(.5),
          blurRadius: 20.0, // soften the shadow
          spreadRadius: 0.0, //extend the shadow
          offset: Offset(
            5.0, // Move to right 10  horizontally
            5.0, // Move to bottom 10 Vertically
          ),
        ),
      ]),
    );
  }

  Widget buildButtons(int index) {
    switch (widget.sp.serviceProvidersConnections[index].status) {
      case 0:
        {
          return Padding(
            padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                FlatButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0)),
                  color: Colors.green,
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.check_circle,
                        color: Colors.white,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 8),
                      ),
                      Text(
                        "Approve",
                        style: TextStyle(color: Colors.white),
                      )
                    ],
                  ),
                  onPressed: () {
                    sendConfirm("Approved", index);
                  },
                ),
                FlatButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0)),
                  color: Colors.red,
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.cancel,
                        color: Colors.white,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 8),
                      ),
                      Text(
                        "Reject",
                        style: TextStyle(color: Colors.white),
                      )
                    ],
                  ),
                  onPressed: () {
                    sendConfirm("Rejected", index);
                  },
                ),
              ],
            ),
          );
        }
        break;
      case 1:
        {
          return Padding(
            padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                FlatButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0)),
                  color: Colors.green,
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.check_circle,
                        color: Colors.white,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 8),
                      ),
                      Text(
                        "Approve",
                        style: TextStyle(color: Colors.white),
                      )
                    ],
                  ),
                  onPressed: () {
                    sendConfirm("Approved", index);
                  },
                ),
                FlatButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0)),
                  color: Colors.red,
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.cancel,
                        color: Colors.white,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 8),
                      ),
                      Text(
                        "Reject",
                        style: TextStyle(color: Colors.white),
                      )
                    ],
                  ),
                  onPressed: () {
                    sendConfirm("Rejected", index);
                  },
                ),
              ],
            ),
          );
        }
        break;
      case 2:
        {
          return Padding(
            padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                FlatButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0)),
                  color: Colors.red,
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.cancel,
                        color: Colors.white,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 8),
                      ),
                      Text(
                        "Tpa Deactivated",
                        style: TextStyle(color: Colors.white),
                      )
                    ],
                  ),
                  onPressed: () {
                    sendConfirm("TpaDeactivated", index);
                  },
                ),
              ],
            ),
          );
        }
        break;
      case 3:
        {
          return Container();
        }
        break;
      case 4:
        {
          return Padding(
            padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                FlatButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0)),
                  color: Colors.green,
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.check_circle,
                        color: Colors.white,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 8),
                      ),
                      Text(
                        "Approve",
                        style: TextStyle(color: Colors.white),
                      )
                    ],
                  ),
                  onPressed: () {
                    sendConfirm("Approved", index);
                  },
                ),
              ],
            ),
          );
        }
        break;
      case 5:
        {
          return Padding(
            padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                FlatButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0)),
                  color: Colors.green,
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.check_circle,
                        color: Colors.white,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 8),
                      ),
                      Text(
                        "Approve",
                        style: TextStyle(color: Colors.white),
                      )
                    ],
                  ),
                  onPressed: () {
                    sendConfirm("Approved", index);
                  },
                ),
              ],
            ),
          );
        }
        break;
    }
    return Container();
  }
}
