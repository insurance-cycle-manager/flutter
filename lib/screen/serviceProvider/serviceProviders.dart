import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:medcare/components/customAppBar.dart';
import 'package:medcare/components/customCard.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/auth/user.dart';
import 'package:medcare/screen/profile/role/Role.dart';
import 'package:medcare/screen/serviceProvider/serviceProvider.dart';
import 'package:medcare/screen/serviceProvider/serviceProviderMe.dart';
import 'package:medcare/screen/unauthorized.dart';

class ServiceProviders extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ServiceProviderState();
  }
}

class _ServiceProviderState extends State<ServiceProviders> {
  User user;
  bool isSpRole;
  Role activeRole;
  final TextEditingController _searchQuery = new TextEditingController();
  bool _isSearching;
  String _searchText = "";
  List<ServiceProvider> sPs = [];
  String type;
  String title;
  Future<List<ServiceProvider>> _listFuture;

  _ServiceProviderState() {
    _searchQuery.addListener(() {
      if (_searchQuery.text.isEmpty) {
        setState(() {
          _isSearching = false;
          _searchText = "";
        });
      } else {
        setState(() {
          _isSearching = true;
          _searchText = _searchQuery.text;
        });
      }
    });
  }

  @override
  void initState() {
    setState(() {});
    isSpRole = false;
    _isSearching = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    type = ModalRoute.of(context).settings.arguments;
    _listFuture = updateAndGetList();
    return Scaffold(
      appBar: CustomAppBar(
        title: "Service providers",
        actionIcon: Icon(Icons.search),
        isSearching: _isSearching,
        searchText: _searchText,
        searchQuery: _searchQuery,
      ),
      body: FutureBuilder<List<ServiceProvider>>(
        future: _listFuture,
        builder: (BuildContext context,
            AsyncSnapshot<List<ServiceProvider>> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return SpinKitCircle(
              //https://github.com/jogboms/flutter_spinkit#-showcase
              color: Colors.teal,
            );
          } else if (snapshot.hasError) {
            return Scaffold(
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 250,
                    ),
                    Icon(Icons.error),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[Text("Something went wrong")],
                    ),
                    Expanded(
                      child: Text(
                        "Please check your internet connection",
                        style: TextStyle(fontSize: 15),
                      ),
                    )
                  ],
                ),
              ),
            );
          } else {
            sPs = snapshot.data ?? <ServiceProvider>[];
            return Scaffold(
              backgroundColor: Color(0xffE5E5E5),
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView.builder(
                    itemCount: sPs.length,
                    itemBuilder: _isSearching ? _buildSearchList : _buildList),
              ),
              floatingActionButton: isSpRole
                  ? FloatingActionButton(
                      child: Text("Me"),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => ServiceProviderMe(
                                SPtype: type,
                              ),
                            ));
                      },
                    )
                  : Container(),
            );
          }
        },
      ),
    );
  }

  Widget _buildList(BuildContext context, int index) {
    return CustomCard(
      name: sPs[index].title,
      dateOfBirth: sPs[index].joinedAt,
      gender: sPs[index].type,
      object: sPs[index],
      route: "detailsSP",
    );
  }

  Widget _buildSearchList(BuildContext context, int index) {
    if (_searchText.isEmpty) {
      return CustomCard(
        name: sPs[index].title,
        dateOfBirth: sPs[index].joinedAt,
        gender: sPs[index].type,
        object: sPs[index],
        route: "detailsSP",
      );
    } else {
      String name = sPs[index].title;
      if (name.toLowerCase().contains(_searchText.toLowerCase())) {
        return CustomCard(
          name: sPs[index].title,
          dateOfBirth: sPs[index].joinedAt,
          gender: sPs[index].type,
          object: sPs[index],
          route: "detailsSP",
        );
      }
    }
    return Container(
      height: 0,
    );
  }

  Future<void> refreshList() {
    // reload
    setState(() {
      _listFuture = updateAndGetList();
    });
  }

  Future<List<ServiceProvider>> updateAndGetList() async {
    activeRole = await client.getActiveRole();
    String url = DotEnv().env['GET_ACC_INFO'];

    try {
      var response1 = await client.getPrivateData(url, context);
      final parsed1 = await jsonDecode(response1.body);
      user = User.fromJson(parsed1['data']);
      user.userRoles.forEach((element) {
        _setSpRole(element);
      });

      url = DotEnv().env['GET_SERVICE_PROVIDERS'];
      url = url.replaceAll("TYPE", type);
      var response = await client.getPrivateData(url, context);

      final parsed = await jsonDecode(response.body);

      return (parsed["data"] as List)
          .map<ServiceProvider>((json) => ServiceProvider.fromJson(json))
          .toList();
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
      return [];
    }
  }

  void _setSpRole(Role element) {
    if ((activeRole.roleName == element.roleName &&
            element.roleName == "Hospital" &&
            type == "HOSPITAL") ||
        (activeRole.roleName == element.roleName &&
            element.roleName == "Pharmacy" &&
            type == "PHARMACY") ||
        (activeRole.roleName == element.roleName &&
            element.roleName == "Laboratory" &&
            type == "LABORATORY") ||
        (activeRole.roleName == element.roleName &&
            element.roleName == "Clinic" &&
            type == "RADIOGRAPHOFFICE") ||
        (activeRole.roleName == element.roleName &&
            element.roleName == "RadiographOffice" &&
            type == "CLINIC")) {
      isSpRole = true;
    }
  }
}
