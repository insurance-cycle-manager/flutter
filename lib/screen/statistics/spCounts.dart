class SPCounts {
  final String role;
  final int count;

  factory SPCounts.fromJson(Map<String, dynamic> json) {
    return SPCounts(role: json['role'], count: json['count']);
  }

  SPCounts({this.role, this.count});
}
