import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:pie_chart/pie_chart.dart';

enum LegendShape { Circle, Rectangle }

class MyPieChart extends StatefulWidget {
  final Map<String, double> dataMap;
  final String title;
  final ChartType chartType;
  final bool showChartValuesInPercentage;

  const MyPieChart(
      {Key key,
      this.dataMap,
      this.title,
      this.chartType,
      this.showChartValuesInPercentage})
      : super(key: key);

  @override
  _MyPieChartState createState() => _MyPieChartState();
}

class _MyPieChartState extends State<MyPieChart> {
  List<Color> colorList = [
    Colors.red,
    Colors.yellow,
    Colors.deepPurple,
    Colors.green,
    Colors.blue
  ];

  ChartType _chartType = ChartType.disc;
  bool _showCenterText = true;
  double _ringStrokeWidth = 49;
  double _chartLegendSpacing = 55;

  bool _showLegendsInRow = false;
  bool _showLegends = true;

  bool _showChartValueBackground = false;
  bool _showChartValues = true;
  bool _showChartValuesInPercentage = true;
  bool _showChartValuesOutside = true;

  LegendShape _legendShape = LegendShape.Circle;
  LegendPosition _legendPosition = LegendPosition.right;

  int key = 0;

  @override
  Widget build(BuildContext context) {
    final chart = PieChart(
      key: ValueKey(key),
      dataMap: widget.dataMap,
      animationDuration: Duration(milliseconds: 800),
      chartLegendSpacing: _chartLegendSpacing,
      chartRadius: MediaQuery.of(context).size.width / 3.2 > 300
          ? 300
          : MediaQuery.of(context).size.width / 3.2,
      colorList: colorList,
      initialAngleInDegree: 0,
      chartType: _chartType,
      legendOptions: LegendOptions(
        showLegendsInRow: _showLegendsInRow,
        legendPosition: _legendPosition,
        showLegends: _showLegends,
        legendShape: _legendShape == LegendShape.Circle
            ? BoxShape.circle
            : BoxShape.rectangle,
//        legendTextStyle:
      ),
      chartValuesOptions: ChartValuesOptions(
        showChartValueBackground: _showChartValueBackground,
        showChartValues: _showChartValues,
        showChartValuesInPercentage: widget.showChartValuesInPercentage,
        showChartValuesOutside: _showChartValuesOutside,
      ),
      ringStrokeWidth: _ringStrokeWidth,
    );
    return Container(
      decoration: BoxDecoration(boxShadow: [
        BoxShadow(
          color: Colors.grey.withOpacity(.5),
          blurRadius: 20.0, // soften the shadow
          spreadRadius: 0.0, //extend the shadow
          offset: Offset(
            5.0, // Move to right 10  horizontally
            5.0, // Move to bottom 10 Vertically
          ),
        ),
      ]),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        margin: EdgeInsets.only(left: 8, right: 8, top: 8, bottom: 4),
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 8,
            ),
            Text(
              widget.title,
              style: TextStyle(
                fontSize: 15.0,
                color: Colors.teal,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 25),
              child: LayoutBuilder(
                builder: (_, constraints) {
                  if (constraints.maxWidth >= 600) {
                    return Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Flexible(
                          flex: 2,
                          fit: FlexFit.tight,
                          child: chart,
                        ),
                      ],
                    );
                  } else {
                    return Column(
                      children: [
                        Container(
                          child: chart,
                          margin: EdgeInsets.symmetric(
                            vertical: 45,
                          ),
                        ),
                      ],
                    );
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
