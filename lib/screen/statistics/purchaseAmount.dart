class PurchaseAmount {
  double amount;
  String type;

  PurchaseAmount({this.amount, this.type});

  factory PurchaseAmount.fromJson(Map<String, dynamic> json) {
    return PurchaseAmount(amount: json['amount'], type: json['type']);
  }
}
