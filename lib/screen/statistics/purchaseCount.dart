class PurchaseCount {
  double count;
  String type;

  PurchaseCount({this.count, this.type});

  factory PurchaseCount.fromJson(Map<String, dynamic> json) {
    return PurchaseCount(type: json['type'], count: json['count'] * 1.0);
  }
}
