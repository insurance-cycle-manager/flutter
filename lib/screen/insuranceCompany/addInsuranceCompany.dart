import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:medcare/components/customButton.dart';
import 'package:medcare/components/customDatePicker.dart';
import 'package:medcare/components/customTextField.dart';
import 'package:medcare/networking/httpClient.dart' as client;

class AddIC extends StatefulWidget {
  final name = TextEditingController();
  final companyCode = TextEditingController();
  final supervisoryCommission = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  State createState() {
    return _AddICState();
  }
}

class _AddICState extends State<AddIC> {
  String _dateJoined = "Not set";
  String _dateFounding = "Not set";

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add Insurance Company'),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.only(top: 40),
        child: Form(
          key: widget._formKey,
          child: Wrap(
            runSpacing: 25,
            children: <Widget>[
              CustomTextField(
                labelText: "Name",
                validationFun: (value) {
                  if (value.isEmpty) {
                    return 'can\'t be empty';
                  } else
                    return null;
                },
                controller: widget.name,
              ),
              CustomTextField(
                labelText: "Company Code",
                validationFun: (value) {
                  if (value.isEmpty) {
                    return 'can\'t be empty';
                  } else if (value.length > 5 || value.length < 3)
                    return 'length must be between 3 and 5';
                  else
                    return null;
                },
                controller: widget.companyCode,
              ),
              CustomTextField(
                labelText: "Supervisory Commission",
                validationFun: (value) {
                  if (value.isEmpty) {
                    return 'can\'t be empty';
                  } else
                    return null;
                },
                controller: widget.supervisoryCommission,
              ),
              CustomDatePicker(
                name: "Date of Founding",
                onPressed: (date) {
                  setState(() {
                    _dateFounding = date;
                  });
                },
              ),
              CustomDatePicker(
                name: "Joined at",
                onPressed: (date) {
                  setState(() {
                    _dateJoined = date;
                  });
                },
              ),
              CustomButton(
                bName: "Submit",
                bFunc: () {
                  var json = {
                    "name": widget.name.text,
                    "dateOfFounding": _dateFounding,
                    "joinedAt": _dateJoined,
                    "supervisoryComission": widget.supervisoryCommission.text,
                    "companyCode": widget.companyCode.text
                  };
                  client.postWithTokenJSON(
                      json, DotEnv().env['ADD_IC'], context);
                  Navigator.popUntil(
                      context, ModalRoute.withName('/insurance companies'));
                  Navigator.pop(context);
                  Navigator.pushNamed(
                      context, '/insurance companies');
                  //Navigator.popUntil(context, ModalRoute.withName('/insurance companies'));
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
