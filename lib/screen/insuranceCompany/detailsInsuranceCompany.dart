import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl/intl.dart';
import 'package:medcare/components/customEditButton.dart';
import 'package:medcare/components/customEditIcon.dart';
import 'package:medcare/components/customeTFEdit.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/insuranceCompany/insuranceCompany.dart';
import 'package:medcare/screen/package/packages.dart';
import 'package:medcare/screen/profile/role/Role.dart';

class DetailsIC extends StatefulWidget {
  @override
  State createState() {
    return _DetailsICState();
  }
}

class _DetailsICState extends State<DetailsIC> {
  bool _status = true;
  final FocusNode myFocusNode = FocusNode();
  TextEditingController nameCont = TextEditingController();
  TextEditingController stateCont = TextEditingController();
  TextEditingController addressCont = TextEditingController();
  TextEditingController codeCont = TextEditingController();
  TextEditingController joinedATCont = TextEditingController();
  TextEditingController dateOfFoundingCont = TextEditingController();
  TextEditingController emailCont = TextEditingController();
  TextEditingController phoneCont = TextEditingController();
  TextEditingController supervisoryComissionCont = TextEditingController();
  InsuranceCompany ic;

  Future<void> _listFuture;
  Role activeRole;

  @override
  void initState() {
    _listFuture = updateAndGetList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ic = ModalRoute.of(context).settings.arguments;
    nameCont.text = ic.name;
    stateCont.text = ic.status;
    codeCont.text = ic.companyCode;
    joinedATCont.text = DateFormat('d/M/y').format(DateTime.parse(ic.joinedAt));
    dateOfFoundingCont.text =
        DateFormat('d/M/y').format(DateTime.parse(ic.dateOfFounding));
    addressCont.text = ic.address.title;
    emailCont.text = ic.email.email;
    phoneCont.text = ic.phone.number;
    supervisoryComissionCont.text = ic.supervisoryCommission;
    List<String> _menu = ['Packages'];

    return Scaffold(
      body: FutureBuilder<void>(
        future: _listFuture,
        builder: (BuildContext context, AsyncSnapshot<void> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return SpinKitCircle(
              //https://github.com/jogboms/flutter_spinkit#-showcase
              color: Colors.teal,
            );
          } else if (snapshot.hasError) {
            return Scaffold(
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 250,
                    ),
                    Icon(Icons.error),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[Text("Something went wrong")],
                    ),
                    Expanded(
                      child: Text(
                        "Please check your internet connection",
                        style: TextStyle(fontSize: 15),
                      ),
                    )
                  ],
                ),
              ),
            );
          } else {
            if (activeRole.roleName == "InsuranceCompany") {
              _menu.add('Delete');
            }
            return NestedScrollView(
              headerSliverBuilder:
                  (BuildContext context, bool innerBoxIsScrolled) {
                return <Widget>[
                  SliverAppBar(
                    expandedHeight: 200.0,
                    floating: false,
                    pinned: true,
                    flexibleSpace: FlexibleSpaceBar(
                        centerTitle: true,
                        title: Text(ic.name,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 16.0,
                            )),
                        background: Image.asset(
                          'assets/images/ic.png',
                          fit: BoxFit.cover,
                        )),
                    actions: <Widget>[
                      PopupMenuButton<String>(
                        onSelected: handleClick,
                        itemBuilder: (BuildContext context) {
                          List<String> s = _menu;
                          return s.map((String choice) {
                            return PopupMenuItem<String>(
                              value: choice,
                              child: Text(choice),
                            );
                          }).toList();
                        },
                      )
                    ],
                  ),
                ];
              },
              body: Container(
                color: Color(0xffFFFFFF),
                child: Padding(
                  padding: EdgeInsets.only(bottom: 25.0),
                  child: ListView(
                    children: <Widget>[
                      activeRole.roleName == "InsuranceCompany"
                          ? Padding(
                          padding: EdgeInsets.only(
                              left: 25.0, right: 25.0, top: 25.0),
                          child: new Row(
                            mainAxisAlignment:
                            MainAxisAlignment.spaceBetween,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              new Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  new Text(
                                    'Edit',
                                    style: TextStyle(
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                              new Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  _status
                                      ? CustomEditIcon(
                                    onClick: () {
                                      setState(() {
                                        _status = false;
                                      });
                                    },
                                  )
                                      : new Container(),
                                ],
                              )
                            ],
                          ))
                          : Container(),
                      CustomTFEdit(
                        label: "Name",
                        textCont: nameCont,
                      ),
                      CustomTFEdit(
                        label: "Email",
                        textCont: emailCont,
                      ),
                      CustomTFEdit(
                        label: "Phone",
                        textCont: phoneCont,
                      ),
                      CustomTFEdit(
                        label: "Company Code",
                        textCont: codeCont,
                      ),
                      CustomTFEdit(
                        label: "Date of founding",
                        textCont: dateOfFoundingCont,
                      ),
                      CustomTFEdit(
                        label: "Joined at",
                        textCont: joinedATCont,
                      ),
                      CustomTFEdit(
                        label: "State",
                        textCont: stateCont,
                      ),
                      CustomTFEdit(
                        label: "Address",
                        textCont: addressCont,
                        status: _status,
                      ),
                      CustomTFEdit(
                        label: "supervisory Commission",
                        textCont: supervisoryComissionCont,
                        status: _status,
                      ),
                      !_status
                          ? CustomEditButton(
                        onSave: () {
                          setState(() {
                            ic.name = nameCont.text;
                            ic.joinedAt = joinedATCont.text;
                            ic.dateOfFounding = dateOfFoundingCont.text;
                            ic.address.title = addressCont.text;
                            String url = DotEnv().env['EDIT_IC'];
                            url =
                                url.replaceAll("ICCODE", ic.companyCode);
                            client.editObject(url, ic.toJson(), context);
                            _status = true;
                            FocusScope.of(context)
                                .requestFocus(new FocusNode());
                          });
                        },
                        onCancel: () {
                          setState(() {
                            _status = true;
                            FocusScope.of(context)
                                .requestFocus(new FocusNode());
                          });
                        },
                      )
                          : new Container(),
                    ],
                  ),
                ),
              ),
            );
          }
        },
      ),
    );
  }

  void handleClick(String value) async {
    switch (value) {
      case 'Packages':
        {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => Packages(
                        ic: ic,
                  )));
        }
        break;
      case 'Delete':
        String url = DotEnv().env['DELETE_IC'];
        url = url.replaceAll("ICCODE", ic.companyCode);
        client.deleteObject(url, context, '/insurance companies');
        break;
    }
  }

  Future<void> refreshList() {
    // reload
    setState(() {
      _listFuture = updateAndGetList();
    });
  }

  Future<void> updateAndGetList() async {
    activeRole = await client.getActiveRole();
  }
}
