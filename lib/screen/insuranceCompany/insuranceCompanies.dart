import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:medcare/components/customAppBar.dart';
import 'package:medcare/components/customCard.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/auth/user.dart';
import 'package:medcare/screen/insuranceCompany/insuranceCompany.dart';
import 'package:medcare/screen/profile/role/Role.dart';

import '../unauthorized.dart';

class IC extends StatefulWidget {
  @override
  State createState() {
    return _ICState();
  }
}

class _ICState extends State<IC> {
  User user;
  Role activeRole;
  final TextEditingController _searchQuery = new TextEditingController();
  bool _isSearching;
  String _searchText = "";
  List<InsuranceCompany> ics = [];
  Future<List<InsuranceCompany>> _listFuture;

  _ICState() {
    _searchQuery.addListener(() {
      if (_searchQuery.text.isEmpty) {
        setState(() {
          _isSearching = false;
          _searchText = "";
        });
      } else {
        setState(() {
          _isSearching = true;
          _searchText = _searchQuery.text;
        });
      }
    });
  }

  @override
  void initState() {
    setState(() {});
    _isSearching = false;
    super.initState();
    _listFuture = updateAndGetList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        title: "Insurance Companies",
        isSearching: _isSearching,
        searchText: _searchText,
        searchQuery: _searchQuery,
      ),
      body: FutureBuilder<List<InsuranceCompany>>(
        future: _listFuture,
        builder: (BuildContext context,
            AsyncSnapshot<List<InsuranceCompany>> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return SpinKitCircle(
              color: Colors.teal,
            );
          } else if (snapshot.hasError) {
            return Scaffold(
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 250,
                    ),
                    Icon(Icons.error),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[Text("Something went wrong")],
                    ),
                    Expanded(
                      child: Text(
                        "Please check your internet connection",
                        style: TextStyle(fontSize: 15),
                      ),
                    )
                  ],
                ),
              ),
            );
          } else {
            ics = snapshot.data ?? <InsuranceCompany>[];
            return Scaffold(
              backgroundColor: Color(0xffE5E5E5),
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView.builder(
                    itemCount: ics.length,
                    itemBuilder: _isSearching ? _buildSearchList : _buildList),
              ),
              floatingActionButton: activeRole.roleName == "InsuranceCompany"
                  ? FloatingActionButton(
                      child: Text("Me"),
                      onPressed: () {
                        Navigator.pushNamed(context, '/my insurance companies');
                      },
                    )
                  : Container(),
            );
          }
        },
      ),
    );
  }

  Widget _buildList(BuildContext context, int index) {
    return CustomCard(
      name: ics[index].name,
      dateOfBirth: ics[index].joinedAt,
      gender: ics[index].status,
      route: 'detailsIC',
      object: ics[index],
    );
  }

  Widget _buildSearchList(BuildContext context, int index) {
    if (_searchText.isEmpty) {
      return CustomCard(
        name: ics[index].name,
        dateOfBirth: ics[index].joinedAt,
        gender: ics[index].status,
        route: 'detailsIC',
        object: ics[index],
      );
    } else {
      String name = ics[index].name;
      if (name.toLowerCase().contains(_searchText.toLowerCase())) {
        return CustomCard(
          name: ics[index].name,
          dateOfBirth: ics[index].joinedAt,
          gender: ics[index].status,
          route: 'detailsIC',
          object: ics[index],
        );
      }
    }
    return Container(
      height: 0,
    );
  }

  Future<void> refreshList() {
    // reload
    setState(() {
      _listFuture = updateAndGetList();
    });
    return null;
  }

  Future<List<InsuranceCompany>> updateAndGetList() async {
    activeRole = await client.getActiveRole();
    String url = DotEnv().env['GET_ACC_INFO'];
    dynamic response1;
    try {
      response1 = await client.getPrivateData(url, context);
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
      return [];
    }

    dynamic response;
    try {
      response = await client.getPrivateData(DotEnv().env['GET_ICS'], context);
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
      return [];
    }


    final parsed = await jsonDecode(response.body);

    return (parsed["data"] as List)
        .map<InsuranceCompany>((json) => InsuranceCompany.fromJson(json))
        .toList();
  }
}
