import 'package:medcare/screen/addressAdmin/address.dart';
import 'package:medcare/screen/profile/email/Email.dart';
import 'package:medcare/screen/profile/phoneNumber/number.dart';

class InsuranceCompany {
  String name;
  String dateOfFounding;
  String joinedAt;
  Address address;
  String status;
  String companyCode;
  String supervisoryCommission;
  Email email;
  Number phone;

  InsuranceCompany(
      {this.name,
      this.dateOfFounding,
      this.joinedAt,
      this.address,
      this.companyCode: "DEFU",
      this.status,
      this.supervisoryCommission,
      this.email,
      this.phone});

  factory InsuranceCompany.fromJson(Map<String, dynamic> json) {
    Address _address;
    Email _email;
    Number _phone;
    if (json['address'] != null) {
      _address = Address.fromJson(json['address']['address']);
    }
    if (json['email'] != null) {
      _email = Email.fromJson(json['email']);
    }
    if (json['phoneNumber'] != null) {
      _phone = Number.fromJson(json['phoneNumber']);
    }
    return InsuranceCompany(
        name: json['name'] as String,
        dateOfFounding: json['dateOfFounding'] as String,
        joinedAt: json['joinedAt'] as String,
        address: _address,
        status: json['status'] as String,
        email: _email,
        phone: _phone,
        companyCode: json['insuranceCompanyCode'] as String,
        supervisoryCommission: json['supervisoryComission'] as String);
  }

  Map<String, dynamic> toMap() {
    return {
      'name': name,
      'dateOfFounding': dateOfFounding,
      'joinedAt': joinedAt,
      'address': address
    };
  }

  Map<String, dynamic> toJson() => {
    'name': name,
    'dateOfFounding': dateOfFounding,
    'joinedAt': joinedAt,
    'address': address
  };
}
