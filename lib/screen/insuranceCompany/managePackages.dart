import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:medcare/components/customButton.dart';
import 'package:medcare/components/customTextField.dart';
import 'package:medcare/networking/httpClient.dart' as client;

class ManagePackages extends StatefulWidget {
  final packageCode = TextEditingController();
  final patientCode = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  State createState() {
    return _ManagePackagesState();
  }
}

class _ManagePackagesState extends State<ManagePackages> {
  String status;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Manage Package"),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.only(top: 40),
        child: Form(
          key: widget._formKey,
          child: Wrap(
            runSpacing: 25,
            children: <Widget>[
              CustomTextField(
                labelText: "Package code",
                controller: widget.packageCode,
              ),
              CustomTextField(
                labelText: "Patient code",
                controller: widget.patientCode,
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(30.0, 10.0, 20.0, 10.0),
                child: DropdownButton<String>(
                  hint: Text("Status"),
                  value: status,
                  icon: Icon(Icons.arrow_downward),
                  iconSize: 24,
                  elevation: 16,
                  style: TextStyle(color: Colors.teal),
//                  underline: Container(
//                    height: 2,
//                    color: Colors.teal,
//                  ),
                  onChanged: (String newValue) {
                    setState(() {
                      status = newValue;
                    });
                  },
                  items: <String>['Approved', 'Rejected']
                      .map<DropdownMenuItem<String>>((String value) {
                    Color color =
                        value == "Approved" ? Colors.green : Colors.red;

                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(
                        value,
                        style: TextStyle(color: color),
                      ),
                    );
                  }).toList(),
                ),
              ),
              CustomButton(
                bName: "Submit",
                bFunc: () {
                  var json = {
                    "PackageCode": widget.packageCode.text,
                    "PatientCode": widget.patientCode.text,
                    "Status": status
                  };
                  client.editObject(
                      DotEnv().env['PUT_CONFIRM_PATIENT_IC_PACKAGE'], json,
                      context);
                  Navigator.pop(context);
                },
                bTextColor: Colors.white,
              )
            ],
          ),
        ),
      ),
    );
  }
}
