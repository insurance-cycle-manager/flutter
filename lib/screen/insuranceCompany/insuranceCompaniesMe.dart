import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:medcare/components/customEditButton.dart';
import 'package:medcare/components/customEditIcon.dart';
import 'package:medcare/components/customeTFEdit.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/insuranceCompany/insuranceCompany.dart';
import 'package:medcare/screen/insuranceCompany/managePackages.dart';
import 'package:medcare/screen/package/packages.dart';
import 'package:medcare/screen/unauthorized.dart';

class MyIC extends StatefulWidget {
  @override
  State createState() {
    return _MyICState();
  }
}

class _MyICState extends State<MyIC> {
  bool _status = true;
  final FocusNode myFocusNode = FocusNode();
  TextEditingController nameCont = TextEditingController();
  TextEditingController stateCont = TextEditingController();
  TextEditingController addressCont = TextEditingController();
  TextEditingController codeCont = TextEditingController();
  TextEditingController joinedATCont = TextEditingController();
  TextEditingController dateOfFoundingCont = TextEditingController();
  TextEditingController emailCont = TextEditingController();
  TextEditingController phoneCont = TextEditingController();
  TextEditingController supervisoryComissionCont = TextEditingController();
  InsuranceCompany ic;

//  String _searchText = "";
  List<InsuranceCompany> ics = [];
  Future<void> _listFuture;

  @override
  void initState() {
    super.initState();
    _listFuture = updateAndGetList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder<void>(
        future: _listFuture,
        builder: (BuildContext context, AsyncSnapshot<void> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return SpinKitCircle(
              color: Colors.teal,
            );
          } else if (snapshot.hasError) {
            return Scaffold(
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 250,
                    ),
                    Icon(Icons.error),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[Text("Something went wrong")],
                    ),
                    Expanded(
                      child: Text(
                        "Please check your internet connection",
                        style: TextStyle(fontSize: 15),
                      ),
                    )
                  ],
                ),
              ),
            );
          } else {
            return Scaffold(
              body: NestedScrollView(
                headerSliverBuilder:
                    (BuildContext context, bool innerBoxIsScrolled) {
                  return <Widget>[
                    SliverAppBar(
                      expandedHeight: 200.0,
                      floating: false,
                      pinned: true,
                      flexibleSpace: FlexibleSpaceBar(
                          centerTitle: true,
                          title: Text(ic.name,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16.0,
                              )),
                          background: Image.network(
                            "https://images.pexels.com/photos/396547/pexels-photo-396547.jpeg?auto=compress&cs=tinysrgb&h=350",
                            fit: BoxFit.cover,
                          )),
                      actions: <Widget>[
                        PopupMenuButton<String>(
                          onSelected: handleClick,
                          itemBuilder: (BuildContext context) {
                            List<String> s = [
                              'Packages',
                              'Manage Packages',
                              'Delete'
                            ];
                            return s.map((String choice) {
                              return PopupMenuItem<String>(
                                value: choice,
                                child: Text(choice),
                              );
                            }).toList();
                          },
                        )
                      ],
                    ),
                  ];
                },
                body: Container(
                  color: Color(0xffFFFFFF),
                  child: Padding(
                    padding: EdgeInsets.only(bottom: 25.0),
                    child: ListView(
                      children: <Widget>[
                        Padding(
                            padding: EdgeInsets.only(
                                left: 25.0, right: 25.0, top: 25.0),
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                new Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    new Text(
                                      'Edit',
                                      style: TextStyle(
                                          fontSize: 18.0,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                                new Column(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    _status
                                        ? CustomEditIcon(
                                            onClick: () {
                                              setState(() {
                                                _status = false;
                                              });
                                            },
                                          )
                                        : new Container(),
                                  ],
                                )
                              ],
                            )),
                        CustomTFEdit(
                          label: "Name",
                          textCont: nameCont,
                        ),
                        CustomTFEdit(
                          label: "Email",
                          textCont: emailCont,
                        ),
                        CustomTFEdit(
                          label: "Phone",
                          textCont: phoneCont,
                        ),
                        CustomTFEdit(
                          label: "Company Code",
                          textCont: codeCont,
                        ),
                        CustomTFEdit(
                          label: "Date of founding",
                          textCont: dateOfFoundingCont,
                          status: _status,
                        ),
                        CustomTFEdit(
                          label: "Joined at",
                          textCont: joinedATCont,
                          status: _status,
                        ),
                        CustomTFEdit(
                          label: "State",
                          textCont: stateCont,
                        ),
                        CustomTFEdit(
                          label: "Address",
                          textCont: addressCont,
                          status: _status,
                        ),
                        CustomTFEdit(
                          label: "supervisory Commission",
                          textCont: supervisoryComissionCont,
                          status: _status,
                        ),
                        !_status
                            ? CustomEditButton(
                                onSave: () {
                                  setState(() {
                                    ic.name = nameCont.text;
                                    ic.joinedAt = joinedATCont.text;
                                    ic.dateOfFounding = dateOfFoundingCont.text;
                                    ic.address.title = addressCont.text;
                                    String url = DotEnv().env['EDIT_IC'];
                                    url = url.replaceAll(
                                        "ICCODE", ic.companyCode);
                                    client.editObject(
                                        url, ic.toJson(), context);
                                    _status = true;
                                    FocusScope.of(context)
                                        .requestFocus(new FocusNode());
                                  });
                                },
                                onCancel: () {
                                  setState(() {
                                    _status = true;
                                    FocusScope.of(context)
                                        .requestFocus(new FocusNode());
                                  });
                                },
                              )
                            : new Container(),
                      ],
                    ),
                  ),
                ),
              ),
            );
          }
        },
      ),
    );
  }

  /* Widget _buildList(BuildContext context, int index) {
    return CustomCard(
      name: ics[index].name,
      dateOfBirth: ics[index].dateOfFounding,
      gender: ics[index].status,
      route: 'detailsIC',
      object: ics[index],
    );
  }*/

  /*Widget _buildSearchList(BuildContext context, int index) {
    if (_searchText.isEmpty) {
      return CustomCard(
        name: ics[index].name,
        route: 'detailsIC',
        dateOfBirth: ics[index].dateOfFounding.toString(),
      );
    } else {
      String name = ics[index].name;
      if (name.toLowerCase().contains(_searchText.toLowerCase())) {
        return CustomCard(
          name: ics[index].name,
          dateOfBirth: ics[index].dateOfFounding.toString(),
          route: 'detailsIC',
        );
      }
    }
    return Container(
      height: 0,
    );
  }*/

  Future<void> refreshList() {
    // reload
    setState(() {
      _listFuture = updateAndGetList();
    });
    return null;
  }

  void handleClick(String value) async {
    switch (value) {
      case 'Packages':
        {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => Packages(
                        ic: ic,
                      )));
        }
        break;
      case 'Manage Packages':
        {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => ManagePackages(),
              ));
        }
        break;
      case 'Delete':
        String url = DotEnv().env['DELETE_IC'];
        url = url.replaceAll("ICCODE", ic.companyCode);
        client.deleteObject(url, context, '/insurance companies');
        break;
    }
  }

  Future<void> updateAndGetList() async {
    dynamic response;
    try {
      response =
      await client.getPrivateData(DotEnv().env['GET_MYICS'], context);
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
      return;
    }

    final parsed = await jsonDecode(response.body);

    //todo remove  as list
    ic = InsuranceCompany.fromJson(parsed['data']);

//        (parsed["data"] as List)
//        .map<InsuranceCompany>((json) => InsuranceCompany.fromJson(json))
//        .toList();

    nameCont.text = ic.name;
    stateCont.text = ic.status;
    codeCont.text = ic.companyCode;
    joinedATCont.text = ic.joinedAt;
    dateOfFoundingCont.text = ic.dateOfFounding;
    addressCont.text = ic.address.title;
    emailCont.text = ic.email.email;
    phoneCont.text = ic.phone.number;
    supervisoryComissionCont.text = ic.supervisoryCommission;
  }
}
