import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:medcare/components/customAppBar.dart';
import 'package:medcare/components/customHasNoData.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/template/argsTemplate.dart';
import 'package:medcare/screen/template/labTest/labTest.dart';
import 'package:medcare/screen/template/radiograph/radiograph.dart';
import 'package:medcare/screen/template/surgery/surgery.dart';
import 'package:medcare/screen/unauthorized.dart';

import 'drug/drug.dart';

class ListTemplate extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ListTemplateState();
  }
}

class _ListTemplateState extends State<ListTemplate> {
  final TextEditingController _searchQuery = new TextEditingController();
  bool _isSearching;
  String _searchText = "";
  List<Temp> temps = [];
  Future<List<Temp>> _listFuture;
  String templateType;
  bool refresh = true;
  List<String> codes = [];

  _ListTemplateState() {
    _searchQuery.addListener(() {
      if (_searchQuery.text.isEmpty) {
        setState(() {
          _isSearching = false;
          _searchText = "";
        });
      } else {
        setState(() {
          _isSearching = true;
          _searchText = _searchQuery.text;
        });
      }
    });
  }

  @override
  void initState() {
    _isSearching = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ArgsTemplate arg = ModalRoute.of(context).settings.arguments;
    templateType = arg.templateType;
    codes = arg.codes;
    if (refresh) refreshList();
    return Scaffold(
      appBar: CustomAppBar(
        title: templateType,
        actionIcon: Icon(Icons.search),
        isSearching: _isSearching,
        searchText: _searchText,
        searchQuery: _searchQuery,
      ),
      body: FutureBuilder<List<Temp>>(
        future: _listFuture,
        builder: (BuildContext context, AsyncSnapshot<List<Temp>> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return SpinKitCircle(
              color: Colors.teal,
            );
          } else if (snapshot.hasError) {
            return Scaffold(
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 250,
                    ),
                    Icon(Icons.error),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[Text("Something went wrong")],
                    ),
                    Expanded(
                      child: Text(
                        "Please check your internet connection",
                        style: TextStyle(fontSize: 15),
                      ),
                    )
                  ],
                ),
              ),
            );
          } else {
            temps = snapshot.data ?? <Temp>[];
            print(temps);
            if (temps.length > 0) {
              return Scaffold(
                backgroundColor: Color(0xffE5E5E5),
                body: RefreshIndicator(
                  onRefresh: refreshList,
                  child: ListView.builder(
                      itemCount: temps.length + 1,
                      itemBuilder:
                          _isSearching ? _buildSearchList : _buildList),
                ),
              );
            } else {
              return Scaffold(
                backgroundColor: Color(0xffE5E5E5),
                body: RefreshIndicator(
                  onRefresh: refreshList,
                  child: CustomHasNoData(),
                ),
              );
            }
          }
        },
      ),
      floatingActionButton: Padding(
        padding: const EdgeInsets.only(left: 33),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            FloatingActionButton.extended(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(16.0))),
              label: Text("Submit"),
              onPressed: () {
                Navigator.pop(context, codes);
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildList(BuildContext context, int index) {
    if (index == temps.length)
      return SizedBox(
        height: 30,
      );
    return CheckboxListTile(
      title: Text(temps[index].name),
      value: codes.contains(temps[index].name),
      onChanged: (bool value) {
        if (value)
          codes.add(temps[index].name);
        else
          codes.remove(temps[index].name);
        setState(() {
          refresh = false;
        });
      },
    );
  }

  Widget _buildSearchList(BuildContext context, int index) {
    if (index == temps.length)
      return SizedBox(
        height: 30,
      );
    if (_searchText.isEmpty) {
      return CheckboxListTile(
        title: Text(temps[index].name),
        value: codes.contains(temps[index].name),
        onChanged: (bool value) {
          if (value)
            codes.add(temps[index].name);
          else
            codes.remove(temps[index].name);
        },
      );
    } else {
      String name = temps[index].name;
      if (name.toLowerCase().contains(_searchText.toLowerCase())) {
        return CheckboxListTile(
          title: Text(temps[index].name),
          value: codes.contains(temps[index].name),
          onChanged: (bool value) {
            if (value)
              codes.add(temps[index].name);
            else
              codes.remove(temps[index].name);
          },
        );
      }
    }
    return Container(
      height: 0,
    );
  }

  Future<void> refreshList() {
    // reload
    setState(() {
      _listFuture = updateAndGetList();
    });
  }

  Future<List<Temp>> updateAndGetList() async {
    try {
      String url = DotEnv().env['GET_' + templateType];

      var response = await client.getPrivateData(url, context);

      final parsed = await jsonDecode(response.body);

      switch (templateType) {
        case 'DRUGS':
          {
            return (parsed["data"] as List)
                .map<Temp>((json) => Temp(
                    Drug.fromJson(json).name, Drug.fromJson(json).drugCode))
                .toList();
          }
          break;
        case 'SURGERIES':
          {
            return (parsed["data"] as List)
                .map<Temp>((json) => Temp(Surgery.fromJson(json).name,
                    Surgery.fromJson(json).surgeryCode))
                .toList();
          }
          break;
        case 'LABTESTS':
          {
            return (parsed["data"] as List)
                .map<Temp>((json) => Temp(LabTest.fromJson(json).name,
                    LabTest.fromJson(json).labTestCode))
                .toList();
          }
          break;
        case 'RADIOGRAPHS':
          {
            return (parsed["data"] as List)
                .map<Temp>((json) => Temp(Radiograph.fromJson(json).name,
                    Radiograph.fromJson(json).radiographCode))
                .toList();
          }
          break;
        default:
          {
            return [];
          }
      }
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
      return [];
    }
  }
}

class Temp {
  String code;
  String name;

  Temp(this.code, this.name);
}
