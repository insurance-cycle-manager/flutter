import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:medcare/components/customAppBar.dart';
import 'package:medcare/components/customCard.dart';
import 'package:medcare/components/customHasNoData.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/template/drug/drug.dart';
import 'package:medcare/screen/unauthorized.dart';

class Drugs extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _DrugsState();
  }
}

class _DrugsState extends State<Drugs> {
  final TextEditingController _searchQuery = new TextEditingController();
  bool _isSearching;
  String _searchText = "";
  List<Drug> drugs = [];
  Future<List<Drug>> _listFuture;

  _DrugsState() {
    _searchQuery.addListener(() {
      if (_searchQuery.text.isEmpty) {
        setState(() {
          _isSearching = false;
          _searchText = "";
        });
      } else {
        setState(() {
          _isSearching = true;
          _searchText = _searchQuery.text;
        });
      }
    });
  }

  @override
  void initState() {
    setState(() {});
    _listFuture = updateAndGetList();
    _isSearching = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _listFuture = updateAndGetList();
    return Scaffold(
      appBar: CustomAppBar(
        title: "Drugs",
        actionIcon: Icon(Icons.search),
        isSearching: _isSearching,
        searchText: _searchText,
        searchQuery: _searchQuery,
      ),
      body: FutureBuilder<List<Drug>>(
        future: _listFuture,
        builder: (BuildContext context, AsyncSnapshot<List<Drug>> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return SpinKitCircle(
              //https://github.com/jogboms/flutter_spinkit#-showcase
              color: Colors.teal,
            );
          } else if (snapshot.hasError) {
            return Scaffold(
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 250,
                    ),
                    Icon(Icons.error),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[Text("Something went wrong")],
                    ),
                    Expanded(
                      child: Text(
                        "Please check your internet connection",
                        style: TextStyle(fontSize: 15),
                      ),
                    )
                  ],
                ),
              ),
            );
          } else {
            drugs = snapshot.data ?? <Drug>[];
            print(drugs);
            if (drugs.length > 0) {
              return Scaffold(
                backgroundColor: Color(0xffE5E5E5),
                body: RefreshIndicator(
                  onRefresh: refreshList,
                  child: ListView.builder(
                      itemCount: drugs.length,
                      itemBuilder:
                          _isSearching ? _buildSearchList : _buildList),
                ),
                floatingActionButton: FloatingActionButton(
                  child: Icon(Icons.add),
                  onPressed: () {
                    Navigator.pushNamed(context, '/addDrug');
                  },
                ),
              );
            } else {
              return Scaffold(
                body: CustomHasNoData(),
                floatingActionButton: FloatingActionButton(
                  child: Icon(Icons.add),
                  onPressed: () {
                    Navigator.pushNamed(context, '/addDrug');
                  },
                ),
              );
            }
          }
        },
      ),
    );
  }

  Widget _buildList(BuildContext context, int index) {
    return CustomCard(
      name: drugs[index].name,
      object: drugs[index],
      route: "detailsDrug",
    );
  }

  Widget _buildSearchList(BuildContext context, int index) {
    if (_searchText.isEmpty) {
      return CustomCard(
        name: drugs[index].name,
        object: drugs[index],
        route: "detailsDrug",
      );
    } else {
      String name = drugs[index].name;
      if (name.toLowerCase().contains(_searchText.toLowerCase())) {
        return CustomCard(
          name: drugs[index].name,
          object: drugs[index],
          route: "detailsDrug",
        );
      }
    }
    return Container(
      height: 0,
    );
  }

  Future<void> refreshList() {
    // reload
    setState(() {
      _listFuture = updateAndGetList();
    });
  }

  Future<List<Drug>> updateAndGetList() async {
    try {
      String url = DotEnv().env['GET_DRUGS'];

      var response = await client.getPrivateData(url, context);

      final parsed = await jsonDecode(response.body);

      return (parsed["data"] as List)
          .map<Drug>((json) => Drug.fromJson(json))
          .toList();
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
      return [];
    }
  }
}
