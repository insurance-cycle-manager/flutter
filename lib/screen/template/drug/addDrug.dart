import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:medcare/components/customButton.dart';
import 'package:medcare/components/customTextField.dart';
import 'package:medcare/networking/httpClient.dart' as client;

class AddDrug extends StatefulWidget {
  final name = TextEditingController();
  final manufacturer = TextEditingController();
  final priceCont = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  State createState() {
    return _AddDrugState();
  }
}

class _AddDrugState extends State<AddDrug> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add Drug"),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.only(top: 40),
        child: Form(
          key: widget._formKey,
          child: Wrap(
            runSpacing: 25,
            children: <Widget>[
              CustomTextField(
                labelText: "Name",
                controller: widget.name,
              ),
              CustomTextField(
                labelText: "Manufacturer",
                controller: widget.manufacturer,
              ),
              CustomTextField(
                labelText: "Price",
                controller: widget.priceCont,
              ),
              CustomButton(
                bName: "Submit",
                bFunc: () {
                  var json = {
                    "name": widget.name.text,
                    "manufacturer": widget.manufacturer.text,
                    "price": {"value": widget.priceCont.text}
                  };
                  client.postWithTokenJSON(
                      json, DotEnv().env['POST_DRUG'], context);

                  Navigator.popUntil(context, ModalRoute.withName('/drugs'));
                  Navigator.pop(context);
                  Navigator.pushNamed(context, '/drugs');
                },
                bTextColor: Colors.white,
              )
            ],
          ),
        ),
      ),
    );
  }
}
