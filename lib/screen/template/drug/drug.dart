class Drug {
  String name;
  String manufacturer;
  String drugCode;
  String price;

  Drug({this.name, this.manufacturer, this.drugCode, this.price});

  factory Drug.fromJson(Map<String, dynamic> json) {
    return Drug(
        name: json["name"],
        manufacturer: json["manufacturer"],
        drugCode: json["drugCode"]);
  }

  Map<String, dynamic> toJson() => {
        'name': name,
        'manufacturer': manufacturer,
        'drugCode': drugCode,
        'price': {'value': price}
      };
}
