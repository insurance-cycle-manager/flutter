import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:medcare/components/customEditButton.dart';
import 'package:medcare/components/customEditIcon.dart';
import 'package:medcare/components/customeTFEdit.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/template/drug/drug.dart';
import 'package:medcare/screen/unauthorized.dart';

class DetailsDrug extends StatefulWidget {
  @override
  State createState() {
    return _DetailsDrugState();
  }
}

class _DetailsDrugState extends State<DetailsDrug> {
  bool _status = true;
  Drug drug;

  TextEditingController nameCont = TextEditingController();
  TextEditingController manufacturerCont = TextEditingController();
  TextEditingController priceCont = TextEditingController();
  TextEditingController drugCodeCont = TextEditingController();

  @override
  Widget build(BuildContext context) {
    drug = ModalRoute.of(context).settings.arguments;
    nameCont.text = drug.name;
    manufacturerCont.text = drug.manufacturer;
    drugCodeCont.text = drug.drugCode;
    priceCont.text = drug.price;

    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              expandedHeight: 200.0,
              floating: false,
              pinned: true,
              flexibleSpace: FlexibleSpaceBar(
                  centerTitle: true,
                  title: Text(drug.name,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16.0,
                      )),
                  background: Image.asset(
                    'assets/images/drugs.jpg',
                    fit: BoxFit.cover,
                  )),
              actions: <Widget>[
                IconButton(
                  icon: Icon(
                    Icons.delete,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    String url = DotEnv().env['DELETE_DRUG'];
                    url = url.replaceAll("DRUGCODE", drug.drugCode);

                    client.deleteObject(url, context, '/drugs');
                  },
                ),
              ],
            ),
          ];
        },
        body: FutureBuilder<String>(
          future: loadsDrugs(),
          builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              nameCont.text = drug.name == null ? "" : drug.name;
              return Container(
                color: Color(0xffFFFFFF),
                child: Padding(
                  padding: EdgeInsets.only(bottom: 25.0),
                  child: ListView(
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.only(
                              left: 25.0, right: 25.0, top: 25.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Text(
                                    'Edit',
                                    style: TextStyle(
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  _status
                                      ? CustomEditIcon(
                                          onClick: () {
                                            setState(() {
                                              _status = false;
                                            });
                                          },
                                        )
                                      : Container(),
                                ],
                              )
                            ],
                          )),
                      CustomTFEdit(
                        label: "Code",
                        textCont: drugCodeCont,
                      ),
                      CustomTFEdit(
                        label: "Name",
                        status: _status,
                        textCont: nameCont,
                      ),
                      CustomTFEdit(
                        label: "Manufacturer",
                        textCont: manufacturerCont,
                        status: _status,
                      ),
                      CustomTFEdit(
                        label: "Price",
                        textCont: priceCont,
                        status: _status,
                      ),
                      !_status
                          ? CustomEditButton(
                              onSave: () {
                                setState(() {
                                  drug.name = nameCont.text;
                                  drug.manufacturer = manufacturerCont.text;
                                  drug.drugCode = drugCodeCont.text;
                                  drug.price = priceCont.text;
                                  String url = DotEnv().env['PUT_DRUG'];
                                  url =
                                      url.replaceAll("DRUGCODE", drug.drugCode);
                                  print(drug.toJson());
                                  client.editObject(
                                      url, drug.toJson(), context);
                                  _status = true;
                                  FocusScope.of(context)
                                      .requestFocus(new FocusNode());
                                });
                              },
                              onCancel: () {
                                setState(() {
                                  _status = true;
                                  FocusScope.of(context)
                                      .requestFocus(new FocusNode());
                                });
                              },
                            )
                          : Container(),
                    ],
                  ),
                ),
              );
            } else
              return SpinKitCircle(
                //https://github.com/jogboms/flutter_spinkit#-showcase
                color: Colors.teal,
              );
          },
        ),
      ),
    );
  }

  Future<String> loadsDrugs() async {
    try {
      String url = DotEnv().env['GET_DRUGS'];
      var response = await client.getPrivateData(url, context);
      final parsed = await jsonDecode(response.body);
      drug = Drug.fromJson(parsed["data"]);
      return response;
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
      return "";
    }
  }
}
