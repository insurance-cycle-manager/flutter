
class Radiograph {
  String name;
  String radiographCode;
  String price;

  Radiograph({this.name, this.radiographCode, this.price});

  factory Radiograph.fromJson(Map<String, dynamic> json) {
    return Radiograph(
        name: json["name"], radiographCode: json["radiographCode"]);
  }

  Map<String, dynamic> toJson() =>
      {'name': name, 'radiographCode': radiographCode};
}
