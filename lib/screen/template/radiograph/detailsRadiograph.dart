import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:medcare/components/customEditButton.dart';
import 'package:medcare/components/customEditIcon.dart';
import 'package:medcare/components/customeTFEdit.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/template/radiograph/radiograph.dart';
import 'package:medcare/screen/unauthorized.dart';

class DetailsRadiograph extends StatefulWidget {
  @override
  State createState() {
    return _DetailsRadiographState();
  }
}

class _DetailsRadiographState extends State<DetailsRadiograph> {
  bool _status = true;
  Radiograph radiograph;

  TextEditingController nameCont = TextEditingController();
  TextEditingController priceCont = TextEditingController();

  TextEditingController radiographCodeCont = TextEditingController();

  @override
  Widget build(BuildContext context) {
    radiograph = ModalRoute.of(context).settings.arguments;
    nameCont.text = radiograph.name;
    radiographCodeCont.text = radiograph.radiographCode;
    priceCont.text = radiograph.price;

    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              expandedHeight: 200.0,
              floating: false,
              pinned: true,
              flexibleSpace: FlexibleSpaceBar(
                  centerTitle: true,
                  title: Text(radiograph.name,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16.0,
                      )),
                  background: Image.asset(
                    'assets/images/rad.jpg',
                    fit: BoxFit.cover,
                  )),
              actions: <Widget>[
                IconButton(
                  icon: Icon(
                    Icons.delete,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    String url = DotEnv().env['DELETE_RADIOGRAPH'];
                    url = url.replaceAll(
                        "RADIOGRAPHCODE", radiograph.radiographCode);

                    var response =
                        client.deleteObject(url, context, '/radiographs');
                    Fluttertoast.showToast(
                        msg: response.toString(),
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.BOTTOM,
                        timeInSecForIos: 1,
                        backgroundColor: Colors.redAccent,
                        textColor: Colors.white,
                        fontSize: 16.0);
                  },
                ),
              ],
            ),
          ];
        },
        body: FutureBuilder<String>(
          future: loadsRadiographs(),
          builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              nameCont.text = radiograph.name == null ? "" : radiograph.name;
              return Container(
                color: Color(0xffFFFFFF),
                child: Padding(
                  padding: EdgeInsets.only(bottom: 25.0),
                  child: ListView(
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.only(
                              left: 25.0, right: 25.0, top: 25.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Text(
                                    'Edit',
                                    style: TextStyle(
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  _status
                                      ? CustomEditIcon(
                                          onClick: () {
                                            setState(() {
                                              _status = false;
                                            });
                                          },
                                        )
                                      : Container(),
                                ],
                              )
                            ],
                          )),
                      CustomTFEdit(
                        label: "Code",
                        textCont: radiographCodeCont,
                      ),
                      CustomTFEdit(
                        label: "Name",
                        status: _status,
                        textCont: nameCont,
                      ),
                      CustomTFEdit(
                        label: "Price",
                        textCont: priceCont,
                        status: _status,
                      ),
                      !_status
                          ? CustomEditButton(
                              onSave: () {
                                setState(() {
                                  radiograph.name = nameCont.text;
                                  radiograph.radiographCode =
                                      radiographCodeCont.text;
                                  radiograph.price = priceCont.text;

                                  String url = DotEnv().env['PUT_RADIOGRAPH'];
                                  url = url.replaceAll("RADIOGRAPHCODE",
                                      radiograph.radiographCode);
                                  print(radiograph.toJson());
                                  client.editObject(
                                      url, radiograph.toJson(), context);
                                  _status = true;
                                  FocusScope.of(context)
                                      .requestFocus(new FocusNode());
                                });
                              },
                              onCancel: () {
                                setState(() {
                                  _status = true;
                                  FocusScope.of(context)
                                      .requestFocus(new FocusNode());
                                });
                              },
                            )
                          : Container(),
                    ],
                  ),
                ),
              );
            } else
              return SpinKitCircle(
                //https://github.com/jogboms/flutter_spinkit#-showcase
                color: Colors.teal,
              );
          },
        ),
      ),
    );
  }

  Future<String> loadsRadiographs() async {
    try {
      String url = DotEnv().env['GET_RADIOGRAPHS'];
      var response = await client.getPrivateData(url, context);
      final parsed = await jsonDecode(response.body);
      radiograph = Radiograph.fromJson(parsed["data"]);
      return response;
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
      return "";
    }
  }
}
