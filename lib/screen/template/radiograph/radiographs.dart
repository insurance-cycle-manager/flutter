import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:medcare/components/customAppBar.dart';
import 'package:medcare/components/customCard.dart';
import 'package:medcare/components/customHasNoData.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/template/radiograph/radiograph.dart';
import 'package:medcare/screen/unauthorized.dart';

class Radiographs extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _RadiographsState();
  }
}

class _RadiographsState extends State<Radiographs> {
  final TextEditingController _searchQuery = new TextEditingController();
  bool _isSearching;
  String _searchText = "";
  List<Radiograph> radiographs = [];
  Future<List<Radiograph>> _listFuture;

  _RadiographsState() {
    _searchQuery.addListener(() {
      if (_searchQuery.text.isEmpty) {
        setState(() {
          _isSearching = false;
          _searchText = "";
        });
      } else {
        setState(() {
          _isSearching = true;
          _searchText = _searchQuery.text;
        });
      }
    });
  }

  @override
  void initState() {
    setState(() {});
    _listFuture = updateAndGetList();
    _isSearching = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _listFuture = updateAndGetList();
    return Scaffold(
      appBar: CustomAppBar(
        title: "Radiographs",
        actionIcon: Icon(Icons.search),
        isSearching: _isSearching,
        searchText: _searchText,
        searchQuery: _searchQuery,
      ),
      body: FutureBuilder<List<Radiograph>>(
        future: _listFuture,
        builder:
            (BuildContext context, AsyncSnapshot<List<Radiograph>> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return SpinKitCircle(
              //https://github.com/jogboms/flutter_spinkit#-showcase
              color: Colors.teal,
            );
          } else if (snapshot.hasError) {
            return Scaffold(
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 250,
                    ),
                    Icon(Icons.error),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[Text("Something went wrong")],
                    ),
                    Expanded(
                      child: Text(
                        "Please check your internet connection",
                        style: TextStyle(fontSize: 15),
                      ),
                    )
                  ],
                ),
              ),
            );
          } else {
            radiographs = snapshot.data ?? <Radiograph>[];
            print(radiographs);
            if (radiographs.length > 0) {
              return Scaffold(
                backgroundColor: Color(0xffE5E5E5),
                body: RefreshIndicator(
                  onRefresh: refreshList,
                  child: ListView.builder(
                      itemCount: radiographs.length,
                      itemBuilder:
                          _isSearching ? _buildSearchList : _buildList),
                ),
                floatingActionButton: FloatingActionButton(
                  child: Icon(Icons.add),
                  onPressed: () {
                    Navigator.pushNamed(context, '/addRadiograph');
                  },
                ),
              );
            } else {
              return Scaffold(
                body: CustomHasNoData(),
                floatingActionButton: FloatingActionButton(
                  child: Icon(Icons.add),
                  onPressed: () {
                    Navigator.pushNamed(context, '/addRadiograph');
                  },
                ),
              );
            }
          }
        },
      ),
    );
  }

  Widget _buildList(BuildContext context, int index) {
    return CustomCard(
      name: radiographs[index].name,
      object: radiographs[index],
      route: "detailsRadiograph",
    );
  }

  Widget _buildSearchList(BuildContext context, int index) {
    if (_searchText.isEmpty) {
      return CustomCard(
        name: radiographs[index].name,
        object: radiographs[index],
        route: "detailsRadiograph",
      );
    } else {
      String name = radiographs[index].name;
      if (name.toLowerCase().contains(_searchText.toLowerCase())) {
        return CustomCard(
          name: radiographs[index].name,
          object: radiographs[index],
          route: "detailsRadiograph",
        );
      }
    }
    return Container(
      height: 0,
    );
  }

  Future<void> refreshList() {
    // reload
    setState(() {
      _listFuture = updateAndGetList();
    });
  }

  Future<List<Radiograph>> updateAndGetList() async {
    try {
      String url = DotEnv().env['GET_RADIOGRAPHS'];

      var response = await client.getPrivateData(url, context);

      final parsed = await jsonDecode(response.body);

      return (parsed["data"] as List)
          .map<Radiograph>((json) => Radiograph.fromJson(json))
          .toList();
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
      return [];
    }
  }
}
