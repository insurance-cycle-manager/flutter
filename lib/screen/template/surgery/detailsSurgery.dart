import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:medcare/components/customEditButton.dart';
import 'package:medcare/components/customEditIcon.dart';
import 'package:medcare/components/customeTFEdit.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/template/surgery/surgery.dart';
import 'package:medcare/screen/unauthorized.dart';

class DetailsSurgery extends StatefulWidget {
  @override
  State createState() {
    return _DetailsSurgeryState();
  }
}

class _DetailsSurgeryState extends State<DetailsSurgery> {
  bool _status = true;
  Surgery surgery;

  TextEditingController nameCont = TextEditingController();
  TextEditingController surgeryCodeCont = TextEditingController();

  @override
  Widget build(BuildContext context) {
    surgery = ModalRoute.of(context).settings.arguments;
    nameCont.text = surgery.name;
    surgeryCodeCont.text = surgery.surgeryCode;

    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              expandedHeight: 200.0,
              floating: false,
              pinned: true,
              flexibleSpace: FlexibleSpaceBar(
                centerTitle: true,
                title: Text(surgery.name,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16.0,
                    )),
                background: Image.asset(
                  'assets/images/clinic.jpg',
                  fit: BoxFit.cover,
                ),
              ),
              actions: <Widget>[
                IconButton(
                  icon: Icon(
                    Icons.delete,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    String url = DotEnv().env['DELETE_SURGERY'];
                    url = url.replaceAll("SURGERYCODE", surgery.surgeryCode);

                    var response =
                        client.deleteObject(url, context, '/surgeries');
                    Fluttertoast.showToast(
                        msg: response.toString(),
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.BOTTOM,
                        timeInSecForIos: 1,
                        backgroundColor: Colors.redAccent,
                        textColor: Colors.white,
                        fontSize: 16.0);
                  },
                ),
              ],
            ),
          ];
        },
        body: FutureBuilder<String>(
          future: loadsDrugs(),
          builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              nameCont.text = surgery.name == null ? "" : surgery.name;
              return Container(
                color: Color(0xffFFFFFF),
                child: Padding(
                  padding: EdgeInsets.only(bottom: 25.0),
                  child: ListView(
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.only(
                              left: 25.0, right: 25.0, top: 25.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Text(
                                    'Edit',
                                    style: TextStyle(
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  _status
                                      ? CustomEditIcon(
                                          onClick: () {
                                            setState(() {
                                              _status = false;
                                            });
                                          },
                                        )
                                      : Container(),
                                ],
                              )
                            ],
                          )),
                      CustomTFEdit(
                        label: "Code",
                        textCont: surgeryCodeCont,
                      ),
                      CustomTFEdit(
                        label: "Name",
                        status: _status,
                        textCont: nameCont,
                      ),
                      !_status
                          ? CustomEditButton(
                              onSave: () {
                                setState(() {
                                  surgery.name = nameCont.text;
                                  surgery.surgeryCode = surgeryCodeCont.text;

                                  String url = DotEnv().env['PUT_SURGERY'];
                                  url = url.replaceAll(
                                      "SURGERYCODE", surgery.surgeryCode);
                                  print(surgery.toJson());
                                  client.editObject(
                                      url, surgery.toJson(), context);
                                  _status = true;
                                  FocusScope.of(context)
                                      .requestFocus(new FocusNode());
                                });
                              },
                              onCancel: () {
                                setState(() {
                                  _status = true;
                                  FocusScope.of(context)
                                      .requestFocus(new FocusNode());
                                });
                              },
                            )
                          : Container(),
                    ],
                  ),
                ),
              );
            } else
              return SpinKitCircle(
                //https://github.com/jogboms/flutter_spinkit#-showcase
                color: Colors.teal,
              );
          },
        ),
      ),
    );
  }

  Future<String> loadsDrugs() async {
    try {
      String url = DotEnv().env['GET_SURGERIES'];
      var response = await client.getPrivateData(url, context);
      final parsed = await jsonDecode(response.body);
      surgery = Surgery.fromJson(parsed["data"]);
      return response;
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
      return "";
    }
  }
}
