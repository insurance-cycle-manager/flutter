import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:medcare/components/customButton.dart';
import 'package:medcare/components/customTextField.dart';
import 'package:medcare/networking/httpClient.dart' as client;

class AddSurgery extends StatefulWidget {
  final name = TextEditingController();
  final priceCont = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  State createState() {
    return _AddSurgeryState();
  }
}

class _AddSurgeryState extends State<AddSurgery> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add Surgery"),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.only(top: 40),
        child: Form(
          key: widget._formKey,
          child: Wrap(
            runSpacing: 25,
            children: <Widget>[
              CustomTextField(
                labelText: "Name",
                controller: widget.name,
              ),
              CustomTextField(
                labelText: "Price",
                controller: widget.priceCont,
              ),
              CustomButton(
                bName: "Submit",
                bFunc: () {
                  var json = {
                    "name": widget.name.text,
                    "price": {"value": widget.priceCont.text}
                  };
                  client.postWithTokenJSON(
                      json, DotEnv().env['POST_SURGERY'], context);

                  Navigator.popUntil(
                      context, ModalRoute.withName('/surgeries'));
                  Navigator.pop(context);
                  Navigator.pushNamed(context, '/surgeries');
                },
                bTextColor: Colors.white,
              )
            ],
          ),
        ),
      ),
    );
  }
}
