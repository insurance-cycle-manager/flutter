class Surgery {
  String name;
  String surgeryCode;

  Surgery({this.name, this.surgeryCode});

  factory Surgery.fromJson(Map<String, dynamic> json) {
    return Surgery(name: json["name"], surgeryCode: json["surgeryCode"]);
  }

  Map<String, dynamic> toJson() => {'name': name, 'surgeryCode': surgeryCode};
}
