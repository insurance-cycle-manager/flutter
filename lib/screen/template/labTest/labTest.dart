class LabTest {
  String name;
  String labTestCode;
  String price;

  LabTest({this.name, this.labTestCode, this.price});

  factory LabTest.fromJson(Map<String, dynamic> json) {
    return LabTest(name: json["name"], labTestCode: json["labTestCode"]);
  }

  Map<String, dynamic> toJson() => {'name': name, 'labTestCode': labTestCode};
}
