import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:medcare/components/customAppBar.dart';
import 'package:medcare/components/customCard.dart';
import 'package:medcare/components/customHasNoData.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/template/labTest/labTest.dart';
import 'package:medcare/screen/unauthorized.dart';

class LabTests extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _LabTestsState();
  }
}

class _LabTestsState extends State<LabTests> {
  final TextEditingController _searchQuery = new TextEditingController();
  bool _isSearching;
  String _searchText = "";
  List<LabTest> labTests = [];
  Future<List<LabTest>> _listFuture;

  _LabTestsState() {
    _searchQuery.addListener(() {
      if (_searchQuery.text.isEmpty) {
        setState(() {
          _isSearching = false;
          _searchText = "";
        });
      } else {
        setState(() {
          _isSearching = true;
          _searchText = _searchQuery.text;
        });
      }
    });
  }

  @override
  void initState() {
    setState(() {});
    _listFuture = updateAndGetList();
    _isSearching = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _listFuture = updateAndGetList();
    return Scaffold(
      appBar: CustomAppBar(
        title: "Lab Tests",
        actionIcon: Icon(Icons.search),
        isSearching: _isSearching,
        searchText: _searchText,
        searchQuery: _searchQuery,
      ),
      body: FutureBuilder<List<LabTest>>(
        future: _listFuture,
        builder: (BuildContext context, AsyncSnapshot<List<LabTest>> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return SpinKitCircle(
              //https://github.com/jogboms/flutter_spinkit#-showcase
              color: Colors.teal,
            );
          } else if (snapshot.hasError) {
            return Scaffold(
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 250,
                    ),
                    Icon(Icons.error),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[Text("Something went wrong")],
                    ),
                    Expanded(
                      child: Text(
                        "Please check your internet connection",
                        style: TextStyle(fontSize: 15),
                      ),
                    )
                  ],
                ),
              ),
            );
          } else {
            labTests = snapshot.data ?? <LabTest>[];
            print(labTests);
            if (labTests.length > 0) {
              return Scaffold(
                backgroundColor: Color(0xffE5E5E5),
                body: RefreshIndicator(
                  onRefresh: refreshList,
                  child: ListView.builder(
                      itemCount: labTests.length,
                      itemBuilder:
                          _isSearching ? _buildSearchList : _buildList),
                ),
                floatingActionButton: FloatingActionButton(
                  child: Icon(Icons.add),
                  onPressed: () {
                    Navigator.pushNamed(context, '/addLabTest');
                  },
                ),
              );
            } else {
              return Scaffold(
                body: CustomHasNoData(),
                floatingActionButton: FloatingActionButton(
                  child: Icon(Icons.add),
                  onPressed: () {
                    Navigator.pushNamed(context, '/addLabTest');
                  },
                ),
              );
            }
          }
        },
      ),
    );
  }

  Widget _buildList(BuildContext context, int index) {
    return CustomCard(
      name: labTests[index].name,
      object: labTests[index],
      route: "detailsLabTest",
    );
  }

  Widget _buildSearchList(BuildContext context, int index) {
    if (_searchText.isEmpty) {
      return CustomCard(
        name: labTests[index].name,
        object: labTests[index],
        route: "detailsLabTest",
      );
    } else {
      String name = labTests[index].name;
      if (name.toLowerCase().contains(_searchText.toLowerCase())) {
        return CustomCard(
          name: labTests[index].name,
          object: labTests[index],
          route: "detailsLabTest",
        );
      }
    }
    return Container(
      height: 0,
    );
  }

  Future<void> refreshList() {
    // reload
    setState(() {
      _listFuture = updateAndGetList();
    });
  }

  Future<List<LabTest>> updateAndGetList() async {
    try {
      String url = DotEnv().env['GET_LABTESTS'];

      var response = await client.getPrivateData(url, context);

      final parsed = await jsonDecode(response.body);

      return (parsed["data"] as List)
          .map<LabTest>((json) => LabTest.fromJson(json))
          .toList();
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
      return [];
    }
  }
}
