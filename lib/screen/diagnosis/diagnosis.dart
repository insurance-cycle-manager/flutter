class Diagnosis {
  String description;
  String diagnosisCode;

  Diagnosis({this.description: "", this.diagnosisCode:""});

  factory Diagnosis.fromJson(Map<String, dynamic> json) {
    return Diagnosis(
        description: json['description'] ?? "",
        diagnosisCode: json['diagnosisCode'] ?? "");
  }

  Map<String, dynamic> toJson() => {"descreption": description};
}
