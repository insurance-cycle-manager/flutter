import 'package:intl/intl.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:medcare/components/customEditButton.dart';
import 'package:medcare/components/customEditIcon.dart';
import 'package:medcare/components/customeTFEdit.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/package/package.dart';

class DetailsPackage extends StatefulWidget {
  @override
  State createState() {
    return _DetailsPackageState();
  }
}

class _DetailsPackageState extends State<DetailsPackage> {
  bool _status = true;
  final FocusNode myFocusNode = FocusNode();
  TextEditingController nameCont = TextEditingController();
  TextEditingController packageCostCont = TextEditingController();
  TextEditingController packageCodeCont = TextEditingController();
  TextEditingController dateCont = TextEditingController();
  TextEditingController iCNameCont = TextEditingController();
  TextEditingController iCCodeCont = TextEditingController();
  Package package;

  @override
  Widget build(BuildContext context) {
    package = ModalRoute.of(context).settings.arguments;

    nameCont.text = package.name;
    dateCont.text =
        DateFormat('d/M/y').format(DateTime.parse(package.packageDate));
    packageCostCont.text = package.price.toString();
    iCNameCont.text = package.ic.name;
    iCCodeCont.text = package.ic.companyCode;
    packageCodeCont.text = package.insurancePackageCode;
    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              expandedHeight: 200.0,
              floating: false,
              pinned: true,
              flexibleSpace: FlexibleSpaceBar(
                  centerTitle: true,
                  title: Text(package.name,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16.0,
                      )),
                  background: Image.asset(
                    'assets/images/pckg.png',
                    fit: BoxFit.cover,
                  )),
              actions: <Widget>[
                package.onlyDisplay
                    ? IconButton(
                        icon: Icon(
                          Icons.delete,
                          color: Colors.white,
                        ),
                        onPressed: () {
                          String url = DotEnv().env['DELETE_PACKAGE'];
                          url =
                              url.replaceAll("icCode", package.ic.companyCode);
                          url = url.replaceAll("packageName", package.name);

                          var deleteResponse =
                              client.deleteObject(url, context, '/packages');
                          print(deleteResponse);
                          Navigator.pop(context);
                          Navigator.pushNamed(context, '/packages',
                              arguments: package.ic.companyCode);
                        },
                      )
                    : Container()
              ],
            ),
          ];
        },
        body: Container(
          color: Color(0xffFFFFFF),
          child: Padding(
            padding: EdgeInsets.only(bottom: 25.0),
            child: ListView(
              children: <Widget>[
                Padding(
                    padding:
                        EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        package.onlyDisplay
                            ? Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Text(
                                    'Edit',
                                    style: TextStyle(
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              )
                            : Container(),
                        package.onlyDisplay
                            ? Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  _status
                                      ? CustomEditIcon(
                                          onClick: () {
                                            setState(() {
                                              _status = false;
                                            });
                                          },
                                        )
                                      : Container(),
                                ],
                              )
                            : Container()
                      ],
                    )),
                CustomTFEdit(
                  label: "Name",
                  textCont: nameCont,
                ),
                CustomTFEdit(
                  label: "Package code",
                  textCont: packageCodeCont,
                ),
                CustomTFEdit(
                  label: "Date",
                  textCont: dateCont,
                ),
                CustomTFEdit(
                  label: "Package Cost",
                  textCont: packageCostCont,
                ),
                CustomTFEdit(
                  label: "Insurance Company Name",
                  textCont: iCNameCont,
                ),
                CustomTFEdit(
                  label: "Insurance Company Code",
                  textCont: iCCodeCont,
                ),
                !_status
                    ? CustomEditButton(
                        onSave: () {
                          setState(() {
                            package.name = nameCont.text;
                            package.packageDate = dateCont.text;
                            package.price = double.parse(packageCostCont.text);

                            String url = DotEnv().env['EDIT_PACKAGE'];
                            url = url.replaceAll(
                                "ICCODE", package.ic.companyCode);
                            url = url.replaceAll("PACKAGENAME", package.name);
                            client.editObject(url, package.toJson(), context);
                            //client.editObject(url, package.toJson());
                            _status = true;
                            FocusScope.of(context).requestFocus(FocusNode());
                          });
                        },
                        onCancel: () {
                          setState(() {
                            _status = true;
                            FocusScope.of(context).requestFocus(FocusNode());
                          });
                        },
                      )
                    : Container(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
