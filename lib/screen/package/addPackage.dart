import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:medcare/components/customButton.dart';
import 'package:medcare/components/customDatePicker.dart';
import 'package:medcare/components/customTextField.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/insuranceCompany/insuranceCompany.dart';
import 'package:medcare/screen/template/argsTemplate.dart';

class AddPackage extends StatefulWidget {
  final nameCont = TextEditingController();
  final categoryCont = TextEditingController();
  final priceCont = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  State createState() {
    return _AddPackageState();
  }
}

class _AddPackageState extends State<AddPackage> {
  List<String> _drugsCode = [];
  List<String> _labTestsCode = [];
  List<String> _radiographsCode = [];
  List<String> _surgeriesCode = [];
  InsuranceCompany ic;
  String _date = "Not set";

  @override
  Widget build(BuildContext context) {
    ic = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      appBar: AppBar(
        title: Text("Add Package"),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.only(top: 40),
        child: Form(
          key: widget._formKey,
          child: Wrap(
            runSpacing: 25,
            children: <Widget>[
              CustomTextField(
                labelText: "Name",
                controller: widget.nameCont,
              ),
              CustomTextField(
                labelText: "Category",
                controller: widget.categoryCont,
              ),
              CustomTextField(
                labelText: "Package Cost",
                controller: widget.priceCont,
              ),
              CustomDatePicker(
                name: "Date",
                onPressed: (date) {
                  setState(() {
                    _date = date;
                  });
                },
              ),
              CustomButton(
                bName: "Drugs",
                bFunc: () async {
                  ArgsTemplate arg = ArgsTemplate("DRUGS", _drugsCode);
                  final result = await Navigator.pushNamed(
                      context, '/list template',
                      arguments: arg);

                  _drugsCode = result;
                },
              ),
              CustomButton(
                bName: "Lab Tests",
                bFunc: () async {
                  ArgsTemplate arg = ArgsTemplate("LABTESTS", _drugsCode);
                  final result = await Navigator.pushNamed(
                      context, '/list template',
                      arguments: arg);

                  _labTestsCode = result;
                },
              ),
              CustomButton(
                bName: "Radiographs",
                bFunc: () async {
                  ArgsTemplate arg = ArgsTemplate("RADIOGRAPHS", _drugsCode);
                  final result = await Navigator.pushNamed(
                      context, '/list template',
                      arguments: arg);

                  _radiographsCode = result;
                },
              ),
              CustomButton(
                bName: "Surgeries",
                bFunc: () async {
                  ArgsTemplate arg = ArgsTemplate("SURGERIES", _drugsCode);
                  final result = await Navigator.pushNamed(
                      context, '/list template',
                      arguments: arg);

                  _surgeriesCode = result;
                },
              ),
              CustomButton(
                bName: "Submit",
                bFunc: () {
                  var json = {
                    "name": widget.nameCont.text,
                    "packageDate": _date,
                    "category": widget.categoryCont.text,
                    "price": {"value": widget.priceCont.text},
                    "Drugs": _drugsCode,
                    "LabTests": _labTestsCode,
                    "Radiographs": _radiographsCode,
                    "Surgeries": _surgeriesCode
                  };

                  String url = DotEnv().env['ADD_PACKAGE'];
                  url = url.replaceAll("ICCODE", ic.companyCode);
                  client.postWithTokenJSON(json, url, context);

                  Navigator.popUntil(
                      context, ModalRoute.withName('/detailsIC'));
                  Navigator.pushNamed(context, '/packages', arguments: ic);
                },
                bTextColor: Colors.white,
              )
            ],
          ),
        ),
      ),
    );
  }
}
