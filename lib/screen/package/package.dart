import 'package:medcare/screen/insuranceCompany/insuranceCompany.dart';
import 'package:medcare/screen/template/drug/drug.dart';
import 'package:medcare/screen/template/labTest/labTest.dart';
import 'package:medcare/screen/template/radiograph/radiograph.dart';
import 'package:medcare/screen/template/surgery/surgery.dart';

class Package {
  String name;
  String category;
  String packageDate;
  double price;
  InsuranceCompany ic;
  List<Drug> drugs;
  List<LabTest> labTests;
  List<Radiograph> radiographs;
  List<Surgery> surgeries;
  bool onlyDisplay;
  String insurancePackageCode;

  Package(
      {this.name,
      this.category,
      this.price,
      this.packageDate,
      this.ic,
      this.drugs,
      this.labTests,
      this.radiographs,
      this.surgeries,
      this.onlyDisplay: false,
      this.insurancePackageCode});

  factory Package.fromJson(Map<String, dynamic> json) {
    List<Drug> _drugs = [];
    if (json['drugs'] != null) {
      var drugsObjJson = json['drugs'] as List;
      _drugs = drugsObjJson.map((drugJson) => Drug.fromJson(drugJson)).toList();
    }

    List<LabTest> _labTests = [];
    if (json['labTests'] != null) {
      var labTestsObjJson = json['labTests'] as List;
      _labTests = labTestsObjJson
          .map((labTestJson) => LabTest.fromJson(labTestJson))
          .toList();
    }
    List<Radiograph> _radiographs = [];
    if (json['radiographs'] != null) {
      var radiographsObjJson = json['radiographs'] as List;
      _radiographs = radiographsObjJson
          .map((radiographJson) => Radiograph.fromJson(radiographJson))
          .toList();
    }
    List<Surgery> _surgeries = [];
    if (json['surgeries'] != null) {
      var surgeriesObjJson = json['surgeries'] as List;
      _surgeries = surgeriesObjJson
          .map((surgeryJson) => Surgery.fromJson(surgeryJson))
          .toList();
    }
    double _price = 0.0;
    if (json['price'] != null) {
      _price = json['price']['value'];
    }
    InsuranceCompany _ic;
    if (json['insuranceCompany'] != null) {
      _ic = InsuranceCompany.fromJson(json['insuranceCompany']);
    }
    return Package(
        name: json['name'] as String,
        category: json['category'] as String,
        packageDate: json['packageDate'],
        ic: _ic,
        drugs: _drugs,
        price: _price,
        labTests: _labTests,
        radiographs: _radiographs,
        surgeries: _surgeries,
        insurancePackageCode: json['insurancePackageCode']);
  }

  Map<String, dynamic> toJson() => {
        'name': name,
        'category': category,
        'packageDate': packageDate,
        'insuranceCompany': ic.toJson()
      };
}
