import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:medcare/components/customAppBar.dart';
import 'package:medcare/components/customCard.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/auth/user.dart';
import 'package:medcare/screen/insuranceCompany/insuranceCompany.dart';
import 'package:medcare/screen/package/package.dart';
import 'package:medcare/screen/profile/role/Role.dart';
import 'package:medcare/screen/unauthorized.dart';

class Packages extends StatefulWidget {
  final InsuranceCompany ic;

  const Packages({Key key, this.ic}) : super(key: key);

  @override
  State createState() {
    return _PackagesState();
  }
}

class _PackagesState extends State<Packages> {
  User user;
  Role activeRole;
  final TextEditingController _searchQuery = TextEditingController();
  bool _isSearching;
  String _searchText = "";
  List<Package> packages = [];
  Future<List<Package>> _listFuture;

  _PackagesState() {
    _searchQuery.addListener(() {
      if (_searchQuery.text.isEmpty) {
        setState(() {
          _isSearching = false;
          _searchText = "";
        });
      } else {
        setState(() {
          _isSearching = true;
          _searchText = _searchQuery.text;
        });
      }
    });
  }

  @override
  void initState() {
    setState(() {});
    _isSearching = false;
    _listFuture = updateAndGetList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        title: "Packages",
        actionIcon: Icon(Icons.search),
        isSearching: _isSearching,
        searchText: _searchText,
        searchQuery: _searchQuery,
      ),
      body: FutureBuilder<List<Package>>(
        future: _listFuture,
        builder: (BuildContext context, AsyncSnapshot<List<Package>> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return SpinKitCircle(
              //https://github.com/jogboms/flutter_spinkit#-showcase
              color: Colors.teal,
            );
          } else if (snapshot.hasError) {
            return Scaffold(
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 250,
                    ),
                    Icon(Icons.error),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[Text("Something went wrong")],
                    ),
                    Expanded(
                      child: Text(
                        "Please check your internet connection",
                        style: TextStyle(fontSize: 15),
                      ),
                    )
                  ],
                ),
              ),
            );
          } else {
            packages = snapshot.data ?? <Package>[];
            if (widget.ic != null) {
              List<Package> temp = [];
              packages.forEach((e) {
                if (e.ic.name == widget.ic.name) temp.add(e);
              });
              packages = temp;
            }
            return Scaffold(
              backgroundColor: Color(0xffE5E5E5),
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView.builder(
                    itemCount: packages.length,
                    itemBuilder: _isSearching ? _buildSearchList : _buildList),
              ),
              floatingActionButton:
                  (activeRole.roleName == "Administrator" && widget.ic != null)
                      ? FloatingActionButton(
                          child: Icon(Icons.add),
                          onPressed: () {
                            Navigator.pushNamed(context, '/addPackage',
                                arguments: widget.ic);
                          },
                        )
                      : Container(),
            );
          }
        },
      ),
    );
  }

  Widget _buildList(BuildContext context, int index) {
    if (widget.ic != null) packages[index].onlyDisplay = true;
    return CustomCard(
      name: packages[index].name,
      dateOfBirth: packages[index].packageDate,
      gender: packages[index].category,
      object: packages[index],
      route: 'detailsPackage',
    );
  }

  Widget _buildSearchList(BuildContext context, int index) {
    if (widget.ic != null) packages[index].onlyDisplay = true;
    if (_searchText.isEmpty) {
      return CustomCard(
        name: packages[index].name,
        dateOfBirth: packages[index].packageDate,
        gender: packages[index].category,
        object: packages[index],
        route: 'detailsPackage',
      );
    } else {
      String name = packages[index].name;
      if (name.toLowerCase().contains(_searchText.toLowerCase())) {
        return CustomCard(
          name: packages[index].name,
          dateOfBirth: packages[index].packageDate,
          gender: packages[index].category,
          object: packages[index],
          route: 'detailsPackage',
        );
      }
    }
    return Container(
      height: 0,
    );
  }

  Future<void> refreshList() {
    // reload
    setState(() {
      _listFuture = updateAndGetList();
    });
    return null;
  }

  Future<List<Package>> updateAndGetList() async {
    activeRole = await client.getActiveRole();

    try {
      if (widget.ic != null) {
        String url = DotEnv().env['GET_PACKAGES_IC'];
        url = url.replaceAll("ICCODE", widget.ic.companyCode);
        var response = await client.getPrivateData(url, context);

        final parsed = await jsonDecode(response.body);

        return (parsed["data"] as List)
            .map<Package>((json) => Package.fromJson(json))
            .toList();
      } else {
        String url = DotEnv().env['GET_PACKAGES'];
        var response = await client.getPrivateData(url, context);

        final parsed = await jsonDecode(response.body);

        return (parsed["data"] as List)
            .map<Package>((json) => Package.fromJson(json))
            .toList();
      }
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));

      return [];
    } catch (e) {
      return null;
    }
  }
}
