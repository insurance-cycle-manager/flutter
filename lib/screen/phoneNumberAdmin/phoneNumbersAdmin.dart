import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:medcare/components/customAppBar.dart';
import 'package:medcare/components/customCard.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/profile/phoneNumber/Number.dart';
import 'package:medcare/screen/unauthorized.dart';

class PhoneNumbersAdmin extends StatefulWidget {
  @override
  State createState() {
    return _PhoneNumbersAdminState();
  }
}

class _PhoneNumbersAdminState extends State<PhoneNumbersAdmin> {
  final TextEditingController _searchQuery = new TextEditingController();
  bool _isSearching;
  String _searchText = "";
  List<Number> numbers = [];
  Future<List<Number>> _listFuture;

  _PhoneNumbersAdminState() {
    _searchQuery.addListener(() {
      if (_searchQuery.text.isEmpty) {
        setState(() {
          _isSearching = false;
          _searchText = "";
        });
      } else {
        setState(() {
          _isSearching = true;
          _searchText = _searchQuery.text;
        });
      }
    });
  }

  @override
  void initState() {
    setState(() {});
    _isSearching = false;
    super.initState();
    _listFuture = updateAndGetList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        title: "Phone Numbers",
        isSearching: _isSearching,
        searchText: _searchText,
        searchQuery: _searchQuery,
        handleClick: handleClick,
        optionsList: ['Phone Numbers Control'],
      ),
      body: FutureBuilder<List<Number>>(
        future: _listFuture,
        builder: (BuildContext context, AsyncSnapshot<List<Number>> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return SpinKitCircle(
              color: Colors.teal,
            );
          } else if (snapshot.hasError) {
            return Scaffold(
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 250,
                    ),
                    Icon(Icons.error),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[Text("Something went wrong")],
                    ),
                    Expanded(
                      child: Text(
                        "Please check your internet connection",
                        style: TextStyle(fontSize: 15),
                      ),
                    )
                  ],
                ),
              ),
            );
          } else {
            numbers = snapshot.data ?? <Number>[];
            return Scaffold(
              backgroundColor: Color(0xffE5E5E5),
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView.builder(
                    itemCount: numbers.length,
                    itemBuilder: _isSearching ? _buildSearchList : _buildList),
              ),
              floatingActionButton: FloatingActionButton(
                child: Icon(Icons.add),
                onPressed: () {
                  Navigator.pushNamed(context, '/addPhoneAdmin');
                },
              ),
            );
          }
        },
      ),
    );
  }

  Widget _buildList(BuildContext context, int index) {
    return CustomCard(
      name: numbers[index].number,
      dateOfBirth: numbers[index].phoneCode,
      route: 'detailsPhoneAdmin',
      object: numbers[index],
    );
  }

  Widget _buildSearchList(BuildContext context, int index) {
    if (_searchText.isEmpty) {
      return CustomCard(
        name: numbers[index].number,
        route: 'detailsPhoneAdmin',
      );
    } else {
      String name = numbers[index].number;
      if (name.toLowerCase().contains(_searchText.toLowerCase())) {
        return CustomCard(
          name: numbers[index].number,
          route: 'detailsPhoneAdmin',
        );
      }
    }
    return Container(
      height: 0,
    );
  }

  Future<void> refreshList() {
    // reload
    setState(() {
      _listFuture = updateAndGetList();
    });
    return null;
  }

  Future<List<Number>> updateAndGetList() async {
    dynamic response;
    try {
      response =
      await client.getPrivateData(DotEnv().env['GET_PHONES'], context);
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
      return [];
    }

    final parsed = await jsonDecode(response.body);

    return (parsed["data"] as List)
        .map<Number>((json) => Number.fromJson(json))
        .toList();
  }

  void handleClick(String value) async {
    switch (value) {
      case 'Numbers Control':
        {
          Navigator.pushNamed(
            context,
            '/phonesControl',
          );
        }
        break;
    }
  }
}
