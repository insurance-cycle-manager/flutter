import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:medcare/components/customButton.dart';
import 'package:medcare/components/customTextField.dart';
import 'package:medcare/networking/httpClient.dart' as client;

class AddPhoneNumberAdmin extends StatefulWidget {
  final email = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  State createState() {
    return _AddPhoneNumberAdminState();
  }
}

class _AddPhoneNumberAdminState extends State<AddPhoneNumberAdmin> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add Email'),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.only(top: 40),
        child: Form(
          key: widget._formKey,
          child: Wrap(
            runSpacing: 25,
            children: <Widget>[
              CustomTextField(
                labelText: "Email",
                validationFun: (value) {
                  if (value.isEmpty) {
                    return 'can\'t be empty';
                  } else
                    return null;
                },
                controller: widget.email,
              ),
              CustomButton(
                bName: "Submit",
                bFunc: () {
                  var json = {
                    "address": widget.email.text,
                  };
                  client.postWithTokenJSON(
                      json, DotEnv().env['POST_EMAIL'], context);
                  Navigator.popUntil(
                      context, ModalRoute.withName('/emailsAdmin'));
                  Navigator.pop(context);
                  Navigator.pushNamed(context, '/emailsAdmin');
                  //Navigator.popUntil(context, ModalRoute.withName('/insurance companies'));
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
