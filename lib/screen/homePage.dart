import 'dart:convert';

import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_circular_chart/flutter_circular_chart.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:medcare/components/navigationDrawer.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/statistics/barChart.dart';
import 'package:medcare/screen/statistics/pieChart.dart';
import 'package:medcare/screen/statistics/purchaseAmount.dart';
import 'package:medcare/screen/statistics/purchaseCount.dart';
import 'package:medcare/screen/statistics/spCounts.dart';
import 'package:pie_chart/pie_chart.dart';

class HomePage extends StatefulWidget {
  @override
  State createState() {
    return _HomePageState();
  }
}

class _HomePageState extends State<HomePage> {
  Future<void> _listFuture;
  List<CircularStackEntry> circularData;
  List<CircularStackEntry> circularPurchaseAmount;
  List<charts.Series<SPCounts, String>> spCountsData = [];
  List<PurchaseCount> purchaseCounts = [];
  List<PurchaseAmount> purchaseAmount = [];

  List<SPCounts> spCounts = [];
  Map<String, double> purchaseAmountData;
  Map<String, double> purchaseCountsData;

  Widget myBarChart(List<charts.Series<SPCounts, String>> data) {
    return Container(
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        margin: EdgeInsets.only(left: 8, right: 8, top: 8, bottom: 4),
        child: Padding(
          padding: const EdgeInsets.all(5.0),
          child: Column(
            children: <Widget>[
              Text("# of Service Providers",
                  style: TextStyle(
                    fontSize: 15.0,
                    color: Colors.black,
                  )),
              SizedBox(
                  height: 250,
                  child: SimpleBarChart(
                    data,
                    animate: true,
                  )),
            ],
          ),
        ),
      ),
      decoration: BoxDecoration(boxShadow: [
        BoxShadow(
          color: Colors.grey.withOpacity(.5),
          blurRadius: 20.0, // soften the shadow
          spreadRadius: 0.0, //extend the shadow
          offset: Offset(
            5.0, // Move to right 10  horizontally
            5.0, // Move to bottom 10 Vertically
          ),
        ),
      ]),
    );
  }

  @override
  void initState() {
    purchaseAmountData = {};
    purchaseCountsData = {};
    _listFuture = updateAndGetList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    setState(() {});
    return Scaffold(
      drawer: NavigationDrawer(),
      appBar: AppBar(title: Text("MedCare")),
      body: FutureBuilder<void>(
        future: _listFuture,
        builder: (BuildContext context, AsyncSnapshot<void> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return SpinKitCircle(
              color: Colors.teal,
            );
          } else if (snapshot.hasError) {
            return Scaffold(
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 250,
                    ),
                    Icon(Icons.error),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[Text("Something went wrong")],
                    ),
                    Expanded(
                      child: Text(
                        "Please check your internet connection",
                        style: TextStyle(fontSize: 15),
                      ),
                    )
                  ],
                ),
              ),
            );
          } else {
            return Scaffold(
              backgroundColor: Color(0xffE5E5E5),
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: Container(
                  color: Color(0xffE5E5E5),
                  child: StaggeredGridView.count(
                    crossAxisCount: 4,
                    crossAxisSpacing: 12.0,
                    mainAxisSpacing: 12.0,
                    children: <Widget>[
                      myBarChart(spCountsData),
                      MyPieChart(
                        dataMap: purchaseAmountData,
                        title: "Purchase Amounts",
                        showChartValuesInPercentage: true,
                      ),
                      MyPieChart(
                        dataMap: purchaseCountsData,
                        title: "Purchase By type",
                        chartType: ChartType.ring,
                        showChartValuesInPercentage: true,
                      ),
                    ],
                    staggeredTiles: [
                      StaggeredTile.extent(4, 300.0),
                      StaggeredTile.extent(4, 290.0),
                      StaggeredTile.extent(4, 290.0),
                    ],
                  ),
                ),
              ),
            );
          }
        },
      ),
    );
  }

  Future<void> refreshList() {
    setState(() {
      _listFuture = updateAndGetList();
    });
  }

  Future<void> updateAndGetList() async {
    ///Purchase_Counts
    String url = DotEnv().env['Statistics_Purchase_Counts'];
    var response = await client.getPrivateData(url, context);
    final parsed = await jsonDecode(response.body);
    purchaseCounts = (parsed as List)
        .map<PurchaseCount>((json) => PurchaseCount.fromJson(json))
        .toList();

    purchaseCounts.forEach((element) {
      purchaseCountsData[element.type] = element.count;
    });

    ///Purchase_Amounts
    url = DotEnv().env['Statistics_Purchase_Amounts'];
    var response1 = await client.getPrivateData(url, context);
    final parsed1 = await jsonDecode(response1.body);
    purchaseAmount = (parsed1 as List)
        .map<PurchaseAmount>((json) => PurchaseAmount.fromJson(json))
        .toList();

    purchaseAmount.forEach((element) {
      purchaseAmountData[element.type] = element.amount;
    });

    ///SPcounts
    url = DotEnv().env['Statistics_Service_Provider_Counts'];
    response = await client.getPrivateData(url, context);
    final parsed2 = await jsonDecode(response.body);
    spCounts = (parsed2 as List).map<SPCounts>((json) {
      SPCounts sp = SPCounts.fromJson(json);
      if (sp.role == "RadiographOffice") {
        return SPCounts(count: sp.count, role: "Radiograph");
      }
      return sp;
    }).toList();
    spCountsData = [
      charts.Series<SPCounts, String>(
        id: '# of Service Providers',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (SPCounts sales, _) => sales.role,
        measureFn: (SPCounts sales, _) => sales.count,
        data: spCounts,
      )
    ];
  }
}
