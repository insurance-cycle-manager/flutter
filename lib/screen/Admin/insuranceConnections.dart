import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:medcare/components/customButton.dart';
import 'package:medcare/components/customTextField.dart';
import 'package:medcare/networking/httpClient.dart' as client;

class AddICConnection extends StatefulWidget {
  final insuranceCompanyCode = TextEditingController();
  final thirdPartyCode = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  State createState() {
    return _AddICConnectionState();
  }
}

class _AddICConnectionState extends State<AddICConnection> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add IC Connection"),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.only(top: 40),
        child: Form(
          key: widget._formKey,
          child: Wrap(
            runSpacing: 25,
            children: <Widget>[
              CustomTextField(
                labelText: "Insurance Company Code",
                controller: widget.insuranceCompanyCode,
              ),
              CustomTextField(
                labelText: "ThirdParty Code",
                controller: widget.thirdPartyCode,
              ),
              CustomButton(
                bName: "Submit",
                bFunc: () {
                  var json = {
                    "InsuranceCompanyCode": widget.insuranceCompanyCode.text,
                    "ThirdPartyCode": widget.thirdPartyCode.text,
                  };
                  client.postWithTokenJSON(
                      json, DotEnv().env['insuranceConnections'], context);
                  Navigator.pop(context);
                },
                bTextColor: Colors.white,
              )
            ],
          ),
        ),
      ),
    );
  }
}
