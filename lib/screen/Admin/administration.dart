import 'package:flutter/material.dart';

class Administration extends StatefulWidget {
  @override
  State createState() {
    return _Administration();
  }
}

class _Administration extends State<Administration> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Administration"),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context, false);
            },
          ),
        ),
        body: ListView(children: <Widget>[
          ExpansionTile(
            title: Row(
              children: <Widget>[
                Icon(
                  Icons.attach_file,
                  color: Colors.teal,
                ),
                Padding(
                  padding: EdgeInsets.only(right: 33),
                ),
                Flexible(
                  child: Text(
                    "Manage Templates",
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                ),
              ],
            ),
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  FlatButton(
                    padding: EdgeInsets.only(left: 75),
                    child: Text("Drugs"),
                    onPressed: () {
                      Navigator.popUntil(
                          context, ModalRoute.withName('/homePage'));
                      Navigator.pushNamed(context, '/drugs');
                    },
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  FlatButton(
                    padding: EdgeInsets.only(left: 75),
                    child: Text("Surgeries"),
                    onPressed: () {
                      Navigator.popUntil(
                          context, ModalRoute.withName('/homePage'));
                      Navigator.pushNamed(context, '/surgeries');
                    },
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  FlatButton(
                    padding: EdgeInsets.only(left: 75),
                    child: Text("Lab Tests"),
                    onPressed: () {
                      Navigator.popUntil(
                          context, ModalRoute.withName('/homePage'));
                      Navigator.pushNamed(context, '/labtests');
                    },
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  FlatButton(
                    padding: EdgeInsets.only(left: 75),
                    child: Text("Radiographs"),
                    onPressed: () {
                      Navigator.popUntil(
                          context, ModalRoute.withName('/homePage'));
                      Navigator.pushNamed(context, '/radiographs');
                    },
                  ),
                ],
              ),
            ],
          ),
          Divider(),
          ListTile(
              leading: const Icon(
                Icons.person_add,
                color: Colors.teal,
              ),
              title: const Text("Add role"),
              onTap: () {
                ////Navigator.popUntil(context, ModalRoute.withName('/homePage'));
                Navigator.pushNamed(context, '/add role admin');
              }),
          Divider(),
          ListTile(
              leading: const Icon(Icons.delete, color: Colors.teal,),
              title: const Text("Delete role"),
              onTap: () {
                //Navigator.popUntil(context, ModalRoute.withName('/homePage'));
                Navigator.pushNamed(context, '/delete role admin');
              }),
          Divider(),
          ListTile(
              leading: const Icon(Icons.account_box, color: Colors.teal,),
              title: const Text("Confirm Roles"),
              onTap: () {
                //Navigator.popUntil(context, ModalRoute.withName('/homePage'));
                Navigator.pushNamed(context, '/role control');
              }),
          Divider(),
          ListTile(
              leading: const Icon(Icons.home, color: Colors.teal,),
              title: const Text("Addresses"),
              onTap: () {
                //Navigator.popUntil(context, ModalRoute.withName('/homePage'));
                Navigator.pushNamed(context, '/addressesAdmin');
              }),
          Divider(),
          ListTile(
              leading: const Icon(
                Icons.fiber_manual_record, color: Colors.teal,),
              title: const Text("Confirm Addresses"),
              onTap: () {
                //Navigator.popUntil(context, ModalRoute.withName('/homePage'));
                Navigator.pushNamed(context, '/addressesControl');
              }),
          Divider(),
          ListTile(
              leading: const Icon(Icons.add_call, color: Colors.teal,),
              title: const Text("Confirm Phone Numbers"),
              onTap: () {
                //Navigator.popUntil(context, ModalRoute.withName('/homePage'));
                Navigator.pushNamed(context, '/phonesControl');
              }),
          Divider(),
          ListTile(
              leading: const Icon(Icons.email, color: Colors.teal,),
              title: const Text("Confirm Emails"),
              onTap: () {
                //Navigator.popUntil(context, ModalRoute.withName('/homePage'));
                Navigator.pushNamed(context, '/emailsControl');
              }),
          Divider(),
          ListTile(
              leading: const Icon(Icons.link, color: Colors.teal,),
              title: const Text("Add IC Connection"),
              onTap: () {
                //Navigator.popUntil(context, ModalRoute.withName('/homePage'));
                Navigator.pushNamed(context, '/AddICConnection');
              }),
        ]));
  }
}
