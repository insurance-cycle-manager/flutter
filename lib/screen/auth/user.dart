import 'package:medcare/screen/addressAdmin/address.dart';
import 'package:medcare/screen/profile/email/Email.dart';
import 'package:medcare/screen/profile/phoneNumber/number.dart';
import 'package:medcare/screen/profile/role/Role.dart';

class User {
  String username;
  String password;
  List<Role> userRoles;
  String email;
  List<Email> userEmails;
  String token;
  String imagePath;
  String roleName;
  List<Address> userAddresses;
  List<Number> userPhoneNumbers;

  User(
      {this.username: "",
      this.password,
      this.email: "",
      this.imagePath,
      this.userRoles: const [],
        this.userPhoneNumbers: const [],
      this.userEmails: const [],
      this.userAddresses: const []});

  User.userToRegister(this.username, this.email, this.password, this.roleName);

  User.userToLogin(this.username, this.password, {this.userRoles});

  factory User.fromJson(Map<String, dynamic> json) {
    List<Role> _roles = [];
    List<Email> _emails = [];
    List<Address> _addresses = [];
    List<Number> _userPhoneNumbers = [];
    if (json['userRoles'] != null) {
      var roleObjJson = json['userRoles'] as List;
      _roles = roleObjJson.map((roleJson) => Role.fromJson(roleJson)).toList();
    }
    if (json['userEmails'] != null) {
      var emailObjJson = json['userEmails'] as List;
      _emails =
          emailObjJson.map((emailJson) => Email.fromJson(emailJson)).toList();
    }
    if (json['userAddresses'] != null) {
      var addressObjJson = json['userAddresses'] as List;
      _addresses = addressObjJson
          .map((addressJson) => Address.fromJson(addressJson))
          .toList();
    }
    if (json['userPhoneNumbers'] != null) {
      var phoneObjJson = json['userPhoneNumbers'] as List;
      _userPhoneNumbers =
          phoneObjJson.map((phoneJson) => Number.fromJson(phoneJson)).toList();
    }

    return User(
      password: json['password'],
      username: json['userName'] as String,
      email: json['email'] as String,
      userRoles: _roles,
      userEmails: _emails,
      userAddresses: _addresses,
      userPhoneNumbers: _userPhoneNumbers,
    );
  }

  Map<String, dynamic> toJson() => {
        'userName': username,
        'email': email,
        'password': password,
        'userEmails': userEmails,
        'userRoles': userRoles.toString(),
        'userAddresses': userAddresses.toString(),
        'roleName': roleName ?? "",
        'imagePath': imagePath ?? "",
      };
}
