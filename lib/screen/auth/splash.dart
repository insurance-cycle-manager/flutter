import 'dart:async';
import 'dart:math';

import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Splash extends StatefulWidget {
  @override
  SplashScreenState createState() => SplashScreenState();
}

class SplashScreenState extends State<Splash> with TickerProviderStateMixin {
  Animation _arrowAnimation, _heartAnimation;
  AnimationController _arrowAnimationController, _heartAnimationController;

  startTime() async {
    var _duration = Duration(seconds: 1);
    return Timer(_duration, navigationPage);
  }

  void navigationPage() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    myTheme();
    if (prefs.getBool('is_login') == true) {
      Navigator.pushReplacementNamed(context, '/homePage');
    } else {
      Navigator.pushReplacementNamed(context, '/welcome');
    }
  }

  bool isDark;

  void myTheme() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    double value = prefs.getDouble('fontSize') ?? 14;
    if (prefs.getBool('isdark') == null) {
      prefs.setBool('isdark', false);
      DynamicTheme.of(context).setThemeData(ThemeData(
        brightness: Brightness.light,
        primaryColor: Color(0xFF07a383),
        primarySwatch: Colors.teal,
        accentColor: Colors.teal,
        textTheme: TextTheme(
          button: TextStyle(fontSize: value),
          caption: TextStyle(fontSize: value),
        ),
      ));
    } else if (prefs.getBool('isdark') == true) {
      DynamicTheme.of(context).setThemeData(ThemeData(
        brightness: Brightness.dark,
        primaryColor: Color(0xFF07a383),
        primarySwatch: Colors.teal,
        accentColor: Colors.teal,
        textTheme: TextTheme(
          button: TextStyle(fontSize: value),
          caption: TextStyle(fontSize: value),
        ),
      ));
    } else {
      DynamicTheme.of(context).setThemeData(ThemeData(
        brightness: Brightness.light,
        primaryColor: Color(0xFF07a383),
        primarySwatch: Colors.teal,
        accentColor: Colors.teal,
        textTheme: TextTheme(
          button: TextStyle(fontSize: value),
          caption: TextStyle(fontSize: value),
        ),
      ));
    }
  }

  @override
  void initState() {
    super.initState();
    startTime();
    _arrowAnimationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));
    _arrowAnimation =
        Tween(begin: 0.0, end: pi).animate(_arrowAnimationController);

    _heartAnimationController = AnimationController(
        vsync: this, duration: Duration(milliseconds: 1200));
    _heartAnimation = Tween(begin: 150.0, end: 170.0).animate(CurvedAnimation(
        curve: Curves.bounceOut, parent: _heartAnimationController));

    _heartAnimationController.addStatusListener((AnimationStatus status) {
      if (status == AnimationStatus.completed) {
        _heartAnimationController.repeat();
      }
    });
    _heartAnimationController.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          secondChild(),
        ],
      ),
    );
  }

  Widget secondChild() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Expanded(
          child: AnimatedBuilder(
            animation: _heartAnimationController,
            builder: (context, child) {
              return Center(
                  child: Image.asset(
                'assets/images/logo.png',
                width: _heartAnimation.value,
                height: _heartAnimation.value,
              ));
            },
          ),
        ),
      ],
    );
  }

  @override
  void dispose() {
    _arrowAnimationController?.dispose();
    _heartAnimationController?.dispose();
    super.dispose();
  }
}
