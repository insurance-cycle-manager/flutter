import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:medcare/components/customButton.dart';
import 'package:medcare/components/customTextField.dart';
import 'package:medcare/networking/httpAuth.dart' as auth;
import 'package:medcare/screen/auth/user.dart';

class Login extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _LoginState();
  }
}

class _LoginState extends State<Login> {
  final usernameCont = TextEditingController();
  final passwordCont = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top: 90.0),
                ),
                CustomTextField(
                  icon: Icons.email,
                  validationFun: (value) {
                    if (value.isEmpty) {
                      return 'can\'t be empty';
                    } else
                      return null;
                  },
                  controller: usernameCont,
                  labelText: 'Username',
                  keyboardType: TextInputType.emailAddress,
                ),
                CustomTextField(
                  labelText: "Password",
                  obscureText: true,
                  validationFun: (value) {
                    if (value.isEmpty) {
                      return 'can\'t be empty';
                    } else if (value.length < 8) {
                      return 'The password is short';
                    } else
                      return null;
                  },
                  controller: passwordCont,
                  icon: Icons.lock,
                ),
                CustomButton(
                  bName: "LOGIN",
                  bFunc: () async {
                    FocusScope.of(context).requestFocus(FocusNode());
                    if (_formKey.currentState.validate()) {
                      login(context);

//                      var loginResponse = await auth.login(User.userToLogin(
//                          usernameCont.text.trim(), passwordCont.text.trim()),context);
//                      if (loginResponse != null) {
//                        await auth.setMobileToken(loginResponse);
//                        print(loginResponse);
//
//                        Navigator.pushReplacementNamed(context, '/homePage');
//                      }
                    }
                  },
                  bTextColor: Colors.white,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 20.0),
                      child: FlatButton(
                        child: Text(
                          "Forgot password?",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 15.0,
                          ),
                          textAlign: TextAlign.center,
                        ),
                        onPressed: () {},
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 20.0),
                      child: FlatButton(
                        child: Text(
                          "Create New Account",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.lightGreen,
                            fontSize: 16.0,
                            decoration: TextDecoration.underline,
                          ),
                          textAlign: TextAlign.end,
                        ),
                        onPressed: () {
                          Navigator.pushReplacementNamed(context, '/signup');
                        },
                      ),
                    ),
                  ],
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  margin:
                      const EdgeInsets.only(left: 30.0, right: 30.0, top: 20.0),
                  alignment: Alignment.center,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.all(8.0),
                          decoration:
                              BoxDecoration(border: Border.all(width: 0.25)),
                        ),
                      ),
                      Text(
                        "OR CONNECT WITH",
                        style: TextStyle(
                          color: Colors.grey,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.all(8.0),
                          decoration:
                          BoxDecoration(border: Border.all(width: 0.25)),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  width: MediaQuery
                      .of(context)
                      .size
                      .width,
                  margin: const EdgeInsets.fromLTRB(30.0, 100.0, 30.0, 0),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: FlatButton(
                          onPressed: () {},
                          child: Image.asset(
                            'assets/images/facebook.png',
                            width: 50,
                          ),
                        ),
                      ),
                      Expanded(
                        child: FlatButton(
                          onPressed: () {},
                          child: Image.asset(
                            'assets/images/gmail.png',
                            width: 50,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  width: MediaQuery
                      .of(context)
                      .size
                      .width,
                  height: 30,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _showLoadingDialog() {
    showDialog(
      context: context,
      builder: (context) {
        return WillPopScope(
          onWillPop: () async => false,
          child: Center(
            child: SpinKitCircle(
              //https://github.com/jogboms/flutter_spinkit#-showcase
              color: Colors.teal,
            ),
          ),
        );
      },
    );
  }

  void login(BuildContext context) async {
    _showLoadingDialog();
    var loginResponse = await auth.login(
        User.userToLogin(usernameCont.text, passwordCont.text), context);
    if (loginResponse != null) {
      await auth.setMobileToken(loginResponse);
    }

    Navigator.pop(context);
    Navigator.pushReplacementNamed(
        context, '/homePage');
    //return loginResponse;
  }
}
