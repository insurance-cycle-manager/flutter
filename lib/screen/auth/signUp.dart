import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:medcare/components/customButton.dart';
import 'package:medcare/components/customTextField.dart';
import 'package:medcare/networking/httpAuth.dart' as auth;
import 'package:medcare/screen/auth/user.dart';

class SignUp extends StatefulWidget {
  final emailCont = TextEditingController();
  final usernameCont = TextEditingController();
  final passwordCont = TextEditingController();
  final confPasswordCont = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  @override
  State createState() {
    return _SignUp();
  }
}

class _SignUp extends State<SignUp> {
  List<String> _roles = []; // Option 2
  String _selectedRole;

  @override
  void initState() {
    load();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: SingleChildScrollView(
        child: Form(
          key: widget._formKey,
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 100.0),
              ),
              CustomTextField(
                labelText: "Username",
                validationFun: (value) {
                  if (value.isEmpty) {
                    return 'can\'t be empty';
                  } else
                    return null;
                },
                controller: widget.usernameCont,
                icon: Icons.person,
              ),
              CustomTextField(
                labelText: "Email",
                validationFun: (value) {
                  if (value.isEmpty) {
                    return 'can\'t be empty';
                  } else
                    return null;
                },
                controller: widget.emailCont,
                icon: Icons.email,
                keyboardType: TextInputType.emailAddress,
              ),
              CustomTextField(
                labelText: "Password",
                obscureText: true,
                validationFun: (value) {
                  if (value.isEmpty) {
                    return 'can\'t be empty';
                  } else if (value.length < 8) {
                    return 'The password is short';
                  } else
                    return null;
                },
                controller: widget.passwordCont,
                icon: Icons.lock,
              ),
              CustomTextField(
                labelText: "Confirm Password",
                obscureText: true,
                validationFun: (value) {
                  if (value.isEmpty) {
                    return 'can\'t be empty';
                  } else
                    return null;
                },
                controller: widget.confPasswordCont,
                icon: Icons.lock,
              ),
//              Container(
//                padding: EdgeInsets.only(left: 50.0, right: 40.0),
//                child: DropdownButtonFormField<String>(
//                  value: _selectedRole,
//                  isExpanded: true,
//                  hint: Text('Role'),
//                  icon: Icon(Icons.keyboard_arrow_down),
//                  iconSize: 24,
//                  style: TextStyle(color: Colors.green),
//                  onChanged: (String newValue) {
//                    setState(() {
//                      _selectedRole = newValue;
//                    });
//                  },
//                  validator: (value) => value == null ? 'field required' : null,
//                  items: _roles.map((role) {
//                    return DropdownMenuItem(
//                      child: Text(role),
//                      value: role,
//                    );
//                  }).toList(),
//                ),
//              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 20.0, top: 15.0),
                    child: FlatButton(
                      child: Text(
                        "Already have an account?",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.lightGreen,
                          fontSize: 15.0,
                        ),
                        textAlign: TextAlign.end,
                      ),
                      onPressed: () {
                        Navigator.pushReplacementNamed(context, '/login');
                      },
                    ),
                  ),
                ],
              ),
              CustomButton(
                bName: "Sign UP",
                bFunc: () {
                  FocusScope.of(context).requestFocus(FocusNode());

                  if (widget.confPasswordCont.text !=
                      widget.passwordCont.text) {
                    Fluttertoast.showToast(
                        msg: "Conformation dosen't match",
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.BOTTOM,
                        timeInSecForIos: 1,
                        backgroundColor: Colors.redAccent,
                        textColor: Colors.white,
                        fontSize: 16.0);
                  } else if (widget._formKey.currentState.validate()) {
                    var signUpResponse = auth.register(User.userToRegister(
                        widget.usernameCont.text,
                        widget.emailCont.text,
                        widget.passwordCont.text,
                        "User"), context);
                    signUpResponse.then((e) {
                      if (e != null)
                        Navigator.pushReplacementNamed(context, '/login');
                    });
                    if (signUpResponse != null) {
                      Navigator.pushReplacementNamed(context, '/login');
                    }
                  }
                },
                bTextColor: Colors.white,
              ),
              SizedBox(
                height: 31,
              )
            ],
          ),
        ),
      ),
    );
  }

  void load() async {
//    var listResponse =
//        await client.getPublicData(DotEnv().env['GET_ROLES_SIGNUP'], {});
//
//    final parsed = jsonDecode(listResponse.body);
//    _roles =
//        (parsed["data"] as List).map<String>((json) => json['name']).toList();
//    setState(() {
//      _roles = List.from(_roles);
//      if (_roles.isEmpty) {
//        _roles.add('Empty');
//      }
//    });
  }
}
