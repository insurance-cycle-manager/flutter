import 'package:flutter/material.dart';
import 'package:medcare/components/customButton.dart';

class WelcomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Container(
      color: Colors.white,
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
                padding: EdgeInsets.only(top: 120.0),
                child: FlatButton(
                  onPressed: () {},
                  child: Image.asset(
                    'assets/images/logo.png',
                    width: 157,
                  ),
                )),
            Container(
              padding: EdgeInsets.only(top: 10.0, bottom: 150.0),
              /*child: Text(
                'MedCare',
                style: TextStyle(
                  color: Colors.green[700],
                  fontSize: 20.0,
                ),
              )*/
            ),
            CustomButton(
                bTextColor: Colors.white,
                bName: "SIGN UP",
                bFunc: () {
                  Navigator.pushReplacementNamed(context, '/signup');
                }),
            CustomButton(
                bTextColor: Colors.white,
                bName: "LOGIN",
                bFunc: () {
                  Navigator.pushReplacementNamed(context, '/login');
                }),
            SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 30,
            ),
          ],
        ),
      ),
    ));
  }
}
