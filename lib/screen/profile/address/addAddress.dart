import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:medcare/components/customAppBar.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/addressAdmin/address.dart';
import 'package:medcare/screen/unauthorized.dart';

class AddUserAddress extends StatefulWidget {
  @override
  State createState() {
    return _AddUserAddressState();
  }
}

class _AddUserAddressState extends State<AddUserAddress> {
  final title = TextEditingController();
  final longitude = TextEditingController();
  final latitude = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _searchQuery = new TextEditingController();
  bool _isSearching;
  String _searchText = "";
  List<String> codes = [];
  List<Address> addresses = [];

  _AddUserAddressState() {
    _searchQuery.addListener(() {
      if (_searchQuery.text.isEmpty) {
        setState(() {
          _isSearching = false;
          _searchText = "";
        });
      } else {
        setState(() {
          _isSearching = true;
          _searchText = _searchQuery.text;
        });
      }
    });
  }

  @override
  void initState() {
    load();
    _isSearching = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        title: "Add address",
        actionIcon: Icon(Icons.search),
        isSearching: _isSearching,
        searchText: _searchText,
        searchQuery: _searchQuery,
      ),
      body: Form(
          key: _formKey,
          child: ListView.builder(
              itemCount: addresses.length + 1,
              itemBuilder: _isSearching ? _buildSearchList : _buildList)),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          RawMaterialButton(
            fillColor: Colors.teal,
            splashColor: Colors.grey,
            shape: StadiumBorder(),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 8.0),
              child: Text(
                " Submit ",
                style: TextStyle(color: Colors.white),
              ),
            ),
            onPressed: () {
              codes.forEach((c) {
                var json = {
                  "AddressCode": addresses
                      .firstWhere((element) => element.title == c)
                      .addressCode,
                };
                client.postWithTokenJSON(
                    json, DotEnv().env['POST_USER_Address'], context);
              });

              Navigator.popUntil(context, ModalRoute.withName('/profile'));
              Navigator.pushReplacementNamed(context, '/profile');
            },
          ),
        ],
      ),
    );
  }

  Widget _buildList(BuildContext context, int index) {
    if (index == addresses.length)
      return SizedBox(
        height: 45,
      );
    return CheckboxListTile(
      title: Text(addresses[index].title),
      value: codes.contains(addresses[index].title),
      onChanged: (bool value) {
        if (value)
          codes.add(addresses[index].title);
        else
          codes.remove(addresses[index].title);
        setState(() {});
      },
    );
  }

  Widget _buildSearchList(BuildContext context, int index) {
    if (index == addresses.length)
      return SizedBox(
        height: 30,
      );
    if (_searchText.isEmpty) {
      return CheckboxListTile(
        title: Text(addresses[index].title),
        value: codes.contains(addresses[index].addressCode),
        onChanged: (bool value) {
          if (value)
            codes.add(addresses[index].addressCode);
          else
            codes.remove(addresses[index].addressCode);
          setState(() {});
        },
      );
    } else {
      String name = addresses[index].title;
      if (name.toLowerCase().contains(_searchText.toLowerCase())) {
        return CheckboxListTile(
          title: Text(addresses[index].title),
          value: codes.contains(addresses[index].addressCode),
          onChanged: (bool value) {
            if (value)
              codes.add(addresses[index].addressCode);
            else
              codes.remove(addresses[index].addressCode);
            setState(() {});
          },
        );
      }
    }
    return Container(
      height: 0,
    );
  }

  void load() async {
    dynamic response;
    try {
      response =
      await client.getPrivateData(DotEnv().env['GET_USER_Addresses'], context);
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
      return;
    }

    final parsed = await jsonDecode(response.body);
    List<Address> _userAddresses = [];

    _userAddresses = (parsed["data"] as List)
        .map<Address>((json) => Address.fromJson(json['address']))
        .toList();

    // load all addresses
    try {
      response =
      await client.getPrivateData(DotEnv().env['GET_ADDRESSES'], context);
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
      return;
    }

    final parsed1 = await jsonDecode(response.body);
    addresses = (parsed1["data"] as List)
        .map<Address>((json) => Address.fromJson(json))
        .toList();

    _userAddresses.forEach((element) {
      addresses.removeWhere((e) => e.title == element.title);
    });
    setState(() {});
  }
}
