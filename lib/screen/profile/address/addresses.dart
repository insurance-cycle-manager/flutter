import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/auth/user.dart';
import 'package:medcare/screen/addressAdmin/address.dart';
import 'package:medcare/screen/unauthorized.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AddressesPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _AddressesPageState();
  }
}

class _AddressesPageState extends State<AddressesPage> {
  User user;
  List<Address> _addresses = [];
  final SlidableController slidableController = SlidableController();

  @override
  void initState() {
    loadUserInfo();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
        itemBuilder: _buildList,
        itemCount: _addresses.length + 1,
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.pushNamed(context, '/add user address');
        },
      ),
    );
  }

  Widget _buildList(BuildContext context, int index) {
    if (index == _addresses.length)
      return SizedBox(
        height: 17,
      );
    return Padding(
      padding: const EdgeInsets.only(top: 1, bottom: 1),
      child: Slidable(
        actionPane: SlidableDrawerActionPane(),
        controller: slidableController,
        actionExtentRatio: 0.25,
        child: Container(
          color: Colors.white,
          child: ListTile(
            leading: CircleAvatar(
              backgroundColor: Colors.teal,
              child: Text('${index + 1}'),
              foregroundColor: Colors.white,
            ),
            title: Text(_addresses[index].title),
          ),
        ),
        actions: <Widget>[],
        secondaryActions: <Widget>[
          IconSlideAction(
            caption: 'Delete',
            color: Colors.red,
            icon: Icons.delete,
            onTap: () {
              String url = DotEnv().env['DELETE_USER_Address'];
              url =
                  url.replaceAll("ADDRESSCODE", _addresses[index].addressCode);
              client.deleteObject(url, context, '/profile');
            },
          ),
        ],
      ),
    );
  }

  void loadUserInfo() async {
    String url = DotEnv().env['GET_USER_Addresses'];

    dynamic response;
    try {
      response = await client.getPrivateData(url, context);
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
      return;
    }

    final parsed = await jsonDecode(response.body);

    _addresses = (parsed["data"] as List)
        .map<Address>((json) => Address.fromJson(json['address']))
        .toList();
    if (_addresses.isEmpty) {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      user = User.fromJson(jsonDecode(prefs.getString('ACC_INFO')));
      _addresses = user.userAddresses;
    }
    setState(() {});
  }
}
