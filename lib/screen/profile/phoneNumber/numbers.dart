import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/auth/user.dart';
import 'package:medcare/screen/profile/phoneNumber/number.dart';
import 'package:medcare/screen/unauthorized.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NumbersPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _NumbersPageState();
  }
}

class _NumbersPageState extends State<NumbersPage> {
  User user;
  List<Number> _numbers = [];
  final SlidableController slidableController = SlidableController();

  @override
  void initState() {
    loadUserInfo();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
        itemBuilder: _buildList,
        itemCount: _numbers.length + 1,
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.pushNamed(context, '/addPhone');
        },
      ),
    );
  }

  Widget _buildList(BuildContext context, int index) {
    if (index == _numbers.length)
      return SizedBox(
        height: 17,
      );
    return Padding(
      padding: const EdgeInsets.only(top: 1, bottom: 1),
      child: Slidable(
        actionPane: SlidableDrawerActionPane(),
        controller: slidableController,
        actionExtentRatio: 0.25,
        child: Container(
          color: Colors.white,
          child: ListTile(
            leading: CircleAvatar(
              backgroundColor: Colors.teal,
              child: Text('${index + 1}'),
              foregroundColor: Colors.white,
            ),
            title: Text(_numbers[index].number),
          ),
        ),
        actions: <Widget>[],
        secondaryActions: <Widget>[
          IconSlideAction(
            caption: 'Delete',
            color: Colors.red,
            icon: Icons.delete,
            onTap: () {
              String url = DotEnv().env['DELETE_PHONE'];
              url = url.replaceAll("PHONECODE", _numbers[index].phoneCode);
              client.deleteObject(url, context, '/profile');
            },
          ),
        ],
      ),
    );
  }

  void loadUserInfo() async {
    String url = DotEnv().env['GET_PHONES'];

    dynamic response;
    try {
      response = await client.getPrivateData(url, context);
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
      return;
    }

    final parsed = await jsonDecode(response.body);

    _numbers = (parsed["data"] as List)
        .map<Number>((json) => Number.fromJson(json))
        .toList();

    if (_numbers.isEmpty) {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      user = User.fromJson(jsonDecode(prefs.getString('ACC_INFO')));
      _numbers = user.userPhoneNumbers;
    }
    setState(() {});
  }
}
