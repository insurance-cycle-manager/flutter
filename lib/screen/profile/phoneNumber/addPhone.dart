import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:medcare/components/customButton.dart';
import 'package:medcare/components/customTextField.dart';
import 'package:medcare/networking/httpClient.dart' as client;

class AddPhone extends StatefulWidget {
  final number = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  State createState() {
    return _AddPhoneState();
  }
}

class _AddPhoneState extends State<AddPhone> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add Phone Number"),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.only(top: 40),
        child: Form(
          key: widget._formKey,
          child: Wrap(
            runSpacing: 25,
            children: <Widget>[
              CustomTextField(
                labelText: "Number",
                validationFun: (value) {
                  if (value.isEmpty) {
                    return 'can\'t be empty';
                  } else
                    return null;
                },
                controller: widget.number,
                icon: Icons.phone,
                keyboardType: TextInputType.phone,
              ),
              CustomButton(
                bName: "Submit",
                bFunc: () {
                  var json = {
                    "number": widget.number.text.trim(),
                  };
                  client.postWithTokenJSON(
                      json, DotEnv().env['POST_PHONE'], context);

                  Navigator.popUntil(context, ModalRoute.withName('/profile'));
                  Navigator.pushReplacementNamed(context, '/profile');
                },
                bTextColor: Colors.white,
              )
            ],
          ),
        ),
      ),
    );
  }
}
