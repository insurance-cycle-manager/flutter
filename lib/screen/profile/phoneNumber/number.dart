class Number {
  String number;
  String phoneCode;
  bool confirmed;

  Number({this.number: "", this.confirmed, this.phoneCode});

  Map<String, dynamic> toJson() {
    return {
      "number": number,
      "phoneCode": phoneCode,
      "confirmed": confirmed,
    };
  }

  Number.fromJson(Map<String, dynamic> json)
      : number = json['number'],
        phoneCode = json['phoneCode'],
        confirmed = json['confirmed'];
}
