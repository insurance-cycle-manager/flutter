import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/auth/user.dart';
import 'package:medcare/screen/profile/role/Role.dart';
import 'package:medcare/screen/unauthorized.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RolesPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _RolesPageState();
  }
}

class _RolesPageState extends State<RolesPage> {
  User user;
  List<Role> _roles = [];
  List<String> rolesName = [];
  final SlidableController slidableController = SlidableController();
  Future<void> _listFuture;
  Role activeRole;

  @override
  void initState() {
//    loadUserInfo();
    _listFuture = updateAndGetList();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder<void>(
        future: _listFuture,
        builder: (BuildContext context, AsyncSnapshot<void> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return SpinKitCircle(
              color: Colors.teal,
            );
          } else if (snapshot.hasError) {
            return Scaffold(
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 250,
                    ),
                    Icon(Icons.error),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[Text("Something went wrong")],
                    ),
                    Expanded(
                      child: Text(
                        "Please check your internet connection",
                        style: TextStyle(fontSize: 15),
                      ),
                    )
                  ],
                ),
              ),
            );
          } else {
            return Scaffold(
              body: ListView.builder(
                itemBuilder: _buildList,
                itemCount: _roles.length + 1,
              ),
              floatingActionButton: FloatingActionButton(
                child: Icon(Icons.add),
                onPressed: () {
                  Navigator.pushNamed(context, '/addRole', arguments: user);
                },
              ),
            );
          }
        },
      ),
    );
  }

  Future<void> refreshList() {
    setState(() {
      _listFuture = updateAndGetList();
    });
  }

  Future<void> updateAndGetList() async {
    activeRole = await client.getActiveRole();

    String url = DotEnv().env['GET_ROLES'];

    dynamic response;
    try {
      response = await client.getPrivateData(url, context);
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
      return;
    }

    final parsed = await jsonDecode(response.body);

    _roles = (parsed["data"] as List)
        .map<Role>((json) => Role.fromJsonToProfile(json))
        .toList();
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    user = User.fromJson(jsonDecode(prefs.getString('ACC_INFO')));
    if (_roles.isEmpty) {
      _roles = user.userRoles;
    }
  }

  Widget _buildList(BuildContext context, int index) {
    if (index == _roles.length)
      return SizedBox(
        height: 17,
      );
    return Padding(
      padding: const EdgeInsets.only(top: 1, bottom: 1),
      child: Slidable(
        actionPane: SlidableDrawerActionPane(),
        controller: slidableController,
        actionExtentRatio: 0.25,
        child: Container(
          color: Colors.white,
          child: ListTile(
            leading: CircleAvatar(
              backgroundColor: Colors.teal,
              child: Text('${index + 1}'),
              foregroundColor: Colors.white,
            ),
            title: Text(_roles[index].roleName),
            subtitle: isActiveRole(index)
                ? Text(
                    "Active",
                    style: TextStyle(color: Colors.green),
                  )
                : Container(),
            onTap: () {
              if (!isActiveRole(index))
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: Text(
                        "Active Role",
                      ),
                      content: Text("Do you want to activate this role?"),
                      actions: <Widget>[
                        FlatButton(
                          child: Text(
                            "Yes",
                            style: TextStyle(
                              color: Color(0xFF07a383),
                            ),
                          ),
                          onPressed: () async {
                            client.setActiveRole(_roles[index]);
                            Navigator.pop(context);
                            setState(() {
                              refreshList();
                            });
                          },
                        ),
                        FlatButton(
                          child: Text(
                            "No",
                            style: TextStyle(
                              color: Colors.red,
                            ),
                          ),
                          onPressed: () => Navigator.pop(context),
                        ),
                      ],
                    );
                  },
                );
            },
          ),
        ),
        actions: <Widget>[],
        secondaryActions: <Widget>[
          IconSlideAction(
            caption: 'Delete',
            color: Colors.red,
            icon: Icons.delete,
            onTap: () {
              deleteRole(_roles[index].roleName);
            },
          ),
        ],
      ),
    );
  }

  bool isActiveRole(int index) {
    return _roles[index].roleName == activeRole.roleName;
  }

  Future<String> deleteRole(String roleName) async {
    String url = DotEnv().env['DELETE_ROLE'];
    url = url.replaceAll("ROLENAME", roleName.toUpperCase());
    return client.deleteObject(url, context, "/roles");
  }
}
