import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:medcare/components/customButton.dart';
import 'package:medcare/components/customTextField.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/addressAdmin/address.dart';
import 'package:medcare/screen/auth/user.dart';

class AddRole extends StatefulWidget {
  @override
  State createState() {
    return _AddRoleState();
  }
}

class _AddRoleState extends State<AddRole> {
  final title = TextEditingController();
  final username = TextEditingController();
  final email = TextEditingController();

  //final addressCode = TextEditingController();
  final phoneNumber = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  String roleName;
  List<String> _roles = []; // Option 2
  User _user;

  String addressTitle;
  String addressCode;
  List<Address> _addresses = [];

  @override
  void initState() {
    load();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _user = ModalRoute
        .of(context)
        .settings
        .arguments;

    return Scaffold(
      appBar: AppBar(
        title: Text("Add Role"),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.only(top: 40),
        child: Form(
          key: _formKey,
          child: Wrap(
            runSpacing: 25,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(30.0, 10.0, 20.0, 10.0),
                child: DropdownButtonFormField<String>(
                  value: roleName,
                  hint: Text('Role'),
                  icon: Icon(Icons.arrow_downward),
                  iconSize: 24,
                  style: TextStyle(color: Colors.teal),
                  onChanged: (String newValue) {
                    setState(() {
                      roleName = newValue;
                    });
                  },
                  validator: (value) => value == null ? 'field required' : null,
                  items: _roles.map((role) {
                    return DropdownMenuItem(
                      child: Text(role),
                      value: role,
                    );
                  }).toList(),
                ),
              ),
              CustomTextField(
                labelText: "Title",
                controller: title,
              ),
              CustomTextField(
                labelText: "Email",
                controller: email,
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(30.0, 10.0, 20.0, 10.0),
                child: DropdownButtonFormField<String>(
                  value: addressTitle,
                  isExpanded: true,
                  hint: Text('Address', overflow: TextOverflow.ellipsis,),
                  icon: Icon(Icons.arrow_downward),
                  iconSize: 24,
                  style: TextStyle(color: Colors.teal),
                  onChanged: (String newValue) {
                    setState(() {
                      addressTitle = newValue;
                      _addresses.forEach((e) {
                        if (e.title == newValue) {
                          addressCode = e.addressCode;
                        }
                      });
                    });
                  },
                  validator: (value) => value == null ? 'field required' : null,
                  items: _addresses.map((address) {
                    return DropdownMenuItem(
                      child: Text(address.title, overflow: TextOverflow.fade,),
                      value: address.title,
                    );
                  }).toList(),
                ),
              ),
              CustomTextField(
                labelText: "Phone Number",
                controller: phoneNumber,
              ),
              CustomButton(
                bName: "Submit",
                bFunc: () {
                  var json = {
                    "RoleName": roleName.trim(),
                    "Title": title.text,
                    "PreferredEmail": email.text.trim(),
                    "PreferredAddressCode": addressCode,
                    "PreferredPhoneNumber": phoneNumber.text.trim(),
                  };
                  client.postWithTokenJSON(
                      json, DotEnv().env['ADD_ROLE'], context);

                  Navigator.popUntil(context, ModalRoute.withName('/profile'));
                  //Navigator.pop(context);
                  Navigator.pushNamed(context, '/profile');
                },
                bTextColor: Colors.white,
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<String> load() async {
    //load roles
    var listResponse =
    await client.getPublicData(DotEnv().env['GET_ROLES_SIGNUP'], {}, context);

    final parsed = jsonDecode(listResponse.body);
    _roles =
        (parsed["data"] as List).map<String>((json) => json['name']).toList();

    _user.userRoles.forEach((e) {
      if (_roles.contains(e.roleName)) _roles.remove(e.roleName);
    });

    // load addresses

    listResponse =
    await client.getPrivateData(DotEnv().env['GET_USER_Addresses'], context);

    final parsed1 = jsonDecode(listResponse.body);
    _addresses = (parsed1["data"] as List)
        .map<Address>((json) => Address.fromJson(json['address']))
        .toList();

    setState(() {
      _roles = List.from(_roles);
      if (_roles.isEmpty) {
        _roles.add('Empty');
      }
    });
  }
}
