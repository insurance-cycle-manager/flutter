import 'package:medcare/screen/profile/address/Permission.dart';

class Role {
  String roleName;
  List<Permission> permissions;
  bool confirmed;

  Role({this.roleName: "", this.permissions: const [], this.confirmed: false});

  Map<String, dynamic> toJson() => {
        'roleName': roleName,
        'permissions': permissions,
        'confirmed': confirmed,
      };

  factory Role.fromJson(Map<String, dynamic> json) {
    List<Permission> _permissions = [];
    if (json['permissions'] != null) {
      var permissionsObjJson = json['permissions'] as List;
      _permissions =
          permissionsObjJson.map((e) => Permission.fromJson(e)).toList();
    }

    return Role(
        roleName: json['roleName'],
        permissions: _permissions,
        confirmed: json['confirmed'] ?? false);
  }

  factory Role.fromJsonToProfile(Map<String, dynamic> json) {
    return Role(
      roleName: json['role']['name'],
    );
  }
}
