class Email {
  String email;
  String code;

  Email({this.email: "", this.code: ""});

  Map<String, dynamic> toJson() {
    return {
      "email": email,
      "code": code,
    };
  }

  Email.fromJson(Map<String, dynamic> json)
      : email = json["address"],
        code = json['emailCode'];
}
