import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:medcare/components/customButton.dart';
import 'package:medcare/components/customTextField.dart';
import 'package:medcare/networking/httpClient.dart' as client;

class AddEmail extends StatefulWidget {
  final address = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  State createState() {
    return _AddEmailState();
  }
}

class _AddEmailState extends State<AddEmail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add Email"),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.only(top: 40),
        child: Form(
          key: widget._formKey,
          child: Wrap(
            runSpacing: 25,
            children: <Widget>[
              CustomTextField(
                labelText: "Email",
                validationFun: (value) {
                  if (value.isEmpty) {
                    return 'can\'t be empty';
                  } else
                    return null;
                },
                controller: widget.address,
                icon: Icons.email,
                keyboardType: TextInputType.emailAddress,
              ),
              CustomButton(
                bName: "Submit",
                bFunc: () {
                  var json = {
                    "address": widget.address.text.trim(),
                  };
                  client.postWithTokenJSON(
                      json, DotEnv().env['POST_EMAIL'], context);

                  Navigator.popUntil(context, ModalRoute.withName('/profile'));
                  Navigator.pushReplacementNamed(context, '/profile');
                },
                bTextColor: Colors.white,
              )
            ],
          ),
        ),
      ),
    );
  }
}
