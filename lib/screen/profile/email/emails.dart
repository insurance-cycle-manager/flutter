import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/auth/user.dart';
import 'package:medcare/screen/profile/email/Email.dart';
import 'package:medcare/screen/unauthorized.dart';
import 'package:shared_preferences/shared_preferences.dart';

class EmailsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _EmailsPageState();
  }
}

class _EmailsPageState extends State<EmailsPage> {
  User user;
  List<Email> _emails = [];
  final SlidableController slidableController = SlidableController();

  @override
  void initState() {
    loadUserInfo();
    super.initState();
    ;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
        itemBuilder: _buildList,
        itemCount: _emails.length + 1,
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.pushNamed(context, '/addEmail');
        },
      ),
    );
  }

  Widget _buildList(BuildContext context, int index) {
    if (index == _emails.length)
      return SizedBox(
        height: 17,
      );
    return Padding(
      padding: const EdgeInsets.only(top: 1, bottom: 1),
      child: Slidable(
        actionPane: SlidableDrawerActionPane(),
        controller: slidableController,
        actionExtentRatio: 0.25,
        child: Container(
          color: Colors.white,
          child: ListTile(
            leading: CircleAvatar(
              backgroundColor: Colors.teal,
              child: Text('${index + 1}'),
              foregroundColor: Colors.white,
            ),
            title: Text(_emails[index].email),
          ),
        ),
        actions: <Widget>[],
        secondaryActions: <Widget>[
          IconSlideAction(
            caption: 'Delete',
            color: Colors.red,
            icon: Icons.delete,
            onTap: () {
              String url = DotEnv().env['DELETE_EMAIL'];
              url = url.replaceAll("EMAILCODE", _emails[index].code);
              client.deleteObject(url, context, '/profile');
            },
          ),
        ],
      ),
    );
  }

  void loadUserInfo() async {
    String url = DotEnv().env['GET_EMAILS'];

    dynamic response;
    try {
      response = await client.getPrivateData(url, context);
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
      return;
    }

    final parsed = await jsonDecode(response.body);

    _emails = (parsed["data"] as List)
        .map<Email>((json) => Email.fromJson(json))
        .toList();

    if (_emails.isEmpty) {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      user = User.fromJson(jsonDecode(prefs.getString('ACC_INFO')));
      _emails = user.userEmails;
    }
    setState(() {});
  }
}
