import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:medcare/screen/profile/address/addresses.dart';
import 'package:medcare/screen/profile/role/roles.dart';

import 'email/emails.dart';
import 'mainPage.dart';
import 'phoneNumber/numbers.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage>
    with SingleTickerProviderStateMixin {
  int _currentIndex = 0;
  PageController _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController(initialPage: 0);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Profile"),
      ),
      body: PageView(
        controller: _pageController,
        onPageChanged: (index) {
          setState(() => _currentIndex = index);
        },
        children: <Widget>[
          MainProfile(),
          RolesPage(),
          EmailsPage(),
          AddressesPage(),
          NumbersPage(),
        ],
      ),
      bottomNavigationBar: BottomNavyBar(
        selectedIndex: _currentIndex,
        showElevation: true, // use this to remove appBar's elevation
        onItemSelected: (index) =>
            setState(() {
              _currentIndex = index;
              /*_pageController.animateToPage(index,
              duration: Duration(milliseconds: 300), curve: Curves.ease);*/
              _pageController.jumpToPage(index);
            }),
        items: [
          BottomNavyBarItem(
            icon: Icon(Icons.apps),
            title: Text('Profile'),
            activeColor: Colors.teal,
          ),
          BottomNavyBarItem(
              icon: Icon(Icons.people),
              title: Text('Roles'),
              activeColor: Colors.teal),
          BottomNavyBarItem(
              icon: Icon(Icons.email),
              title: Text('Emails'),
              activeColor: Colors.teal),
          BottomNavyBarItem(
              icon: Icon(Icons.location_on),
              title: Text('Addes'),
              activeColor: Colors.teal),
          BottomNavyBarItem(
              icon: Icon(Icons.call),
              title: Text('Numbers'),
              activeColor: Colors.teal),
        ],
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
