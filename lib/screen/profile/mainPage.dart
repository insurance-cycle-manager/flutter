import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:image_picker/image_picker.dart';
import 'package:medcare/components/customEditButton.dart';
import 'package:medcare/components/customeTFEdit.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/addressAdmin/address.dart';
import 'package:medcare/screen/auth/user.dart';
import 'package:medcare/screen/fullScreen.dart';
import 'package:medcare/screen/unauthorized.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MainProfile extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MainProfileState();
  }
}

class _MainProfileState extends State<MainProfile> {
  bool _status = true;
  TextEditingController nameCont = TextEditingController();
  TextEditingController emailCont = TextEditingController();
  File _image;
  User user;
  final SlidableController slidableController = SlidableController();

  List<Address> _addresses;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder<String>(
        future: loadUserInfo(),
        builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return PageView(
              children: <Widget>[
                Container(
                  color: Colors.white,
                  child: ListView(
                    children: <Widget>[
                      Container(
                        height: 250.0,
                        color: Colors.white,
                        child: Padding(
                          padding: EdgeInsets.only(top: 45.0),
                          child: Stack(fit: StackFit.loose, children: <Widget>[
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  width: 140.0,
                                  height: 140.0,
                                  child: InkWell(
                                    child: CircleAvatar(
                                      backgroundImage: _image == null
                                          ? AssetImage(
                                              "assets/images/person.jpeg")
                                          : FileImage(_image),
                                    ),
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  FullScreenPage(_image)));
                                    },
                                  ),
                                ),
                              ],
                            ),
                            Padding(
                                padding:
                                    EdgeInsets.only(top: 90.0, right: 100.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    InkWell(
                                      child: CircleAvatar(
                                        backgroundColor: Colors.teal,
                                        radius: 25.0,
                                        child: Icon(
                                          Icons.camera_alt,
                                          color: Colors.white,
                                        ),
                                      ),
                                      onTap: () {
                                        mainBottomSheet(context);
                                      },
                                    )
                                  ],
                                )),
                          ]),
                        ),
                      ),
                      Container(
                        color: Color(0xffFFFFFF),
                        child: Padding(
                          padding: EdgeInsets.only(bottom: 25.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              CustomTFEdit(
                                label: "Name",
                                textCont: nameCont,
                              ),
                              CustomTFEdit(
                                label: "Email",
                                textCont: emailCont,
                                status: _status,
                              ),
                              !_status
                                  ? CustomEditButton(
                                      onSave: () {
                                        //TODO Edit profile
                                        setState(() {
                                          _status = true;
                                          FocusScope.of(context)
                                              .requestFocus(FocusNode());
                                        });
//                      ic.joinedAt = joinedATCont.text;
//                      ic.dateOfFounding = dateOfFoundingCont.text;
//                      ic.address.title=addressCont.text;
//                      String url = DotEnv().env['EDIT_IC'];
//                      url = url.replaceAll("ICCODE", ic.companyCode);
//                      client.editObject(url, ic.toJson());
                                      },
                                      onCancel: () {
                                        setState(() {
                                          _status = true;
                                          FocusScope.of(context)
                                              .requestFocus(FocusNode());
                                        });
                                      },
                                    )
                                  : Container(),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            );
          } else
            return SpinKitCircle(
              //https://github.com/jogboms/flutter_spinkit#-showcase
              color: Colors.teal,
            );
        },
      ),
    );
  }

  Widget _buildList(BuildContext context, int index) {
    if (index == _addresses.length)
      return SizedBox(
        height: 17,
      );
    return Padding(
      padding: const EdgeInsets.only(top: 1, bottom: 1),
      child: Slidable(
        actionPane: SlidableDrawerActionPane(),
        controller: slidableController,
        actionExtentRatio: 0.25,
        child: Container(
          color: Colors.white,
          child: ListTile(
            leading: CircleAvatar(
              backgroundColor: Colors.teal,
              child: Text('${index + 1}'),
              foregroundColor: Colors.white,
            ),
            title: Text(_addresses[index].title),
          ),
        ),
        actions: <Widget>[],
        secondaryActions: <Widget>[
          IconSlideAction(
            caption: 'Delete',
            color: Colors.red,
            icon: Icons.delete,
            onTap: () {
              String url = DotEnv().env['DELETE_EMAIL'];
              url = url.replaceAll("EMAILCODE", _addresses[index].addressCode);
              client.deleteObject(url, context, '/profile');
            },
          ),
        ],
      ),
    );
  }

  mainBottomSheet(BuildContext context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return Padding(
            padding: const EdgeInsets.all(25),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Profile photo",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 21),
                ),
                Row(
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        InkWell(
                          child: CircleAvatar(
                            backgroundColor: Colors.teal,
                            radius: 25.0,
                            child: Icon(
                              Icons.camera,
                              color: Colors.white,
                            ),
                          ),
                          onTap: () {
                            Navigator.of(context).pop();
                            getImage(ImageSource.camera);
                          },
                        ),
                        Text(
                          "Camera",
                        )
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 30),
                    ),
                    Column(
                      children: <Widget>[
                        InkWell(
                          child: CircleAvatar(
                            backgroundColor: Colors.teal,
                            radius: 25.0,
                            child: Icon(
                              Icons.photo,
                              color: Colors.white,
                            ),
                          ),
                          onTap: () {
                            Navigator.of(context).pop();
                            getImage(ImageSource.gallery);
                          },
                        ),
                        Text(
                          "Gallery",
                        )
                      ],
                    ),
                  ],
                ),
              ],
            ),
          );
        });
  }

  Future<String> loadUserInfo() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    try {
      _image = File(prefs.getString('Profile photo'));
    } catch (e) {}

    String body = prefs.getString('UserLoginData');
    Map<String, dynamic> json = jsonDecode(body);

    String url = DotEnv().env['GET_ACC_INFO'];
    dynamic response;
    try {
      response = await client.getPrivateData(url, context);
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
      return "";
    }

    final parsed = await jsonDecode(response.body);

    prefs.setString('ACC_INFO', jsonEncode(parsed['data']));
    user = User.fromJson(parsed['data']);
    nameCont.text = user.username;
    emailCont.text = user.email;
    return response;
  }

  Future getImage(ImageSource imageSource) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    var image = await ImagePicker.pickImage(source: imageSource);
    prefs.setString('Profile photo', image.path);
    setState(() {
      _image = image;
    });
    var response =
    await client.postFormData(DotEnv().env['UPLOAD_IMAGE'], _image, context);
    String document = response.data['data'];

    //save image's path in shared

    prefs.setString('document', document);
  }
}
