import 'dart:io';

import 'package:flutter/cupertino.dart';

class FullScreenPage extends StatelessWidget {
  final File _image;

  FullScreenPage(this._image);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
            image: _image == null
                ? AssetImage("assets/images/person.jpeg")
                : FileImage(_image),),
      ),
    );
  }
}
