import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AboutPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("About"),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Divider(
                height: 15,
              ),
              Text(
                'This app was made with love by our wonderful team!',
                style: TextStyle(fontSize: 20, color: Colors.purple),
                textAlign: TextAlign.center,
              ),
              Divider(
                height: 50,
              ),
              Text(
                'Mhd Mouaz Al Jiroudi: mozjer@hotmail.com',
                style: TextStyle(fontSize: 20, color: Colors.black),
                textAlign: TextAlign.center,
              ),
              Divider(
                height: 25,
              ),
              Text(
                'Natheer Allaham: nazeer.allahham@outlook.com',
                style: TextStyle(fontSize: 20, color: Colors.black),
                textAlign: TextAlign.center,
              ),
              Divider(
                height: 15,
              ),
              Text(
                'Mhd Ezaldeen: mohammadEzalden97@gmail.com',
                style: TextStyle(fontSize: 20, color: Colors.black),
                textAlign: TextAlign.center,
              ),
              Divider(
                height: 25,
              ),
              Text(
                'Mhd Anas Al Saidi: mhd.anas.alsaidi@mail.com',
                style: TextStyle(fontSize: 20, color: Colors.black),
                textAlign: TextAlign.center,
              ),
              Divider(
                height: 25,
              ),
              Image.asset("assets/images/du.png")
            ],
          ),
        ),
      ),
    );
  }
}
