import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl/intl.dart';
import 'package:medcare/components/customEditButton.dart';
import 'package:medcare/components/customeTFEdit.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/addressAdmin/address.dart';
import 'package:medcare/screen/serviceStaffer/serviceStaffer.dart';
import 'package:medcare/screen/unauthorized.dart';

class DetailsSS extends StatefulWidget {
  @override
  State createState() {
    return _DetailsSSState();
  }
}

class _DetailsSSState extends State<DetailsSS> {
  bool _status = true;
  ServiceStaffer ss;
  List<Address> addresses = [];
  String addressTitle;
  String addressCode;

  TextEditingController typeCont = TextEditingController();
  TextEditingController emailCont = TextEditingController();
  TextEditingController titleCont = TextEditingController();
  TextEditingController nameCont = TextEditingController();
  TextEditingController stateCont = TextEditingController();
  TextEditingController codeCont = TextEditingController();
  TextEditingController joinedATCont = TextEditingController();
  TextEditingController addressCont = TextEditingController();

  @override
  Widget build(BuildContext context) {
    ss = ModalRoute.of(context).settings.arguments;
    typeCont.text = ss.type;
    joinedATCont.text = DateFormat('d/M/y').format(DateTime.parse(ss.joinedAt));
    nameCont.text = ss.user.username;
    emailCont.text = ss.user.email;

    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              expandedHeight: 200.0,
              floating: false,
              pinned: true,
              flexibleSpace: FlexibleSpaceBar(
                  centerTitle: true,
                  background: Image.asset(
                    'assets/images/staff.png',
                    fit: BoxFit.cover,
                  )),
            ),
          ];
        },
        body: FutureBuilder<String>(
          future: loadsPs(),
          builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              nameCont.text = ss.user.username;
              titleCont.text = ss.title;
              emailCont.text = ss.user.email;
              codeCont.text = ss.serviceStafferCode;
              return Container(
                color: Color(0xffFFFFFF),
                child: Padding(
                  padding: EdgeInsets.only(bottom: 25.0),
                  child: ListView(
                    children: <Widget>[
                      CustomTFEdit(
                        label: "Name",
                        textCont: nameCont,
                      ),
                      CustomTFEdit(
                        label: "Title",
                        status: _status,
                        textCont: titleCont,
                      ),
                      CustomTFEdit(
                        label: "Type",
                        textCont: typeCont,
                      ),
                      CustomTFEdit(
                        label: "Staffer Code",
                        textCont: codeCont,
                      ),
                      CustomTFEdit(
                        label: "Email",
                        status: _status,
                        textCont: emailCont,
                      ),
                      CustomTFEdit(
                        label: "Joined at",
                        textCont: joinedATCont,
                      ),
                      !_status
                          ? CustomEditButton(
                              onSave: () {
                                setState(() {
                                  // String url = DotEnv().env['EDIT_IC'];

                                  _status = true;
                                  FocusScope.of(context)
                                      .requestFocus(new FocusNode());
                                });
                              },
                              onCancel: () {
                                setState(() {
                                  _status = true;
                                  FocusScope.of(context)
                                      .requestFocus(new FocusNode());
                                });
                              },
                            )
                          : new Container(),
                    ],
                  ),
                ),
              );
            } else
              return SpinKitCircle(
                //https://github.com/jogboms/flutter_spinkit#-showcase
                color: Colors.teal,
              );
          },
        ),
      ),
    );
  }

  Future<String> loadsPs() async {
    String url = DotEnv().env['Get_Service_Staffer_Details'];
    url = url.replaceAll("SPTYPE", ss.type);
    url = url.replaceAll("SSCODE", ss.serviceStafferCode);
    try {
      var response = await client.getPrivateData(url, context);

      final parsed = await jsonDecode(response.body);

      ss = ServiceStaffer.fromJson(parsed["data"]);

      return response;
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
      return "";
    }
  }
}
