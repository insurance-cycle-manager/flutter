import 'package:medcare/screen/auth/user.dart';
import 'package:medcare/screen/serviceProvider/serviceProvider.dart';
import 'package:medcare/screen/visit/visit.dart';

class ServiceStaffer {
  String title;
  String type;
  String joinedAt;
  String serviceStafferCode;
  User user;
  List<ServiceProvider> serviceProviders;
  List<Visit> visits;
  int status;

  ServiceStaffer(
      {this.title,
      this.type,
      this.joinedAt,
      this.serviceStafferCode,
      this.user,
      this.serviceProviders,
      this.visits,
      this.status});

  factory ServiceStaffer.fromJson(Map<String, dynamic> json) {
    List<ServiceProvider> _serviceProviders = [];
    List<Visit> _visits = [];

    int _status = 0;
    if (json['serviceProviders'] != null) {
      var sPObjJson = json['serviceProviders'] as List;
      _serviceProviders = sPObjJson
          .map((sPObjJson) => ServiceProvider.fromJson(sPObjJson))
          .toList();
    }
    if (json['visits'] != null) {
      var visitObjJson = json['visits'] as List;
      _visits = visitObjJson
          .map((visitObjJson) => Visit.fromJson(visitObjJson))
          .toList();
    }

    User _user = User(username: "", email: "");
    if (json['user'] != null) {
      _user = User(
          username: json['user']['userName'], email: json['user']['email']);
    }
    if (json['status'] != null) _status = json['status'];
    return ServiceStaffer(
        title: json["title"],
        type: json["type"],
        joinedAt: json["joinedAt"],
        serviceStafferCode: json['serviceStafferCode'],
        user: _user,
        serviceProviders: _serviceProviders,
        visits: _visits,
        status: _status);
  }

  Map<String, dynamic> toJson() => {};
}
