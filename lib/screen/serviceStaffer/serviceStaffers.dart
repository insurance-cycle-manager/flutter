import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:medcare/components/customAppBar.dart';
import 'package:medcare/components/customCard.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/auth/user.dart';
import 'package:medcare/screen/profile/role/Role.dart';
import 'package:medcare/screen/serviceStaffer/serviceStaffer.dart';
import 'package:medcare/screen/serviceStaffer/serviceStafferMe.dart';
import 'package:medcare/screen/unauthorized.dart';

class ServiceStaffers extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ServiceStafferState();
  }
}

class _ServiceStafferState extends State<ServiceStaffers> {
  bool isStRole;

  Role activeRole;
  final TextEditingController _searchQuery = new TextEditingController();
  bool _isSearching;
  String _searchText = "";
  List<ServiceStaffer> sFs = [];
  String type;
  String title;
  Future<List<ServiceStaffer>> _listFuture;

  _ServiceStafferState() {
    _searchQuery.addListener(() {
      if (_searchQuery.text.isEmpty) {
        setState(() {
          _isSearching = false;
          _searchText = "";
        });
      } else {
        setState(() {
          _isSearching = true;
          _searchText = _searchQuery.text;
        });
      }
    });
  }

  @override
  void initState() {
    setState(() {});
    isStRole = false;
    _isSearching = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    type = ModalRoute.of(context).settings.arguments;
    _listFuture = updateAndGetList();
    return Scaffold(
      appBar: CustomAppBar(
        title: "Service Staffer",
        actionIcon: Icon(Icons.search),
        isSearching: _isSearching,
        searchText: _searchText,
        searchQuery: _searchQuery,
      ),
      body: FutureBuilder<List<ServiceStaffer>>(
        future: _listFuture,
        builder: (BuildContext context,
            AsyncSnapshot<List<ServiceStaffer>> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return SpinKitCircle(
              //https://github.com/jogboms/flutter_spinkit#-showcase
              color: Colors.teal,
            );
          } else if (snapshot.hasError) {
            print(snapshot.error);
            return Scaffold(
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 250,
                    ),
                    Icon(Icons.error),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[Text("Something went wrong")],
                    ),
                    Expanded(
                      child: Text(
                        "Please check your internet connection",
                        style: TextStyle(fontSize: 15),
                      ),
                    )
                  ],
                ),
              ),
            );
          } else {
            sFs = snapshot.data ?? <ServiceStaffers>[];
            return Scaffold(
              backgroundColor: Color(0xffE5E5E5),
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView.builder(
                    itemCount: sFs.length,
                    itemBuilder: _isSearching ? _buildSearchList : _buildList),
              ),
              floatingActionButton: isStRole
                  ? FloatingActionButton(
                      child: Text("Me"),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => ServiceStafferMe(
                                stType: type,
                              ),
                            ));
                      },
                    )
                  : Container(),
            );
          }
        },
      ),
    );
  }

  Widget _buildList(BuildContext context, int index) {
    return CustomCard(
      name: sFs[index].title,
      dateOfBirth: sFs[index].joinedAt,
      gender: sFs[index].type,
      object: sFs[index],
      route: "detailsSS",
    );
  }

  Widget _buildSearchList(BuildContext context, int index) {
    if (_searchText.isEmpty) {
      return CustomCard(
        name: sFs[index].title,
        dateOfBirth: sFs[index].joinedAt,
        gender: sFs[index].type,
        object: sFs[index],
        route: "detailsSS",
      );
    } else {
      String name = sFs[index].title;
      if (name.toLowerCase().contains(_searchText.toLowerCase())) {
        return CustomCard(
          name: sFs[index].title,
          dateOfBirth: sFs[index].joinedAt,
          gender: sFs[index].type,
          object: sFs[index],
          route: "detailsSS",
        );
      }
    }
    return Container(
      height: 0,
    );
  }

  Future<void> refreshList() {
    // reload
    setState(() {
      _listFuture = updateAndGetList();
    });
  }

  Future<List<ServiceStaffer>> updateAndGetList() async {
    activeRole = await client.getActiveRole();
    String url1 = DotEnv().env['GET_ACC_INFO'];
    var response1 = await client.getPrivateData(url1, context);
    final parsed1 = await jsonDecode(response1.body);
    User user = User.fromJson(parsed1['data']);
    user.userRoles.forEach((element) {
      _setStRole(element);
    });

    String url = DotEnv().env['Get_Staffers'];
    url = url.replaceAll("SPTYPE", type);
    try {
      var response = await client.getPrivateData(url, context);

      final parsed = await jsonDecode(response.body);

      return (parsed["data"] as List)
          .map<ServiceStaffer>((json) => ServiceStaffer.fromJson(json))
          .toList();
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
      return [];
    }
  }

  void _setStRole(Role element) {
    if ((activeRole.roleName == element.roleName &&
            element.roleName == "Doctor" &&
            type == "DOCTOR") ||
        (activeRole.roleName == element.roleName &&
            element.roleName == "Nurse" &&
            type == "NURSE") ||
        (activeRole.roleName == element.roleName &&
            element.roleName == "Pharmacist" &&
            type == "PHARMACIST") ||
        (activeRole.roleName == element.roleName &&
            element.roleName == "LabSpecialist" &&
            type == "LAB_SPECIALIST") ||
        (activeRole.roleName == element.roleName &&
            element.roleName == "Radiographer" &&
            type == "RADIOGRAPHER")) {
      isStRole = true;
    }
  }
}
