import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/providersStaffers/providersStaffer.dart';
import 'package:medcare/screen/serviceStaffer/serviceStaffer.dart';

import '../unauthorized.dart';

class ConfirmProvider extends StatefulWidget {
  final spCode = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final ServiceStaffer st;

  ConfirmProvider({Key key, this.st}) : super(key: key);

  @override
  State createState() {
    return _ConfirmProviderState();
  }
}

class _ConfirmProviderState extends State<ConfirmProvider> {

  List<ProviderStaffer> providerStaffers = [];

  Future<List<ProviderStaffer>> _listFuture;

  @override
  void initState() {
    setState(() {});
    _listFuture = updateAndGetList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Confirm")),
      body: FutureBuilder<List<ProviderStaffer>>(
        future: _listFuture,
        builder: (BuildContext context,
            AsyncSnapshot<List<ProviderStaffer>> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return SpinKitCircle(
              color: Colors.teal,
            );
          } else if (snapshot.connectionState == ConnectionState.done) {
            providerStaffers = snapshot.data ?? <ProviderStaffer>[];
            return Scaffold(
              backgroundColor: Color(0xffE5E5E5),
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView.builder(
                    itemCount: providerStaffers.length,
                    itemBuilder: _buildList),
              ),
            );
          } else {
            return Scaffold(
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 250,
                    ),
                    Icon(Icons.error),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[Text("Something went wrong")],
                    ),
                    Expanded(
                      child: Text(
                        "Please check your internet connection",
                        style: TextStyle(fontSize: 15),
                      ),
                    )
                  ],
                ),
              ),
            );
          }
        },
      ),
    );
  }

  void sendConfirm(String response, int index) async {
    if (response == "ACCEPT") {
      String url = DotEnv().env['PUT_CONFIRM_STAFFER_toSP'];
      url = url.replaceAll("STTYPE", widget.st.type);
      url = url.replaceAll("STCODE", widget.st.serviceStafferCode);
      url = url.replaceAll("SPCODE",
          providerStaffers[index].serviceProvider.serviceProviderCode);
      client.editObject(url, {'': ''}, context);
      providerStaffers.removeAt(index);
      setState(() {});
    }
  }

  Widget _buildList(BuildContext context, int index) {
    return Container(
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        margin: EdgeInsets.only(left: 8, right: 8, top: 8, bottom: 4),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 5, bottom: 5),
            ),
            ListTile(
              leading: Icon(
                Icons.arrow_forward_ios,
                color: Colors.teal,
              ),
              title: Text(providerStaffers[index].serviceProvider.title),
            ),
            ListTile(
              leading: Icon(Icons.title, color: Colors.teal),
              title: Text(providerStaffers[index].serviceProvider.type),
            ),
            providerStaffers[index].status == 0
                ? Padding(
                    padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        FlatButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30.0)),
                          color: Colors.green,
                          child: Row(
                            children: <Widget>[
                              Icon(
                                Icons.check_circle,
                                color: Colors.white,
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: 8),
                              ),
                              Text(
                                "Accept",
                                style: TextStyle(color: Colors.white),
                              )
                            ],
                          ),
                          onPressed: () {
                            sendConfirm("ACCEPT", index);
                          },
                        ),
                        FlatButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30.0)),
                          color: Colors.red,
                          child: Row(
                            children: <Widget>[
                              Icon(
                                Icons.cancel,
                                color: Colors.white,
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: 8),
                              ),
                              Text(
                                "Reject",
                                style: TextStyle(color: Colors.white),
                              )
                            ],
                          ),
                          onPressed: () {
                            sendConfirm("REJECT", index);
                          },
                        ),
                      ],
                    ),
                  )
                : Container()
          ],
        ),
      ),
      decoration: BoxDecoration(boxShadow: [
        BoxShadow(
          color: Colors.grey.withOpacity(.5),
          blurRadius: 20.0, // soften the shadow
          spreadRadius: 0.0, //extend the shadow
          offset: Offset(
            5.0, // Move to right 10  horizontally
            5.0, // Move to bottom 10 Vertically
          ),
        ),
      ]),
    );
  }

  Future<void> refreshList() {
    // reload
    setState(() {
      _listFuture = updateAndGetList();
    });
    return null;
  }

  Future<List<ProviderStaffer>> updateAndGetList() async {
    dynamic response;
    try {
      String url = DotEnv().env['GET_CONFIRM_STAFFER'];
      url = url.replaceAll("STTYPE", widget.st.type);
      url = url.replaceAll("STCODE", widget.st.serviceStafferCode);
      response = await client.getPrivateData(url, context);
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
      return [];
    }

    final parsed = await jsonDecode(response.body);

    return (parsed["data"] as List)
        .map<ProviderStaffer>((json) => ProviderStaffer.fromJson(json))
        .toList();
  }
}
