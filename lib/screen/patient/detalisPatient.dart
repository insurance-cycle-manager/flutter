import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:medcare/components/customEditButton.dart';
import 'package:medcare/components/customEditIcon.dart';
import 'package:medcare/components/customeTFEdit.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/patient/patient.dart';
import 'package:medcare/screen/profile/role/Role.dart';

class DetailsPatient extends StatefulWidget {
  @override
  State createState() {
    return _DetailsPatientState();
  }
}

class _DetailsPatientState extends State<DetailsPatient> {
  bool _status = true;
  final FocusNode myFocusNode = FocusNode();
  TextEditingController nameCont = TextEditingController();
  TextEditingController addressCont = TextEditingController();
  TextEditingController codeCont = TextEditingController();
  TextEditingController dateCont = TextEditingController();
  TextEditingController phoneNumberCont = TextEditingController();
  TextEditingController genderCont = TextEditingController();
  Patient patient;
  Future<void> _listFuture;
  Role activeRole;

  @override
  void initState() {
    _listFuture = updateAndGetList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    patient = ModalRoute.of(context).settings.arguments;
    nameCont.text = patient.name;
    dateCont.text =
        DateFormat('d/M/y').format(DateTime.parse(patient.dateOfBirth));
    phoneNumberCont.text = patient.phoneNumber.number;
    codeCont.text = patient.patientCode;
    genderCont.text = patient.gender;
    return Scaffold(
      body: FutureBuilder<void>(
        future: _listFuture,
        builder: (BuildContext context, AsyncSnapshot<void> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return SpinKitCircle(
              //https://github.com/jogboms/flutter_spinkit#-showcase
              color: Colors.teal,
            );
          } else if (snapshot.hasError) {
            return Scaffold(
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 250,
                    ),
                    Icon(Icons.error),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[Text("Something went wrong")],
                    ),
                    Expanded(
                      child: Text(
                        "Please check your internet connection",
                        style: TextStyle(fontSize: 15),
                      ),
                    )
                  ],
                ),
              ),
            );
          } else {
            return NestedScrollView(
              headerSliverBuilder: (BuildContext context,
                  bool innerBoxIsScrolled) {
                return <Widget>[
                  SliverAppBar(
                    expandedHeight: 200.0,
                    floating: false,
                    pinned: true,
                    flexibleSpace: FlexibleSpaceBar(
                        centerTitle: true,
                        title: Text(patient.name,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 16.0,
                            )),
                        background: Image.asset(
                          'assets/images/patient.png',
                          fit: BoxFit.cover,
                        )),
                    actions: <Widget>[
                      activeRole.roleName == "Patient" ? IconButton(
                        icon: Icon(
                          Icons.delete,
                          color: Colors.white,
                        ),
                        onPressed: () {
                          String url = DotEnv().env['DELETE_PATIENT'];
                          url = url.replaceAll(
                              "PATIENTCODE", patient.patientCode);

                          var response =
                          client.deleteObject(url, context, '/patients');
                          Fluttertoast.showToast(
                              msg: response.toString(),
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.BOTTOM,
                              timeInSecForIos: 1,
                              backgroundColor: Colors.redAccent,
                              textColor: Colors.white,
                              fontSize: 16.0);
//                    Navigator.pop(context);
//                    Navigator.pushReplacementNamed(context, '/insurance companies');
                        },
                      ) : Container(),
                    ],
                  ),
                ];
              },
              body: Container(
                color: Color(0xffFFFFFF),
                child: Padding(
                  padding: EdgeInsets.only(bottom: 25.0),
                  child: ListView(
                    children: <Widget>[
                      activeRole.roleName == "Patient" ? Padding(
                          padding:
                          EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              new Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  new Text(
                                    'Edit',
                                    style: TextStyle(
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                              new Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  _status
                                      ? CustomEditIcon(
                                    onClick: () {
                                      setState(() {
                                        _status = false;
                                      });
                                    },
                                  )
                                      : new Container(),
                                ],
                              )
                            ],
                          )) : Container(),
                      CustomTFEdit(
                        label: "Name",
                        textCont: nameCont,
                      ),
                      CustomTFEdit(
                        label: "Date",
                        textCont: dateCont,
                      ),
//                CustomTFEdit(
//                  label: "Phone",
//                  textCont: phoneNumberCont,
//                  status: _status,
//                ),
//                CustomTFEdit(
//                  label: "Address",
//                  textCont: addressCont,
//                  status: _status,
//                ),
                      CustomTFEdit(
                        label: "Gender",
                        textCont: genderCont,
                      ),
                      CustomTFEdit(
                        label: "Patient Code",
                        textCont: codeCont,
                      ),
                      !_status
                          ? CustomEditButton(
                        onSave: () {
                          setState(() {
                            patient.name = nameCont.text;
                            patient.dateOfBirth = dateCont.text;
                            patient.phoneNumber.number = phoneNumberCont.text;
                            patient.address.title = addressCont.text;

                            String url = DotEnv().env['EDIT_PATIENT'];
                            url = url.replaceAll(
                                "PATIENTCODE", patient.patientCode);
                            print(patient.toJson());
                            client.editObject(url, patient.toJson(), context);
                            _status = true;
                            FocusScope.of(context)
                                .requestFocus(new FocusNode());
                          });
                        },
                        onCancel: () {
                          setState(() {
                            _status = true;
                            FocusScope.of(context)
                                .requestFocus(new FocusNode());
                          });
                        },
                      )
                          : new Container(),
                    ],
                  ),
                ),
              ),
            );
          }
        },
      ),
    );
  }


  Future<void> refreshList() {
    // reload
    setState(() {
      _listFuture = updateAndGetList();
    });
  }

  Future<void> updateAndGetList() async {
    activeRole = await client.getActiveRole();
  }
}
