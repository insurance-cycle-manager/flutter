import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:medcare/components/customeTFEdit.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/patient/SuspendPatientPackage.dart';
import 'package:medcare/screen/patient/addPatientPackage.dart';
import 'package:medcare/screen/patient/myPackages.dart';
import 'package:medcare/screen/patient/patient.dart';
import 'package:medcare/screen/unauthorized.dart';

class PatientMe extends StatefulWidget {
  @override
  State createState() {
    return _PatientMeState();
  }
}

class _PatientMeState extends State<PatientMe> {
  bool _status = true;
  final FocusNode myFocusNode = FocusNode();
  TextEditingController nameCont = TextEditingController();
  TextEditingController addressCont = TextEditingController();
  TextEditingController codeCont = TextEditingController();
  TextEditingController dateCont = TextEditingController();
  TextEditingController phoneNumberCont = TextEditingController();
  TextEditingController genderCont = TextEditingController();
  Patient patient;
  Future<void> _listFuture;

  @override
  void initState() {
    setState(() {});
    _listFuture = updateAndGetList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder<void>(
        future: _listFuture,
        builder: (BuildContext context, AsyncSnapshot<void> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return SpinKitCircle(
              color: Colors.teal,
            );
          } else if (snapshot.hasError) {
            return Scaffold(
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 250,
                    ),
                    Icon(Icons.error),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[Text("Something went wrong")],
                    ),
                    Expanded(
                      child: Text(
                        "Please check your internet connection",
                        style: TextStyle(fontSize: 15),
                      ),
                    )
                  ],
                ),
              ),
            );
          } else {
            return Scaffold(
              body: NestedScrollView(
                headerSliverBuilder:
                    (BuildContext context, bool innerBoxIsScrolled) {
                  return <Widget>[
                    SliverAppBar(
                      expandedHeight: 200.0,
                      floating: false,
                      pinned: true,
                      flexibleSpace: FlexibleSpaceBar(
                          centerTitle: true,
                          title: Text(patient.name,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16.0,
                              )),
                          background: Image.asset('assets/images/patient.png')),
                      actions: <Widget>[
                        PopupMenuButton<String>(
                          onSelected: handleClick,
                          itemBuilder: (BuildContext context) {
                            return {
                              'My Packages',
                              'Add Package',
                              'Suspend Package'
                            }.map((String choice) {
                              return PopupMenuItem<String>(
                                value: choice,
                                child: Text(choice),
                              );
                            }).toList();
                          },
                        ),
                      ],
                    ),
                  ];
                },
                body: ListView(
                  children: <Widget>[
                    CustomTFEdit(
                      label: "Name",
                      textCont: nameCont,
                    ),
                    CustomTFEdit(
                      label: "Date",
                      textCont: dateCont,
                      status: _status,
                    ),
                    CustomTFEdit(
                      label: "Gender",
                      textCont: genderCont,
                    ),
                    CustomTFEdit(
                      label: "Patient Code",
                      textCont: codeCont,
                    ),
                  ],
                ),
              ),
            );
          }
        },
      ),
    );
  }

  Future<void> refreshList() {
    setState(() {
      _listFuture = updateAndGetList();
    });
  }

  void handleClick(String value) {
    switch (value) {
      case 'My Packages':
        {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => MyPackages(
                  patient: patient,
                ),
              ));
        }
        break;
      case 'My Visits':
        {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => AddPackagePatient(),
              ));
        }
        break;
      case 'Add Package':
        {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => AddPackagePatient(),
              ));
        }
        break;
      case 'Suspend Package':
        {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => SuspendPackagePatient(),
              ));
        }
        break;
    }
  }

  Future<void> updateAndGetList() async {
    dynamic response;

    try {
      response =
      await client.getPrivateData(DotEnv().env['GET_Me_PATIENT'], context);
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
      return [];
    }

    final parsed = await jsonDecode(response.body);

    patient = Patient.fromJson(parsed['data']);
    nameCont.text = patient.name;
    dateCont.text = patient.dateOfBirth;
    phoneNumberCont.text = patient.phoneNumber.number;
    codeCont.text = patient.patientCode;
    genderCont.text = patient.gender;
  }
}
