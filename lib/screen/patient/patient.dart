import 'package:medcare/screen/addressAdmin/address.dart';
import 'package:medcare/screen/package/package.dart';
import 'package:medcare/screen/profile/email/Email.dart';
import 'package:medcare/screen/profile/phoneNumber/number.dart';
import 'package:medcare/screen/visit/visit.dart';

class Patient {
  String name;
  String dateOfBirth;
  String gender;
  String patientCode;
  Address address;
  Number phoneNumber;
  Email email;
  List<Visit> visits;
  List<Package> packages;

  Patient(
      {this.name,
      this.dateOfBirth,
      this.gender,
      this.address,
      this.patientCode,
      this.phoneNumber,
      this.email,
      this.visits,
      this.packages});

  factory Patient.fromJson(Map<String, dynamic> json) {
    Address _address = Address();
    Email _email;
    Number _phone = Number();
    List<Package> _packages = [];
    List<Visit> _visits = [];
    if (json['address'] != null) {
      _address = Address.fromJson(json['address']['address']);
    }
    if (json['email'] != null) {
      _email = Email.fromJson(json['email']);
    }
    if (json['phoneNumber'] != null) {
      _phone = Number.fromJson(json['phoneNumber']);
    }

    if (json['visits'] != null) {
      var visitObjJson = json['visits'] as List;
      _visits =
          visitObjJson.map((visitJson) => Visit.fromJson(visitJson)).toList();
    }
    if (json['packages'] != null) {
      var packageObjJson = json['packages'] as List;
      _packages = packageObjJson
          .map((packageJson) => Package.fromJson(packageJson))
          .toList();
    }
    return Patient(
        name: json['name'] as String,
        dateOfBirth: json['dateOfBirth'] as String,
        gender: json['gender'] == 0 ? "Male" : "Female",
        patientCode: json['patientCode'] as String,
        address: _address,
        phoneNumber: _phone,
        email: _email,
        visits: _visits,
        packages: _packages);
  }

  Map<String, dynamic> toJson() =>
      {
        'name': name,
        'dateOfBirth': dateOfBirth,
        'gender': gender,
        'phoneNumber': phoneNumber,
        'patientCode': patientCode,
        'address': address.toJson(),
      };
}
