import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:medcare/components/customButton.dart';
import 'package:medcare/components/customTextField.dart';
import 'package:medcare/networking/httpClient.dart' as client;

class SuspendPackagePatient extends StatefulWidget {
  final packageCode = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  State createState() {
    return _SuspendPackagePatientState();
  }
}

class _SuspendPackagePatientState extends State<SuspendPackagePatient> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Suspend Package Patient"),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.only(top: 40),
        child: Form(
          key: widget._formKey,
          child: Wrap(
            runSpacing: 25,
            children: <Widget>[
              CustomTextField(
                labelText: "Package code",
                controller: widget.packageCode,
              ),
              CustomButton(
                bName: "Submit",
                bFunc: () {
                  var json = {
                    "PackageCode": widget.packageCode.text,
                  };
                  client.postWithTokenJSON(
                      json, DotEnv().env['Post_Suspend_Package_PATIENT'],
                      context);
                  Navigator.pop(context);
                },
                bTextColor: Colors.white,
              )
            ],
          ),
        ),
      ),
    );
  }
}
