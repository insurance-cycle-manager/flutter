import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:medcare/components/customButton.dart';
import 'package:medcare/components/customDatePicker.dart';
import 'package:medcare/components/customTextField.dart';
import 'package:medcare/networking/httpClient.dart' as client;

class AddPatient extends StatefulWidget {
  final name = TextEditingController();
  final phoneNumber = TextEditingController();
  final address = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  State createState() {
    return _AddPatientState();
  }
}

class _AddPatientState extends State<AddPatient> {
  String _dateOfBirth = "Not set";
  String selectGender = "MALE";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add Patient"),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.only(top: 40),
        child: Form(
          key: widget._formKey,
          child: Wrap(
            runSpacing: 25,
            children: <Widget>[
              CustomTextField(
                labelText: "Name",
                controller: widget.name,
              ),
              CustomTextField(
                labelText: "Phone Number",
                controller: widget.phoneNumber,
              ),
              CustomTextField(
                labelText: "Address",
                controller: widget.address,
              ),
              CustomDatePicker(
                name: "Date of birth",
                onPressed: (date) {
                  setState(() {
                    _dateOfBirth = date;
                  });
                },
              ),
              Container(
                padding: EdgeInsets.fromLTRB(30.0, 10.0, 20.0, 10.0),
                child: Row(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Radio(
                          activeColor: Theme.of(context).primaryColor,
                          value: "MALE",
                          groupValue: selectGender,
                          onChanged: (value) {
                            setState(() {
                              selectGender = value;
                            });
                          },
                        ),
                        Text("Male")
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Radio(
                          activeColor: Theme.of(context).primaryColor,
                          value: "FEMALE",
                          groupValue: selectGender,
                          onChanged: (value) {
                            setState(() {
                              selectGender = value;
                            });
                          },
                        ),
                        Text("Female")
                      ],
                    )
                  ],
                ),
              ),
              CustomButton(
                bName: "Submit",
                bFunc: () {
                  var json = {
                    "name": widget.name.text,
                    "dateOfBirth": _dateOfBirth,
                    "phoneNumber": widget.phoneNumber.text,
                    "address": widget.address.text,
                    "gender": selectGender
                  };
                  client.postWithTokenJSON(
                      json, DotEnv().env['ADD_PATIENT'], context);

                  Navigator.popUntil(context, ModalRoute.withName('/patients'));
                  Navigator.pop(context);
                  Navigator.pushNamed(context, '/patients');
                },
                bTextColor: Colors.white,
              )
            ],
          ),
        ),
      ),
    );
  }
}
