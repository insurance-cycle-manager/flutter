import 'package:flutter/material.dart';
import 'package:medcare/components/customAppBar.dart';
import 'package:medcare/components/customCard.dart';
import 'package:medcare/screen/patient/patient.dart';

class MyPackages extends StatefulWidget {
  final Patient patient;

  const MyPackages({Key key, this.patient}) : super(key: key);

  @override
  State createState() {
    return _MyPackagesState();
  }
}

class _MyPackagesState extends State<MyPackages> {
  final TextEditingController _searchQuery = TextEditingController();
  bool _isSearching;
  String _searchText = "";

  _MyPackagesState() {
    _searchQuery.addListener(() {
      if (_searchQuery.text.isEmpty) {
        setState(() {
          _isSearching = false;
          _searchText = "";
        });
      } else {
        setState(() {
          _isSearching = true;
          _searchText = _searchQuery.text;
        });
      }
    });
  }

  @override
  void initState() {
    setState(() {});
    _isSearching = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        title: "My packages",
        actionIcon: Icon(Icons.search),
        isSearching: _isSearching,
        searchText: _searchText,
        searchQuery: _searchQuery,
      ),
      backgroundColor: Color(0xffE5E5E5),
      body: ListView.builder(
          itemCount: widget.patient.packages.length,
          itemBuilder: _isSearching ? _buildSearchList : _buildList),
    );
  }

  Widget _buildList(BuildContext context, int index) {
    return CustomCard(
      name: widget.patient.packages[index].name,
      dateOfBirth: widget.patient.packages[index].packageDate,
      gender: widget.patient.packages[index].category,
      object: widget.patient.packages[index],
    );
  }

  Widget _buildSearchList(BuildContext context, int index) {
    if (widget.patient != null)
      widget.patient.packages[index].onlyDisplay = true;
    if (_searchText.isEmpty) {
      return CustomCard(
        name: widget.patient.packages[index].name,
        dateOfBirth: widget.patient.packages[index].packageDate,
        object: widget.patient.packages[index],
      );
    } else {
      String name = widget.patient.packages[index].name;
      if (name.toLowerCase().contains(_searchText.toLowerCase())) {
        return CustomCard(
          name: widget.patient.packages[index].name,
          dateOfBirth: widget.patient.packages[index].packageDate,
          object: widget.patient.packages[index],
        );
      }
    }
    return Container(
      height: 0,
    );
  }
}
