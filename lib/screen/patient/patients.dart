import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:medcare/components/customAppBar.dart';
import 'package:medcare/components/customCard.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/auth/user.dart';
import 'package:medcare/screen/patient/patient.dart';
import 'package:medcare/screen/patient/patientMe.dart';
import 'package:medcare/screen/profile/role/Role.dart';
import 'package:medcare/screen/unauthorized.dart';

class Patients extends StatefulWidget {
  @override
  State createState() {
    return _PatientState();
  }
}

class _PatientState extends State<Patients> {
  User user;
  Role activeRole;
  final TextEditingController _searchQuery = new TextEditingController();
  bool _isSearching;
  String _searchText = "";
  List<Patient> patients = [];

  Future<List<Patient>> _listFuture;

  _PatientState() {
    _searchQuery.addListener(() {
      if (_searchQuery.text.isEmpty) {
        setState(() {
          _isSearching = false;
          _searchText = "";
        });
      } else {
        setState(() {
          _isSearching = true;
          _searchText = _searchQuery.text;
        });
      }
    });
  }

  @override
  void initState() {
    setState(() {});
    _isSearching = false;
    _listFuture = updateAndGetList();
    super.initState();
  }

  Future<void> refreshList() {
    // reload
    setState(() {
      _listFuture = updateAndGetList();
    });
  }

  Future<List<Patient>> updateAndGetList() async {
    activeRole = await client.getActiveRole();

    dynamic response;
    try {
      response =
      await client.getPrivateData(DotEnv().env['GET_PATIENTS'], context);
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
      return [];
    }

    final parsed = await jsonDecode(response.body);

    return (parsed["data"] as List)
        .map<Patient>((json) => Patient.fromJson(json))
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        title: "Patients",
        actionIcon: Icon(Icons.search),
        isSearching: _isSearching,
        searchText: _searchText,
        searchQuery: _searchQuery,
      ),
      body: FutureBuilder<List<Patient>>(
        future: _listFuture,
        builder: (BuildContext context, AsyncSnapshot<List<Patient>> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return SpinKitCircle(
              //https://github.com/jogboms/flutter_spinkit#-showcase
              color: Colors.teal,
            );
          } else if (snapshot.hasError) {
            return Scaffold(
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 250,
                    ),
                    Icon(Icons.error),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[Text("Something went wrong")],
                    ),
                    Expanded(
                      child: Text(
                        "Please check your internet connection",
                        style: TextStyle(fontSize: 15),
                      ),
                    )
                  ],
                ),
              ),
            );
          } else {
            patients = snapshot.data ?? <Patient>[];
            return Scaffold(
              backgroundColor: Color(0xffE5E5E5),
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView.builder(
                    itemCount: patients.length,
                    itemBuilder: _isSearching ? _buildSearchList : _buildList),
              ),
              floatingActionButton: activeRole.roleName == "Patient"
                  ? FloatingActionButton(
                      child: Text("Me"),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => PatientMe(),
                            ));
                      },
                    )
                  : Container(),
            );
          }
        },
      ),
    );
  }

  Widget _buildList(BuildContext context, int index) {
    return CustomCard(
      name: patients[index].name,
      dateOfBirth: patients[index].dateOfBirth,
      gender: patients[index].gender,
      route: 'detailsPatient',
      object: patients[index],
    );
  }

  Widget _buildSearchList(BuildContext context, int index) {
    if (_searchText.isNotEmpty) {
      String name = patients[index].name;
      if (name.toLowerCase().contains(_searchText.toLowerCase())) {
        return CustomCard(
          name: patients[index].name,
          dateOfBirth: patients[index].dateOfBirth,
          gender: patients[index].gender,
          route: 'detailsPatient',
          object: patients[index],
        );
      } else
        return Container(
          height: 0,
        );
    } else
      return CustomCard(
        name: patients[index].name,
        dateOfBirth: patients[index].dateOfBirth,
        gender: patients[index].gender,
        route: 'detailsPatient',
        object: patients[index],
      );
  }
}
