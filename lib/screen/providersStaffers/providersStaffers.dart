import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:medcare/components/customAppBar.dart';
import 'package:medcare/components/customCard.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/providersStaffers/addStafferToProvider.dart';
import 'package:medcare/screen/serviceStaffer/serviceStaffer.dart';
import 'package:medcare/screen/unauthorized.dart';

class ProvidersStaffers extends StatefulWidget {
  final String SPtype;
  final String SPcode;

  const ProvidersStaffers({Key key, this.SPtype, this.SPcode})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _ProvidersStaffersState();
  }
}

class _ProvidersStaffersState extends State<ProvidersStaffers> {
  final TextEditingController _searchQuery = new TextEditingController();
  bool _isSearching;
  String _searchText = "";
  List<ServiceStaffer> sFs = [];
  String title;
  Future<List<ServiceStaffer>> _listFuture;

  _ProvidersStaffersState() {
    _searchQuery.addListener(() {
      if (_searchQuery.text.isEmpty) {
        setState(() {
          _isSearching = false;
          _searchText = "";
        });
      } else {
        setState(() {
          _isSearching = true;
          _searchText = _searchQuery.text;
        });
      }
    });
  }

  @override
  void initState() {
    setState(() {});
    _isSearching = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _listFuture = updateAndGetList();
    return Scaffold(
      appBar: CustomAppBar(
        title: "Service Staffer",
        actionIcon: Icon(Icons.search),
        isSearching: _isSearching,
        searchText: _searchText,
        searchQuery: _searchQuery,
      ),
      body: FutureBuilder<List<ServiceStaffer>>(
        future: _listFuture,
        builder: (BuildContext context,
            AsyncSnapshot<List<ServiceStaffer>> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return SpinKitCircle(
              //https://github.com/jogboms/flutter_spinkit#-showcase
              color: Colors.teal,
            );
          } else if (snapshot.hasError) {
            print(snapshot.error);
            return Scaffold(
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 250,
                    ),
                    Icon(Icons.error),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[Text("Something went wrong")],
                    ),
                    Expanded(
                      child: Text(
                        "Please check your internet connection",
                        style: TextStyle(fontSize: 15),
                      ),
                    )
                  ],
                ),
              ),
            );
          } else {
            sFs = snapshot.data ?? <ProvidersStaffers>[];
            return Scaffold(
              backgroundColor: Color(0xffE5E5E5),
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView.builder(
                    itemCount: sFs.length,
                    itemBuilder: _isSearching ? _buildSearchList : _buildList),
              ),
              floatingActionButton: FloatingActionButton(
                child: Icon(Icons.add),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => AddProviderToStaffer(
                                sPType: widget.SPtype,
                                sPCode: widget.SPcode,
                              )));
                },
              ),
            );
          }
        },
      ),
    );
  }

  Widget _buildList(BuildContext context, int index) {
    return CustomCard(
      name: sFs[index].title,
      dateOfBirth: sFs[index].joinedAt,
      gender: sFs[index].type,
      object: sFs[index],
      route: "detailsSS",
    );
  }

  Widget _buildSearchList(BuildContext context, int index) {
    if (_searchText.isEmpty) {
      return CustomCard(
        name: sFs[index].title,
        dateOfBirth: sFs[index].joinedAt,
        gender: sFs[index].type,
        object: sFs[index],
        route: "detailsSS",
      );
    } else {
      String name = sFs[index].title;
      if (name.toLowerCase().contains(_searchText.toLowerCase())) {
        return CustomCard(
          name: sFs[index].title,
          dateOfBirth: sFs[index].joinedAt,
          gender: sFs[index].type,
          object: sFs[index],
          route: "detailsSS",
        );
      }
    }
    return Container(
      height: 0,
    );
  }

  Future<void> refreshList() {
    // reload
    setState(() {
      _listFuture = updateAndGetList();
    });
  }

  Future<List<ServiceStaffer>> updateAndGetList() async {
    String url = DotEnv().env['GET_Staffs_By_Provider'];
    url = url.replaceAll("SPTYPE", widget.SPtype);
    url = url.replaceAll("SPCODE", widget.SPcode);
    dynamic response;
    try {
      response = await client.getPrivateData(url, context);
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
      return [];
    }

    final parsed = await jsonDecode(response.body);

    //TODO read from json
    return (parsed["data"] as List)
        .map<ServiceStaffer>((json) => ServiceStaffer.fromJson(json["staffer"]))
        .toList();
  }
}
