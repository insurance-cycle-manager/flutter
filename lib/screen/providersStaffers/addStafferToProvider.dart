import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:medcare/components/customButton.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/serviceStaffer/serviceStaffer.dart';

class AddProviderToStaffer extends StatefulWidget {
  final String sPCode;
  final String sPType;

  const AddProviderToStaffer({Key key, this.sPCode, this.sPType})
      : super(key: key);

  @override
  _AddProviderToStafferState createState() => _AddProviderToStafferState();
}

class _AddProviderToStafferState extends State<AddProviderToStaffer> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _typeAheadController = TextEditingController();
  final stafferCodeCont = TextEditingController();
  String _stafferCode;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add Staffer"),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.only(top: 40),
        child: Form(
          key: _formKey,
          child: Wrap(
            runSpacing: 25,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(30.0, 10.0, 20.0, 10.0),
                child: TypeAheadFormField(
                  textFieldConfiguration: TextFieldConfiguration(
                      controller: this._typeAheadController,
                      decoration: InputDecoration(
                        labelText: 'Staffer Title',
                        border: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.teal)),
                      )),
                  suggestionsCallback: suggestionsCallbackTypeAhead,
                  itemBuilder: (context, suggestion) {
                    if (suggestion != null)
                      return ListTile(
                        title: Text(suggestion.title),
                      );
                    return Container(
                      height: 0,
                    );
                  },
                  transitionBuilder: (context, suggestionsBox, controller) {
                    return suggestionsBox;
                  },
                  onSuggestionSelected: (suggestion) {
                    this._typeAheadController.text = suggestion.title;
                    _stafferCode = suggestion.serviceStafferCode;
                  },
                  validator: (String value) {
                    if (value.isEmpty) {
                      return 'Please select a Staffer';
                    }
                    return null;
                  },
                  onSaved: (value) => this._stafferCode = value,
                ),
              ),
              CustomButton(
                bName: "Submit",
                bFunc: () {
                  var json = {
                    "StafferCode": _stafferCode,
                  };
                  String url = DotEnv().env['POST_Staffs_By_Provider'];
                  url = url.replaceAll("SPTYPE", widget.sPType);
                  url = url.replaceAll("SPCODE", widget.sPCode);
                  client.postWithTokenJSON(json, url, context);
                  Navigator.pop(context);
                },
                bTextColor: Colors.white,
              )
            ],
          ),
        ),
      ),
    );
  }

  FutureOr<Iterable> suggestionsCallbackTypeAhead(String pattern) async {
    var response =
    await client.getPrivateData(
        DotEnv().env['Get_Staffers_Without_Type'], context);

    final parsed = await jsonDecode(response.body);

    return (parsed["data"] as List).map<ServiceStaffer>((json) {
      if ((json['title'] as String).contains(pattern)) {
        return ServiceStaffer.fromJson(json);
      }
      return null;
    }).toList();
  }
}
