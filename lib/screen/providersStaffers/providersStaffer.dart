import 'package:medcare/screen/serviceProvider/serviceProvider.dart';
import 'package:medcare/screen/serviceStaffer/serviceStaffer.dart';

class ProviderStaffer {
  int status;
  ServiceStaffer staffer;
  ServiceProvider serviceProvider;

  ProviderStaffer({this.status, this.staffer, this.serviceProvider});

  factory ProviderStaffer.fromJson(Map<String, dynamic> json) {
    return ProviderStaffer(
      status: json['status'],
      staffer: ServiceStaffer.fromJson(json['staffer']),
      serviceProvider: ServiceProvider.fromJson(json['serviceProvider']),
    );
  }
}
