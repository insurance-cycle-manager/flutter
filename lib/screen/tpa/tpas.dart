import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:medcare/components/customAppBar.dart';
import 'package:medcare/components/customCard.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/auth/user.dart';
import 'package:medcare/screen/profile/role/Role.dart';
import 'package:medcare/screen/tpa/tpa.dart';
import 'package:medcare/screen/unauthorized.dart';

class TPAs extends StatefulWidget {
  @override
  State createState() {
    return _TPAsState();
  }
}

class _TPAsState extends State<TPAs> {
  Role activeRole;
  User user;
  final TextEditingController _searchQuery = TextEditingController();
  bool _isSearching;
  String _searchText = "";
  List<ThirdPartyAdministrator> tpas = [];
  Future<List<ThirdPartyAdministrator>> _listFuture;

  _TPAsState() {
    _searchQuery.addListener(() {
      if (_searchQuery.text.isEmpty) {
        setState(() {
          _isSearching = false;
          _searchText = "";
        });
      } else {
        setState(() {
          _isSearching = true;
          _searchText = _searchQuery.text;
        });
      }
    });
  }

  @override
  void initState() {
    setState(() {});

    _listFuture = updateAndGetList();
    _isSearching = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        title: "TPAs",
        actionIcon: Icon(Icons.search),
        isSearching: _isSearching,
        searchText: _searchText,
        searchQuery: _searchQuery,
      ),
      body: FutureBuilder<List<ThirdPartyAdministrator>>(
        future: _listFuture,
        builder: (BuildContext context,
            AsyncSnapshot<List<ThirdPartyAdministrator>> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return SpinKitCircle(
              //https://github.com/jogboms/flutter_spinkit#-showcase
              color: Colors.teal,
            );
          } else if (snapshot.hasError) {
            return Scaffold(
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 250,
                    ),
                    Icon(Icons.error),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[Text("Something went wrong")],
                    ),
                    Expanded(
                      child: Text(
                        "Please check your internet connection",
                        style: TextStyle(fontSize: 15),
                      ),
                    )
                  ],
                ),
              ),
            );
          } else {
            tpas = snapshot.data ?? <ThirdPartyAdministrator>[];
            return Scaffold(
              backgroundColor: Color(0xffE5E5E5),
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView.builder(
                    itemCount: tpas.length,
                    itemBuilder: _isSearching ? _buildSearchList : _buildList),
              ),
              floatingActionButton: activeRole.roleName == "TPA"
                  ? FloatingActionButton(
                      child: Text("Me"),
                      onPressed: () {
                        Navigator.pushNamed(
                            context, '/my third party administrators');
                      },
                    )
                  : Container(),
            );
          }
        },
      ),
    );
  }

  Widget _buildList(BuildContext context, int index) {
    return CustomCard(
      name: tpas[index].name,
      dateOfBirth: tpas[index].dateOfFounding,
      gender: tpas[index].status.replaceAll('_', ' '),
      object: tpas[index],
      route: 'detailsTpa',
    );
  }

  Widget _buildSearchList(BuildContext context, int index) {
    if (_searchText.isEmpty) {
      return CustomCard(
        name: tpas[index].name,
        dateOfBirth: tpas[index].dateOfFounding,
        gender: tpas[index].status.replaceAll('_', ' '),
        object: tpas[index],
        route: 'detailsTpa',
      );
    } else {
      String name = tpas[index].name;
      if (name.toLowerCase().contains(_searchText.toLowerCase())) {
        return CustomCard(
          name: tpas[index].name,
          dateOfBirth: tpas[index].dateOfFounding,
          gender: tpas[index].status.replaceAll('_', ' '),
          object: tpas[index],
          route: 'detailsTpa',
        );
      }
    }
    return Container(
      height: 0,
    );
  }

  Future<void> refreshList() {
    setState(() {
      _listFuture = updateAndGetList();
    });
  }

  Future<List<ThirdPartyAdministrator>> updateAndGetList() async {
    activeRole = await client.getActiveRole();
    String url = DotEnv().env['GET_ACC_INFO'];
    dynamic response1;

    try {
      response1 = await client.getPrivateData(url, context);
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
      return [];
    }

    dynamic response;
    try {
      response = await client.getPrivateData(DotEnv().env['GET_TPAS'], context);
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
      return [];
    }

    final parsed = await jsonDecode(response.body);

    return (parsed["data"] as List)
        .map<ThirdPartyAdministrator>(
            (json) => ThirdPartyAdministrator.fromJson(json))
        .toList();
  }
}
