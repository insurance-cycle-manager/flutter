import 'package:medcare/screen/addressAdmin/address.dart';
import 'package:medcare/screen/insuranceCompany/insuranceCompany.dart';
import 'package:medcare/screen/profile/email/Email.dart';
import 'package:medcare/screen/profile/phoneNumber/number.dart';
import 'package:medcare/screen/tpa/serviceProviderConnection.dart';

class ThirdPartyAdministrator {
  String name;
  String dateOfFounding;
  String joinedAt;
  Address address;
  String status;
  String companyCode;
  String supervisoryCommission;
  Email email;
  Number phone;
  List<InsuranceCompany> insuranceConnections;
  List<ServiceProviderConnection> serviceProvidersConnections;

  ThirdPartyAdministrator(
      {this.name,
      this.dateOfFounding,
      this.joinedAt,
      this.address,
      this.companyCode,
      this.status,
      this.supervisoryCommission,
      this.phone,
      this.email,
      this.insuranceConnections,
      this.serviceProvidersConnections});

  factory ThirdPartyAdministrator.fromJson(Map<String, dynamic> json) {
    List<InsuranceCompany> _insuranceConnections = [];
    List<ServiceProviderConnection> _serviceProvidersConnections = [];

    if (json['insuranceConnections'] != null) {
      var icObjJson = json['insuranceConnections'] as List;
      _insuranceConnections = icObjJson
          .map((icObjJson) => InsuranceCompany.fromJson(icObjJson))
          .toList();
    }
    if (json['serviceProvidersConnections'] != null) {
      var spConnectionObjJson = json['serviceProvidersConnections'] as List;
      _serviceProvidersConnections = spConnectionObjJson
          .map((spConnectionJson) =>
              ServiceProviderConnection.fromJson(spConnectionJson))
          .toList();
    }
    Address _address;
    Email _email;
    Number _phone;
    String _supervisoryComission;
    if (json['address'] != null) {
      _address = Address.fromJson(json['address']['address']);
    }
    if (json['email'] != null) {
      _email = Email.fromJson(json['email']);
    }
    if (json['phoneNumber'] != null) {
      _phone = Number.fromJson(json['phoneNumber']);
    }
    if (json['supervisoryComission'] != null)
      _supervisoryComission = json['supervisoryComission'];
    return ThirdPartyAdministrator(
        name: json['name'] as String,
        dateOfFounding: json['dateOfFounding'] as String,
        joinedAt: json['joinedAt'] as String,
        status: json['status'] as String,
        companyCode: json['thirdPartyCode'] as String,
        insuranceConnections: _insuranceConnections,
        address: _address,
        email: _email,
        phone: _phone,
        supervisoryCommission: _supervisoryComission,
        serviceProvidersConnections: _serviceProvidersConnections);
  }

  Map<String, dynamic> toJson() =>
      {
        'name': name,
        'dateOfFounding': dateOfFounding,
        'joinedAt': joinedAt,
        'address': address.toJson(),
        'status': status,
        'companyCode': companyCode,
        'supervisoryCommission': supervisoryCommission
      };
}
