import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:medcare/components/customButton.dart';
import 'package:medcare/components/customTextField.dart';
import 'package:medcare/networking/httpClient.dart' as client;

class InviteSp extends StatefulWidget {
  final spCode = TextEditingController();
  final iCCode = TextEditingController();
  final String tpaCode;
  final _formKey = GlobalKey<FormState>();

  InviteSp({Key key, this.tpaCode}) : super(key: key);

  @override
  State createState() {
    return _InviteSpState();
  }
}

class _InviteSpState extends State<InviteSp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Invite SP"),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.only(top: 40),
        child: Form(
          key: widget._formKey,
          child: Wrap(
            runSpacing: 25,
            children: <Widget>[
              CustomTextField(
                labelText: "Sp code",
                controller: widget.spCode,
              ),
              CustomTextField(
                labelText: "IC code",
                controller: widget.iCCode,
              ),
              CustomButton(
                bName: "Submit",
                bFunc: () {
                  var json = {
                    "ServiceProviderCode": widget.spCode.text,
                    "InsuranceCompanies": [widget.iCCode.text],
                    "tpacode": widget.tpaCode
                  };
                  client.postWithTokenJSON(
                      json, DotEnv().env['Invite_SP'], context);
                  Navigator.pop(context);
                },
                bTextColor: Colors.white,
              )
            ],
          ),
        ),
      ),
    );
  }
}
