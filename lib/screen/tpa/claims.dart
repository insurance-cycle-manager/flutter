import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/unauthorized.dart';

class Claims extends StatefulWidget {
  @override
  _ClaimsState createState() => _ClaimsState();
}

class _ClaimsState extends State<Claims> {
  List<Claim> claims = [];

  Future<List<Claim>> _listFuture;
  List<String> status = ["pending", "approve", "reject", "canceled"];

  @override
  void initState() {
    _listFuture = updateAndGetList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Claims"),
      ),
      body: FutureBuilder<List<Claim>>(
        future: _listFuture,
        builder: (BuildContext context, AsyncSnapshot<List<Claim>> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return SpinKitCircle(
              color: Colors.teal,
            );
          } else if (snapshot.connectionState == ConnectionState.done) {
            claims = snapshot.data ?? <Claim>[];
            return Scaffold(
              backgroundColor: Color(0xffE5E5E5),
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView.builder(
                    itemCount: claims.length, itemBuilder: _buildList),
              ),
            );
          } else {
            return Scaffold(
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 250,
                    ),
                    Icon(Icons.error),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[Text("Something went wrong")],
                    ),
                    Expanded(
                      child: Text(
                        "Please check your internet connection",
                        style: TextStyle(fontSize: 15),
                      ),
                    )
                  ],
                ),
              ),
            );
          }
        },
      ),
    );
  }

  Widget _buildList(BuildContext context, int index) {
    return Container(
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        margin: EdgeInsets.only(left: 8, right: 8, top: 8, bottom: 4),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 5, bottom: 5),
            ),
            ListTile(
              leading: Icon(Icons.code),
              title: Text(claims[index].visitClaimCode),
            ),
            ListTile(
              leading: Icon(Icons.panorama_wide_angle),
              title: Text(claims[index].status),
            ),
            claims[index].status == "Pending"
                ? Padding(
                    padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        FlatButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30.0)),
                          color: Colors.green,
                          child: Row(
                            children: <Widget>[
                              Icon(
                                Icons.check_circle,
                                color: Colors.white,
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: 8),
                              ),
                              Text(
                                "Approve",
                                style: TextStyle(color: Colors.white),
                              )
                            ],
                          ),
                          onPressed: () {
                            sendConfirm("APPROVE", index);
                          },
                        ),
                        FlatButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30.0)),
                          color: Colors.red,
                          child: Row(
                            children: <Widget>[
                              Icon(
                                Icons.cancel,
                                color: Colors.white,
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: 8),
                              ),
                              Text(
                                "Reject",
                                style: TextStyle(color: Colors.white),
                              )
                            ],
                          ),
                          onPressed: () {
                            sendConfirm("REJECT", index);
                          },
                        ),
                      ],
                    ),
                  )
                : Container()
          ],
        ),
      ),
      decoration: BoxDecoration(boxShadow: [
        BoxShadow(
          color: Colors.grey.withOpacity(.5),
          blurRadius: 20.0, // soften the shadow
          spreadRadius: 0.0, //extend the shadow
          offset: Offset(
            5.0, // Move to right 10  horizontally
            5.0, // Move to bottom 10 Vertically
          ),
        ),
      ]),
    );
  }

  Future<void> refreshList() {
    // reload
    setState(() {
      _listFuture = updateAndGetList();
    });
    return null;
  }

  void sendConfirm(String response, int index) async {
    var json = {
      "Status": status.indexOf(response.toLowerCase()),
    };
    String url = DotEnv().env['Put_Manage_Claims'];
    url = url.replaceAll("claimCode", claims[index].visitClaimCode);
    url = url.replaceAll("requester", "TPA");
    client.editObject(url, json, context);
    claims[index].status = "Approve";
    setState(() {});
  }

  Future<List<Claim>> updateAndGetList() async {
    try {
      var response = await client.getPrivateData(
          DotEnv().env['Get_Claims'].replaceAll("requester", "TPA"), context);

      final parsed = await jsonDecode(response.body);

      return (parsed["data"] as List)
          .map<Claim>((json) => Claim.fromJson(json))
          .toList();
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
      return [];
    }
  }
}

class Claim {
  String visitClaimCode;
  String status;

  Claim({this.visitClaimCode, this.status});

  factory Claim.fromJson(Map<String, dynamic> json) {
    return Claim(
      visitClaimCode: json['visitClaimCode'] as String,
      status: json['status'] as String,
    );
  }
}
