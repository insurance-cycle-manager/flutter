import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:medcare/components/customEditButton.dart';
import 'package:medcare/components/customEditIcon.dart';
import 'package:medcare/components/customeTFEdit.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/tpa/chanageStatus.dart';
import 'package:medcare/screen/tpa/claims.dart';
import 'package:medcare/screen/tpa/inviteSp.dart';
import 'package:medcare/screen/tpa/tpa.dart';
import 'package:medcare/screen/unauthorized.dart';

class MyTPA extends StatefulWidget {
  @override
  State createState() {
    return _MyTPAState();
  }
}

class _MyTPAState extends State<MyTPA> {
  bool _status = true;
  final FocusNode myFocusNode = FocusNode();
  TextEditingController nameCont = TextEditingController();
  TextEditingController stateCont = TextEditingController();
  TextEditingController addressCont = TextEditingController();
  TextEditingController codeCont = TextEditingController();
  TextEditingController joinedATCont = TextEditingController();
  TextEditingController dateOfFoundingCont = TextEditingController();
  TextEditingController emailCont = TextEditingController();
  TextEditingController phoneCont = TextEditingController();
  TextEditingController supervisoryComissionCont = TextEditingController();
  ThirdPartyAdministrator tpa;

//  String _searchText = "";
  List<ThirdPartyAdministrator> tpas = [];
  Future<void> _listFuture;

  @override
  void initState() {
    super.initState();

    _listFuture = updateAndGetList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder<void>(
        future: _listFuture,
        builder: (BuildContext context, AsyncSnapshot<void> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return SpinKitCircle(
              color: Colors.teal,
            );
          } else if (snapshot.hasError) {
            return Scaffold(
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 250,
                    ),
                    Icon(Icons.error),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[Text("Something went wrong")],
                    ),
                    Expanded(
                      child: Text(
                        "Please check your internet connection",
                        style: TextStyle(fontSize: 15),
                      ),
                    )
                  ],
                ),
              ),
            );
          } else {
            return Scaffold(
              body: NestedScrollView(
                headerSliverBuilder:
                    (BuildContext context, bool innerBoxIsScrolled) {
                  return <Widget>[
                    SliverAppBar(
                      expandedHeight: 200.0,
                      floating: false,
                      pinned: true,
                      flexibleSpace: FlexibleSpaceBar(
                          centerTitle: true,
                          title: Text(tpa.name,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16.0,
                              )),
                          background: Image.network(
                            "https://images.pexels.com/photos/396547/pexels-photo-396547.jpeg?auto=compress&cs=tinysrgb&h=350",
                            fit: BoxFit.cover,
                          )),
                      actions: <Widget>[
                        PopupMenuButton<String>(
                          onSelected: handleClick,
                          itemBuilder: (BuildContext context) {
                            List<String> s = [
                              'Claims',
                              'Invite SP',
                              'Change Status',
                              'Delete'
                            ];
                            return s.map((String choice) {
                              return PopupMenuItem<String>(
                                value: choice,
                                child: Text(choice),
                              );
                            }).toList();
                          },
                        )
                      ],
                    ),
                  ];
                },
                body: Container(
                  color: Color(0xffFFFFFF),
                  child: Padding(
                    padding: EdgeInsets.only(bottom: 25.0),
                    child: ListView(
                      children: <Widget>[
                        Padding(
                            padding: EdgeInsets.only(
                                left: 25.0, right: 25.0, top: 25.0),
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                new Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    new Text(
                                      'Edit',
                                      style: TextStyle(
                                          fontSize: 18.0,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                                new Column(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    _status
                                        ? CustomEditIcon(
                                            onClick: () {
                                              setState(() {
                                                _status = false;
                                              });
                                            },
                                          )
                                        : new Container(),
                                  ],
                                )
                              ],
                            )),
                        CustomTFEdit(
                          label: "Name",
                          textCont: nameCont,
                        ),
                        CustomTFEdit(
                          label: "Email",
                          textCont: emailCont,
                        ),
                        CustomTFEdit(
                          label: "Phone",
                          textCont: phoneCont,
                        ),
                        CustomTFEdit(
                          label: "Company Code",
                          textCont: codeCont,
                        ),
                        CustomTFEdit(
                          label: "Date of founding",
                          textCont: dateOfFoundingCont,
                          status: _status,
                        ),
                        CustomTFEdit(
                          label: "Joined at",
                          textCont: joinedATCont,
                          status: _status,
                        ),
                        CustomTFEdit(
                          label: "State",
                          textCont: stateCont,
                        ),
                        CustomTFEdit(
                          label: "Address",
                          textCont: addressCont,
                          status: _status,
                        ),
                        CustomTFEdit(
                          label: "supervisory Commission",
                          textCont: supervisoryComissionCont,
                          status: _status,
                        ),
                        !_status
                            ? CustomEditButton(
                                onSave: () {
                                  setState(() {
                                    tpa.name = nameCont.text;
                                    tpa.joinedAt = joinedATCont.text;
                                    tpa.dateOfFounding =
                                        dateOfFoundingCont.text;
                                    tpa.address.title = addressCont.text;
                                    String url = DotEnv().env['EDIT_TPA'];
                                    url = url.replaceAll(
                                        "TPACODE", tpa.companyCode);
                                    client.editObject(
                                        url, tpa.toJson(), context);
                                    _status = true;
                                    FocusScope.of(context)
                                        .requestFocus(new FocusNode());
                                  });
                                },
                                onCancel: () {
                                  setState(() {
                                    _status = true;
                                    FocusScope.of(context)
                                        .requestFocus(new FocusNode());
                                  });
                                },
                              )
                            : new Container(),
                      ],
                    ),
                  ),
                ),
              ),
            );
          }
        },
      ),
    );
  }

  /* Widget _buildList(BuildContext context, int index) {
    return CustomCard(
      name: tpas[index].name,
      dateOfBirth: tpas[index].dateOfFounding,
      gender: tpas[index].status,
      route: 'detailsTPA',
      object: tpas[index],
    );
  }*/

  /*Widget _buildSearchList(BuildContext context, int index) {
    if (_searchText.isEmpty) {
      return CustomCard(
        name: tpas[index].name,
        route: 'detailsTPA',
        dateOfBirth: tpas[index].dateOfFounding.toString(),
      );
    } else {
      String name = tpas[index].name;
      if (name.toLowerCase().contains(_searchText.toLowerCase())) {
        return CustomCard(
          name: tpas[index].name,
          dateOfBirth: tpas[index].dateOfFounding.toString(),
          route: 'detailsTPA',
        );
      }
    }
    return Container(
      height: 0,
    );
  }*/

  Future<void> refreshList() {
    // reload
    setState(() {
      _listFuture = updateAndGetList();
    });
    return null;
  }

  void handleClick(String value) async {
    switch (value) {
      case 'Claims':
        {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => Claims(),
              ));
        }
        break;
      case 'Invite SP':
        {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => InviteSp(
                  tpaCode: tpa.companyCode,
                ),
              ));
        }
        break;
      case 'Change Status':
        {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => ChangeStatusTPA(
                        tpa: tpa,
                      )));
        }
        break;
      case 'Delete':
        String url = DotEnv().env['DELETE_TPA'];
        url = url.replaceAll("TPACODE", tpa.companyCode);
        client.deleteObject(url, context, '/third party administrators');
        break;
    }
  }

  Future<void> updateAndGetList() async {
    try {
      var response = await client.getPrivateData(
          DotEnv().env['GET_MYTPAS'], context);

      final parsed = await jsonDecode(response.body);

      //todo remove  as list
      tpa = ThirdPartyAdministrator.fromJson(parsed['data']);

//        (parsed["data"] as List)
//        .map<InsuranceCompany>((json) => InsuranceCompany.fromJson(json))
//        .toList();

      nameCont.text = tpa.name;
      stateCont.text = tpa.status;
      codeCont.text = tpa.companyCode;
      joinedATCont.text = tpa.joinedAt;
      dateOfFoundingCont.text = tpa.dateOfFounding;
      addressCont.text = tpa.address.title;
      emailCont.text = tpa.email.email;
      phoneCont.text = tpa.phone.number;
      supervisoryComissionCont.text = tpa.supervisoryCommission;
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
      return;
    }
  }
}
