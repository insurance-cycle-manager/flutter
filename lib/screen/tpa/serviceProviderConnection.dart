import 'package:medcare/screen/tpa/tpa.dart';

class ServiceProviderConnection {
  String connectionCode;
  String createdAt;
  int status;
  ThirdPartyAdministrator tpa;

  ServiceProviderConnection(
      {this.connectionCode, this.createdAt, this.status, this.tpa});

  factory ServiceProviderConnection.fromJson(Map<String, dynamic> json) {
    ThirdPartyAdministrator _tpa;
    if (json['tpa'] != null) {
      _tpa = ThirdPartyAdministrator.fromJson(json['tpa']);
    }
    return ServiceProviderConnection(
      connectionCode: json['connectionCode'] as String,
      createdAt: json['createdAt'] as String,
      status: json['status'],
      tpa: _tpa,
    );
  }
}
