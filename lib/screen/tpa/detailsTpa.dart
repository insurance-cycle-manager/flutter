import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl/intl.dart';
import 'package:medcare/components/customEditButton.dart';
import 'package:medcare/components/customEditIcon.dart';
import 'package:medcare/components/customeTFEdit.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/profile/role/Role.dart';
import 'package:medcare/screen/tpa/tpa.dart';

class DetailsTPA extends StatefulWidget {
  @override
  State createState() {
    return _DetailsTPAState();
  }
}

class _DetailsTPAState extends State<DetailsTPA> {
  bool _status = true;
  final FocusNode myFocusNode = FocusNode();
  ThirdPartyAdministrator tpa;

  TextEditingController nameCont = TextEditingController();
  TextEditingController stateCont = TextEditingController();
  TextEditingController codeCont = TextEditingController();
  TextEditingController joinedATCont = TextEditingController();
  TextEditingController dateOfFoundingCont = TextEditingController();
  TextEditingController addressCont = TextEditingController();
  TextEditingController emailCont = TextEditingController();
  TextEditingController phoneCont = TextEditingController();
  TextEditingController supervisoryComissionCont = TextEditingController();

  Future<void> _listFuture;
  Role activeRole;

  @override
  void initState() {
    _listFuture = updateAndGetList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    tpa = ModalRoute.of(context).settings.arguments;
    nameCont.text = tpa.name;
    stateCont.text = tpa.status;
    codeCont.text = tpa.companyCode;
    joinedATCont.text =
        DateFormat('d/M/y').format(DateTime.parse(tpa.joinedAt));
    dateOfFoundingCont.text =
        DateFormat('d/M/y').format(DateTime.parse(tpa.dateOfFounding));
    addressCont.text = tpa.address.title;
    emailCont.text = tpa.email.email;
    phoneCont.text = tpa.phone.number;
    supervisoryComissionCont.text = tpa.supervisoryCommission;
    return Scaffold(
      body: FutureBuilder<void>(
        future: _listFuture,
        builder: (BuildContext context, AsyncSnapshot<void> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return SpinKitCircle(
              //https://github.com/jogboms/flutter_spinkit#-showcase
              color: Colors.teal,
            );
          } else if (snapshot.hasError) {
            return Scaffold(
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 250,
                    ),
                    Icon(Icons.error),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[Text("Something went wrong")],
                    ),
                    Expanded(
                      child: Text(
                        "Please check your internet connection",
                        style: TextStyle(fontSize: 15),
                      ),
                    )
                  ],
                ),
              ),
            );
          } else {
            return NestedScrollView(
              headerSliverBuilder: (BuildContext context,
                  bool innerBoxIsScrolled) {
                return <Widget>[
                  SliverAppBar(
                    expandedHeight: 200.0,
                    floating: false,
                    pinned: true,
                    flexibleSpace: FlexibleSpaceBar(
                        centerTitle: true,
                        title: Text(tpa.name,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 16.0,
                            )),
                        background: Image.asset(
                          'assets/images/tpa.jpg',
                          fit: BoxFit.cover,
                        )),
                    actions: <Widget>[
                      activeRole.roleName == "TPA" ? IconButton(
                        icon: Icon(
                          Icons.delete,
                          color: Colors.white,
                        ),
                        onPressed: () async {
                          String url = DotEnv().env['DELETE_TPA'];
                          url = url.replaceAll("TPACODE", tpa.companyCode);

                          await client.deleteObject(
                              url, context, '/third party administrators');
//                    Navigator.pop(context);
//                    Navigator.pushReplacementNamed(context, '/insurance companies');
                        },
                      ) : Container(),
                    ],
                  ),
                ];
              },
              body: Container(
                color: Color(0xffFFFFFF),
                child: Padding(
                  padding: EdgeInsets.only(bottom: 25.0),
                  child: ListView(
                    children: <Widget>[
                      activeRole.roleName == "TPA" ? Padding(
                          padding:
                          EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Text(
                                    'Edit',
                                    style: TextStyle(
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  _status
                                      ? CustomEditIcon(
                                    onClick: () {
                                      setState(() {
                                        _status = false;
                                      });
                                    },
                                  )
                                      : Container(),
                                ],
                              )
                            ],
                          )) : Container(),
                      CustomTFEdit(
                        label: "Name",
                        status: _status,
                        textCont: nameCont,
                      ),
                      CustomTFEdit(
                        label: "Email",
                        textCont: emailCont,
                      ),
                      CustomTFEdit(
                        label: "Phone",
                        textCont: phoneCont,
                      ),
                      CustomTFEdit(
                        label: "Date of founding",
                        textCont: dateOfFoundingCont,
                      ),
                      CustomTFEdit(
                        label: "Joined at",
                        textCont: joinedATCont,
                      ),
                      CustomTFEdit(
                        label: "Company Code",
                        textCont: codeCont,
                      ),
                      CustomTFEdit(
                        label: "State",
                        textCont: stateCont,
                      ),
                      CustomTFEdit(
                        label: "Address",
                        textCont: addressCont,
                        status: _status,
                      ),
                      CustomTFEdit(
                        label: "supervisory Commission",
                        textCont: supervisoryComissionCont,
                        status: _status,
                      ),
                      !_status
                          ? CustomEditButton(
                        onSave: () {
                          setState(() {
                            tpa.name = nameCont.text;
                            tpa.joinedAt = joinedATCont.text;
                            tpa.dateOfFounding = dateOfFoundingCont.text;
                            tpa.address.title = addressCont.text;
                            String url = DotEnv().env['EDIT_TPA'];
                            url = url.replaceAll("TPACODE", tpa.companyCode);
                            client.editObject(url, tpa.toJson(), context);

                            _status = true;
                            FocusScope.of(context).requestFocus(FocusNode());
                          });
                        },
                        onCancel: () {
                          setState(() {
                            _status = true;
                            FocusScope.of(context).requestFocus(FocusNode());
                          });
                        },
                      )
                          : Container(),
                    ],
                  ),
                ),
              ),
            );
          }
        },
      ),

    );
  }


  Future<void> refreshList() {
    // reload
    setState(() {
      _listFuture = updateAndGetList();
    });
  }

  Future<void> updateAndGetList() async {
    activeRole = await client.getActiveRole();
  }
}
