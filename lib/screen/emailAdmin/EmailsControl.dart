import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/profile/email/Email.dart';
import 'package:medcare/screen/unauthorized.dart';

class EmailsControl extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _EmailsControlState();
  }
}

class _EmailsControlState extends State<EmailsControl> {
  Widget appBarTitle = new Text("Emails Requests");
  Icon actionIcon = new Icon(Icons.search);
  final TextEditingController _searchQuery = new TextEditingController();
  final key = new GlobalKey<ScaffoldState>();
  bool _isSearching;
  String _searchText = "";
  List<RequestEmail> requestEmails = [];

  Future<List<RequestEmail>> _listFuture;

  _EmailsControlState() {
    _searchQuery.addListener(() {
      if (_searchQuery.text.isEmpty) {
        setState(() {
          _isSearching = false;
          _searchText = "";
        });
      } else {
        setState(() {
          _isSearching = true;
          _searchText = _searchQuery.text;
        });
      }
    });
  }

  @override
  void initState() {
    setState(() {});
    _isSearching = false;
    _listFuture = updateAndGetList();
    super.initState();
  }

  Future<void> refreshList() {
    // reload
    setState(() {
      _listFuture = updateAndGetList();
    });
    return null;
  }

  Future<List<RequestEmail>> updateAndGetList() async {
    dynamic response;

    try {
      response =
      await client.getPrivateData(DotEnv().env['GET_PENDING_EMAILS'], context);
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
      return [];
    }

    final parsed = await jsonDecode(response.body);

    return (parsed["data"] as List)
        .map<RequestEmail>((json) => RequestEmail.fromJson(json))
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: key,
      appBar: AppBar(
        title: this.appBarTitle,
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: GestureDetector(
              child: IconButton(
                  icon: actionIcon,
                  onPressed: () {
                    setState(() {
                      if (this.actionIcon.icon == Icons.search) {
                        this.actionIcon = new Icon(
                          Icons.close,
                        );
                        this.appBarTitle = new TextField(
                          controller: _searchQuery,
                          decoration: new InputDecoration(
                            prefixIcon: new Icon(Icons.search),
                            hintText: "Search...",
                          ),
                        );
                        _handleSearchStart();
                      } else {
                        _handleSearchEnd();
                      }
                    });
                  }),
            ),
          ),
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {},
                child: Icon(Icons.more_vert),
              )),
        ],
      ),
      body: FutureBuilder<List<RequestEmail>>(
        future: _listFuture,
        builder:
            (BuildContext context, AsyncSnapshot<List<RequestEmail>> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return SpinKitCircle(
              color: Colors.teal,
            );
          } else if (snapshot.connectionState == ConnectionState.done) {
            requestEmails = snapshot.data ?? <RequestEmail>[];
            return Scaffold(
              backgroundColor: Color(0xffE5E5E5),
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView.builder(
                    itemCount: requestEmails.length,
                    itemBuilder: _isSearching ? _buildSearchList : _buildList),
              ),
            );
          } else {
            return Scaffold(
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 250,
                    ),
                    Icon(Icons.error),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[Text("Something went wrong")],
                    ),
                    Expanded(
                      child: Text(
                        "Please check your internet connection",
                        style: TextStyle(fontSize: 15),
                      ),
                    )
                  ],
                ),
              ),
            );
          }
        },
      ),
    );
  }

  void sendConfirm(String response, int index) async {
    String url = DotEnv().env['PUT_EMAIL_ADMIN'];
    url = url.replaceAll("USERNAME", requestEmails[index].userName);
    url = url.replaceAll("EMAILCODE", requestEmails[index].emailCode);
    client.editObject(url, {'': ''}, context);
    requestEmails.removeAt(index);
    setState(() {});
  }

  Widget _buildList(BuildContext context, int index) {
    return InkWell(
      child: Container(
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          margin: EdgeInsets.only(left: 8, right: 8, top: 8, bottom: 4),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 5, bottom: 5),
              ),
              ListTile(
                leading: Icon(Icons.sentiment_very_satisfied),
                title: Text(requestEmails[index].userName),
              ),
              ListTile(
                leading: Icon(Icons.sentiment_very_satisfied),
                title: Text(requestEmails[index].emailCode),
              ),
              ListTile(
                leading: Icon(Icons.sentiment_very_satisfied),
                title: Text(requestEmails[index].address),
              ),
              /*ListTile(
                leading: Icon(Icons.directions_walk),
                title: Text(requestEmails[index].email.email),
              ),*/
              Padding(
                padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    FlatButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0)),
                      color: Colors.green,
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.check_circle,
                            color: Colors.white,
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: 8),
                          ),
                          Text(
                            "Accept",
                            style: TextStyle(color: Colors.white),
                          )
                        ],
                      ),
                      onPressed: () {
                        sendConfirm("ACCEPT", index);
                      },
                    ),
                    FlatButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0)),
                      color: Colors.red,
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.cancel,
                            color: Colors.white,
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: 8),
                          ),
                          Text(
                            "Reject",
                            style: TextStyle(color: Colors.white),
                          )
                        ],
                      ),
                      onPressed: () {
                        sendConfirm("REJECT", index);
                      },
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
        decoration: BoxDecoration(boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(.5),
            blurRadius: 20.0, // soften the shadow
            spreadRadius: 0.0, //extend the shadow
            offset: Offset(
              5.0, // Move to right 10  horizontally
              5.0, // Move to bottom 10 Vertically
            ),
          ),
        ]),
      ),
      onTap: () {
        Navigator.pushNamed(context, '/user info',
            arguments: requestEmails[index].userName);
      },
    );
  }

  Widget _buildSearchList(BuildContext context, int index) {
    if (_searchText.isNotEmpty) {
      String name = requestEmails[index].userName;
      if (name.toLowerCase().contains(_searchText.toLowerCase())) {
        return InkWell(
          child: Container(
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              margin: EdgeInsets.only(left: 8, right: 8, top: 8, bottom: 4),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(top: 5, bottom: 5),
                  ),
                  ListTile(
                    leading: Icon(Icons.sentiment_very_satisfied),
                    title: Text(requestEmails[index].userName),
                  ),
                  ListTile(
                    leading: Icon(Icons.sentiment_very_satisfied),
                    title: Text(requestEmails[index].emailCode),
                  ),
                  ListTile(
                    leading: Icon(Icons.sentiment_very_satisfied),
                    title: Text(requestEmails[index].address),
                  ),
                  /* ListTile(
                    leading: Icon(Icons.directions_walk),
                    title: Text(requestEmails[index].email.email),
                  ),*/
                  Padding(
                    padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        FlatButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30.0)),
                          color: Colors.green,
                          child: Row(
                            children: <Widget>[
                              Icon(
                                Icons.check_circle,
                                color: Colors.white,
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: 8),
                              ),
                              Text(
                                "Accept",
                                style: TextStyle(color: Colors.white),
                              )
                            ],
                          ),
                          onPressed: () {
                            sendConfirm("ACCEPT", index);
                          },
                        ),
                        FlatButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30.0)),
                          color: Colors.red,
                          child: Row(
                            children: <Widget>[
                              Icon(
                                Icons.cancel,
                                color: Colors.white,
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: 8),
                              ),
                              Text(
                                "Reject",
                                style: TextStyle(color: Colors.white),
                              )
                            ],
                          ),
                          onPressed: () {
                            sendConfirm("REJECT", index);
                          },
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
            decoration: BoxDecoration(boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(.5),
                blurRadius: 20.0, // soften the shadow
                spreadRadius: 0.0, //extend the shadow
                offset: Offset(
                  5.0, // Move to right 10  horizontally
                  5.0, // Move to bottom 10 Vertically
                ),
              ),
            ]),
          ),
          onTap: () {
            Navigator.pushNamed(context, '/user info',
                arguments: requestEmails[index].userName);
          },
        );
      } else
        return Container(
          height: 0,
        );
    } else
      return InkWell(
        child: Container(
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            margin: EdgeInsets.only(left: 8, right: 8, top: 8, bottom: 4),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top: 5, bottom: 5),
                ),
                ListTile(
                  leading: Icon(Icons.sentiment_very_satisfied),
                  title: Text(requestEmails[index].userName),
                ),
                ListTile(
                  leading: Icon(Icons.sentiment_very_satisfied),
                  title: Text(requestEmails[index].emailCode),
                ),
                ListTile(
                  leading: Icon(Icons.sentiment_very_satisfied),
                  title: Text(requestEmails[index].address),
                ),
                /* ListTile(
                  leading: Icon(Icons.directions_walk),
                  title: Text(requestEmails[index].email.email),
                ),*/
                Padding(
                  padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      FlatButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0)),
                        color: Colors.green,
                        child: Row(
                          children: <Widget>[
                            Icon(
                              Icons.check_circle,
                              color: Colors.white,
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: 8),
                            ),
                            Text(
                              "Accept",
                              style: TextStyle(color: Colors.white),
                            )
                          ],
                        ),
                        onPressed: () {
                          sendConfirm("ACCEPT", index);
                        },
                      ),
                      FlatButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0)),
                        color: Colors.red,
                        child: Row(
                          children: <Widget>[
                            Icon(
                              Icons.cancel,
                              color: Colors.white,
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: 8),
                            ),
                            Text(
                              "Reject",
                              style: TextStyle(color: Colors.white),
                            )
                          ],
                        ),
                        onPressed: () {
                          sendConfirm("REJECT", index);
                        },
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(.5),
              blurRadius: 20.0, // soften the shadow
              spreadRadius: 0.0, //extend the shadow
              offset: Offset(
                5.0, // Move to right 10  horizontally
                5.0, // Move to bottom 10 Vertically
              ),
            ),
          ]),
        ),
        onTap: () {
          Navigator.pushNamed(context, '/user info',
              arguments: requestEmails[index].userName);
        },
      );
  }

  void _handleSearchStart() {
    setState(() {
      _isSearching = true;
    });
  }

  void _handleSearchEnd() {
    setState(() {
      this.actionIcon = new Icon(
        Icons.search,
      );
      this.appBarTitle = new Text(
        "Search Sample",
      );
      _isSearching = false;
      _searchQuery.clear();
    });
  }
}

class RequestEmail {
  String userName;
  String address;
  String emailCode;

  RequestEmail({this.userName, this.address, this.emailCode});

  factory RequestEmail.fromJson(Map<String, dynamic> json) {
    return RequestEmail(
        userName: json['userName'],
        emailCode: json['emailCode'],
        address: json['address']
        //email: Email.fromJson(json['address']),
        );
  }
}
