import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:medcare/components/customAppBar.dart';
import 'package:medcare/components/customCard.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/profile/email/Email.dart';
import 'package:medcare/screen/unauthorized.dart';

class EmailsAdmin extends StatefulWidget {
  @override
  State createState() {
    return _EmailsAdminState();
  }
}

class _EmailsAdminState extends State<EmailsAdmin> {
  final TextEditingController _searchQuery = new TextEditingController();
  bool _isSearching;
  String _searchText = "";
  List<Email> emails = [];
  Future<List<Email>> _listFuture;

  _EmailsAdminState() {
    _searchQuery.addListener(() {
      if (_searchQuery.text.isEmpty) {
        setState(() {
          _isSearching = false;
          _searchText = "";
        });
      } else {
        setState(() {
          _isSearching = true;
          _searchText = _searchQuery.text;
        });
      }
    });
  }

  @override
  void initState() {
    setState(() {});
    _isSearching = false;
    super.initState();
    _listFuture = updateAndGetList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        title: "Emails",
        isSearching: _isSearching,
        searchText: _searchText,
        searchQuery: _searchQuery,
        handleClick: handleClick,
        optionsList: ['Emails Control'],
      ),
      body: FutureBuilder<List<Email>>(
        future: _listFuture,
        builder: (BuildContext context, AsyncSnapshot<List<Email>> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return SpinKitCircle(
              color: Colors.teal,
            );
          } else if (snapshot.hasError) {
            return Scaffold(
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 250,
                    ),
                    Icon(Icons.error),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[Text("Something went wrong")],
                    ),
                    Expanded(
                      child: Text(
                        "Please check your internet connection",
                        style: TextStyle(fontSize: 15),
                      ),
                    )
                  ],
                ),
              ),
            );
          } else {
            emails = snapshot.data ?? <Email>[];
            return Scaffold(
              backgroundColor: Color(0xffE5E5E5),
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView.builder(
                    itemCount: emails.length,
                    itemBuilder: _isSearching ? _buildSearchList : _buildList),
              ),
              floatingActionButton: FloatingActionButton(
                child: Icon(Icons.add),
                onPressed: () {
                  Navigator.pushNamed(context, '/addEmailAdmin');
                },
              ),
            );
          }
        },
      ),
    );
  }

  Widget _buildList(BuildContext context, int index) {
    return CustomCard(
      name: emails[index].email,
      dateOfBirth: emails[index].code,
      route: 'detailsEmailAdmin',
      object: emails[index],
    );
  }

  Widget _buildSearchList(BuildContext context, int index) {
    if (_searchText.isEmpty) {
      return CustomCard(
        name: emails[index].email,
        route: 'detailsEmailAdmin',
      );
    } else {
      String name = emails[index].email;
      if (name.toLowerCase().contains(_searchText.toLowerCase())) {
        return CustomCard(
          name: emails[index].email,
          route: 'detailsEmailAdmin',
        );
      }
    }
    return Container(
      height: 0,
    );
  }

  Future<void> refreshList() {
    // reload
    setState(() {
      _listFuture = updateAndGetList();
    });
    return null;
  }

  Future<List<Email>> updateAndGetList() async {
    dynamic response;

    try {
      response =
      await client.getPrivateData(DotEnv().env['GET_EMAILS'], context);
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
      return [];
    }

    final parsed = await jsonDecode(response.body);

    return (parsed["data"] as List)
        .map<Email>((json) => Email.fromJson(json))
        .toList();
  }

  void handleClick(String value) async {
    switch (value) {
      case 'Emails Control':
        {
          Navigator.pushNamed(
            context,
            '/emailsControl',
          );
        }
        break;
    }
  }
}
