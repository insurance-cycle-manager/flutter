import 'package:medcare/screen/visit/visit.dart';

class PurchaseRadiograph {
  String note;
  String code;
  Visit visit;
  String scheduledDate;

  PurchaseRadiograph({this.note, this.code, this.scheduledDate, this.visit});

  factory PurchaseRadiograph.fromJson(Map<String, dynamic> json,
      {String visitCode}) {
    return PurchaseRadiograph(
      note: json['note'],
      code: json['prescribedRadiographCode'],
      scheduledDate: json['scheduledDate'],
      visit: Visit.fromJson(json['visit']),
    );
  }
}
