import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/unauthorized.dart';
import 'package:medcare/screen/visit/purchaseRadiograph/purchaseRadiograph.dart';
import 'package:medcare/screen/visit/visit.dart';

class PurchasesRadiographs extends StatefulWidget {
  final Visit newVisit;

  PurchasesRadiographs({Key key, this.newVisit}) : super(key: key);

  @override
  State createState() {
    return _PurchasesRadiographsState();
  }
}

class _PurchasesRadiographsState extends State<PurchasesRadiographs> {
  List<PurchaseRadiograph> PurchaseRadiographs = [];

  Future<List<PurchaseRadiograph>> _listFuture;

  @override
  void initState() {
    setState(() {});
    _listFuture = updateAndGetList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Purchases Radiograph")),
      body: FutureBuilder<List<PurchaseRadiograph>>(
        future: _listFuture,
        builder: (BuildContext context,
            AsyncSnapshot<List<PurchaseRadiograph>> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return SpinKitCircle(
              color: Colors.teal,
            );
          } else if (snapshot.connectionState == ConnectionState.done) {
            PurchaseRadiographs = snapshot.data ?? <PurchaseRadiograph>[];
            return Scaffold(
              backgroundColor: Color(0xffE5E5E5),
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView.builder(
                    itemCount: PurchaseRadiographs.length,
                    itemBuilder: _buildList),
              ),
            );
          } else {
            return Scaffold(
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 250,
                    ),
                    Icon(Icons.error),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[Text("Something went wrong")],
                    ),
                    Expanded(
                      child: Text(
                        "Please check your internet connection",
                        style: TextStyle(fontSize: 15),
                      ),
                    )
                  ],
                ),
              ),
            );
          }
        },
      ),
    );
  }

  void sendConfirm(String response, int index) async {
    if (response == "Purchase") {
      String url = DotEnv().env['ADD_Visit_Prescribed_Radiographs_Purchase'];
      url = url.replaceAll("VISITCODE", widget.newVisit.visitCode);
      url = url.replaceAll(
          "PRESCRIBEDRADIOGRAPHCODE", PurchaseRadiographs[index].code);

      client.postWithTokenJSON({}, url, context);

      PurchaseRadiographs.removeAt(index);
      setState(() {});
    }
  }

  Widget _buildList(BuildContext context, int index) {
    return Container(
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        margin: EdgeInsets.only(left: 8, right: 8, top: 8, bottom: 4),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 5, bottom: 5),
            ),
            ListTile(
              leading: Icon(
                Icons.arrow_forward_ios,
                color: Colors.teal,
              ),
              title: Text(PurchaseRadiographs[index].note),
            ),
            ListTile(
              leading: Icon(Icons.date_range, color: Colors.teal),
              title: Text(PurchaseRadiographs[index].scheduledDate),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  FlatButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0)),
                    color: Colors.green,
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.check_circle,
                          color: Colors.white,
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 8),
                        ),
                        Text(
                          "Purchase",
                          style: TextStyle(color: Colors.white),
                        )
                      ],
                    ),
                    onPressed: () {
                      sendConfirm("Purchase", index);
                    },
                  ),
                ],
              ),
            )
          ],
        ),
      ),
      decoration: BoxDecoration(boxShadow: [
        BoxShadow(
          color: Colors.grey.withOpacity(.5),
          blurRadius: 20.0, // soften the shadow
          spreadRadius: 0.0, //extend the shadow
          offset: Offset(
            5.0, // Move to right 10  horizontally
            5.0, // Move to bottom 10 Vertically
          ),
        ),
      ]),
    );
  }

  Future<void> refreshList() {
    // reload
    setState(() {
      _listFuture = updateAndGetList();
    });
    return null;
  }

  Future<List<PurchaseRadiograph>> updateAndGetList() async {
    dynamic response;
    try {
      String url = DotEnv().env['GET_Visit_Prescribed_Radiographs_Purchase'];

      ///the code for old visit
      url = url.replaceAll("VISITCODE", widget.newVisit.baseVisit.visitCode);
      response = await client.getPrivateData(url, context);
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
      return [];
    }

    final parsed = await jsonDecode(response.body);

    return (parsed["data"] as List)
        .map<PurchaseRadiograph>((json) => PurchaseRadiograph.fromJson(json))
        .toList();
  }
}
