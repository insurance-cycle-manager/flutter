import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:medcare/components/customEditButton.dart';
import 'package:medcare/components/customEditIcon.dart';
import 'package:medcare/components/customeTFEdit.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/diagnosis/diagnosis.dart';
import 'package:medcare/screen/profile/role/Role.dart';
import 'package:medcare/screen/visit/purchaseLabTest/purchasesLabTest.dart';
import 'package:medcare/screen/visit/purchaseRadiograph/purchaseRadiographs.dart';
import 'package:medcare/screen/visit/purchaseSurgery/purchasesSurgeries.dart';
import 'package:medcare/screen/visit/visit.dart';

class DetailsVisit extends StatefulWidget {
  @override
  State createState() {
    return _DetailsVisitState();
  }
}

class _DetailsVisitState extends State<DetailsVisit> {
  bool _status = true;
  Visit visit;

  List<String> _menu;
  TextEditingController costCont = TextEditingController();
  TextEditingController diagnosisCont = TextEditingController();
  TextEditingController patientCodeCont = TextEditingController();
  TextEditingController visitDateCont = TextEditingController();
  TextEditingController patientNameCont = TextEditingController();
  TextEditingController visitCodeCont = TextEditingController();
  Future<void> _listFuture;
  bool isStRole = false;
  Role activeRole;

  @override
  void initState() {
    _menu = [];
    _listFuture = updateAndGetList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    visit = ModalRoute.of(context).settings.arguments;
    costCont.text = visit.cost.toString();
    patientCodeCont.text = visit.patient.patientCode;
    visitDateCont.text =
        DateFormat('d/M/y').format(DateTime.parse(visit.visitDate));
    patientNameCont.text = visit.patient.name;
    visitCodeCont.text = visit.visitCode;
    _menu = [
      'Diagnosis',
      'Prescriptions',
      'Surgeries',
      'LabTests',
      'Radriographs',
      'Claim',
      'Delete'
    ];
    if (visit.baseVisit != null) {
      _menu.add('Purchases Surgeries');
      _menu.add('Purchase LabTests');
      _menu.add('Purchases Radiographs');
    }

    return Scaffold(
      body: FutureBuilder<void>(
        future: _listFuture,
        builder: (BuildContext context, AsyncSnapshot<void> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return SpinKitCircle(
              color: Colors.teal,
            );
          } else if (snapshot.hasError) {
            return Scaffold(
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 250,
                    ),
                    Icon(Icons.error),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[Text("Something went wrong")],
                    ),
                    Expanded(
                      child: Text(
                        "Please check your internet connection",
                        style: TextStyle(fontSize: 15),
                      ),
                    )
                  ],
                ),
              ),
            );
          } else {
            return NestedScrollView(
                headerSliverBuilder:
                    (BuildContext context, bool innerBoxIsScrolled) {
                  return <Widget>[
                    SliverAppBar(
                      expandedHeight: 200.0,
                      floating: false,
                      pinned: true,
                      flexibleSpace: FlexibleSpaceBar(
                          centerTitle: true,
                          title: Text(visit.type,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16.0,
                              )),
                          background: Image.asset(
                            'assets/images/visit.png',
                            fit: BoxFit.cover,
                          )),
                      actions: <Widget>[
                        isStRole
                            ? PopupMenuButton<String>(
                                onSelected: handleClick,
                                itemBuilder: (BuildContext context) {
                                  List<String> s = _menu;
                                  return s.map((String choice) {
                                    return PopupMenuItem<String>(
                                      value: choice,
                                      child: Text(choice),
                                    );
                                  }).toList();
                                },
                              )
                            : Container(),
                      ],
                    ),
                  ];
                },
                body: Container(
                  color: Color(0xffFFFFFF),
                  child: Padding(
                    padding: EdgeInsets.only(bottom: 25.0),
                    child: ListView(
                      children: <Widget>[
                        isStRole
                            ? Padding(
                                padding: EdgeInsets.only(
                                    left: 25.0, right: 25.0, top: 25.0),
                                child: new Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    new Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        new Text(
                                          'Edit',
                                          style: TextStyle(
                                              fontSize: 18.0,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                    new Column(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        _status
                                            ? CustomEditIcon(
                                                onClick: () {
                                                  setState(() {
                                                    _status = false;
                                                  });
                                                },
                                              )
                                            : new Container(),
                                      ],
                                    )
                                  ],
                                ))
                            : Container(),
                        CustomTFEdit(
                          label: "Cost",
                          status: _status,
                          textCont: costCont,
                        ),
                        CustomTFEdit(
                          label: "Visit Date",
                          textCont: visitDateCont,
                        ),
                        CustomTFEdit(
                          label: "Patient Name",
                          textCont: patientNameCont,
                          status: _status,
                        ),
                        CustomTFEdit(
                          label: "Visit Code",
                          textCont: visitCodeCont,
                        ),
                        CustomTFEdit(
                          label: "Patient Code",
                          textCont: patientCodeCont,
                        ),
                        !_status
                            ? CustomEditButton(
                          onSave: () {
                            setState(() {
                              visit.cost = double.parse(costCont.text);
                              visit.visitDate = visitDateCont.text;
                              visit.addressCode = patientNameCont.text;
                              String url = DotEnv().env['EDIT_VISIT'];
                              url = url.replaceAll(
                                  "VISITCODE", visit.visitCode);
                              client.editObject(url, visit.toJson(), context);

                              _status = true;
                              FocusScope.of(context)
                                  .requestFocus(new FocusNode());
                            });
                          },
                          onCancel: () {
                            setState(() {
                              _status = true;
                              FocusScope.of(context)
                                  .requestFocus(FocusNode());
                            });
                          },
                        )
                            : new Container(),
                      ],
                    ),
                  ),
                ));
          }
        },
      ),
    );
  }

  Future<void> refreshList() {
    setState(() {
      _listFuture = updateAndGetList();
    });
  }

  Future<void> updateAndGetList() async {
    activeRole = await client.getActiveRole();
    if (activeRole.roleName == "Doctor" ||
        activeRole.roleName == "Nurse" ||
        activeRole.roleName == "Pharmacist" ||
        activeRole.roleName == "LabSpecialist" ||
        activeRole.roleName == "Radiographer") {
      isStRole = true;
    }
  }

  void handleClick(String value) async {
    switch (value) {
      case 'Diagnosis':
        {
          showDiagnosis();
        }
        break;
      case 'Prescriptions':
        {
          Navigator.pushNamed(context, '/prescriptions', arguments: visit);
        }
        break;
      case 'LabTests':
        {
          Navigator.pushNamed(context, '/LabTestsVisit', arguments: visit);
        }
        break;
      case 'Purchase LabTests':
        {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) =>
                    PurchasesLabTests(
                      newVisit: visit,
                    ),
              ));
        }
        break;
      case 'Surgeries':
        {
          Navigator.pushNamed(context, '/SurgeriesVisit', arguments: visit);
        }
        break;
      case 'Purchases Surgeries':
        {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) =>
                    PurchasesSurgeries(
                      newVisit: visit,
                    ),
              ));
        }
        break;
      case 'Radriographs':
        {
          Navigator.pushNamed(context, '/RadriographsVisit', arguments: visit);
        }
        break;
      case 'Purchases Radiographs':
        {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) =>
                    PurchasesRadiographs(
                      newVisit: visit,
                    ),
              ));
        }
        break;
      case 'Claim':
        {
          showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text(
                  "Add Claim",
                ),
                content: Text("new Claim"),
                actions: <Widget>[
                  FlatButton(
                    child: Text(
                      "Add",
                      style: TextStyle(
                        color: Color(0xFF07a383),
                      ),
                    ),
                    onPressed: () async {
                      String url = DotEnv().env['Add_Claim'];
                      url = url.replaceAll("visitCode", visit.visitCode);
                      var response = client.postWithTokenJSON({}, url, context);

                      Navigator.pop(context);
                    },
                  ),
                  FlatButton(
                    child: Text(
                      "Cancel",
                      style: TextStyle(
                        color: Color(0xFF07a383),
                      ),
                    ),
                    onPressed: () => Navigator.pop(context),
                  ),
                ],
              );
            },
          );
        }
        break;
      case 'Delete':
        {
          String url = DotEnv().env['DELETE_VISIT'];
          url = url.replaceAll("VISITCODE", visit.visitCode);
          await client.deleteObject(url, context, '/visits');
        }
        break;
    }
  }

  void showDiagnosis() {
    String action = "Save";
    if (visit.diagnosis == null)
      action = "Add";
    else
      diagnosisCont.text = visit.diagnosis.description;
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            "Diagnosis",
          ),
          content: TextField(
            controller: diagnosisCont,
          ),
          actions: <Widget>[
            FlatButton(
              child: Text(
                action,
                style: TextStyle(
                  color: Color(0xFF07a383),
                ),
              ),
              onPressed: () async {
                String url;
                var json;
                if (action == "Add") {
                  //new diagnosis
                  url = DotEnv().env['ADD_DIAGNOSIS'];
                  url = url.replaceAll("VISITCODE", visit.visitCode);
                  json = {"description": diagnosisCont.text};
                  print(url);
                  var response = client.postWithTokenJSON(json, url, context);
                  print(response);
                  Fluttertoast.showToast(
                      msg: response.toString(),
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.BOTTOM,
                      timeInSecForIos: 1,
                      backgroundColor: Colors.redAccent,
                      textColor: Colors.white,
                      fontSize: 16.0);
                } else {
                  // edit
                  url = DotEnv().env['EDIT_DIAGNOSIS'];
                  url = url.replaceAll("VISITCODE", visit.visitCode);
                  url = url.replaceAll(
                      "DIAGNOSISCODE", visit.diagnosis.diagnosisCode);
                  json = {
                    "description": diagnosisCont.text,
                    "diagnosisCode": ""
                  };
                  print(url);
                  var response = client.editObject(url, json, context);
                  print(response);
                  Fluttertoast.showToast(
                      msg: response.toString(),
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.BOTTOM,
                      timeInSecForIos: 1,
                      backgroundColor: Colors.redAccent,
                      textColor: Colors.white,
                      fontSize: 16.0);
                }
                visit.diagnosis = Diagnosis.fromJson(json);
                Navigator.pop(context);
                Navigator.pop(context);
                Navigator.pushReplacementNamed(context, '/detailsVisit',
                    arguments: visit);
              },
            ),
            FlatButton(
              child: Text(
                "Cancel",
                style: TextStyle(
                  color: Color(0xFF07a383),
                ),
              ),
              onPressed: () => Navigator.pop(context),
            ),
          ],
        );
      },
    );
  }
}
