import 'package:medcare/screen/visit/visit.dart';

class PurchaseLabTest {
  String note;
  String code;
  Visit visit;
  String scheduledDate;

  PurchaseLabTest({this.note, this.code, this.scheduledDate, this.visit});

  factory PurchaseLabTest.fromJson(Map<String, dynamic> json,
      {String visitCode}) {
    return PurchaseLabTest(
      note: json['note'],
      code: json['prescribedLabTestCode'],
      scheduledDate: json['scheduledDate'],
      visit: Visit.fromJson(json['visit']),
    );
  }
}
