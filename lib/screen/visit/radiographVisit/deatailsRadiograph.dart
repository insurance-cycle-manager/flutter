import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl/intl.dart';
import 'package:medcare/components/customEditButton.dart';
import 'package:medcare/components/customeTFEdit.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/unauthorized.dart';
import 'package:medcare/screen/visit/prescription/prescription.dart';
import 'package:medcare/screen/visit/radiographVisit/radriographVisit.dart';

class DetailsRadriographVisit extends StatefulWidget {
  @override
  State createState() {
    return _DetailsRadriographVisitState();
  }
}

class _DetailsRadriographVisitState extends State<DetailsRadriographVisit> {
  bool _status = true;
  final FocusNode myFocusNode = FocusNode();

  TextEditingController noteCont = TextEditingController();
  TextEditingController codeCont = TextEditingController();
  TextEditingController dateCont = TextEditingController();
  RadiographVisit radiograph;
  Future<RadiographVisit> _future;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    radiograph = ModalRoute.of(context).settings.arguments;

    refreshList();

    noteCont.text = radiograph.note;
    codeCont.text = radiograph.code;
    dateCont.text =
        DateFormat('d/M/y').format(DateTime.parse(radiograph.scheduledDate));
    return FutureBuilder<RadiographVisit>(
      future: _future,
      builder: (BuildContext context, AsyncSnapshot<RadiographVisit> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return SpinKitCircle(
            //https://github.com/jogboms/flutter_spinkit#-showcase
            color: Colors.teal,
          );
        } else if (snapshot.hasError) {
          return Scaffold(
            body: RefreshIndicator(
              onRefresh: refreshList,
              child: ListView(
                children: <Widget>[
                  SizedBox(
                    height: 250,
                  ),
                  Icon(Icons.error),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[Text("Something went wrong")],
                  ),
                  Expanded(
                    child: Text(
                      "Please check your internet connection",
                      style: TextStyle(fontSize: 15),
                    ),
                  )
                ],
              ),
            ),
          );
        } else {
          radiograph = snapshot.data ?? Prescription;
          return Scaffold(
            backgroundColor: Color(0xffE5E5E5),
            body: RefreshIndicator(
              onRefresh: refreshList,
              child: NestedScrollView(
                headerSliverBuilder:
                    (BuildContext context, bool innerBoxIsScrolled) {
                  return <Widget>[
                    SliverAppBar(
                      expandedHeight: 200.0,
                      floating: false,
                      pinned: true,
                      flexibleSpace: FlexibleSpaceBar(
                          centerTitle: true,
                          title: Text("Lab Test",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16.0,
                              )),
                          background: Image.asset(
                            'assets/images/prsc.jpg',
                            fit: BoxFit.cover,
                          )),
                      actions: <Widget>[
                        IconButton(
                          icon: Icon(
                            Icons.delete,
                            color: Colors.white,
                          ),
                          onPressed: () async {
                            String url = DotEnv().env['DELETE_Surgery_Visit'];
                            url =
                                url.replaceAll("SurgeryCode", radiograph.code);
                            url = url.replaceAll(
                                "VISITCODE", radiograph.visitCode);

                            await client.deleteObject(
                                url, context, '/SurgeriesVisit');
                          },
                        ),
                      ],
                    ),
                  ];
                },
                body: Container(
                  color: Color(0xffFFFFFF),
                  child: Padding(
                    padding: EdgeInsets.only(bottom: 25.0),
                    child: ListView(
                      children: <Widget>[
                        CustomTFEdit(
                          label: "Note",
                          status: _status,
                          textCont: noteCont,
                        ),
                        CustomTFEdit(
                          label: "Scheduled Date",
                          textCont: dateCont,
                        ),
                        CustomTFEdit(
                          label: "Lab Test Code",
                          textCont: codeCont,
                        ),
                        !_status
                            ? CustomEditButton(
                                onSave: () {
                                  setState(() async {
                                    var json = {
                                      "RadiographCode": radiograph.code,
                                      "visitCode": radiograph.visitCode,
                                      "note": radiograph.note,
                                    };
                                    String url =
                                        DotEnv().env['PUT_Radriograph_Visit'];
                                    url = url.replaceAll(
                                        "VISITCODE", radiograph.visitCode);
                                    url = url.replaceAll(
                                        "RadriographCode", radiograph.code);
                                    client.editObject(url, json, context);

                                    _status = true;
                                    FocusScope.of(context)
                                        .requestFocus(FocusNode());
                                  });
                                },
                                onCancel: () {
                                  setState(() {
                                    _status = true;
                                    FocusScope.of(context)
                                        .requestFocus(FocusNode());
                                  });
                                },
                              )
                            : Container(),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          );
        }
      },
    );
  }

  Future<void> refreshList() {
    setState(() {
      _future = updateAndGetList();
    });
  }

  Future<RadiographVisit> updateAndGetList() async {
    try {
      String url = DotEnv().env['GET_Radriograph_Visit'];
      url = url.replaceAll("VISITCODE", radiograph.visitCode);
      url = url.replaceAll("RadriographCode", radiograph.code);
      var response = await client.getPrivateData(url, context);

      final parsed = await jsonDecode(response.body);

      return RadiographVisit.fromJson(parsed["data"]);
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
    } catch (e) {}
  }
}
