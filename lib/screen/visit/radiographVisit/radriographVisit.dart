class RadiographVisit {
  String note;
  String code;
  String visitCode;
  String scheduledDate;

  RadiographVisit({this.note, this.code, this.scheduledDate, this.visitCode});

  factory RadiographVisit.fromJson(Map<String, dynamic> json,
      {String visitCode}) {
    return RadiographVisit(
      note: json['note'],
      code: json['RadiographCode'],
      scheduledDate: json['scheduledDate'],
      visitCode: visitCode,
    );
  }
}
