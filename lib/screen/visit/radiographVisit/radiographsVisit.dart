import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:medcare/components/customAppBar.dart';
import 'package:medcare/components/customCard.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/unauthorized.dart';
import 'package:medcare/screen/visit/radiographVisit/radriographVisit.dart';
import 'package:medcare/screen/visit/visit.dart';

class RadriographsVisit extends StatefulWidget {
  @override
  State createState() {
    return _RadriographsVisitState();
  }
}

class _RadriographsVisitState extends State<RadriographsVisit> {
  final TextEditingController _searchQuery = TextEditingController();
  bool _isSearching;
  String _searchText = "";
  List<RadiographVisit> radiographsVisit = [];
  Future<List<RadiographVisit>> _listFuture;
  Visit _visit;

  _RadriographsVisitState() {
    _searchQuery.addListener(() {
      if (_searchQuery.text.isEmpty) {
        setState(() {
          _isSearching = false;
          _searchText = "";
        });
      } else {
        setState(() {
          _isSearching = true;
          _searchText = _searchQuery.text;
        });
      }
    });
  }

  @override
  void initState() {
    setState(() {});
    _listFuture = updateAndGetList();
    _isSearching = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    refreshList();
    return Scaffold(
      appBar: CustomAppBar(
        title: "Radiograph Visit",
        actionIcon: Icon(Icons.search),
        isSearching: _isSearching,
        searchText: _searchText,
        searchQuery: _searchQuery,
      ),
      body: FutureBuilder<List<RadiographVisit>>(
        future: _listFuture,
        builder: (BuildContext context,
            AsyncSnapshot<List<RadiographVisit>> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return SpinKitCircle(
              //https://github.com/jogboms/flutter_spinkit#-showcase
              color: Colors.teal,
            );
          } else if (snapshot.hasError) {
            return Scaffold(
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 250,
                    ),
                    Icon(Icons.error),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[Text("Something went wrong")],
                    ),
                    Expanded(
                      child: Text(
                        "Please check your internet connection",
                        style: TextStyle(fontSize: 15),
                      ),
                    )
                  ],
                ),
              ),
            );
          } else {
            radiographsVisit = snapshot.data ?? <RadiographVisit>[];
            return Scaffold(
              backgroundColor: Color(0xffE5E5E5),
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView.builder(
                    itemCount: radiographsVisit.length,
                    itemBuilder: _isSearching ? _buildSearchList : _buildList),
              ),
              floatingActionButton: FloatingActionButton(
                child: Icon(Icons.add),
                onPressed: () {
                  Navigator.pushNamed(context, '/AddRadriographVisit',
                      arguments: _visit);
                },
              ),
            );
          }
        },
      ),
    );
  }

  Widget _buildList(BuildContext context, int index) {
    return CustomCard(
      name: radiographsVisit[index].note,
      dateOfBirth: radiographsVisit[index].scheduledDate,
      object: radiographsVisit[index],
      route: 'DetailsRadriographVisit',
    );
  }

  Widget _buildSearchList(BuildContext context, int index) {
    if (_searchText.isEmpty) {
      return CustomCard(
        name: radiographsVisit[index].note,
        object: radiographsVisit[index],
        route: 'DetailsRadriographVisit',
      );
    } else {
      String name = radiographsVisit[index].code;
      if (name.toLowerCase().contains(_searchText.toLowerCase())) {
        return CustomCard(
          name: radiographsVisit[index].note,
          object: radiographsVisit[index],
          route: 'DetailsRadriographVisit',
        );
      }
    }
    return Container(
      height: 0,
    );
  }

  Future<void> refreshList() {
    setState(() {
      _listFuture = updateAndGetList();
    });
  }

  Future<List<RadiographVisit>> updateAndGetList() async {
    try {
      _visit = ModalRoute.of(context).settings.arguments;
      String url = DotEnv().env['GET_Radriographs_Visit'];
      url = url.replaceAll("VISITCODE", _visit.visitCode);
      var response = await client.getPrivateData(url, context);

      final parsed = await jsonDecode(response.body);

      return (parsed["data"] as List)
          .map<RadiographVisit>((json) =>
              RadiographVisit.fromJson(json, visitCode: _visit.visitCode))
          .toList();
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
      return [];
    } catch (e) {}
  }
}
