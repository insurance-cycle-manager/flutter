class SurgeryVisit {
  String note;
  String code;
  String visitCode;
  String scheduledDate;

  SurgeryVisit({this.note, this.code, this.scheduledDate, this.visitCode});

  factory SurgeryVisit.fromJson(Map<String, dynamic> json, {String visitCode}) {
    return SurgeryVisit(
      note: json['note'],
      code: json['prescribedLabTestCode'],
      scheduledDate: json['scheduledDate'],
      visitCode: visitCode,
    );
  }
}
