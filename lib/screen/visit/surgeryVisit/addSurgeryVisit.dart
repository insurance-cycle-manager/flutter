import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:medcare/components/customButton.dart';
import 'package:medcare/components/customDatePicker.dart';
import 'package:medcare/components/customTextField.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/template/surgery/surgery.dart';
import 'package:medcare/screen/visit/visit.dart';

class AddSurgeryVisit extends StatefulWidget {
  final noteCon = TextEditingController();
  final labCodeCon = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  State createState() {
    return _AddSurgeryVisitState();
  }
}

class _AddSurgeryVisitState extends State<AddSurgeryVisit> {
  // final TextEditingController _typeAheadController = TextEditingController();

  Visit _visit;
  String _date = "Not set";
  Future<void> _listFuture;
  List<Surgery> _surgeries = [];
  Surgery selectedSurgery;

  @override
  void initState() {
    _listFuture = updateAndGetList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _visit = ModalRoute.of(context).settings.arguments;
    return Scaffold(
        appBar: AppBar(
          title: Text('Add Surgery'),
        ),
        body: FutureBuilder<void>(
          future: _listFuture,
          builder: (BuildContext context, AsyncSnapshot<void> snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return SpinKitCircle(
                color: Colors.teal,
              );
            } else if (snapshot.hasError) {
              return Scaffold(
                body: RefreshIndicator(
                  onRefresh: refreshList,
                  child: ListView(
                    children: <Widget>[
                      SizedBox(
                        height: 250,
                      ),
                      Icon(Icons.error),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[Text("Something went wrong")],
                      ),
                      Expanded(
                        child: Text(
                          "Please check your internet connection",
                          style: TextStyle(fontSize: 15),
                        ),
                      )
                    ],
                  ),
                ),
              );
            } else {
              return SingleChildScrollView(
                padding: EdgeInsets.only(top: 40),
                child: Form(
                  key: widget._formKey,
                  child: Wrap(
                    runSpacing: 25,
                    children: <Widget>[
                      CustomTextField(
                        labelText: "Note",
                        validationFun: (value) {
                          if (value.isEmpty) {
                            return 'can\'t be empty';
                          } else
                            return null;
                        },
                        controller: widget.noteCon,
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(30.0, 0.0, 20.0, 10.0),
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              color: Colors.grey[200],
                              border: Border.all()),
                          child: Padding(
                            padding: const EdgeInsets.only(left: 8, right: 8),
                            child: DropdownButtonFormField<Surgery>(
                              validator: (value) {
                                if (selectedSurgery == null)
                                  return "Please Choose one";
                                return null;
                              },
                              isExpanded: true,
                              hint: Text("Surgery"),
                              items: _surgeries
                                  .map((itm) => DropdownMenuItem(
                                        child: Text(itm.name),
                                        value: itm,
                                      ))
                                  .toList(),
                              value: selectedSurgery,
                              onChanged: (Surgery selectedSurgery) {
                                setState(() {
                                  this.selectedSurgery = selectedSurgery;
                                });
                              },
                            ),
                          ),
                        ),
                      ),
                      CustomDatePicker(
                        name: "Scheduled Date",
                        onPressed: (date) {
                          setState(() {
                            _date = date;
                          });
                        },
                      ),
                      CustomButton(
                        bName: "Submit",
                        bFunc: () {
                          var json = {
                            "Note": widget.noteCon.text,
                            "ScheduledDate": _date,
                            "SurgeryCode": selectedSurgery.surgeryCode
                          };
                          String url = DotEnv().env['POST_Surgery_Visit'];
                          url = url.replaceAll("VISITCODE", _visit.visitCode);
                          client.postWithTokenJSON(json, url, context);

                          Navigator.popUntil(
                              context, ModalRoute.withName('/SurgeriesVisit'));
                          Navigator.pop(context);
                          Navigator.pushNamed(context, '/SurgeriesVisit',
                              arguments: _visit);
                        },
                      )
                    ],
                  ),
                ),
              );
            }
          },
        ));
  }

  Future<void> refreshList() {
    setState(() {
      _listFuture = updateAndGetList();
    });
  }

  Future updateAndGetList() async {
    String url = DotEnv().env['GET_SURGERIES'];
    var response = await client.getPrivateData(url, context);
    final parsed = await jsonDecode(response.body);
    _surgeries = (parsed["data"] as List)
        .map<Surgery>((json) => Surgery.fromJson(json))
        .toList();
  }
}
