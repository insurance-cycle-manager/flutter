class LabTestVisit {
  String note;
  String code;
  String visitCode;
  String scheduledDate;

  LabTestVisit({this.note, this.code, this.scheduledDate, this.visitCode});

  factory LabTestVisit.fromJson(Map<String, dynamic> json, {String visitCode}) {
    return LabTestVisit(
      note: json['note'],
      code: json['prescribedLabTestCode'],
      scheduledDate: json['scheduledDate'],
      visitCode: visitCode,
    );
  }
}
