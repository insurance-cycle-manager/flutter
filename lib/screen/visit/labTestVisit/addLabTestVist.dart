import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:medcare/components/customButton.dart';
import 'package:medcare/components/customDatePicker.dart';
import 'package:medcare/components/customTextField.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/template/labTest/labTest.dart';
import 'package:medcare/screen/visit/visit.dart';

class AddLabTestVisit extends StatefulWidget {
  final noteCon = TextEditingController();
  final labCodeCon = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  State createState() {
    return _AddLabTestVisitState();
  }
}

class _AddLabTestVisitState extends State<AddLabTestVisit> {
  // final TextEditingController _typeAheadController = TextEditingController();

  Visit _visit;
  String _date = "Not set";
  Future<void> _listFuture;
  List<LabTest> _labTests = [];
  LabTest selectedLabTest;

  @override
  void initState() {
    _listFuture = updateAndGetList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _visit = ModalRoute.of(context).settings.arguments;
    return Scaffold(
        appBar: AppBar(
          title: Text('Add LabTest'),
        ),
        body: FutureBuilder<void>(
          future: _listFuture,
          builder: (BuildContext context, AsyncSnapshot<void> snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return SpinKitCircle(
                color: Colors.teal,
              );
            } else if (snapshot.hasError) {
              return Scaffold(
                body: RefreshIndicator(
                  onRefresh: refreshList,
                  child: ListView(
                    children: <Widget>[
                      SizedBox(
                        height: 250,
                      ),
                      Icon(Icons.error),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[Text("Something went wrong")],
                      ),
                      Expanded(
                        child: Text(
                          "Please check your internet connection",
                          style: TextStyle(fontSize: 15),
                        ),
                      )
                    ],
                  ),
                ),
              );
            } else {
              return SingleChildScrollView(
                padding: EdgeInsets.only(top: 40),
                child: Form(
                  key: widget._formKey,
                  child: Wrap(
                    runSpacing: 25,
                    children: <Widget>[
                      CustomTextField(
                        labelText: "Note",
                        validationFun: (value) {
                          if (value.isEmpty) {
                            return 'can\'t be empty';
                          } else
                            return null;
                        },
                        controller: widget.noteCon,
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(30.0, 0.0, 20.0, 10.0),
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              color: Colors.grey[200],
                              border: Border.all()),
                          child: Padding(
                            padding: const EdgeInsets.only(left: 8, right: 8),
                            child: DropdownButtonFormField<LabTest>(
                              validator: (value) {
                                if (selectedLabTest == null)
                                  return "Please Choose one";
                                return null;
                              },
                              isExpanded: true,
                              hint: Text("Lab Test"),
                              items: _labTests
                                  .map((itm) => DropdownMenuItem(
                                        child: Text(itm.name),
                                        value: itm,
                                      ))
                                  .toList(),
                              value: selectedLabTest,
                              onChanged: (LabTest selectedLabTest) {
                                setState(() {
                                  this.selectedLabTest = selectedLabTest;
                                });
                              },
                            ),
                          ),
                        ),
                      ),
                      CustomDatePicker(
                        name: "Scheduled Date",
                        onPressed: (date) {
                          setState(() {
                            _date = date;
                          });
                        },
                      ),
                      CustomButton(
                        bName: "Submit",
                        bFunc: () {
                          var json = {
                            "note": widget.noteCon.text,
                            "scheduledDate": _date,
                            "labTestCode": selectedLabTest.labTestCode
                          };
                          String url = DotEnv().env['POST_LabTest_Visit'];
                          url = url.replaceAll("VISITCODE", _visit.visitCode);
                          client.postWithTokenJSON(json, url, context);

                          Navigator.popUntil(
                              context, ModalRoute.withName('/LabTestsVisit'));
                          Navigator.pop(context);
                          Navigator.pushNamed(context, '/LabTestsVisit',
                              arguments: _visit);
                        },
                      )
                    ],
                  ),
                ),
              );
            }
          },
        ));
  }

  Future<void> refreshList() {
    setState(() {
      _listFuture = updateAndGetList();
    });
  }

  Future updateAndGetList() async {
    String url = DotEnv().env['GET_LABTESTS'];
    var response = await client.getPrivateData(url, context);
    final parsed = await jsonDecode(response.body);
    _labTests = (parsed["data"] as List)
        .map<LabTest>((json) => LabTest.fromJson(json))
        .toList();
  }
}
