import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:medcare/components/customButton.dart';
import 'package:medcare/components/customTextField.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/template/argsTemplate.dart';
import 'package:medcare/screen/visit/visit.dart';

class AddPrescription extends StatefulWidget {
  final noteCon = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  State createState() {
    return _AddPrescriptionState();
  }
}

class _AddPrescriptionState extends State<AddPrescription> {
  // final TextEditingController _typeAheadController = TextEditingController();

  List<String> _drugsCode = [];

  Visit _visit;

  @override
  Widget build(BuildContext context) {
    _visit = ModalRoute
        .of(context)
        .settings
        .arguments;
    return Scaffold(
      appBar: AppBar(
        title: Text('Add Insurance Company'),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.only(top: 40),
        child: Form(
          key: widget._formKey,
          child: Wrap(
            runSpacing: 25,
            children: <Widget>[
              CustomTextField(
                labelText: "Note",
                validationFun: (value) {
                  if (value.isEmpty) {
                    return 'can\'t be empty';
                  } else
                    return null;
                },
                controller: widget.noteCon,
              ),
              CustomButton(
                bName: "Drugs",
                bFunc: () async {
                  ArgsTemplate arg = ArgsTemplate("DRUGS", _drugsCode);
                  final result = await Navigator.pushNamed(
                      context, '/list template',
                      arguments: arg);
                  _drugsCode = result;
                },
              ),
              CustomButton(
                bName: "Submit",
                bFunc: () {
                  var json = {
                    "note": widget.noteCon.text,
                    "drugs": _drugsCode,
                  };
                  String url = DotEnv().env['POST_PRESCRIPTION'];
                  url = url.replaceAll("VISITCODE", _visit.visitCode);
                  client.postWithTokenJSON(json, url, context);

                  Navigator.popUntil(
                      context, ModalRoute.withName('/prescriptions'));
                  Navigator.pop(context);
                  Navigator.pushNamed(context, '/prescriptions',
                      arguments: _visit);
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
