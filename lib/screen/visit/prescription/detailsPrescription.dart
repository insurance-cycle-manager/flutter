import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:medcare/components/customEditButton.dart';
import 'package:medcare/components/customEditIcon.dart';
import 'package:medcare/components/customeTFEdit.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/template/argsTemplate.dart';
import 'package:medcare/screen/unauthorized.dart';
import 'package:medcare/screen/visit/prescription/prescription.dart';

class DetailsPrescription extends StatefulWidget {
  @override
  State createState() {
    return _DetailsPrescriptionState();
  }
}

class _DetailsPrescriptionState extends State<DetailsPrescription> {
  bool _status = true;
  final FocusNode myFocusNode = FocusNode();

  TextEditingController noteCont = TextEditingController();
  TextEditingController codeCont = TextEditingController();
  TextEditingController drugsCont = TextEditingController();
  Prescription prescription;
  Future<Prescription> _future;
  List<String> _drugsCode;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    prescription = ModalRoute.of(context).settings.arguments;

    refreshList();

    noteCont.text = prescription.note;
    codeCont.text = prescription.code;
    return FutureBuilder<Prescription>(
      future: _future,
      builder: (BuildContext context, AsyncSnapshot<Prescription> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return SpinKitCircle(
            //https://github.com/jogboms/flutter_spinkit#-showcase
            color: Colors.teal,
          );
        } else if (snapshot.hasError) {
          return Scaffold(
            body: RefreshIndicator(
              onRefresh: refreshList,
              child: ListView(
                children: <Widget>[
                  SizedBox(
                    height: 250,
                  ),
                  Icon(Icons.error),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[Text("Something went wrong")],
                  ),
                  Expanded(
                    child: Text(
                      "Please check your internet connection",
                      style: TextStyle(fontSize: 15),
                    ),
                  )
                ],
              ),
            ),
          );
        } else {
          prescription = snapshot.data ?? Prescription;
          prescription.drugs.forEach((f) {
            drugsCont.text += (f.name + " - ");
          });
          return Scaffold(
            backgroundColor: Color(0xffE5E5E5),
            body: RefreshIndicator(
              onRefresh: refreshList,
              child: NestedScrollView(
                headerSliverBuilder:
                    (BuildContext context, bool innerBoxIsScrolled) {
                  return <Widget>[
                    SliverAppBar(
                      expandedHeight: 200.0,
                      floating: false,
                      pinned: true,
                      flexibleSpace: FlexibleSpaceBar(
                          centerTitle: true,
                          title: Text("prescription",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16.0,
                              )),
                          background: Image.asset(
                            'assets/images/prsc.jpg',
                            fit: BoxFit.cover,
                          )),
                      actions: <Widget>[
                        IconButton(
                          icon: Icon(
                            Icons.delete,
                            color: Colors.white,
                          ),
                          onPressed: () async {
                            String url = DotEnv().env['DELETE_PRESCRIPTION'];
                            url = url.replaceAll(
                                "PRESCRIPTIONCODE", prescription.code);
                            url = url.replaceAll(
                                "VISITCODE", prescription.visitCode);

                            await client.deleteObject(
                                url, context, '/prescriptions');
                          },
                        ),
                      ],
                    ),
                  ];
                },
                body: Container(
                  color: Color(0xffFFFFFF),
                  child: Padding(
                    padding: EdgeInsets.only(bottom: 25.0),
                    child: ListView(
                      children: <Widget>[
                        Padding(
                            padding: EdgeInsets.only(
                                left: 25.0, right: 25.0, top: 25.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Text(
                                      'Edit',
                                      style: TextStyle(
                                          fontSize: 18.0,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    _status
                                        ? CustomEditIcon(
                                            onClick: () {
                                              setState(() {
                                                _status = false;
                                              });
                                            },
                                          )
                                        : Container(),
                                  ],
                                )
                              ],
                            )),
                        CustomTFEdit(
                          label: "Note",
                          status: _status,
                          textCont: noteCont,
                        ),
                        CustomTFEdit(
                          label: "Drugs",
                          textCont: drugsCont,
                        ),
                        CustomTFEdit(
                          label: "Prescription Code",
                          textCont: codeCont,
                        ),
                        !_status
                            ? CustomEditButton(
                                onSave: () {
                                  setState(() async {
                                    ArgsTemplate arg =
                                        ArgsTemplate("DRUGS", _drugsCode);
                                    final result = await Navigator.pushNamed(
                                        context, '/list template',
                                        arguments: arg);

                                    _drugsCode = result;
                                    var json = {
                                      "prescriptionCode": prescription.code,
                                      "visitCode": prescription.visitCode,
                                      "note": prescription.note,
                                      "drugs": _drugsCode
                                    };
                                    String url =
                                        DotEnv().env['PUT_PRESCRIPTION'];
                                    url = url.replaceAll(
                                        "VISITCODE", prescription.visitCode);
                                    url = url.replaceAll(
                                        "PRESCRIPTIONCODE", prescription.code);
                                    client.editObject(url, json, context);

                                    _status = true;
                                    FocusScope.of(context)
                                        .requestFocus(FocusNode());
                                  });
                                },
                                onCancel: () {
                                  setState(() {
                                    _status = true;
                                    FocusScope.of(context)
                                        .requestFocus(FocusNode());
                                  });
                                },
                              )
                            : Container(),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          );
        }
      },
    );
  }

  Future<void> refreshList() {
    setState(() {
      _future = updateAndGetList();
    });
  }

  Future<Prescription> updateAndGetList() async {
    try {
      String url = DotEnv().env['GET_PRESCRIPTION'];
      url = url.replaceAll("VISITCODE", prescription.visitCode);
      url = url.replaceAll("PRESCRIPTIONCODE", prescription.code);
      var response = await client.getPrivateData(url, context);

      final parsed = await jsonDecode(response.body);

      return Prescription.fromJson(parsed["data"]);
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
    } catch (e) {}
  }

//  @override
//  Widget build(BuildContext context) {
//    prescription = ModalRoute.of(context).settings.arguments;
//
//    noteCont.text = prescription.note;
//    codeCont.text = prescription.code;
//    prescription.drugs.forEach((f) {
//      drugsCont.text += (f.name + "\n");
//    });
//    return Scaffold(
//      body: NestedScrollView(
//        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
//          return <Widget>[
//            SliverAppBar(
//              expandedHeight: 200.0,
//              floating: false,
//              pinned: true,
//              flexibleSpace: FlexibleSpaceBar(
//                  centerTitle: true,
//                  title: Text("prescription",
//                      style: TextStyle(
//                        color: Colors.white,
//                        fontSize: 16.0,
//                      )),
//                  background: Image.network(
//                    "https://images.pexels.com/photos/396547/pexels-photo-396547.jpeg?auto=compress&cs=tinysrgb&h=350",
//                    fit: BoxFit.cover,
//                  )),
//              actions: <Widget>[
//                IconButton(
//                  icon: Icon(
//                    Icons.delete,
//                    color: Colors.white,
//                  ),
//                  onPressed: () async {
//                    String url = DotEnv().env['DELETE_PRESCRIPTION'];
//                    url = url.replaceAll("PRESCRIPTIONCODE", prescription.code);
//                    url = url.replaceAll("VISITCODE", prescription.visitCode);
//
//                    await client.deleteObject(url, context, '/prescriptions');
//                  },
//                ),
//              ],
//            ),
//          ];
//        },
//        body: Container(
//          color: Color(0xffFFFFFF),
//          child: Padding(
//            padding: EdgeInsets.only(bottom: 25.0),
//            child: ListView(
//              children: <Widget>[
//                Padding(
//                    padding:
//                        EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
//                    child: Row(
//                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                      mainAxisSize: MainAxisSize.max,
//                      children: <Widget>[
//                        Column(
//                          mainAxisAlignment: MainAxisAlignment.start,
//                          mainAxisSize: MainAxisSize.min,
//                          children: <Widget>[
//                            Text(
//                              'Edit',
//                              style: TextStyle(
//                                  fontSize: 18.0, fontWeight: FontWeight.bold),
//                            ),
//                          ],
//                        ),
//                        Column(
//                          mainAxisAlignment: MainAxisAlignment.end,
//                          mainAxisSize: MainAxisSize.min,
//                          children: <Widget>[
//                            _status
//                                ? CustomEditIcon(
//                                    onClick: () {
//                                      setState(() {
//                                        _status = false;
//                                      });
//                                    },
//                                  )
//                                : Container(),
//                          ],
//                        )
//                      ],
//                    )),
//                CustomTFEdit(
//                  label: "Note",
//                  status: _status,
//                  textCont: noteCont,
//                ),
//                CustomTFEdit(
//                  label: "Drugs",
//                  textCont: drugsCont,
//                ),
//                CustomTFEdit(
//                  label: "Prescription Code",
//                  textCont: codeCont,
//                ),
//                !_status
//                    ? CustomEditButton(
//                        onSave: () {
//                          setState(() {
//                            prescription.note = noteCont.text;
//                            //prescription.drugs = drugsCont.text;
//                            //TODO edit prescription
//                            String url = DotEnv().env['PUT_PRESCRIPTION'];
//                            url = url.replaceAll(
//                                "VISITCODE", prescription.visitCode);
//                            url = url.replaceAll(
//                                "PRESCRIPTIONCODE", prescription.code);
//                            client.editObject(url, prescription.toJson());
//
//                            _status = true;
//                            FocusScope.of(context).requestFocus(FocusNode());
//                          });
//                        },
//                        onCancel: () {
//                          setState(() {
//                            _status = true;
//                            FocusScope.of(context).requestFocus(FocusNode());
//                          });
//                        },
//                      )
//                    : Container(),
//              ],
//            ),
//          ),
//        ),
//      ),
//    );
//  }
}
