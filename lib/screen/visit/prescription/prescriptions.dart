import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:medcare/components/customAppBar.dart';
import 'package:medcare/components/customCard.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/unauthorized.dart';
import 'package:medcare/screen/visit/prescription/prescription.dart';
import 'package:medcare/screen/visit/visit.dart';

class Prescriptions extends StatefulWidget {
  @override
  State createState() {
    return _PrescriptionsState();
  }
}

class _PrescriptionsState extends State<Prescriptions> {
  final TextEditingController _searchQuery = TextEditingController();
  bool _isSearching;
  String _searchText = "";
  List<Prescription> prescriptions = [];
  Future<List<Prescription>> _listFuture;
  Visit _visit;

  _PrescriptionsState() {
    _searchQuery.addListener(() {
      if (_searchQuery.text.isEmpty) {
        setState(() {
          _isSearching = false;
          _searchText = "";
        });
      } else {
        setState(() {
          _isSearching = true;
          _searchText = _searchQuery.text;
        });
      }
    });
  }

  @override
  void initState() {
    setState(() {});
    _listFuture = updateAndGetList();
    _isSearching = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    refreshList();
    return Scaffold(
      appBar: CustomAppBar(
        title: "Prescriptions",
        actionIcon: Icon(Icons.search),
        isSearching: _isSearching,
        searchText: _searchText,
        searchQuery: _searchQuery,
      ),
      body: FutureBuilder<List<Prescription>>(
        future: _listFuture,
        builder:
            (BuildContext context, AsyncSnapshot<List<Prescription>> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return SpinKitCircle(
              //https://github.com/jogboms/flutter_spinkit#-showcase
              color: Colors.teal,
            );
          } else if (snapshot.hasError) {
            return Scaffold(
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 250,
                    ),
                    Icon(Icons.error),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[Text("Something went wrong")],
                    ),
                    Expanded(
                      child: Text(
                        "Please check your internet connection",
                        style: TextStyle(fontSize: 15),
                      ),
                    )
                  ],
                ),
              ),
            );
          } else {
            prescriptions = snapshot.data ?? <Prescription>[];
            return Scaffold(
              backgroundColor: Color(0xffE5E5E5),
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView.builder(
                    itemCount: prescriptions.length,
                    itemBuilder: _isSearching ? _buildSearchList : _buildList),
              ),
              floatingActionButton: FloatingActionButton(
                child: Icon(Icons.add),
                onPressed: () {
                  Navigator.pushNamed(context, '/add prescription',
                      arguments: _visit);
                },
              ),
            );
          }
        },
      ),
    );
  }

  Widget _buildList(BuildContext context, int index) {
    return CustomCard(
      name: prescriptions[index].note,
      object: prescriptions[index],
      route: 'detailsPrescription',
    );
  }

  Widget _buildSearchList(BuildContext context, int index) {
    if (_searchText.isEmpty) {
      return CustomCard(
        name: prescriptions[index].note,
        object: prescriptions[index],
        route: 'detailsPrescription',
      );
    } else {
      String name = prescriptions[index].code;
      if (name.toLowerCase().contains(_searchText.toLowerCase())) {
        return CustomCard(
          name: prescriptions[index].note,
          object: prescriptions[index],
          route: 'detailsPrescription',
        );
      }
    }
    return Container(
      height: 0,
    );
  }

  Future<void> refreshList() {
    setState(() {
      _listFuture = updateAndGetList();
    });
  }

  Future<List<Prescription>> updateAndGetList() async {
    try {
      _visit = ModalRoute.of(context).settings.arguments;
      String url = DotEnv().env['GET_PRESCRIPTIONS'];
      url = url.replaceAll("VISITCODE", _visit.visitCode);
      var response = await client.getPrivateData(url, context);

      final parsed = await jsonDecode(response.body);

      return (parsed["data"] as List)
          .map<Prescription>((json) =>
              Prescription.fromJson(json, visitCode: _visit.visitCode))
          .toList();
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
    } catch (e) {}
  }
}
