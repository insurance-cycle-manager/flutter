import 'package:medcare/screen/template/drug/drug.dart';

class Prescription {
  String note;
  String code;
  String visitCode;
  List<Drug> drugs;

  Prescription({this.note, this.code, this.drugs, this.visitCode});

  factory Prescription.fromJson(Map<String, dynamic> json, {String visitCode}) {
    List<Drug> _drugs = [];
    if (json['drugs'] != null) {
      var drugsObjJson = json['drugs'] as List;
      _drugs = drugsObjJson.map((drugJson) => Drug.fromJson(drugJson)).toList();
    }
    return Prescription(
      note: json['note'],
      code: json['prescriptionCode'],
      visitCode: visitCode,
      drugs: _drugs,
    );
  }

  Map<String, dynamic> toJson() =>
      {'note': note, 'code': code, 'visitCode': visitCode, 'drugs': drugs};
}
