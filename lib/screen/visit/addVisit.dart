import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:medcare/components/customButton.dart';
import 'package:medcare/components/customTextField.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/package/package.dart';
import 'package:medcare/screen/patient/patient.dart';
import 'package:medcare/screen/profile/role/Role.dart';
import 'package:medcare/screen/serviceProvider/serviceProvider.dart';
import 'package:medcare/screen/tpa/serviceProviderConnection.dart';

class AddVisit extends StatefulWidget {
  final costCont = TextEditingController();
  final baseVisitCodeCont = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  State<StatefulWidget> createState() {
    return _AddVisitState();
  }
}

class _AddVisitState extends State<AddVisit> {
  final TextEditingController _typeAheadController = TextEditingController();
  String _patientCode;
  String selectedVisitType;
  Package selectedPatientPackage;
  ServiceProvider selectedSp;
  ServiceProviderConnection selectedSPConnCode;
  List<ServiceProvider> _sps = [];
  List<ServiceProviderConnection> _spConnection = [];
  List<Package> _packages = [];

  Future<void> _listFuture;
  Role activeRole;
  List<String> staffTypes = [];

  @override
  void initState() {
    _listFuture = updateAndGetList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Add Visit'),
        ),
        body: FutureBuilder<void>(
          future: _listFuture,
          builder: (BuildContext context, AsyncSnapshot<void> snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return SpinKitCircle(
                color: Colors.teal,
              );
            } else if (snapshot.hasError) {
              return Scaffold(
                body: RefreshIndicator(
                  onRefresh: refreshList,
                  child: ListView(
                    children: <Widget>[
                      SizedBox(
                        height: 250,
                      ),
                      Icon(Icons.error),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[Text("Something went wrong")],
                      ),
                      Expanded(
                        child: Text(
                          "Please check your internet connection",
                          style: TextStyle(fontSize: 15),
                        ),
                      )
                    ],
                  ),
                ),
              );
            } else {
              return SingleChildScrollView(
                padding: EdgeInsets.only(top: 40),
                child: Form(
                  key: widget._formKey,
                  child: Wrap(
                    runSpacing: 25,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.fromLTRB(30.0, 10.0, 20.0, 10.0),
                        child: TypeAheadFormField(
                          textFieldConfiguration: TextFieldConfiguration(
                              controller: this._typeAheadController,
                              decoration: InputDecoration(
                                labelText: 'Patient name',
                                border: OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.teal)),
                              )),
                          suggestionsCallback: (pattern) async {
                            var response = await client.getPrivateData(
                                DotEnv().env['GET_PATIENTS'], context);

                            final parsed = await jsonDecode(response.body);

                            return (parsed["data"] as List)
                                .map<Patient>((json) {
                              if ((json['name'] as String).contains(pattern)) {
                                return Patient.fromJson(json);
                              }
                              return null;
                            }).toList();
                          },
                          itemBuilder: (context, suggestion) {
                            if (suggestion != null)
                              return ListTile(
                                title: Text(suggestion.name),
                              );
                            return Container(
                              height: 0,
                            );
                          },
                          transitionBuilder:
                              (context, suggestionsBox, controller) {
                            return suggestionsBox;
                          },
                          onSuggestionSelected: (suggestion) {
                            selectedPatientPackage = null;
                            _packages.clear();
                            this._typeAheadController.text = suggestion.name;
                            _patientCode = suggestion.patientCode;
                            onPatientSelected();
                          },
                          validator: (String value) {
                            if (value.isEmpty) {
                              return 'Please select a Patient';
                            }
                            return null;
                          },
                          onSaved: (value) => this._patientCode = value,
                        ),
                      ),
                      _buildPatientPackages(),
                      _buildSpCodeAndConnection(),
                      Padding(
                        padding: EdgeInsets.fromLTRB(30.0, 0.0, 20.0, 0.0),
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              color: Colors.grey[200],
                              border: Border.all()),
                          child: Padding(
                            padding: const EdgeInsets.only(left: 8, right: 8),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButtonFormField<String>(
                                  validator: (value) {
                                    if (selectedVisitType == null)
                                      return "Visit must have a type";
                                    return null;
                                  },
                                  isExpanded: true,
                                  hint: Text("Visit type"),
                                  value: selectedVisitType,
                                  items: <String>['CheckUp', 'FollowUp']
                                      .map<DropdownMenuItem<String>>(
                                          (String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  }).toList(),
                                  onChanged: (value) {
                                    setState(() {
                                      selectedVisitType = value;
                                    });
                                  }),
                            ),
                          ),
                        ),
                      ),
                      CustomTextField(
                        labelText: "Cost",
                        keyboardType: TextInputType.number,
                        validationFun: (String value) {
                          if (value.isEmpty) {
                            return 'can\'t be empty';
                          } else if (int.parse(value) < 0) {
                            return "Cost Can't be negative";
                          } else
                            return null;
                        },
                        controller: widget.costCont,
                      ),
                      CustomTextField(
                        labelText: "Base Visit Code",
                        controller: widget.baseVisitCodeCont,
                      ),
                      CustomButton(
                        bName: "Submit",
                        bFunc: () {
                          if (widget._formKey.currentState.validate()) {
                            var json = {
                              "Cost": widget.costCont.text,
                              "Type": selectedVisitType,
                              "PatientCode": _patientCode,
                              "PatientPackageCode":
                                  selectedPatientPackage.insurancePackageCode,
                              "ServiceProviderCode":
                                  selectedSp.serviceProviderCode,
                              "ServiceProviderConnectionCode":
                                  selectedSPConnCode.connectionCode
                            };
                            if (widget.baseVisitCodeCont.text.isNotEmpty) {
                              json["baseVisitCode"] =
                                  widget.baseVisitCodeCont.text;
                            }
                            client.postWithTokenJSON(
                                json,
                                DotEnv()
                                    .env['ADD_VISIT']
                                    .replaceAll("STType", activeRole.roleName),
                                context);
                            Navigator.popUntil(
                                context, ModalRoute.withName('/visits'));
                            Navigator.pop(context);
                            Navigator.pushNamed(context, '/visits');
                          }
                        },
                      ),
                      SizedBox(
                        height: 150,
                      )
                    ],
                  ),
                ),
              );
            }
          },
        ));
  }

  Future<void> refreshList() {
    setState(() {
      _listFuture = updateAndGetList();
    });
  }

  Future updateAndGetList() async {
    activeRole = await client.getActiveRole();

    ///Load SP by staff(me)
    String url = DotEnv()
        .env['GET_USER_SERVICE_STAFFERS_DETAILS']
        .replaceAll("SPTYPE", activeRole.roleName);
    var response = await client.getPrivateData(url, context);
    final parsed = await jsonDecode(response.body);
    _sps = (parsed["data"]["serviceProviders"] as List)
        .map<ServiceProvider>((json) => ServiceProvider.fromJson(json))
        .toList();
  }

  Widget _buildSpCodeAndConnection() {
    return Padding(
      padding: EdgeInsets.fromLTRB(30.0, 0.0, 20.0, 10.0),
      child: Column(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.0),
                color: Colors.grey[200],
                border: Border.all()),
            child: Padding(
              padding: const EdgeInsets.only(left: 8, right: 8),
              child: DropdownButtonFormField<ServiceProvider>(
                validator: (value) {
                  if (selectedSp == null) return "Please choose la bla bla ";
                  return null;
                },
                isExpanded: true,
                hint: Text("Service Provider"),
                items: _sps
                    .map((itm) =>
                    DropdownMenuItem(
                      child: Text(itm.title),
                      value: itm,
                    ))
                    .toList(),
                value: selectedSp,
                onChanged: _onSPCodeSelected,
              ),
            ),
          ),
          SizedBox(
            height: 30,
          ),
          Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.0),
                color: Colors.grey[200],
                border: Border.all()),
            child: Padding(
              padding: const EdgeInsets.only(left: 8, right: 8),
              child: DropdownButtonFormField<ServiceProviderConnection>(
                validator: (value) {
                  if (selectedSPConnCode == null)
                    return "Please choose the tpa";
                  return null;
                },
                isExpanded: true,
                hint: Text("Service Provider Connected TPA"),
                items: _spConnection
                    .map((itm) =>
                    DropdownMenuItem(
                      child: Text(itm.tpa.name),
                      value: itm,
                    ))
                    .toList(),
                value: selectedSPConnCode,
                onChanged: _onSPConnCodeSelected,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildPatientPackages() {
    return Padding(
      padding: EdgeInsets.fromLTRB(30.0, 0.0, 20.0, 10.0),
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            color: Colors.grey[200],
            border: Border.all()),
        child: Padding(
          padding: const EdgeInsets.only(left: 8, right: 8),
          child: DropdownButtonFormField<Package>(
            validator: (value) {
              if (selectedPatientPackage == null)
                return "Patient must be covered by health insurance";
              return null;
            },
            isExpanded: true,
            hint: Text("Patient Package"),
            items: _packages
                .map((itm) =>
                DropdownMenuItem(
                  child: Text(itm.name),
                  value: itm,
                ))
                .toList(),
            value: selectedPatientPackage,
            onChanged: (Package selectedPatientPackage) {
              setState(() {
                this.selectedPatientPackage = selectedPatientPackage;
              });
            },
          ),
        ),
      ),
    );
  }

  void _showLoadingDialog() {
    showDialog(
      context: context,
      builder: (context) {
        return WillPopScope(
          onWillPop: () async => false,
          child: Center(
            child: CupertinoActivityIndicator(animating: true),
          ),
        );
      },
    );
  }

  void _onSPCodeSelected(ServiceProvider selectedSpCode) async {
    try {
      _showLoadingDialog();
      selectedSPConnCode = null;
      _spConnection.clear();

      ///fetch list connection
      String url = DotEnv().env['Get_Connection_By_Staff_And_SpCode'];
      url = url.replaceAll("SpType", selectedSpCode.type);
      url = url.replaceAll("SpCode", selectedSpCode.serviceProviderCode);
      var response = await client.getPrivateData(url, context);
      final parsed = await jsonDecode(response.body);
      _spConnection = (parsed["data"] as List)
          .map<ServiceProviderConnection>(
              (json) => ServiceProviderConnection.fromJson(json))
          .toList();
      setState(() {
        this.selectedSp = selectedSpCode;
      });

      Navigator.pop(context);
    } catch (e) {
      //TODO: handle error
      rethrow;
    }
  }

  void _onSPConnCodeSelected(ServiceProviderConnection selectedSPConnCode) {
    setState(() {
      this.selectedSPConnCode = selectedSPConnCode;
    });
  }

  void onPatientSelected() async {
    _showLoadingDialog();
    String url = DotEnv()
        .env['GET_USER_PATIENT_PACKAGE']
        .replaceAll("PATIENTCODE", _patientCode);
    var response = await client.getPrivateData(url, context);
    final parsed = await jsonDecode(response.body);
    _packages = (parsed["data"] as List)
        .map<Package>((json) => Package.fromJson(json))
        .toList();
    setState(() {});
    Navigator.pop(context);
  }
}

//https://dartpad.dartlang.org/5dd318e491031df8b31a8fc7f2fbe320
