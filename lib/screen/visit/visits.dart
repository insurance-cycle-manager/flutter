import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:medcare/components/customAppBar.dart';
import 'package:medcare/components/customCard.dart';
import 'package:medcare/components/customHasNoData.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/auth/user.dart';
import 'package:medcare/screen/profile/role/Role.dart';
import 'package:medcare/screen/unauthorized.dart';
import 'package:medcare/screen/visit/visit.dart';

class VisitsPage extends StatefulWidget {
  @override
  State createState() {
    return _VisitsPageState();
  }
}

class _VisitsPageState extends State<VisitsPage> {
  final TextEditingController _searchQuery = TextEditingController();
  bool _isSearching;
  String _searchText = "";
  List<Visit> visits = [];
  Future<List<Visit>> _listFuture;
  bool isStRole = false;
  Role activeRole;

  _VisitsPageState() {
    _searchQuery.addListener(() {
      if (_searchQuery.text.isEmpty) {
        setState(() {
          _isSearching = false;
          _searchText = "";
        });
      } else {
        setState(() {
          _isSearching = true;
          _searchText = _searchQuery.text;
        });
      }
    });
  }

  @override
  void initState() {
    setState(() {});
    _listFuture = updateAndGetList();
    _isSearching = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        title: "Visits",
        actionIcon: Icon(Icons.search),
        isSearching: _isSearching,
        searchText: _searchText,
        searchQuery: _searchQuery,
      ),
      body: FutureBuilder<List<Visit>>(
        future: _listFuture,
        builder: (BuildContext context, AsyncSnapshot<List<Visit>> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return SpinKitCircle(
              color: Colors.teal,
            );
          } else if (snapshot.hasError) {
            return Scaffold(
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 250,
                    ),
                    Icon(Icons.error),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[Text("Something went wrong")],
                    ),
                    Expanded(
                      child: Text(
                        "Please check your internet connection",
                        style: TextStyle(fontSize: 15),
                      ),
                    )
                  ],
                ),
              ),
            );
          } else {
            visits = snapshot.data ?? <Visit>[];
            if (visits.length > 0) {
              return Scaffold(
                backgroundColor: Color(0xffE5E5E5),
                body: RefreshIndicator(
                  onRefresh: refreshList,
                  child: ListView.builder(
                      itemCount: visits.length,
                      itemBuilder:
                          _isSearching ? _buildSearchList : _buildList),
                ),
                floatingActionButton: isStRole
                    ? FloatingActionButton(
                        child: Icon(Icons.add),
                        onPressed: () {
                          Navigator.pushNamed(context, '/addVisit');
                        },
                      )
                    : Container(),
              );
            } else {
              return Scaffold(
                backgroundColor: Color(0xffE5E5E5),
                body: RefreshIndicator(
                  onRefresh: refreshList,
                  child: CustomHasNoData(),
                ),
                floatingActionButton: isStRole
                    ? FloatingActionButton(
                        child: Icon(Icons.add),
                        onPressed: () {
                          Navigator.pushNamed(context, '/addVisit');
                        },
                      )
                    : Container(),
              );
            }
          }
        },
      ),
    );
  }

  Widget _buildList(BuildContext context, int index) {
    return CustomCard(
      name: visits[index].type,
      gender: visits[index].cost.toString(),
      dateOfBirth: visits[index].visitDate,
      object: visits[index],
      route: 'detailsVisit',
      dateFormatPattern: 'd/M/y H:m',
    );
  }

  Widget _buildSearchList(BuildContext context, int index) {
    if (_searchText.isEmpty) {
      return CustomCard(
        name: visits[index].type,
        gender: visits[index].cost.toString(),
        dateOfBirth: visits[index].visitDate,
        object: visits[index],
        route: 'detailsVisit',
        dateFormatPattern: 'd/M/y H:m',
      );
    } else {
      String name = visits[index].toString();
      if (name.toLowerCase().contains(_searchText.toLowerCase())) {
        return CustomCard(
          name: visits[index].type,
          gender: visits[index].cost.toString(),
          dateOfBirth: visits[index].visitDate,
          object: visits[index],
          route: 'detailsVisit',
          dateFormatPattern: 'd/M/y H:m',
        );
      }
    }
    return Container(
      height: 0,
    );
  }

  Future<void> refreshList() {
    setState(() {
      _listFuture = updateAndGetList();
    });
  }

  Future<List<Visit>> updateAndGetList() async {
    try {
      activeRole = await client.getActiveRole();
      String url1 = DotEnv().env['GET_ACC_INFO'];
      var response1 = await client.getPrivateData(url1, context);
      final parsed1 = await jsonDecode(response1.body);
      User user = User.fromJson(parsed1['data']);
      user.userRoles.forEach((element) {
        _setStRole(element);
      });

      var response = await client.getPrivateData(
          DotEnv().env['GET_VISITS'], context);

      final parsed = await jsonDecode(response.body);

      return (parsed["data"] as List)
          .map<Visit>((json) => Visit.fromJson(json))
          .toList();
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
    }
  }

  void _setStRole(Role element) {
    if ((activeRole.roleName == element.roleName &&
            element.roleName == "Doctor") ||
        (activeRole.roleName == element.roleName &&
            element.roleName == "Nurse") ||
        (activeRole.roleName == element.roleName &&
            element.roleName == "Pharmacist") ||
        (activeRole.roleName == element.roleName &&
            element.roleName == "LabSpecialist") ||
        (activeRole.roleName == element.roleName &&
            element.roleName == "Radiographer")) {
      isStRole = true;
    }
  }
}
