import 'package:medcare/screen/visit/visit.dart';

class PurchaseSurgery {
  String note;
  String code;
  Visit visit;
  String scheduledDate;

  PurchaseSurgery({this.note, this.code, this.scheduledDate, this.visit});

  factory PurchaseSurgery.fromJson(Map<String, dynamic> json,
      {String visitCode}) {
    return PurchaseSurgery(
      note: json['note'],
      code: json['prescribedSurgeryCode'],
      scheduledDate: json['scheduledDate'],
      visit: Visit.fromJson(json['visit']),
    );
  }
}
