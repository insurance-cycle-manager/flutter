import 'package:medcare/screen/diagnosis/diagnosis.dart';
import 'package:medcare/screen/patient/patient.dart';

class Visit {
  String visitCode;
  String visitDate;
  double cost;
  String type;
  Patient patient;
  String addressCode;
  String serviceProviderCode;
  String serviceProviderConnectionCode;
  Diagnosis diagnosis;
  Visit baseVisit;

  Visit(
      {this.visitCode,
      this.visitDate,
      this.cost,
      this.type,
      this.patient,
      this.addressCode,
      this.serviceProviderCode,
      this.serviceProviderConnectionCode,
      this.diagnosis,
      this.baseVisit});

  factory Visit.fromJson(Map<String, dynamic> json) {
    Patient _patient = Patient();
    Visit _baseVisit;
    if (json['baseVisit'] != null) {
      _baseVisit = Visit.fromJson(json['baseVisit']);
    }

    if (json['patient'] != null) {
      _patient = Patient.fromJson(json['patient']);
    }
    return Visit(
        visitCode: json['visitCode'] ?? "",
        visitDate: json['visitDate'] ?? "",
        cost: json['cost'],
        type: json['type'] ?? "",
        patient: _patient,
        addressCode: json['addressCode'] ?? "",
        serviceProviderCode: json['serviceProviderCode'] ?? "",
        serviceProviderConnectionCode:
        json['serviceProviderConnectionCode'] ?? "",
        diagnosis: json['diagnosis'] != null
            ? Diagnosis.fromJson(json['diagnosis'])
            : null,
        baseVisit: _baseVisit);
  }

  Map<String, dynamic> toJson() =>
      {
        "visitCode": visitCode,
        "visitDate": visitDate,
        "patientCode": patient.patientCode,
        "type": type.toUpperCase(),
        "cost": cost,
        "addressCode": addressCode,
        "serviceProviderCode": serviceProviderCode,
        "serviceProviderConnectionCode": serviceProviderConnectionCode,
      };
}
