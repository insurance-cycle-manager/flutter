import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Unauthorized extends StatefulWidget {
  @override
  _UnauthorizedState createState() => _UnauthorizedState();
}

class _UnauthorizedState extends State<Unauthorized> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: Colors.,
      appBar: AppBar(
        title: Text("Unauthorized"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              'assets/images/un.png',
              height: 150,
            ),
            Text(
              "Access Denied!",
              style: TextStyle(
                color: Colors.black,
                fontSize: 25,
              ),
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }
}
