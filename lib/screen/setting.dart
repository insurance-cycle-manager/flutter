import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingPage extends StatefulWidget {
  @override
  State createState() {
    return _SettingPage();
  }
}

class _SettingPage extends State<SettingPage> {
  bool isDark;
  double value;

  @override
  void initState() {
    value = DynamicTheme.of(context).data.textTheme.headline.fontSize ?? 14;
    isDark = DynamicTheme.of(context).data.brightness == Brightness.dark;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DynamicTheme(
        defaultBrightness: DynamicTheme.of(context).brightness,
        data: (brightness) => DynamicTheme.of(context).data,
        themedWidgetBuilder: (context, theme) {
          return MaterialApp(
              theme: theme,
              home: Scaffold(
                  appBar: AppBar(
                    title: Text("Setting"),
                  ),
                  body: ListView(
                    children: <Widget>[
                      ListTile(
                        title: Row(
                          children: <Widget>[
                            Icon(Icons.brightness_2),
                            Padding(
                              padding: EdgeInsets.only(right: 9),
                            ),
                            Text("Dark Mode")
                          ],
                        ),
                        trailing: Switch(
                          value: isDark,
                          onChanged: (bool value) {
                            setState(() {
                              changeBrightness();
                              isDark = value;
                            });
                          },
                        ),
                      ),
                      Divider(),

                      ListTile(
                        title: Row(
                          children: <Widget>[
                            Icon(Icons.format_size),
                            Padding(
                              padding: EdgeInsets.only(right: 9),
                            ),
                            Text("Font Size"),
                            Slider.adaptive(
                          value: value,
                          onChanged: (newValue) {
                      setState(() {
                      value = newValue;
                      setTextSize(value);
                      });
                      },
                        label: "${value.ceil()}",
                        divisions: 10,
                        max: 30,
                        min: 10,
                      ),
                          ],
                        ),

                      ),
                      Divider(),
                    ],
                  )));
        });
  }

  void changeBrightness() {
    if (!isDark) {
      DynamicTheme.of(context).setThemeData(darkTheme());
    } else
      DynamicTheme.of(context).setThemeData(lightTheme());
  }

  ThemeData darkTheme() {
    prefsIsDart(true);
    return ThemeData(
        brightness: Brightness.dark,
        primaryColor: Color(0xFF07a383),
        primarySwatch: Colors.teal,
        accentColor: Colors.teal);
  }

  ThemeData lightTheme() {
    prefsIsDart(false);
    return ThemeData(
        brightness: Brightness.light,
        primaryColor: Color(0xFF07a383),
        primarySwatch: Colors.teal,
        accentColor: Colors.teal);
  }

  void prefsIsDart(bool isDark) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('isdark', isDark);
  }

  void setTextSize(double value) async {
    if (isDark) {
      DynamicTheme.of(context).setThemeData(ThemeData(
        brightness: Brightness.dark,
        primaryColor: Color(0xFF07a383),
        primarySwatch: Colors.teal,
        accentColor: Colors.teal,
        textTheme: TextTheme(
          button: TextStyle(fontSize: value),
          caption: TextStyle(fontSize: value),
          display1:  TextStyle(fontSize: value),
          display2:  TextStyle(fontSize: value),
          display3:  TextStyle(fontSize: value),
          display4:  TextStyle(fontSize: value),
          body1:  TextStyle(fontSize: value),
          overline:  TextStyle(fontSize: value),
          subtitle:  TextStyle(fontSize: value),
          title:  TextStyle(fontSize: value),
        ),
      ));
    } else
      DynamicTheme.of(context).setThemeData(ThemeData(
        brightness: Brightness.light,
        primaryColor: Color(0xFF07a383),
        primarySwatch: Colors.teal,
        accentColor: Colors.teal,
        textTheme: TextTheme(
          button: TextStyle(fontSize: value),
          caption: TextStyle(fontSize: value),
          display1:  TextStyle(fontSize: value),
          display2:  TextStyle(fontSize: value),
          display3:  TextStyle(fontSize: value),
          display4:  TextStyle(fontSize: value),
          body1:  TextStyle(fontSize: value),
          overline:  TextStyle(fontSize: value),
          subtitle:  TextStyle(fontSize: value),
          title:  TextStyle(fontSize: value),
        ),
      ));

    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setDouble('fontSize', value);
  }
}
