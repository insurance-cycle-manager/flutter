import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:medcare/components/customEditButton.dart';
import 'package:medcare/components/customEditIcon.dart';
import 'package:medcare/components/customeTFEdit.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/addressAdmin/address.dart';

class DetailsAddressAdmin extends StatefulWidget {
  @override
  State createState() {
    return _DetailsAddressAdminState();
  }
}

class _DetailsAddressAdminState extends State<DetailsAddressAdmin> {
  bool _status = true;
  final FocusNode myFocusNode = FocusNode();
  TextEditingController titleCont = TextEditingController();
  TextEditingController longitudeCont = TextEditingController();
  TextEditingController latitudeCont = TextEditingController();
  TextEditingController codeCont = TextEditingController();
  Address address;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    address = ModalRoute.of(context).settings.arguments;
    titleCont.text = address.title;
    longitudeCont.text = address.longitude.toString();
    codeCont.text = address.addressCode;
    latitudeCont.text = address.latitude.toString();
    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              expandedHeight: 200.0,
              floating: false,
              pinned: true,
              flexibleSpace: FlexibleSpaceBar(
                  centerTitle: true,
                  title: Text(address.title,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16.0,
                      )),
                  background: Image.asset(
                    'assets/images/email.png',
                    fit: BoxFit.cover,
                  )),
              actions: <Widget>[
                PopupMenuButton<String>(
                  onSelected: handleClick,
                  itemBuilder: (BuildContext context) {
                    List<String> s = [];
                    return s.map((String choice) {
                      return PopupMenuItem<String>(
                        value: choice,
                        child: Text(choice),
                      );
                    }).toList();
                  },
                )
              ],
            ),
          ];
        },
        body: Container(
          color: Color(0xffFFFFFF),
          child: Padding(
            padding: EdgeInsets.only(bottom: 25.0),
            child: ListView(
              children: <Widget>[
                Padding(
                    padding:
                        EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        new Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            new Text(
                              'Edit',
                              style: TextStyle(
                                  fontSize: 18.0, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        new Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            _status
                                ? CustomEditIcon(
                                    onClick: () {
                                      setState(() {
                                        _status = false;
                                      });
                                    },
                                  )
                                : new Container(),
                          ],
                        )
                      ],
                    )),
                CustomTFEdit(
                  label: "Address Code",
                  textCont: codeCont,
                ),
                CustomTFEdit(
                  label: "Title",
                  textCont: titleCont,
                  status: _status,
                ),
                CustomTFEdit(
                  label: "Longitude",
                  textCont: longitudeCont,
                  status: _status,
                ),
                CustomTFEdit(
                  label: "Latitude",
                  textCont: latitudeCont,
                  status: _status,
                ),
                !_status
                    ? CustomEditButton(
                        onSave: () {
                          setState(() {
                            address.title = titleCont.text;
                            address.latitude = double.parse(latitudeCont.text);
                            address.longitude =
                                double.parse(longitudeCont.text);
                            String url = DotEnv().env['PUT_ADDRESS'];
                            url = url.replaceAll(
                                "ADDRESSCODE", address.addressCode);
                            client.editObject(url, address.toJson(), context);
                            _status = true;
                            FocusScope.of(context)
                                .requestFocus(new FocusNode());
                          });
                        },
                        onCancel: () {
                          setState(() {
                            _status = true;
                            FocusScope.of(context)
                                .requestFocus(new FocusNode());
                          });
                        },
                      )
                    : new Container(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void handleClick(String value) async {
    /*switch (value) {
      case 'Packages':
        {
          Navigator.pushNamed(context, '/packages', arguments: address);
        }
        break;
      case 'Delete':
        String url = DotEnv().env['DELETE_IC'];
        url = url.replaceAll("ICCODE", address.companyCode);
        client.deleteObject(url, context, '/insurance companies');
        break;
    }*/
  }
}
