import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:medcare/components/customAppBar.dart';
import 'package:medcare/components/customCard.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/addressAdmin/address.dart';
import 'package:medcare/screen/unauthorized.dart';

class AddressesAdmin extends StatefulWidget {
  @override
  State createState() {
    return _AddressesAdminState();
  }
}

class _AddressesAdminState extends State<AddressesAdmin> {
  final TextEditingController _searchQuery = new TextEditingController();
  bool _isSearching;
  String _searchText = "";
  List<Address> addresses = [];
  Future<List<Address>> _listFuture;

  _AddressesAdminState() {
    _searchQuery.addListener(() {
      if (_searchQuery.text.isEmpty) {
        setState(() {
          _isSearching = false;
          _searchText = "";
        });
      } else {
        setState(() {
          _isSearching = true;
          _searchText = _searchQuery.text;
        });
      }
    });
  }

  @override
  void initState() {
    setState(() {});
    _isSearching = false;
    super.initState();
    _listFuture = updateAndGetList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        title: "Addresses",
        isSearching: _isSearching,
        searchText: _searchText,
        searchQuery: _searchQuery,
        handleClick: handleClick,
        optionsList: ['Addresses Control'],
      ),
      body: FutureBuilder<List<Address>>(
        future: _listFuture,
        builder: (BuildContext context, AsyncSnapshot<List<Address>> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return SpinKitCircle(
              color: Colors.teal,
            );
          } else if (snapshot.hasError) {
            return Scaffold(
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 250,
                    ),
                    Icon(Icons.error),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[Text("Something went wrong")],
                    ),
                    Expanded(
                      child: Text(
                        "Please check your internet connection",
                        style: TextStyle(fontSize: 15),
                      ),
                    )
                  ],
                ),
              ),
            );
          } else {
            addresses = snapshot.data ?? <Address>[];
            return Scaffold(
              backgroundColor: Color(0xffE5E5E5),
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView.builder(
                    itemCount: addresses.length,
                    itemBuilder: _isSearching ? _buildSearchList : _buildList),
              ),
              floatingActionButton: FloatingActionButton(
                child: Icon(Icons.add),
                onPressed: () {
                  Navigator.pushNamed(context, '/addAddressAdmin');
                },
              ),
            );
          }
        },
      ),
    );
  }

  Widget _buildList(BuildContext context, int index) {
    return CustomCard(
      name: addresses[index].title,
      dateOfBirth: addresses[index].longitude.toString(),
      gender: addresses[index].latitude.toString(),
      route: 'detailsAddressAdmin',
      object: addresses[index],
    );
  }

  Widget _buildSearchList(BuildContext context, int index) {
    if (_searchText.isEmpty) {
      return CustomCard(
        name: addresses[index].title,
        route: 'detailsAddressAdmin',
      );
    } else {
      String name = addresses[index].title;
      if (name.toLowerCase().contains(_searchText.toLowerCase())) {
        return CustomCard(
          name: addresses[index].title,
          route: 'detailsAddressAdmin',
        );
      }
    }
    return Container(
      height: 0,
    );
  }

  Future<void> refreshList() {
    // reload
    setState(() {
      _listFuture = updateAndGetList();
    });
    return null;
  }

  Future<List<Address>> updateAndGetList() async {
    dynamic response;
    try {
      response =
      await client.getPrivateData(DotEnv().env['GET_ADDRESSES'], context);

    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
      return [];
    }

    final parsed = await jsonDecode(response.body);

    return (parsed["data"] as List)
        .map<Address>((json) => Address.fromJson(json))
        .toList();
  }

  void handleClick(String value) async {
    switch (value) {
      case 'Addresses Control':
        {
          Navigator.pushNamed(
            context,
            '/addressesControl',
          );
        }
        break;
    }
  }
}
