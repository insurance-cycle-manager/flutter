class Address {
  double longitude;
  double latitude;
  String title;
  String addressCode;

  Address(
      {this.latitude: 0.0,
      this.longitude: 0.0,
      this.title: '',
      this.addressCode});

  factory Address.fromJson(Map<String, dynamic> json) {
    if (json == null) return Address(longitude: 0.0, latitude: 0.0, title: "");
    return Address(
        longitude: json['longitude'] ?? 0.0,
        latitude: json['latitude'] ?? 0.0,
        title: json['title'] ?? "",
        addressCode: json['addressCode']);
  }

  Map<String, dynamic> toJson() =>
      {'longitude': longitude, 'latitude': latitude, 'title': title};
}
