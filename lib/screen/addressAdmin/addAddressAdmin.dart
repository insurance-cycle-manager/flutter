import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:medcare/components/customButton.dart';
import 'package:medcare/components/customTextField.dart';
import 'package:medcare/networking/httpClient.dart' as client;

class AddAddressAdmin extends StatefulWidget {
  final title = TextEditingController();
  final longitude = TextEditingController();
  final latitude = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  State createState() {
    return _AddAddressAdminState();
  }
}

class _AddAddressAdminState extends State<AddAddressAdmin> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add Address'),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.only(top: 40),
        child: Form(
          key: widget._formKey,
          child: Wrap(
            runSpacing: 25,
            children: <Widget>[
              CustomTextField(
                labelText: "Title",
                validationFun: (value) {
                  if (value.isEmpty) {
                    return 'can\'t be empty';
                  } else
                    return null;
                },
                controller: widget.title,
              ),
              CustomTextField(
                labelText: "Longitude",
                validationFun: (value) {
                  if (value.isEmpty) {
                    return 'can\'t be empty';
                  } else
                    return null;
                },
                controller: widget.longitude,
              ),
              CustomTextField(
                labelText: "Latitude",
                validationFun: (value) {
                  if (value.isEmpty) {
                    return 'can\'t be empty';
                  } else
                    return null;
                },
                controller: widget.latitude,
              ),
              CustomButton(
                bName: "Submit",
                bFunc: () {
                  var json = {
                    "title": widget.title.text,
                    "latitude": widget.latitude.text,
                    "longitude": widget.longitude.text
                  };
                  client.postWithTokenJSON(
                      json, DotEnv().env['POST_ADDRESS'], context);
                  Navigator.popUntil(
                      context, ModalRoute.withName('/addressesAdmin'));
                  Navigator.pop(context);
                  Navigator.pushNamed(context, '/addressesAdmin');
                  //Navigator.popUntil(context, ModalRoute.withName('/insurance companies'));
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
