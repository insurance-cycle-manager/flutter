import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:medcare/networking/abacException.dart';
import 'package:medcare/networking/httpClient.dart' as client;
import 'package:medcare/screen/addressAdmin/address.dart';
import 'package:medcare/screen/auth/user.dart';
import 'package:medcare/screen/unauthorized.dart';

class AddressesControl extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _AddressesControlState();
  }
}

class _AddressesControlState extends State<AddressesControl> {
  Widget appBarTitle = new Text("Addresses Requests");
  Icon actionIcon = new Icon(Icons.search);
  final TextEditingController _searchQuery = new TextEditingController();
  final key = new GlobalKey<ScaffoldState>();
  bool _isSearching;
  String _searchText = "";
  List<RequestAddress> requestAddresses = [];

  Future<List<RequestAddress>> _listFuture;

  _AddressesControlState() {
    _searchQuery.addListener(() {
      if (_searchQuery.text.isEmpty) {
        setState(() {
          _isSearching = false;
          _searchText = "";
        });
      } else {
        setState(() {
          _isSearching = true;
          _searchText = _searchQuery.text;
        });
      }
    });
  }

  @override
  void initState() {
    setState(() {});
    _isSearching = false;
    _listFuture = updateAndGetList();
    super.initState();
  }

  Future<void> refreshList() {
    // reload
    setState(() {
      _listFuture = updateAndGetList();
    });
    return null;
  }

  Future<List<RequestAddress>> updateAndGetList() async {
    dynamic response;
    try {
      response = await client
          .getPrivateData(DotEnv().env['GET_PENDING_USER_Addresses'], context);
    } on AbacException {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Unauthorized()));
      return [];
    }

    final parsed = await jsonDecode(response.body);

    return (parsed["data"] as List)
        .map<RequestAddress>((json) => RequestAddress.fromJson(json))
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: key,
      appBar: AppBar(
        title: this.appBarTitle,
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: GestureDetector(
              child: IconButton(
                  icon: actionIcon,
                  onPressed: () {
                    setState(() {
                      if (this.actionIcon.icon == Icons.search) {
                        this.actionIcon = new Icon(
                          Icons.close,
                        );
                        this.appBarTitle = new TextField(
                          controller: _searchQuery,
                          decoration: new InputDecoration(
                            prefixIcon: new Icon(Icons.search),
                            hintText: "Search...",
                          ),
                        );
                        _handleSearchStart();
                      } else {
                        _handleSearchEnd();
                      }
                    });
                  }),
            ),
          ),
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {},
                child: Icon(Icons.more_vert),
              )),
        ],
      ),
      body: FutureBuilder<List<RequestAddress>>(
        future: _listFuture,
        builder: (BuildContext context,
            AsyncSnapshot<List<RequestAddress>> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return SpinKitCircle(
              color: Colors.teal,
            );
          } else if (snapshot.connectionState == ConnectionState.done) {
            requestAddresses = snapshot.data ?? <RequestAddress>[];
            return Scaffold(
              backgroundColor: Color(0xffE5E5E5),
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView.builder(
                    itemCount: requestAddresses.length,
                    itemBuilder: _isSearching ? _buildSearchList : _buildList),
              ),
            );
          } else {
            return Scaffold(
              body: RefreshIndicator(
                onRefresh: refreshList,
                child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 250,
                    ),
                    Icon(Icons.error),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[Text("Something went wrong")],
                    ),
                    Expanded(
                      child: Text(
                        "Please check your internet connection",
                        style: TextStyle(fontSize: 15),
                      ),
                    )
                  ],
                ),
              ),
            );
          }
        },
      ),
    );
  }

  void sendConfirm(String response, int index) async {
    String url = DotEnv().env['PUT_USER_Address_ADMIN'];
    url = url.replaceAll("USERNAME", requestAddresses[index].userName);
    url = url.replaceAll(
        "ADDRESSCODE", requestAddresses[index].address.addressCode);
    client.editObject(url, {'': ''}, context);
    requestAddresses.removeAt(index);
    setState(() {});
  }

  Widget _buildList(BuildContext context, int index) {
    return InkWell(
      child: Container(
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          margin: EdgeInsets.only(left: 8, right: 8, top: 8, bottom: 4),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 5, bottom: 5),
              ),
              ListTile(
                leading: Icon(Icons.sentiment_very_satisfied),
                title: Text(requestAddresses[index].userName),
              ),
              ListTile(
                leading: Icon(Icons.directions_walk),
                title: Text(requestAddresses[index].address.addressCode),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    FlatButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0)),
                      color: Colors.green,
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.check_circle,
                            color: Colors.white,
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: 8),
                          ),
                          Text(
                            "Accept",
                            style: TextStyle(color: Colors.white),
                          )
                        ],
                      ),
                      onPressed: () {
                        sendConfirm("ACCEPT", index);
                      },
                    ),
                    FlatButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0)),
                      color: Colors.red,
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.cancel,
                            color: Colors.white,
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: 8),
                          ),
                          Text(
                            "Reject",
                            style: TextStyle(color: Colors.white),
                          )
                        ],
                      ),
                      onPressed: () {
                        sendConfirm("REJECT", index);
                      },
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
        decoration: BoxDecoration(boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(.5),
            blurRadius: 20.0, // soften the shadow
            spreadRadius: 0.0, //extend the shadow
            offset: Offset(
              5.0, // Move to right 10  horizontally
              5.0, // Move to bottom 10 Vertically
            ),
          ),
        ]),
      ),
      onTap: () {
        Navigator.pushNamed(context, '/user info',
            arguments: requestAddresses[index].userName);
      },
    );
  }

  Widget _buildSearchList(BuildContext context, int index) {
    if (_searchText.isNotEmpty) {
      String name = requestAddresses[index].userName;
      if (name.toLowerCase().contains(_searchText.toLowerCase())) {
        return InkWell(
          child: Container(
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              margin: EdgeInsets.only(left: 8, right: 8, top: 8, bottom: 4),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(top: 5, bottom: 5),
                  ),
                  ListTile(
                    leading: Icon(Icons.sentiment_very_satisfied),
                    title: Text(requestAddresses[index].userName),
                  ),
                  ListTile(
                    leading: Icon(Icons.directions_walk),
                    title: Text(requestAddresses[index].address.addressCode),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        FlatButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30.0)),
                          color: Colors.green,
                          child: Row(
                            children: <Widget>[
                              Icon(
                                Icons.check_circle,
                                color: Colors.white,
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: 8),
                              ),
                              Text(
                                "Accept",
                                style: TextStyle(color: Colors.white),
                              )
                            ],
                          ),
                          onPressed: () {
                            sendConfirm("ACCEPT", index);
                          },
                        ),
                        FlatButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30.0)),
                          color: Colors.red,
                          child: Row(
                            children: <Widget>[
                              Icon(
                                Icons.cancel,
                                color: Colors.white,
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: 8),
                              ),
                              Text(
                                "Reject",
                                style: TextStyle(color: Colors.white),
                              )
                            ],
                          ),
                          onPressed: () {
                            sendConfirm("REJECT", index);
                          },
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
            decoration: BoxDecoration(boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(.5),
                blurRadius: 20.0, // soften the shadow
                spreadRadius: 0.0, //extend the shadow
                offset: Offset(
                  5.0, // Move to right 10  horizontally
                  5.0, // Move to bottom 10 Vertically
                ),
              ),
            ]),
          ),
          onTap: () {
            Navigator.pushNamed(context, '/user info',
                arguments: requestAddresses[index].userName);
          },
        );
      } else
        return Container(
          height: 0,
        );
    } else
      return InkWell(
        child: Container(
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            margin: EdgeInsets.only(left: 8, right: 8, top: 8, bottom: 4),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top: 5, bottom: 5),
                ),
                ListTile(
                  leading: Icon(Icons.sentiment_very_satisfied),
                  title: Text(requestAddresses[index].userName),
                ),
                ListTile(
                  leading: Icon(Icons.directions_walk),
                  title: Text(requestAddresses[index].address.addressCode),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      FlatButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0)),
                        color: Colors.green,
                        child: Row(
                          children: <Widget>[
                            Icon(
                              Icons.check_circle,
                              color: Colors.white,
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: 8),
                            ),
                            Text(
                              "Accept",
                              style: TextStyle(color: Colors.white),
                            )
                          ],
                        ),
                        onPressed: () {
                          sendConfirm("ACCEPT", index);
                        },
                      ),
                      FlatButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0)),
                        color: Colors.red,
                        child: Row(
                          children: <Widget>[
                            Icon(
                              Icons.cancel,
                              color: Colors.white,
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: 8),
                            ),
                            Text(
                              "Reject",
                              style: TextStyle(color: Colors.white),
                            )
                          ],
                        ),
                        onPressed: () {
                          sendConfirm("REJECT", index);
                        },
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(.5),
              blurRadius: 20.0, // soften the shadow
              spreadRadius: 0.0, //extend the shadow
              offset: Offset(
                5.0, // Move to right 10  horizontally
                5.0, // Move to bottom 10 Vertically
              ),
            ),
          ]),
        ),
        onTap: () {
          Navigator.pushNamed(context, '/user info',
              arguments: requestAddresses[index].userName);
        },
      );
  }

  void _handleSearchStart() {
    setState(() {
      _isSearching = true;
    });
  }

  void _handleSearchEnd() {
    setState(() {
      this.actionIcon = new Icon(
        Icons.search,
      );
      this.appBarTitle = new Text(
        "Search Sample",
      );
      _isSearching = false;
      _searchQuery.clear();
    });
  }
}

class RequestAddress {
  String userName;
  Address address;

  RequestAddress({this.userName, this.address});

  factory RequestAddress.fromJson(Map<String, dynamic> json) {
    return RequestAddress(
      userName: json['userName'],
      address: Address.fromJson(json['address']),
    );
  }
}
